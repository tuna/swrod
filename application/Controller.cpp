/*
 *  Controller.cpp
 *
 *  Created on: Apr 24, 2019
 */
#include <map>
#include <sstream>
#include <boost/property_tree/ptree.hpp>           // for basic_ptree

#include "Controller.h"
#include "InputLinkId.h"                           // for InputLinkId, swrod
#include "Configuration.h"             // for Configuration

#include "swrod/Core.h"
#include "swrod/exceptions.h"
#include "swrod/ROBFragmentConsumer.h"
#include "swrod/ROBFragmentBuilder.h"
#include <RunControl/Common/RunControlCommands.h>
#include "RunControl/Common/UserExceptions.h"

#include "is/infodictionary.h"
#include "is/exceptions.h"
#include "rc/RunParams.h"
#include "monsvc/PublishingController.h"
#include "monsvc/ConfigurationRules.h"
#include "ers/ers.h"                               // for ERS_LOG, error, info

#include <signal.h>

using namespace swrod;

Controller::Controller(const std::string & db_connect_string,
        const IPCPartition & partition, const std::string & name) :
        m_connect_string(db_connect_string),
        m_partition(partition),
        m_name(name) {
}

Controller::~Controller(){
   removeIsInfo();
}

void Controller::configure(const daq::rc::TransitionCmd & cmd) {
    ERS_LOG("command '" << cmd.toString() << "' received");

    // Destroy the current configuration before creating a new one
    // That should have been done in unconfigure but let's be paranoid
    m_config.reset(nullptr);

    try {
        m_config.reset(
                new Configuration(m_connect_string, m_partition,
                        m_name));
    }
    catch (ers::Issue & ex) {
        ERS_LOG(ex);
        throw; // RC framework will take care of this exception
    }

    auto propTree=m_config->propertyTree();

    struct sigaction saveSigAct;
    sigaction(SIGTERM, 0, &saveSigAct);
    m_core.reset(new Core(propTree));
    sigaction(SIGTERM, &saveSigAct, 0);
    sigaction(SIGINT, &saveSigAct, 0);

    m_dictionary=ISInfoDictionary(m_partition);
    m_isInfoName=propTree.get<std::string>("ISServerName")+".swrod."+m_name;

    auto interval=propTree.get<unsigned int>("Application.FullStatisticsInterval");
    if (interval!=0) {
       m_publishingController.reset(
          new monsvc::PublishingController(m_partition, m_name));
       std::ostringstream sStream;
       sStream << "Histogramming:.*/=>oh:(" << interval
               << ",1,Histogramming," << m_name << ")";
       m_publishingController->add_configuration_rule(
          *monsvc::ConfigurationRule::from(sStream.str()));
    }
    ERS_LOG("done");
}


void Controller::connect(const daq::rc::TransitionCmd & cmd) {
    ERS_LOG("command '" << cmd.toString() << "' received");
    m_core->connectToFelix();
    ERS_LOG("done");
}

void Controller::prepareForRun(const daq::rc::TransitionCmd & cmd) {
    ERS_LOG("command '" << cmd.toString() << "' received");
    RunParams runPars;
    // Get run number from IS
    try {
       m_dictionary.getValue("RunParams.RunParams", runPars);
    }
    catch (daq::is::Exception& runParamsException) {
       runPars.run_number=0;
       runPars.recording_enabled=true;
       runPars.max_events=0;
       ers::error(PrepRunIssue(ERS_HERE));
    }
    m_core->runStarted(runPars);
    if (m_publishingController) {
       m_publishingController->start_publishing();
    }
    ERS_LOG("done");
}

void Controller::stopArchiving(const daq::rc::TransitionCmd & cmd) {
    ERS_LOG("command '" << cmd.toString() << "' received");
    m_core->runStopped();
    if (m_publishingController) {
       m_publishingController->stop_publishing();
    }
    ERS_LOG("done");
}


void Controller::disconnect(const daq::rc::TransitionCmd & cmd) {
    ERS_LOG("command '" << cmd.toString() << "' received");
    m_core->disconnectFromFelix();
    ERS_LOG("done");
}

void Controller::unconfigure(const daq::rc::TransitionCmd & cmd) {
    ERS_LOG("command '" << cmd.toString() << "' received");
    m_core.reset();
    m_config.reset(nullptr);
    removeIsInfo();
    m_publishingController.reset();
    ERS_LOG("done");
}

void Controller::disable(const std::vector<std::string>& argVec) {
    ERS_LOG("disable received");
    if (m_core) {
       std::vector<unsigned int> robs;
       std::vector<InputLinkId> links;
       for (auto module : argVec) {
          try {
             auto rob=m_config->robLookup(module);
             robs.push_back(rob);
          }
          catch (UnknownRobUidIssue& issue) {
             try {
                InputLinkId link=m_config->inputLinkLookup(module);
                links.push_back(link);
             }
             catch (UnknownRobUidIssue& issue2) {
                ers::error(BadCommandIssue(ERS_HERE,"Resynch",issue));
             }
          }
       }
       if (robs.size()!=0) {
          m_core->disableROBs(robs);
       }
       if (links.size()!=0) {
          m_core->disableInputLinks(links);
       }
    }
}
void Controller::resynch(const daq::rc::ResynchCmd& cmd) {
    ERS_LOG("resynch received");
    if (m_core) {
       auto l1Id=cmd.extendedL1ID();
       if (cmd.modules().size()==0) {
          // Resynch applies to whole swrod
          m_core->resynchAfterRestart(l1Id);
       }
       else {
          std::vector<unsigned int> robs;
          std::map<unsigned int,std::string> robUid;
          std::vector<InputLinkId> links;
          std::map<InputLinkId,std::string> linkUid;
          for (auto module : cmd.modules()) {
             try {
                auto rob=m_config->robLookup(module);
                robs.push_back(rob);
                robUid[rob]=module;
             }
             catch (UnknownRobUidIssue& issue) {
                try {
                   InputLinkId link=m_config->inputLinkLookup(module);
                   links.push_back(link);
                   linkUid[link]=module;
                }
                catch (UnknownRobUidIssue& issue2) {
                   ers::error(BadCommandIssue(ERS_HERE,"Resynch",issue));
                }
             }
          }
          if (robs.size()!=0) {
             m_core->enableROBs(robs,l1Id);
          }
          if (links.size()!=0) {
             m_core->enableInputLinks(links,l1Id);
          }

          // Prepare ERS message for expert system
          std::string modList;
          for (auto rob : robs) {
             if (modList.size()!=0) {
                modList+=",";
             }
             modList+=robUid[rob];
          }
          for (auto link : links) {
             if (modList.size()!=0) {
                modList+=",";
             }
             modList+=linkUid[link];
          }

          daq::rc::HardwareRecovered msg(ERS_HERE, modList.c_str(),m_name.c_str());
          msg.add_qualifier("SWROD");
          ers::info(msg);
       }
    }
}
void Controller::removeIsInfo() {
   for (auto name : m_isItems) {
      try {
         m_dictionary.remove(name);
      }
      catch (daq::is::Exception& exc) {
         // Ignore error at this stage.  It may be that somebody removed the item behind our back
      }
   }
   m_isItems.clear();
}

void Controller::publish() {
    ERS_DEBUG(1, "command 'publish' received");
    if (m_core) {
       try {
          for (auto builder : m_core->getBuilders()) {
             auto stats=builder->getStatistics();
             if (stats) {
                std::ostringstream name;
                name << m_isInfoName << ".ROB-"
                     << std::hex << std::setw(8) << std::setfill('0')
                     << builder->getROBId();
                m_dictionary.checkin(name.str(),*stats);
                m_isItems.insert(name.str());
             }
          }
          for (auto consumer : m_core->getConsumers()) {
             auto stats=consumer->getStatistics();
             if (stats) {
                std::string name=m_isInfoName+".";
                auto consName=consumer->getName();
                if (consName!="") {
                   name+=consName;
                }
                else {
                   name+=stats->type().name();
                }
                m_isItems.insert(name);
                m_dictionary.checkin(name,*stats);
             }
          }
       }
       catch (daq::is::Exception& except) {
          ers::warning(except);
       }
    }
    ERS_DEBUG(1, "done");
}

void Controller::user(const daq::rc::UserCmd & cmd) {
    ERS_LOG("user command '" << cmd.toString() << "' received");
    if (m_core) {
        m_core->userCommand(cmd);
    }
}
