/*
 *  Configuration.cpp
 *
 *  Created on: Apr 24, 2019
 */

#include <cstdint>                             // for uint32_t, int16_t, int...
#include <iostream>                            // for operator<<, basic_ostream

#include "config/Schema.h"
#include "swrod/exceptions.h"
#include "ers/ers.h"                           // for error
#include "Configuration.h"
#include "DFdal/DFParameters.h"
#include "DFdal/ReadoutApplication.h"
#include "DFdal/ReadoutModule.h"
#include "swrod/dal/SwRodApplication.h"
#include "swrod/dal/SwRodConfiguration.h"
#include "swrod/dal/SwRodRob.h"
#include "swrod/dal/SwRodModule.h"
#include "swrod/dal/SwRodInputLink.h"
#include "swrod/dal/SwRodDataChannel.h"

using boost::property_tree::ptree;

swrod::Configuration::Configuration(
        const std::string & connect_string,
        const IPCPartition & partition, const std::string & name)
try :
        ::Configuration(connect_string),
        m_name(name),
        m_part_config(get<daq::core::Partition>(partition.name())),
             m_swrod_app(get<daq::df::ReadoutApplication>(m_name))
{

    if (!m_swrod_app) {
       throw BadConfigurationException(
          ERS_HERE, "SW ROD Application "+m_name+" not found");
    }

    if ( !m_part_config ) {
       throw BadConfigurationException(
          ERS_HERE, "Partition "+partition.name()+" not found");
    }

    if (m_swrod_app->disabled(*m_part_config)) {
       throw BadConfigurationException(
          ERS_HERE, m_name+" is disabled in database");
    }

    const daq::df::DFParameters* dfPars=
       cast<daq::df::DFParameters>(m_part_config->get_DataFlowParameters());

    if (dfPars) {
        ptree dfParConfig;
        dfParConfig.put("UID",dfPars->UID());
        dfParConfig.put("name",dfPars->UID());
        dfParConfig.put("MulticastAddress",dfPars->get_MulticastAddress());
        for (auto net : dfPars->get_DefaultDataNetworks()) {
           dfParConfig.add("networks.network", net);
        }
        m_ptree.add_child("DataFlowParameters", dfParConfig);
    }

    m_ptree.add("Partition.UID", m_part_config->UID());
    m_ptree.add("Partition.Name", m_part_config->UID());
//    m_ptree.add("Application.Name", m_name);

    ptree appConfig;
    appConfig.add("Name",m_name);
    getAttributes(m_swrod_app->class_name(), m_name, appConfig, true);
    m_ptree.add_child("Application",appConfig);

    const swrod::dal::SwRodConfiguration * swrodConfig =
            m_swrod_app->get_Configuration()->cast<swrod::dal::SwRodConfiguration>();
    if ( !swrodConfig ) {
        throw swrod::BadConfigurationException(ERS_HERE, "SW ROD configuration is not of a SwRodConfiguration type");
    }
    getAttributes(swrodConfig->class_name(), swrodConfig->UID(), m_ptree, false);

    // Parse ROBs resources
    for (auto moduleIter = m_swrod_app->get_Contains().begin();
              moduleIter != m_swrod_app->get_Contains().end(); moduleIter++) {
       const swrod::dal::SwRodModule* roModule=
                cast<swrod::dal::SwRodModule, daq::core::ResourceBase> (*moduleIter);
        if (roModule==0) {
            ers::error(BadConfigurationException(
                    ERS_HERE,
                    "SW ROD contains something that is not a ReadoutModule"));
            continue;
        }
        ptree moduleConf;
        getAttributes(roModule->class_name(), roModule->UID(),
                      moduleConf, false);
        bool moduleEnabled=false;
        const daq::core::ResourceSet* modResources=
                cast<daq::core::ResourceSet, daq::core::ResourceBase> (*moduleIter);
        if (modResources==0) {
            ers::error(BadConfigurationException(
                    ERS_HERE,
                    "SW ROD ReadoutModule contains something that is not a ResourceSet"));
            continue;
        }
        for (auto channelIter = modResources->get_Contains().begin();
                    channelIter != modResources->get_Contains().end(); channelIter++) {

            if (!(*channelIter)->disabled(*m_part_config)) {
                auto rod=cast<swrod::dal::SwRodRob>(*channelIter);
                if (rod==0) {
                    ers::error(BadConfigurationException(
                            ERS_HERE,"SW ROD ReadoutModule contains something not a SwRodRob"));
                    continue;
                }
                else {
                    ptree rob;
                    bool enabled=false;
                    getAttributes(rod->class_name(),rod->UID(),rob,false);
                    auto robContains=rob.get_child("Contains");
                    // The rod Contains should only have one entry
                    // which is a SwRodDataChannel which in turn
                    // Contains the swrodinputlinks
                    if (robContains.size()!=1) {
                       ers::error(BadConfigurationException(
                                     ERS_HERE,"SW ROD ROB does not have 1:1 mapping to SwRodDataChannel"));
                    }
                    else {
                       auto rcIter=robContains.begin();
                       rob.erase("Contains");
                       rob.push_back(
                          std::pair<std::string,ptree>("Contains",
                                                       rcIter->second));
                       auto& contains=rob.get_child("Contains.Contains");
                       auto cIter=contains.begin();
                       auto rodResourceIter=rod->get_Contains();
                       auto rodRes=rodResourceIter.begin();
                       auto dc=cast<swrod::dal::SwRodDataChannel>(*rodRes);
                       for (auto resource : dc->get_Contains()) {
                          auto link=cast<swrod::dal::SwRodInputLink>(resource);
                          if (link==0) {
                             ers::error(BadConfigurationException(
                                           ERS_HERE,"SW ROD ROB contains something not a SwRodInputLink"));
                             cIter=contains.erase(cIter);
                          }
                          else {
                             if (link->disabled(*m_part_config)) {
                                std::cout << "Link " << cIter->first << " disabled, removing from Contains list\n";
                                cIter=contains.erase(cIter);
                             }
                             else {
                                cIter++;
                                enabled=true;
                                m_linkUidMap[link->UID()]=link->get_FelixId();
                             }
                          }
                       }
                       if (enabled) {
                          moduleConf.add_child("ROBs.ROB", rob);
                          m_robUidMap[rod->UID()]=rod->get_Id();
                          moduleEnabled=true;
                       }
                    }
                }
            }
        }
        if (moduleEnabled) {
           moduleConf.erase("Contains");
           m_ptree.add_child("Modules.Module",moduleConf);
        }
    }
}
catch (ers::Issue& ex) {
   throw BadConfigurationException(ERS_HERE, ex.what());
}

swrod::Configuration::~Configuration() {
    ;
}

unsigned int swrod::Configuration::robLookup(const std::string& uid) const {
   auto iter=m_robUidMap.find(uid);
   if (iter!=m_robUidMap.end()) {
      return iter->second;
   }
   else {
      throw (UnknownRobUidIssue(ERS_HERE,uid));
   }
}
swrod::InputLinkId
swrod::Configuration::inputLinkLookup(const std::string& uid) const {
   auto iter=m_linkUidMap.find(uid);
   if (iter!=m_linkUidMap.end()) {
      return iter->second;
   }
   else {
      throw (UnknownRobUidIssue(ERS_HERE,uid));
   }
}

void swrod::Configuration::getAttributes(const std::string& className,
                                         const std::string& uid,
                                         ptree& config,
                                         bool directOnly) {
   std::cout << "Getting attributes for " << uid
             << " of class " << className << std::endl; std::cout.flush();
   config.put("UID",uid);
   auto classInfo=get_class_info(className);
   ConfigObject obj;
   get(className,uid,obj);
   for (auto attr : classInfo.p_attributes) {
      if (attr.p_type==daq::config::u8_type) {
         addValue<uint8_t>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::u16_type) {
         addValue<uint16_t>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::u32_type) {
         addValue<uint32_t>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::u64_type) {
         addValue<uint64_t>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::s8_type) {
         addValue<int8_t>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::s16_type) {
         addValue<int16_t>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::s32_type ||
               attr.p_type==daq::config::s16_type) {
         addValue<int32_t>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::s64_type) {
         addValue<int64_t>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::float_type) {
         addValue<float>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::double_type) {
         addValue<double>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if (attr.p_type==daq::config::bool_type) {
         addValue<bool>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
      else if ((attr.p_type==daq::config::string_type) ||
               (attr.p_type==daq::config::enum_type) ||
               (attr.p_type==daq::config::date_type) ||
               (attr.p_type==daq::config::time_type)) {
         addValue<std::string>(obj,attr.p_name,attr.p_is_multi_value,config);
      }
   }
   if (!directOnly) {
      for (auto iter: classInfo.p_relationships) {
         std::string relName=iter.p_name;
         std::string relType=iter.p_type;

         if (iter.p_cardinality==daq::config::zero_or_one ||
             iter.p_cardinality==daq::config::only_one) {
            ConfigObject relObj;
            obj.get(relName,relObj);
            ptree relConfig;
            if (!relObj.is_null()) {
               getAttributes(relObj.class_name(),relObj.UID(),relConfig);
               config.add_child(relName,relConfig);
            }
         }
         else {
            //std::cout << "Relationship " << relName << " is multi value\n";
            std::vector<ConfigObject> relVec;
            obj.get(relName,relVec);
            ptree relConfigs;
            for (auto relObj : relVec) {
               ptree relConfig;
               getAttributes(relObj.class_name(),relObj.UID(),relConfig);
               relConfigs.add_child(relObj.UID(),relConfig);
            }
            config.add_child(relName,relConfigs);
         }
      }
   }
}
