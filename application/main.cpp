/*
 *  main.cpp
 *
 *  Created on: Apr 24, 2019
 *
 *  Author: S. Kolos
 */
#include <sys/resource.h>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <memory>

#include <TSystem.h>

#include "ipc/partition.h"

#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"

#include "Controller.h"
#include "ers/ers.h"

using namespace swrod;

int main(int argc, char *argv[]) {
    // Always enable core file production
    rlimit core_limit = { RLIM_INFINITY, RLIM_INFINITY };
    setrlimit( RLIMIT_CORE, &core_limit );

    // Disable ROOT signal handlers as otherwise no core file will be produced
    if (gSystem) {
        gSystem->ResetSignals();
    }

    try {
        daq::rc::CmdLineParser cmdParser(argc, argv, true);

        IPCPartition partition(cmdParser.partitionName());

        std::string dbConnectString;
        if (!cmdParser.database().empty()) {
            dbConnectString = cmdParser.database();
        }
        std::shared_ptr<daq::rc::Controllable> swrod(
                new swrod::Controller(
                        dbConnectString, partition, cmdParser.applicationName()));

        ERS_LOG("'" << cmdParser.applicationName()
                << "' SW ROD application has been started in '"
                << partition.name() << "' partition");

        daq::rc::ItemCtrl myItem(cmdParser, swrod); 
        myItem.init();
        myItem.run();
    }
    catch(daq::rc::CmdLineHelp& ex) {
        std::cout << "SW ROD application" << std::endl;
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
    catch(std::exception& ex) {
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    ERS_LOG("SW ROD application terminated");

    return 0;
}

