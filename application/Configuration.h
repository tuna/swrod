/* -*- c++ -*-
 *  Configuration.h
 *
 *  Created on: Apr 24, 2019
 */

#ifndef _SWROD_CONFIGURATION_H_
#define _SWROD_CONFIGURATION_H_

#include <map>                                // for map
#include <string>                             // for string
#include <vector>                             // for vector

#include <boost/property_tree/ptree.hpp>

#include <ipc/partition.h>

#include <config/Configuration.h>
#include <dal/Partition.h>

#include "swrod/InputLinkId.h"

namespace daq {
   namespace df {
      class ReadoutApplication;
   }
}
namespace swrod {
    class Configuration: public ::Configuration {
    public:
        Configuration(const std::string & connect_string,
                const IPCPartition & partition, const std::string & name);

        ~Configuration();

       const boost::property_tree::ptree& propertyTree() const{
          return m_ptree;
       }

       unsigned int robLookup(const std::string& uid) const;
       InputLinkId inputLinkLookup(const std::string& uid) const;
    private:
       void getAttributes(const std::string& className,
                          const std::string& uid,
                          boost::property_tree::ptree& config,
                          bool directOnly=false);
       template <typename T> void addValue(ConfigObject& obj, std::string& name,
                                           bool multiValue,
                                           boost::property_tree::ptree& config){
          if (!multiValue) {
             T value;
             obj.get(name,value);
             config.add(name,value);
          }
          else {
             std::vector<T> valueVector;
             obj.get(name,valueVector);
             boost::property_tree::ptree tree;
             for (auto value : valueVector) {
                tree.add(name,value);
             }
             config.add_child(name,tree);
          }
       }
       std::string m_name;
        const daq::core::Partition * m_part_config;
        const daq::df::ReadoutApplication * m_swrod_app;
        boost::property_tree::ptree m_ptree;
       std::map<std::string,unsigned int> m_robUidMap;
       std::map<std::string,InputLinkId> m_linkUidMap;
    };
}

#endif
