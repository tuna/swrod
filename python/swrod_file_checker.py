#!/usr/bin/env tdaq_python

import argparse
import atexit
import sys

from eformat import istream

from rod_parser import parse_rod_payload
from rod_parser import Packet

class Output:
    def __init__(self, events_total):
        self.events_total = events_total
        self.width = len(str(events_total))
        self.offset = self.width * 2 + 7
        
        #hide cursor
        sys.stdout.write("\033[?25l")
        sys.stdout.flush()

        atexit.register(self.showcursor)

    def showcursor(self):
        sys.stdout.write("\033[?25h\n")
        sys.stdout.flush()

    def progress(self, counter):
        self.counter = counter
        sys.stdout.write(f'\r{self.counter: {self.width}d} / {self.events_total:d}')
        sys.stdout.flush()

    def info(self, msg):
        sys.stdout.write(f'\r{" ":{self.offset}s}{msg}\n')
        self.progress(self.counter)

    def warning(self, msg):
        sys.stdout.write(f'\r\033[92m{" ":{self.offset}s}{msg}\033[0m\n')
        self.progress(self.counter)

    def error(self, msg):
        sys.stdout.write(f'\r\033[91m{" ":{self.offset}s}{msg}\033[0m\n')
        self.progress(self.counter)

def run(elinks_count, file_name):
    sin = istream(str(file_name))

    out = Output(len(sin))
    
    for (index, event) in enumerate(sin):
        # show progress
        out.progress(index + 1)

        it = event.children()
        while True:
            try:
                rob = it.next()

                rob_id = rob.rob_source_id().code()
                rob_l1id = rob.rod_lvl1_id()
                rob_bcid = rob.rod_bc_id()
                elink_packets = parse_rod_payload(rob.rod_data())
                if elinks_count == 0:
                    elinks_count = len(elink_packets)
                    out.info("Fragments are expected to have %d packets" % elinks_count)

                duplicates = {}
                for packet in elink_packets:
                    if (rob_l1id & packet.l1id_mask) != packet.l1id:
                        out.error("ROB %x event %d: packet from e-link %x has wrong L1ID = %x instead of %x"
                                % (rob_id, index, packet.elink, packet.l1id, rob_l1id))
                    if packet.bcid != 0xffff and rob_bcid != packet.bcid:
                        out.error("ROB %x event %d: packet from e-link %x has wrong BCID = %x instead of %x"
                                % (rob_id, index, packet.elink, packet.bcid, rob_bcid))
                    if duplicates.get(packet.elink) is not None:
                        duplicates[packet.elink].append(packet)
                        out.error(f"{rob_id:x} event {index:d}: packets from e-link {packet.elink:x} "
                                  f"appear more than once:"
                                  + '[{}]'.format(', '.join('({})'.format(
                                        f"{p.elink:x}, {p.l1id:x}, {p.bcid:x}") for p in duplicates[packet.elink])))
                    else:
                        duplicates[packet.elink] = [packet]

                if len(elink_packets) != elinks_count:
                    out.warning("ROB %x event %d has wrong number of packets %d instead of %d"
                          % (rob_id, index, len(elink_packets), elinks_count))
            except RuntimeError as ex:
                out.error(ex)
            except StopIteration:
                break

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("file_name", type=str,
                        help="data file name")
    parser.add_argument("-e", "--elinks_count", type=int,
                        help="number of elinks per ROB. Zero (default) will use "
                             "the number of packets from the first ROB.",
                        nargs='?', default=0)
    args = parser.parse_args()
    run(**vars(args))
