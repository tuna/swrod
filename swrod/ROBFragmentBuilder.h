/*
 * ROBFragmentBuilder.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTBUILDER_H_
#define SWROD_ROBFRAGMENTBUILDER_H_

#include <swrod/DataInputHandler.h>
#include <swrod/L1AInfo.h>

#include <swrod/ROBFragmentProvider.h>

namespace swrod {
    /**
     * An abstract class that defines interface for a ROB Fragment Builder implementation.
     * It is not recommended to use this class directly, but to derive a ROB Fragment Builder
     * implementation class from the ROBFragmentBuilderBase class.
     */
    class ROBFragmentBuilder: public DataInputHandler,
                              public ROBFragmentProvider {
    public:
        virtual ~ROBFragmentBuilder() = default;

        /**
         * Returns ID of the ROB whose fragments are built by this object.
         * @returns The ID of the ROB.
         */
        virtual uint32_t getROBId() const = 0;

        /**
         * Is called when a new L1Accept message is received.
         *
         * @param[in] l1a A new L1Accept message.
         */
        virtual void l1aReceived(const L1AInfo & l1a) = 0;

    };
}

#endif /* SWROD_ROBFRAGMENTBUILDER_H_ */
