/*
 * Collator.h
 *
 *  Created on: Jul 26, 2019
 *      Author: kolos
 */

#ifndef SWROD_COLLATOR_H_
#define SWROD_COLLATOR_H_

#include <set>
#include <unordered_map>

#include <boost/property_tree/ptree.hpp>

#include <tbb/concurrent_queue.h>

#include <swrod/ROBFragmentConsumerBase.h>

namespace swrod {

    /**
     * \typedef Collection of ROB fragments that contains fragments with the same L1ID
     */
    typedef std::vector<std::shared_ptr<ROBFragment>> Event;

    /**
     * This class can be used to align fragments of different ROBs by their L1 IDs
     */
    class Collator : public ROBFragmentConsumerBase {
    public:

        /**
         * \typedef A callback that is called when all fragments for a particular L1ID have been collected.
         */
        typedef std::function<void(Event &&)> Callback;

        /**
         * Constructs a new collator object.
         *
         * @param[in] robs_number The number of ROBs to be aligned.
         * @param[in] callback The callback function to be called when all ROB fragments
         * for a particular L1 ID have been received. A new event that contains all
         * collected ROB fragments for a particular L1ID will be passed to the callback.
         * @param[in] queue_size Size of the internal queue. Default value -1 means unlimited.
         * @param[in] cpu_set Can be used to limit CPU cores where the pool threads will be executed.
         * CPU range can contain numbers separated by commas and may include ranges.
         * For example: 0,5,7,9-11. Empty value implies no limits.
         */
        Collator(uint32_t robs_number, const Callback & callback, int32_t queue_size,
                const std::string & cpu_set = "");

        /**
         * This function has to be used to pass a new ROB fragment to the collator.
         *
         * @param[in] fragment ROB fragment to be collated.
         */
        void fragmentReceived(const std::shared_ptr<ROBFragment>& fragment);

        /**
         * Clears all internal buffers. This function should be called between subsequent
         * runs to clean up internal buffers from the last run fragments.
         */
        void clear() {
            if (m_running) {
                m_control_queue.push(std::bind(&EventMap::clear, &m_events));
            }
            else {
                m_events.clear();
            }
        }

        /**
         * Informs collator that a certain ROB has been disabled and it should not expect fragments
         * from that ROB to arrive.
         *
         * @param[in] ROB_id
         */
        void ROBDisabled(uint32_t ROB_id) override {
            if (m_running) {
                m_control_queue.push(std::bind(&Collator::disable, this, ROB_id));
            }
            else {
                disable(ROB_id);
            }
        }

        /**
         * Informs collator that a certain ROB has been re enabled and it should expect fragments
         * from that ROB to arrive.
         *
         * @param[in] ROB_id
         */
        void ROBEnabled(uint32_t ROB_id) override {
            if (m_running) {
                m_control_queue.push(std::bind(&Collator::enable, this, ROB_id));
            }
            else {
                enable(ROB_id);
            }
        }

    private:
        void disable(uint32_t ROB_id);

        void enable(uint32_t ROB_id);

    private:
        typedef tbb::concurrent_queue<std::function<void()>> ControlQueue;
        typedef std::unordered_map<uint32_t, Event> EventMap;

        uint32_t m_robs_number;
        std::set<uint32_t> m_disabled_robs;
        EventMap m_events;
        Callback m_callback;
        ControlQueue m_control_queue;
    };
}

#endif /* SWROD_COLLATOR_H_ */
