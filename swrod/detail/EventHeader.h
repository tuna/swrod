/*
 * EventHeader.h
 *
 *  Created on: Aug 16, 2019
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_EVENTHEADER_H_
#define SWROD_DETAIL_EVENTHEADER_H_

namespace swrod {
    namespace detail {
        /**
         * Helper class that represents the layout of the ATLAS top level event header.
         */
        struct EventHeader {
            const uint32_t m_header_marker = 0xaa1234aa;
            uint32_t m_event_size;
            const uint32_t m_header_size = sizeof(EventHeader) / sizeof(uint32_t);
            const uint32_t m_format_version = 0x05000000;
            const uint32_t m_source = 0x007c0000; // HLT
            const uint32_t m_status_words_number = 0;
            const uint32_t m_checksum_type = 0; // no check-sum is present
            const uint32_t m_lhc_info[4] = { (uint32_t)time(0), 0, 0, 0 };
            const uint32_t m_run_type = 0x0000000f; // 'test'
            uint32_t m_run_number;
            const uint32_t m_lumi_block = 0;
            uint32_t m_l1id;
            uint32_t m_bcid;
            uint32_t m_l1_trigger_type;
            const uint32_t m_compression_type = 0; // no compression
            uint32_t m_payload_size;
            const uint32_t m_trigger_info[4] = { 0, 0, 0, 0 };
        };
    }
}

#endif /* SWROD_DETAIL_EVENTHEADER_H_ */
