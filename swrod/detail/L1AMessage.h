/*
 * L1AMessage.h
 *
 *  Created on: Jun 13, 2019
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_L1AMESSAGE_H_
#define SWROD_DETAIL_L1AMESSAGE_H_

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic" // suppress anonymous struct warning

namespace swrod {
    namespace detail {
        /**
         * This class represents the layout of a L1 Accept data packet produced by FELIX.
         */
        struct L1AMessage {
            L1AMessage() :
                    format(0), length(sizeof(L1AMessage)),
                    bcid(0), reserved0(0), ext_l1id(0), orbit(0),
                    trigger_type(0), reserved1(0), l0id(0) {
                ;
            }

            unsigned int format          : 8;
            unsigned int length          : 8;
            unsigned int bcid            : 12;
            unsigned int reserved0       : 4;
            union{
                unsigned int ext_l1id    : 32;
                struct {
                    unsigned int l1id    : 24;
                    unsigned int ecr     : 8;
                };
            };
            unsigned int orbit           : 32;
            unsigned int trigger_type    : 16;
            unsigned int reserved1       : 16;
            unsigned int l0id            : 32;
        } __attribute__((packed));
    }
}

#pragma GCC diagnostic pop

#endif /* SWROD_DETAIL_L1AMESSAGE_H_ */
