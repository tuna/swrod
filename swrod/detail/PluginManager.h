/*
 * PluginManager.h
 *
 *  Created on: Jun 11, 2019
 *      Author: kolos
 */

#ifndef SWROD_PLUGINMANAGER_H_
#define SWROD_PLUGINMANAGER_H_

#include <boost/property_tree/ptree.hpp>

namespace swrod {
    namespace detail {
        /**
         * Implements handling of dynamically loaded libraries.
         */
        class PluginManager {
        public:
            explicit PluginManager(const boost::property_tree::ptree & config);

        private:
            class SharedLibrary {
            public:
                /** Loads the given shared library.
                 * @param name Name of the library to be loaded
                 */
                explicit SharedLibrary(const std::string & library_name);

                /** Unloads the shared library  */
                ~SharedLibrary();

            private:
                const std::string m_name;
                void * m_handle;
            };

            void loadSharedLibrary(const std::string & library_name);

        private:
            typedef std::map<std::string, std::shared_ptr<SharedLibrary>> LibrariesMap;

            LibrariesMap m_libraries;
        };
    }
}

#endif /* SWROD_PLUGINMANAGER_H_ */
