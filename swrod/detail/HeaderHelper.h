// -*- c++ -*-
#ifndef HEADER_HELPER_H
#define HEADER_HELPER_H

#include <vector>
#include <memory>
#include <ctime>

namespace swrod{
   namespace helper {

      class HeaderHelper{
      public:
         HeaderHelper():m_runNumber(0){};
         void runNumber(unsigned int runNum) {m_runNumber=runNum;};

         unsigned int* makeHeader(
            std::vector<std::shared_ptr<ROBFragment>> frags){
            if (frags.size()==0) {
               return 0;
            }
            unsigned int l1Id=frags[0]->m_l1id;
            unsigned int bcid=frags[0]->m_bcid;
            unsigned int triggerType=frags[0]->m_trigger_type;
            auto currentTime=(unsigned int)time(0); // Convert from time_t which is now long!
            const unsigned int ffHeaderSize=24;
            return new unsigned int[ffHeaderSize] {0xaa1234aa,
                  0,            // Total fragment size 
                  ffHeaderSize, // total header size
                  0x05000000,   // format version
                  0x007c0000,   // source
                  1,            // n status words,
                  0x00000000,   // status word
                  0,            // checksum type
                  currentTime,0,   // bc time
                  0,0,          // global id
                  0x0000000f,   // run type 'test'
                  m_runNumber,  // run number
                  0,            // lumi block
                  l1Id,         // ext l1 Id
                  bcid,         // bcid
                  triggerType,  // trigger type,
                  0,            // compression type
                  0,            // payload size
                  0,0,0,        // n trigger info words
                  0             // n stream tag words
                  };
         }
      private:
         unsigned int m_runNumber;
      };
   }
}
#endif
