/*
 * NetioPacketHeader.h
 *
 *  Created on: Jun 13, 2019
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_NETIOPACKETHEADER_H_
#define SWROD_DETAIL_NETIOPACKETHEADER_H_

#include <cstdint>

namespace swrod {
    namespace detail {
        struct NetioPacketHeader {
            uint16_t size;
            uint16_t status;
            uint32_t elink_id;
        };
    }
}

#endif /* SWROD_DETAIL_NETIOPACKETHEADER_H_ */
