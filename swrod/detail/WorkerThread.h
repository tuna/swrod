/*
 * WorkerThread.h
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_WORKERTHREAD_H_
#define SWROD_DETAIL_WORKERTHREAD_H_

#include <functional>
#include <mutex>
#include <thread>

namespace swrod {
    namespace detail {
        class WorkerThread {
        public:

            /**
             * Creates a new thread of execution. The new thread is constructed in the idle state.
             * To start execution the WorkerThread::start method has to be used.
             *
             * @param function A function to be executed by the new thread. The function must periodically
             * check the value of the boolean parameter that is passed to it and terminate if the value
             * of this parameter is equal to false.
             *
             * @param name This name will be displayed by 'ps' and 'top' utilities for this thread.
             *
             * @param cpu_set Can be used to limit CPU cores where this thread can be executed.
             * CPU range can contain numbers separated by commas and may include ranges.
             * For example: 0,5,7,9-11. Empty value implies no limits.
             */
            WorkerThread(const std::function<void(const bool &)> & function,
                    const std::string & name, const std::string & cpu_set = "");

            ~WorkerThread();

            /**
             * Starts execution of the thread.
             */
            void start();

            /**
             * Terminates this thread. This function does not wait for the
             * thread termination. This can be done by calling the
             * WorkerThread::join function. The same object can be used
             * again to start a new thread by calling WorkerThread::start function.
             */
            void stop();

            /**
             * Returns when the current thread is terminated.
             */
            void join() {
                m_thread.join();
            }

            /**
             * Returns the name that is displayed by 'ps' and 'top' utilities for this thread.
             *
             * @return The thread name
             */
            const std::string& getName() const {
                return m_name;
            }

            /**
             * CPU range can contain numbers separated by commas and may include ranges.
             * For example: 0,5,7,9-11
             * @param range
             * @return vector of integers
             */
            static std::vector<int> parseCpuRange(const std::string &range);

        private:
            void execute();

        private:
            const std::string m_name;
            const std::string m_cpu_set;

            std::function<void(bool &)> m_function;
            bool m_active;
            std::mutex m_mutex;
            std::thread m_thread;
        };
    }
}

#endif /* SWROD_DETAIL_WORKERTHREAD_H_ */
