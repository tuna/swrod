/*
 * CircularBuffer.h
 *
 *  Created on: Mar 30, 2021
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_CIRCULARBUFFER_H_
#define SWROD_DETAIL_CIRCULARBUFFER_H_

#include <algorithm>
#include <cmath>
#include <functional>
#include <vector>

#include <swrod/exceptions.h>

namespace swrod {
namespace detail {
    template <class T>
    class CircularBuffer {
        static uint32_t adjustBufferSize(uint32_t size) {
            uint32_t power = std::log2(size);
            if (std::pow(2, power) != size) {
                power++;
            }
            return std::pow(2, power);
        }

    public:
        CircularBuffer(uint32_t maximum_size_hint, uint32_t minimum_size_hint,
                const std::function<T()> & factory,
                const std::function<void()> & overflow_callback) :
                    m_factory(factory),
                    m_overflow_callback(overflow_callback),
                    m_minimum_size(adjustBufferSize(minimum_size_hint)),
                    m_maximum_size(adjustBufferSize(maximum_size_hint)),
                    m_size(m_minimum_size),
                    m_mask(m_size-1),
                    m_shrinking_threshold(m_size)
        {
            for (uint32_t i = 0; i < m_maximum_size; ++i) {
                m_buffer.push_back(factory());
            }
        }

        T * at(uint64_t index) noexcept {
            if (index < m_head || (index - m_head) >= m_size) {
                return nullptr;
            }
            return &m_buffer[index & m_mask];
        }

        uint64_t head() const {
            return m_head;
        }

        T * begin() {
            return &m_buffer[0];
        }

        T * end() {
            return begin() + m_maximum_size;
        }

        uint32_t size() const {
            return m_size;
        }

        T & operator[](uint64_t index) {
#define SWROD_DETAIL_UNLIKELY(x) __builtin_expect(!!(x), 0)

            if (SWROD_DETAIL_UNLIKELY(index < m_head)) {
                throw BufferUnderflowException(ERS_HERE, index, m_head);
            }
            if ((index - m_head) >= m_size) {
                if (m_size < m_maximum_size) {
                    expand();
                } else {
                    while ((index - m_head) >= m_size) {
                        m_overflow_callback();
                    }
                }
            }
            m_tail = std::max(m_tail, index);
            return m_buffer[index & m_mask];
        }

        T pop() {
            T t(m_factory());
            std::swap(m_buffer[m_head++ & m_mask], t);
            if (not (m_head & m_mask)) {
                if (m_size > m_minimum_size && (m_tail - m_head) < m_shrinking_threshold) {
                    shrink();
                }
            }
            return t;
        }

        void reset(uint64_t head = 0) {
            for (auto & e : m_buffer) {
                e.reset();
            }
            m_size = m_minimum_size;
            m_mask = m_size - 1;
            m_head = head;
        }

    private:
        void expand() {
            uint32_t new_size = m_size<<1;
            ERS_DEBUG(2, "Expanding buffer from the current capacity (" << m_size
                    << ") to the new one (" << new_size << ")");
            if ((m_head & m_mask) == (m_head & (new_size-1))) {
                ERS_DEBUG(2, "Relocating " << (m_head & m_mask)
                        << " elements starting from 0");
                std::swap_ranges(m_buffer.begin(),
                        m_buffer.begin() + (m_head & m_mask),
                        m_buffer.begin() + m_size);
            } else {
                ERS_DEBUG(2, "Relocating " << m_size - (m_head & m_mask)
                        << " elements starting from " << (m_head & m_mask));
                std::swap_ranges(m_buffer.begin() + (m_head & m_mask),
                        m_buffer.begin() + m_size,
                        m_buffer.begin() + m_size + (m_head & m_mask));
            }
            m_shrinking_threshold = m_size;
            m_size = new_size;
            m_mask = m_size -1;
        }

        void shrink() {
            uint32_t new_size = m_size>>1;
            ERS_DEBUG(2, "Shrinking buffer from current capacity (" << m_size
                    << ") to a new capacity (" << new_size << ")");
            if ((m_head & m_mask) == (m_head & (new_size-1))) {
                ERS_DEBUG(2, "Relocating " << (m_head & m_mask)
                        << " elements starting from 0");
                std::swap_ranges(m_buffer.begin() + new_size,
                        m_buffer.begin() + new_size + (m_head & m_mask),
                        m_buffer.begin());
            } else {
                ERS_DEBUG(2, "Relocating " << m_size - (m_head & m_mask)
                        << " elements starting from " << (m_head & m_mask));
                std::swap_ranges(m_buffer.begin() + (m_head & m_mask),
                        m_buffer.begin() + m_size,
                        m_buffer.begin() + (m_head & m_mask) - new_size);
            }
            m_size = new_size;
            if (m_size > m_minimum_size) {
                m_shrinking_threshold = m_size>>1;
            }
            m_mask = m_size -1;
        }

    private:
        const std::function<T()> m_factory;
        const std::function<void()> m_overflow_callback;
        const uint32_t m_minimum_size;
        const uint32_t m_maximum_size;
        uint64_t m_head = 0;
        uint64_t m_tail = 0;
        uint32_t m_size;
        uint32_t m_mask;
        uint32_t m_shrinking_threshold;

        std::vector<T> m_buffer;
    };
}}

#endif /* SWROD_DETAIL_CIRCULARBUFFER_H_ */
