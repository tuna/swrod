/*
 * MemoryPool.h
 *
 *  Created on: Mar 13, 2021
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_MEMORYPOOL_H_
#define SWROD_DETAIL_MEMORYPOOL_H_

#include <functional>
#include <mutex>

#include <boost/pool/pool.hpp>

#include <swrod/ROBFragment.h>

namespace swrod {
    namespace detail {
        class MemoryPool {
        public:
            MemoryPool(size_t block_size) :
                m_boost_pool(block_size),
                m_deleter(std::bind(&MemoryPool::free, this, std::placeholders::_1))
            { }

            size_t getBlockSize() const {
                return m_boost_pool.get_requested_size();
            }

            ROBFragment::DataBlock alloc() {
                return ROBFragment::DataBlock(malloc(), getBlockSize()>>2, 0, m_deleter);
            }

        private:
            uint32_t * malloc() {
                std::unique_lock lock(m_mutex);
                return (uint32_t*)m_boost_pool.malloc();
            }

            void free(uint32_t * ptr) {
                std::unique_lock lock(m_mutex);
                m_boost_pool.free(ptr);
            }

        private:
            std::mutex m_mutex;
            boost::pool<> m_boost_pool;
            std::function<void(uint32_t[])> m_deleter;
        };
    }
}

#endif /* SWROD_DETAIL_MEMORYPOOL_H_ */
