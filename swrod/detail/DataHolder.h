/*
 * DataHolder.h
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#ifndef SWROD_DATAHOLDER_H_
#define SWROD_DATAHOLDER_H_

#include <cstdint>
#include <memory>
#include <utility>
#include <vector>

#include <swrod/ROBFragment.h>

namespace swrod {
    namespace detail {
        struct DataHolder {
            DataHolder() : m_index(-1), m_l1id(-1), m_bcid(-1), m_trigger_type(0),
                     m_missed_packets(0), m_corrupted_packets(0), m_status(0) {
            }

            DataHolder(const DataHolder &) = delete;
            DataHolder & operator=(const DataHolder &) = delete;

            DataHolder(DataHolder&& other) = default;
            DataHolder & operator=(DataHolder&& other) = default;

            uint64_t m_index;
            uint32_t m_l1id;
            uint16_t m_bcid;
            uint16_t m_trigger_type;
            uint16_t m_missed_packets;
            uint16_t m_corrupted_packets;
            uint32_t m_status;

            std::vector<ROBFragment::DataBlock> m_data;
        };
    }
}

#endif /* SWROD_DATAHOLDER_H_ */
