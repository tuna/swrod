/*
 * ThreadPool.h
 *
 *  Created on: Jul 16, 2019
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_THREADPOOL_H_
#define SWROD_DETAIL_THREADPOOL_H_

#include <swrod/detail/WorkerThread.h>

namespace swrod {
    namespace detail {
        /**
         * Helper class to create a given number of threads executing the given function(s).
         */
        class ThreadPool {
        public:
            typedef std::function<void(const bool &)> ThreadFunction;

            /**
             * Creates a pool of threads executing the given function. All threads are created
             * in idle state. To start execution the ThreadPool::start function has to be called.
             *
             * @param num Number of threads in the new pool.
             * @param function The function to be executed by all threads.
             * @param name All threads in the pool will have this name shown by the 'top' command.
             * @param cpu_set Can be used to limit CPU cores where the pool threads will be executed.
             * CPU range can contain numbers separated by commas and may include ranges.
             * For example: 0,5,7,9-11. Empty value implies no limits.
             */
            ThreadPool(uint16_t num, const ThreadFunction & function, const std::string & name,
                    const std::string & cpu_set = "");

            /**
             * Creates a pool of threads executing the given functions. All threads are created
             * in idle state. To start execution the ThreadPool::start function has to be called.
             *
             * @param functions The size of the vector defines a number of threads to be created.
             * Each thread will execute a corresponding function from this vector.
             * @param name All threads in the pool will have this name shown by the 'top' command.
             * @param cpu_set Can be used to limit CPU cores where the pool threads will be executed.
             * CPU range can contain numbers separated by commas and may include ranges.
             * For example: 0,5,7,9-11. Empty value implies no limits.
             */
            ThreadPool(const std::vector<ThreadFunction> & functions, const std::string & name,
                    const std::string & cpu_set = "");

            ~ThreadPool();

            /**
             * Starts execution of the pool's threads.
             */
            void start();

            /**
             * Stops execution of the pool's threads.
             */
            void stop();

            /**
             * Return the name of the threads of the pool.
             * @return
             */
            const std::string& getName() const {
                return m_name;
            }

            /**
             * Returns number of threads in the pool.
             * @return
             */
            uint32_t getSize() const {
                return m_workers.size();
            }

        private:
            const std::string m_name;
            std::vector<std::unique_ptr<WorkerThread>> m_workers;
        };
    }
}

#endif /* SWROD_DETAIL_THREADPOOL_H_ */
