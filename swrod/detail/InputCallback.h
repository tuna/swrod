/*
 * InputCallback.h
 *
 *  Created on: Nov 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_DETAILINPUTCALLBACK_H_
#define SWROD_DETAILINPUTCALLBACK_H_

#include <cstdint>

#define CALLBACK(p1, p2, p3, p4) const uint8_t * p1, uint32_t p2, uint8_t p3, uint32_t p4
#define CALLBACK_DECL(P) CALLBACK(P)

#define CALLBACK_PARAMETERS data, size, status, rob_status
#define CALLBACK_SIGNATURE CALLBACK_DECL(CALLBACK_PARAMETERS)

namespace swrod {
    namespace detail {
        class DataCallback;

        /**
         * This is a SW ROD specific replacement of std::function<void(CALLBACK_SIGNATURE)>
         * that is introduced solely for performance optimisation. Using this class
         * gives ~10% performance gain for GBT mode in which such a callback is invoked
         * at the rate of O(10) MHz.
         */
        class InputCallback {
            friend class DataCallback;

            typedef void (*Caller)(const InputCallback *, CALLBACK_SIGNATURE);

        public:
            template<class T, class P, void (T::*M)(P &, CALLBACK_SIGNATURE)>
            static InputCallback make(T & object) {
                return InputCallback(&object, &InputCallback::methodCallerP<T, P, M>);
            }

            template<class T, void (T::*M)(CALLBACK_SIGNATURE)>
            static InputCallback make(T & object) {
                return InputCallback(&object, &InputCallback::methodCaller<T, M>);
            }

            template<class P>
            DataCallback bind(P & param);

        private:
            void operator()(CALLBACK_SIGNATURE) const {
                m_caller(this, CALLBACK_PARAMETERS);
            }

            InputCallback(void * object, Caller function) :
                m_object(object), m_param(0), m_caller(function) {
            }

            template<class T, class P, void (T::*M)(P &, CALLBACK_SIGNATURE)>
            static void methodCallerP(const InputCallback * self, CALLBACK_SIGNATURE) {
                T* o = static_cast<T*>(self->m_object);
                P* p = static_cast<P*>(self->m_param);
                (o->*M)(*p, CALLBACK_PARAMETERS);
            }

            template<class T, void (T::*M)(CALLBACK_SIGNATURE)>
            static void methodCaller(const InputCallback * self, CALLBACK_SIGNATURE) {
                T* o = static_cast<T*>(self->m_object);
                (o->*M)(CALLBACK_PARAMETERS);
            }

        private:
            void * m_object;
            void * m_param;
            Caller m_caller;
        };

        class DataCallback {
            friend class InputCallback;

        public:
            void operator()(CALLBACK_SIGNATURE) const {
                m_callback(CALLBACK_PARAMETERS);
            }

        private:
            DataCallback(InputCallback callback) : m_callback(callback) {}

            InputCallback m_callback;
        };

        template<class P>
        DataCallback InputCallback::bind(P & param) {
            m_param = &param;
            return DataCallback(*this);
        }
    }
}

#undef CALLBACK
#undef CALLBACK_DECL
#undef CALLBACK_PARAMETERS
#undef CALLBACK_SIGNATURE

#endif /* SWROD_DETAILINPUTCALLBACK_H_ */
