/*
 * ROBFragmentWorkerBase.h
 *
 *  Created on: Mar 25, 2020
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTWORKERBASE_H_
#define SWROD_ROBFRAGMENTWORKERBASE_H_

#include <swrod/Core.h>
#include <swrod/CustomProcessingFramework.h>
#include <swrod/DataInputHandlerBase.h>
#include <swrod/detail/ptree.h>

namespace swrod {
    /**
     * This class provides a reusable basic implementation of a worker thread for a ROB fragment
     * builder algorithm. Deriving custom class from the ROBFragmentWorkerBase is a recommended
     * way of implementing a worker thread for a custom ROB fragment building algorithm.
     */
    class ROBFragmentWorkerBase: public DataInputHandlerBase {
    public:
        /**
         * Creates a new worker for the ROB fragment builder with the given configuration.
         *
         * @param[in] links Input links to be used for receiving data
         * @param[in] config Configuration object that contains parameters for the corresponding
         *      fragment builder (SwRodFragmentBuilder)
         * @param[in] core a reference to the swrod::Core object
         * @param[in] input the object to be used to subscribe for receiving data
         * @param[in] callback function to be called when new data arrive
         */
        ROBFragmentWorkerBase(const InputLinkVector & links,
                const boost::property_tree::ptree & config,
                const Core & core,
                const std::shared_ptr<DataInput> & input,
                detail::InputCallback callback) :
                DataInputHandlerBase("ROB" + PTREE_GET_VALUE(config, std::string, "Id"),
                        links, config, input, callback),
                m_ROB_id(PTREE_GET_VALUE(config, uint32_t, "Id")),
                m_max_message_size(PTREE_GET_VALUE(config, uint32_t, "FragmentBuilder.MaxMessageSize")),
                m_trigger_info_extractor(
                        core.getProcessingFramework().getTriggerInfoExtractor(m_ROB_id)),
                m_data_integrity_checker(
                        core.getProcessingFramework().getDataIntegrityChecker(m_ROB_id))
        { }

    protected:
        const uint32_t m_ROB_id;
        const uint32_t m_max_message_size;
        CustomProcessingFramework::TriggerInfoExtractor m_trigger_info_extractor;
        CustomProcessingFramework::DataIntegrityChecker m_data_integrity_checker;
    };
}

#endif /* SWROD_ROBFRAGMENTWORKERBASE_H_ */
