/*
 * L1AInfo.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_L1AINFO_H_
#define SWROD_L1AINFO_H_

#include <cstdint>

namespace swrod {

    /**
     * Contains information from a L1 Accept message.
     */
    struct L1AInfo {
        L1AInfo(uint16_t bcid, uint16_t trigger_type,
                uint32_t l1id, uint64_t index) :
            m_bcid(bcid), m_trigger_type(trigger_type),
            m_l1id(l1id), m_index(index) {
        }

        bool isECR() const noexcept {
            static const uint32_t mask = 0xff000000;
            return ((m_l1id & mask) == m_l1id);
        }

        const uint16_t m_bcid;
        const uint16_t m_trigger_type;
        const uint32_t m_l1id;
        const uint64_t m_index;
    };
}

#endif /* SWROD_L1AINFO_H_ */
