/*
 * ROBFragmentBuilderBase.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTBUILDERBASE_H_
#define SWROD_ROBFRAGMENTBUILDERBASE_H_

#include <chrono>
#include <mutex>
#include <tbb/concurrent_hash_map.h>
#include <tbb/concurrent_queue.h>

#include <rc/RunParams.h>

#include <swrod/DataInputHandlerBase.h>
#include <swrod/InputLinkId.h>
#include <swrod/L1AInfo.h>

#include <swrod/ROBFragmentBuilder.h>
#include <swrod/ROBStatistics.h>
#include <swrod/detail/ThreadPool.h>
#include <swrod/detail/DataHolder.h>

namespace swrod {
    class Core;

    /**
     * This is an abstract class that provides a reusable basic implementation of the
     * ROBFragmentBuilder interface.
     * Deriving custom class from the ROBFragmentBuilderBase is a recommended way of
     * implementing a ROB fragment building algorithm.
     */
    class ROBFragmentBuilderBase : public ROBFragmentBuilder {
    public:
        typedef std::function<DataInputHandlerBase*(
                const InputLinkVector &, const std::shared_ptr<DataInput> &)> WorkerFactory;
        typedef std::function<void (const L1AInfo &)> L1AReceiver;

        ROBFragmentBuilderBase(
                const boost::property_tree::ptree& robConfig, const Core & core,
                const L1AReceiver & l1a_receiver,
                const WorkerFactory & factory,
                bool header_present = false);

        ~ROBFragmentBuilderBase();

        /**
         * Returns unique ID for this object. This ID will be used to publish the fragment builder's
         * statistics to IS.
         *
         * @return The object's unique ID.
         */
        const std::string& getName() const override {
            return m_unique_id;
        }

        /**
         * Returns ID of the ROB whose fragments are built by this object.
         * @returns The ID of the ROB.
         */
        uint32_t getROBId() const final {
            return m_ROB_id;
        }

        /**
         * This function is called by the L1AInputHandler implementation to inform
         * this builder about a new L1A data.
         *
         * @param l1a L1 Accept information.
         */
        void l1aReceived(const L1AInfo & l1a) override {
            m_last_received_L1ID.set(l1a.m_index + 1, l1a.m_l1id);
            if (m_enabled) {
                m_l1a_receiver(l1a);
            }
        }

        /**
         * Should be used to inform the fragment builder that it shall not use
         * data from the given input links for event building. This function unsubscribes
         * from the given input links.
         *
         * @param[in] link_ids The IDs of the disabled input links. If an
         *      attempt to disable a link from this vector succeeds this
         *      function removes the corresponding ID from the vector.
         */
        void disableLinks(std::vector<InputLinkId> & link_ids) override;

        /**
         * Should be used to disable event building. This function unsubscribes from all
         * input links.
         */
        void disable() override;

        /**
         * This function is used to inform the fragment builder that it shall resume
         * using data from the given input links for event building.
         * The IDs of the links that were successfully re enabled are removed from the input vector.
         * This way when the function returns the vector of links IDs contains only IDs of
         * the links which are either not handled by this worker or failed to be re enabled.
         * It is assumed that the Trigger is on hold and no input data may be coming in when this
         * function is executed. A failure to respect this condition may result in a fatal crash.
         *
         * @param[in, out] link_ids A vector of IDs of the input links to be enabled. If an
         *      attempt to enable a link from this vector succeeds the function removes
         *      the corresponding ID from the vector.
         * @param[in] lastL1ID The last L1ID that has been produced when the Trigger had been
         *      put on hold.
         * @param[in] triggersNumber Number of triggers that has been produced when the
         *      Trigger was put on hold.
         */
        void enableLinks(std::vector<InputLinkId> & link_ids,
                uint32_t lastL1ID, uint64_t triggersNumber) override;

        /**
         * Called to inform the fragment builder that it has to resume fragment building.
         * This function re subscribes to all input links. This function assumes that the
         * Trigger is on hold and no new data may be produced. A failure of respect this
         * condition may result in a fatal crash.
         *
         * @param[in] lastL1ID The last L1ID that has been produced when the Trigger was put on hold.
         * @param[in] triggersNumber Number of triggers that has been produced since the last
         *      restart of the SW ROD application.
         *
         * @throws swrod::Exception If an attempt to re enable this fragment builder fails
         *      (e.g. subscription to one or several input link failed) the function will
         *      throw swrod::Exception exception.
         */
        void enable(uint32_t lastL1ID, uint64_t triggersNumber) override;

        /**
         * This function shall be used as part of TTC Restart procedure to synchronise this fragment
         * builder with the TTC system after the restart of the SW ROD application.
         * The Trigger must be put on hold when this function is executed and therefore the
         * fragment builder shall not received any data until this function returns.
         *
         * @param[in] lastL1ID The last L1ID that has been produced when the Trigger was put on hold.
         */
        void resynchAfterRestart(uint32_t lastL1ID);

        /**
         * Called when a new data taking session is started.
         *
         * @param[in] run_params The new run parameters.
         */
        void runStarted(const RunParams & run_params) override;

        /**
         * Called when the current data taking session is terminated.
         */
        void runStopped() override;

        /**
         * This function subscribes to all input links.
         * This function is called during the Run Control CONNECT transition.
         */
        void subscribeToFelix() override;

        /**
         * This function cancels all previously established input links subscriptions.
         * This function is called during the Run Control DISCONNECT transition.
         */
        void unsubscribeFromFelix() override;

        /**
         * Calculates average rates since the last call to this function and updates the
         * statistics object with the new rates before returning.
         *
         * @return A pointer to the IS statistics object.
         */
        ISInfo * getStatistics() override;

    protected:
        class L1ID {
            static const uint32_t c_invalid_value = uint32_t(-1);

        public:
            L1ID(uint32_t value = c_invalid_value) :
                m_value(value) {
            }

            bool invalidOrEqual(uint32_t l1id) const {
                return (m_value == c_invalid_value || l1id == m_value);
            }

            void set(uint64_t counter, uint32_t l1id) {
                // The order of assignment is guaranteed by the comma operator.
                // This is important as we want to be sure that when a new L1ID
                // value is set the corresponding counter is already up to date
                m_counter = counter, m_value = l1id;
            }

            uint64_t getCounter() const {
                return m_counter;
            }

            uint32_t getValue() const {
                return m_value;
            }

        private:
            uint64_t m_counter = 0;
            uint32_t m_value;
        };

        void transmitter(const bool & active);

        void synchronize(uint32_t lastL1ID, const L1ID & data);

    protected:
        struct hash_compare {
            static size_t hash(const uint64_t& a) { return a; }
            static bool equal(const uint64_t& a, const uint64_t& b) { return a == b; }
        };

        typedef tbb::concurrent_hash_map<
                uint64_t, detail::DataHolder, hash_compare> FragmentAssembler;
        typedef tbb::concurrent_bounded_queue<uint64_t> ReadyQueue;
        typedef std::function<void (FragmentAssembler::accessor & )> FragmentReadyCallback;

        void addToReadyQueue(FragmentAssembler::accessor & a);

        void fragmentReady(FragmentAssembler::accessor & a) {
            m_fragment_ready_callback(a);
        }

        void submitFragment(FragmentAssembler::accessor & a);

        const uint32_t m_ROB_id;
        const std::string m_unique_id;
        const bool m_flush_buffer;
        const bool m_ROD_header_present;
        const uint32_t m_l1a_wait_timeout;
        const uint32_t m_resynch_timeout;
        uint64_t m_buffer_size = 0;
        int64_t m_queue_size = 0;
        uint64_t m_good_fragments = 0;
        uint64_t m_fragment_errors = 0;
        bool m_running;
        bool m_enabled;

        std::mutex m_built_mutex;
        L1ID m_last_built_L1ID;
        L1ID m_last_received_L1ID;
        ROBStatistics m_statistics;

        std::chrono::time_point<std::chrono::system_clock> m_start_of_run;
        std::chrono::time_point<std::chrono::system_clock> m_last_update;

        L1AReceiver m_l1a_receiver;
        FragmentAssembler m_fragment_assembler;
        ReadyQueue m_ready;
        detail::ThreadPool m_transmitters;
        std::vector<std::unique_ptr<DataInputHandlerBase>> m_workers;
        FragmentReadyCallback m_fragment_ready_callback;
    };
}

#endif /* SWROD_ROBFRAGMENTBUILDERBASE_H_ */
