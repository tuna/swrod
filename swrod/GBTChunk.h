/*
 * GBTChunk.h
 *
 *  Created on: Jun 13, 2019
 *      Author: kolos
 */

#ifndef SWROD_GBTCHUNK_H_
#define SWROD_GBTCHUNK_H_

#include <cstdint>

namespace swrod {
    namespace GBTChunk {
        namespace Status {
            const uint8_t Ok = 0, Corrupt = 1, CRCError = 2, L1IdMismatch = 4, BCIdMismatch = 8;
        }

        /**
         * This class defines a structure of the header of a data chunk within
         * data payload of a ROB fragments produced by the GBTModeBuilder algorithm.
         */
        struct Header {
            uint16_t m_size;
            uint8_t m_felix_status;
            uint8_t m_swrod_status;
            uint32_t m_link_id;
        } __attribute__((packed));
    }
}

#endif /* SWROD_GBTCHUNK_H_ */
