/*
 * DataInputHandlerBase.h
 *
 *  Created on: Nov 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_DATAINPUTHANDLERBASE_H_
#define SWROD_DATAINPUTHANDLERBASE_H_

#include <memory>
#include <vector>

#include <swrod/InputLinkId.h>
#include <swrod/DataInput.h>
#include <swrod/DataInputHandler.h>
#include <swrod/detail/InputCallback.h>

namespace swrod {

    /**
     * This class provides a basic reusable implementation of the InputDataHandler interface.
     * Deriving from the InputDataHandlerBase is a recommended way of implementing this interface.
     */
    class DataInputHandlerBase : public virtual DataInputHandler {
    public:

        /**
         * This class contains control and monitoring counters for an input link.
         * This information is used for input data bookkeeping, error detection and monitoring.
         */
        struct Link {
            Link(const InputLinkId & id, const DetectorLinkId & det_id) :
                m_fid(id), m_resource_id(det_id),
                m_enabled(true),
                m_last_bcid(0xffff),
                m_expected_l1id(0),
                m_packets_counter(0),
                m_packets_missed(0),
                m_packets_corrupted(0),
                m_packets_dropped(0)
            { ; }

            void reset() {
                m_expected_l1id = m_packets_counter = m_packets_missed
                        = m_packets_corrupted = m_packets_dropped = 0;
            }

            const InputLinkId m_fid;
            const DetectorLinkId m_resource_id;
            bool m_enabled;
            uint16_t m_last_bcid;
            uint32_t m_expected_l1id;
            uint64_t m_packets_counter;
            uint64_t m_packets_missed;
            uint64_t m_packets_corrupted;
            uint64_t m_packets_dropped;
        };

        typedef std::vector<Link> InputLinks;

        /**
         * Creates a new input data handler with the given configuration.
         *
         * @param[in] id Id to be used for this data handler
         * @param[in] links Input links which have to be handled by this object
         * @param[in] config Contains configuration parameters for the new data handler.
         * @param[in] input the object to be used for data subscription
         * @param[in] callback a function to be called when a new data arrive
         */
        DataInputHandlerBase(const std::string & id,
                const InputLinkVector & links,
                const boost::property_tree::ptree & config,
                const std::shared_ptr<DataInput> & input,
                detail::InputCallback callback);

        virtual ~DataInputHandlerBase();

        /**
         * Returns unique object ID.
         *
         * @return A unique object ID.
         */
        const std::string & getName() const override {
            return m_id;
        }

        /**
         * Returns a reference to the vector of input links handled by this object.
         * @return a vector of input links
         */
        const InputLinks & getLinks() const {
            return m_links;
        }

        /**
         * This function can be used to stop getting data from all input links managed by this object.
         * In order to achieve this it unsubscribes from all input links.
         */
        void disable() override;

        /**
         * This function checks the given links and unsubscribes from those that
         * are managed by this data handler. The IDs of the links that were successfully
         * disabled are removed from the input vector.
         *
         * @param link_ids[in, out] IDs of the input links which have to be disabled
         */
        void disableLinks(std::vector<InputLinkId> & link_ids) override;

        /**
         * This function can be called to re enable all input links handled by this object.
         * It re subscribes to all input links, which were not individually disabled
         * by using DataInputHandler::disableLinks() function.
         * It is assumed that the Trigger is on hold and no input data may be coming in when this
         * function is executed. A failure to respect this condition may result in a fatal crash.
         *
         * @param[in] lastL1ID Last L1ID generated before the Trigger had been put on hold.
         * @param[in] triggersNumber A total number of triggers received since the last restart of
         *      the SW ROD application
         */
        void enable(uint32_t lastL1ID, uint64_t triggersNumber) override;

        /**
         * This function checks the given links and tries to re enable those that are handled by this object.
         * The IDs of the links that were successfully re enabled are removed from the input vector. This way
         * when the function returns, the vector of links IDs contains only IDs of the links which are either
         * not handled by this worker or failed to be re enabled.
         * It is assumed that the Trigger is on hold and no input data may be coming in when this
         * function is executed. A failure to respect this condition may result in a fatal crash.
         *
         * @param[in, out] link_ids IDs of the input links which have to be re enabled
         * @param[in] lastL1ID Last L1ID generated when the Trigger had been put on hold.
         * @param[in] triggersNumber A total number of triggers produced since the last restart of this
         * SW ROD application
         */
        void enableLinks(std::vector<InputLinkId> & link_ids,
                uint32_t lastL1ID, uint64_t triggersNumber) override;

        /**
         * This function can be called after restarting the SW ROD application to
         * re synchronise the new SW ROD instance with the TTC system.
         * It is assumed that the Trigger is on hold and no input data may be produced when this
         * function is executed. A failure to respect this condition may result in a fatal crash.
         *
         * @param[in] lastL1ID Last L1ID generated when the trigger had been put on hold.
         */
        void resynchAfterRestart(uint32_t lastL1ID) override;

       /**
         * Is called to inform this object about starting a new run.
         *
         * @param[in] run_params New run parameters
         */
        void runStarted(const RunParams & run_params) override;

        /**
         * Is called to inform this object about the end of the current run.
         */
        void runStopped() override;

       /**
         * Subscribes to all input links handled by this object.
         */
        void subscribeToFelix() override;

        /**
         * Unsubscribes from all input links handled by this object.
         */
        void unsubscribeFromFelix() override;

        /**
         * Is called as part of the ROB fragment builder disabling procedure to notify the derived
         * class that this data handler has just been disabled.
         */
        virtual void disabled() { }

        /**
         * Is called as part of the ROB fragment builder enabling procedure to notify the derived
         * class that this data handler has just been re enabled.
         *
         * @param[in] triggersCounter A total number of triggers received by the SW ROD application
         * since the last restart. This number can be used to synchronise this data handler with
         * the TTC input stream.
         */
        virtual void enabled(uint64_t triggersCounter) { }

        /**
         * Is called as part of the input links disabling procedure to notify the derived
         * class that the given input link has just been disabled.
         *
         * @param[in] link A reference to the object that contains description of the just
         *      disabled input link.
         */
        virtual void linkDisabled(const Link & link) { }

        /**
         * Is called as part of the input links re enabling procedure to notify the derived
         * class that the given input link has just been re enabled.
         *
         * @param[in] link A reference to the object that contains description of the just
         *      re enabled input link
         */
        virtual void linkEnabled(const Link & link) { }

    protected:
        const std::string m_id;
        bool m_running = false;

        uint32_t m_links_number;
        InputLinks m_links;
        detail::InputCallback m_callback;
        std::shared_ptr<DataInput> m_input;
    };
}

#endif /* SWROD_DATAINPUTHANDLERBASE_H_ */
