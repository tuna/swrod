/*
 * DataInputHandler.h
 *
 *  Created on: Sep 3, 2019
 *      Author: kolos
 */

#ifndef SWROD_DATAINPUTHANDLER_H_
#define SWROD_DATAINPUTHANDLER_H_

#include <swrod/Component.h>

namespace swrod {

    /**
     * This is an abstract class that defines interface for data handling
     * components of the SW ROD.
     */
    class DataInputHandler : public Component {
    public:

        /**
         * This function can be used to stop getting data from all input links managed
         * by this object. A typical implementation of this function should unsubscribe
         * from the input links.
         */
        virtual void disable() = 0;

        /**
         * This function is used to inform the data handler that it shall ignore
         * data from the given input links. A typical implementation of this function
         * should unsubscribe from the given links.
         *
         * @param[in, out] link_ids The IDs of the input links to be disabled. If an
         *      attempt to disable a link from the given vector succeeds an implementation
         *      of this function shall remove the corresponding ID from the vector.
         */
        virtual void disableLinks(std::vector<InputLinkId> & link_ids) = 0;

        /**
         * This function can be called to re enable all input links handled by this object,
         * which had been previously disabled via DataInputHandler::disable() function.
         * It is assumed that the Trigger is on hold and no input data may be coming in when this
         * function is executed.
         *
         * @param lastL1ID Last L1ID generated when the Trigger had been put on hold.
         * @param triggersNumber A total number of triggers received since the last restart of
         *      the SW ROD application
         */
        virtual void enable(uint32_t lastL1ID, uint64_t triggersNumber) = 0;

        /**
         * This function is used to inform the data handler that it shall resume
         * getting data from the given input links. A typical implementation of this function
         * should re subscribe to the given links.
         * It is assumed that the Trigger is put on hold and no input data can be produced when this
         * function is executed. A failure to respect this condition may result in a fatal crash
         * of the SW ROD application.
         *
         * @param[in, out] link_ids A vector of IDs of the input links to be re enabled. If an
         *      attempt to enable a link from this vector succeeds an implementation of this
         *      function shall remove the corresponding ID from the vector.
         * @param[in] lastL1ID The last L1ID that has been produced before the Trigger has been put on hold.
         * @param triggersNumber A total number of triggers produced since the last restart of the
         *      SW ROD application
         */
        virtual void enableLinks(std::vector<InputLinkId> & link_ids,
                uint32_t lastL1ID, uint64_t triggersNumber) = 0;

        /**
         * This function shall be used as part of TTC Restart procedure to re synchronise
         * SW ROD data handlers after restart. The Trigger must be put on hold when this function
         * is called to guarantee that no data shall arrive to the SW ROD until this function
         * returns. A failure to respect this condition may result in a fatal crash
         * of the SW ROD application.
         *
         * @param[in] lastL1ID The last L1ID that has been produced before the Trigger has been put on hold.
         */
        virtual void resynchAfterRestart(uint32_t lastL1ID) = 0;

        /**
         * An implementation of this function shall subscribe to the input links,
         * which were assigned to this data handler with respect to the current configuration.
         * This function should be called during the Run Control CONNECT transition.
         */
        virtual void subscribeToFelix() = 0;

        /**
         * An implementation of this function shall unsubscribe from all input data links.
         * This function should be called during the Run Control DISCONNECT transition.
         */
        virtual void unsubscribeFromFelix() = 0;
    };
}

#endif /* SWROD_DATAINPUTHANDLER_H_ */
