/*
 * exceptions.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_EXCEPTIONS_H_
#define SWROD_EXCEPTIONS_H_

#include <ers/ers.h>

/*! \class swrod::Exception
 *  This is a base class for any SW ROD exception.
 */
ERS_DECLARE_ISSUE(swrod, Exception, ERS_EMPTY, ERS_EMPTY)

/*! \class swrod::BadConfigurationException
 *  This issue is used to report problems with SW ROD configuration
 */
ERS_DECLARE_ISSUE_BASE(swrod, BadConfigurationException, Exception,
        message,
        ERS_EMPTY,
        ((std::string)message))

/*! \class swrod::GetParameterException
 *  This issue is used to report problems with SW ROD configuration parameters
 */
ERS_DECLARE_ISSUE_BASE(swrod, GetParameterException, Exception,
        "Cannot get '" << parameter << "' parameter of the '" << node << "' object",
        ERS_EMPTY,
        ((std::string)node) ((std::string)parameter))

/*! \class swrod::PluginException
 *  This issue is used to report problems with a SW ROD plugin
 */
ERS_DECLARE_ISSUE_BASE(swrod, PluginException, Exception,
        "Custom plugin library '" << lib_name << "' issue: " << reason,
        ERS_EMPTY,
        ((std::string)lib_name) ((std::string)reason))

/*! \class swrod::UnexpectedL1IdException
 *  This issue is used to report out of order data packets
 */
ERS_DECLARE_ISSUE_BASE(swrod, UnexpectedL1IdException, Exception,
        "Data packet #" << packet_index << " from e-link 0x" << std::hex << elink
        << " has unexpected L1ID 0x" << received_l1id << ", expected L1ID 0x" << expected_l1id,
        ERS_EMPTY,
        ((uint64_t)packet_index) ((uint64_t)elink) ((uint32_t)expected_l1id) ((uint32_t)received_l1id))

/*! \class swrod::BCIDMismatch
 *  This issue is used to report BCID mismatch
 */
ERS_DECLARE_ISSUE_BASE(swrod, BCIDMismatch, Exception,
        "Data fragment with L1ID 0x" << std::hex << l1id
        << " has BCID 0x" << bcid << " that does not match BCID 0x"
        << l1a_bcid << " of the corresponding L1A message",
        ERS_EMPTY,
        ((uint32_t)l1id) ((uint16_t)bcid) ((uint16_t)l1a_bcid))

/*! \class swrod::L1ABrokenException
 *  This issue is used to report out opf order TTC packets
 */
ERS_DECLARE_ISSUE_BASE(swrod, L1ABrokenException, Exception,
        "L1Accept message #" << packet_index << " has unexpected L1ID 0x" << std::hex << received_l1id <<
        ", expected L1ID 0x" << expected_l1id,
        ERS_EMPTY,
        ((uint64_t)packet_index) ((uint32_t)expected_l1id) ((uint32_t)received_l1id))

/*! \class swrod::BilatedL1AException
 *  This issue is used to report a timeout for receiving the given TTC packet
 */
ERS_DECLARE_ISSUE_BASE(swrod, BilatedL1AException, Exception,
        "L1Accept message #" << packet_index << " did not arrive within " << timeout << " milliseconds",
        ERS_EMPTY,
        ((uint64_t)packet_index) ((uint32_t)timeout))

/*! \class swrod::BufferUnderflowException
 *  This issue is used to report an attempt to access data that is not already in the buffer
 */
ERS_DECLARE_ISSUE_BASE(swrod, BufferUnderflowException, Exception,
        "Fragment #" << fragment_index << " is not in the buffer, the first fragment is #" << head,
        ERS_EMPTY,
        ((uint64_t)fragment_index) ((uint64_t)head))

/*! \class swrod::ScarceBufferException
 *  This issue is used to report insufficient space in the buffer
 */
ERS_DECLARE_ISSUE_BASE(swrod, ScarceBufferException, Exception,
        "Insufficient buffer size for the packet #" << packet_index << " from e-link 0x" << std::hex << elink
        << ", packet with size " << std::dec << packet_size << " bytes will be dropped."
        "Advice: Increase the value of SwRodFragmentBuilder.MaxMessageSize configuration parameter",
        ERS_EMPTY,
        ((uint64_t)elink) ((uint64_t)packet_index) ((uint32_t)packet_size))

/*! \class swrod::DuplicatePacketException
 *  This issue is used to report duplicate data packets
 */
ERS_DECLARE_ISSUE_BASE(swrod, DuplicatePacketException, Exception,
        "Data packet #" << packet_index << " from e-link 0x" << std::hex << elink
        << " has the same L1ID = 0x" << l1id << " and BCID = 0x" << bcid << " as the last one",
        ERS_EMPTY,
        ((uint64_t)elink) ((uint64_t)packet_index) ((uint32_t)l1id) ((uint16_t)bcid))

/*! \class swrod::NoDataReceived
 *  This issue is used to report e-links which stop sending data
 */
ERS_DECLARE_ISSUE_BASE(swrod, NoDataReceived, Exception,
        "No data received from e-link 0x" << std::hex << elink
        << " the number of packets received from this e-link is " << std::dec << packet_number
        << " the number of incomplete fragments built is " << fragments_number,
        ERS_EMPTY,
        ((uint64_t)elink) ((uint64_t)packet_number) ((uint64_t)fragments_number))

/*! \class swrod::CorruptPacketException
 *  This issue is used to report data packets which are not aligned with TTC packets
 */
ERS_DECLARE_ISSUE_BASE(swrod, CorruptPacketException, Exception,
        "Data packet #" << packet_index << " from e-link 0x" << std::hex << elink
        << " with L1ID = 0x" << l1id << " L1ID mask = 0x" << l1id_mask << " BCID = 0x" << bcid
        << " is out of synch with L1A packet that has L1ID = 0x"
        << l1a_l1id << " and BCID = 0x" << l1a_bcid << " CRC check " << (crc_failed ? "failed" : "passed"),
        ERS_EMPTY,
        ((uint64_t)elink) ((uint64_t)packet_index) ((uint32_t)l1id_mask)
        ((uint32_t)l1id) ((uint16_t)bcid) ((uint32_t)l1a_l1id) ((uint16_t)l1a_bcid)
        ((bool)crc_failed))

/*! \class swrod::DataCorruptionException
 *  This issue is used to report corrupted data
 */
ERS_DECLARE_ISSUE_BASE(swrod, DataCorruptionException, Exception,
        "Data packet #" << packet_index << " from e-link 0x" << std::hex << elink
        << " is corrupt, total number of corrupt packets for this e-link is "
        << std::dec << corrupt_number,
        ERS_EMPTY,
        ((uint64_t)elink) ((uint64_t)packet_index) ((uint64_t)corrupt_number))

/*! \class swrod::DroppedPacketException
 *  This issue is used to report rejected data packets
 */
ERS_DECLARE_ISSUE_BASE(swrod, DroppedPacketException, Exception,
        "Data packet #" << packet_index << " from e-link 0x" << std::hex << elink
            << " has been dropped, total number of dropped packets for this e-link is "
            << std::dec << dropped_number,
        ERS_EMPTY,
        ((uint64_t)elink) ((uint64_t)packet_index) ((uint32_t)dropped_number))

/*! \class swrod::SpuriousPacketException
 *  This issue is used to report unexpected data packets
 */
ERS_DECLARE_ISSUE_BASE(swrod, SpuriousPacketException, Exception,
        "Data packet from e-link 0x" << std::hex << elink
            << " has been received while SW ROD was not in RUNNING state",
        ERS_EMPTY,
        ((uint64_t)elink))

/*! \class swrod::SubscriptionException
 *  This issue is used to report a failure of subscribing to FELIX
 */
ERS_DECLARE_ISSUE_BASE(swrod, SubscriptionException, Exception,
        "Can not subscribe to e-link 0x" << std::hex << elink << ": " << reason,
        ERS_EMPTY,
        ((uint64_t)elink) ((std::string)reason))

/*! \class swrod::BadAddressException
 *  This issue is used to report invalid e-link address
 */
ERS_DECLARE_ISSUE_BASE(swrod, BadAddressException, Exception,
        "Bad address '" << address << "' for e-link 0x" << std::hex << elink
        << " uuid = '" << uuid << "'",
        ERS_EMPTY,
        ((std::string)address) ((uint64_t)elink) ((std::string)uuid))

/*! \class swrod::ResynchFailed
 *  This issue is used to report failure of a data re synchronisation attempt
 */
ERS_DECLARE_ISSUE_BASE(swrod, ResynchFailed, Exception,
        "Got timeout on waiting for fragment with L1ID = 0x" << std::hex << expected_L1ID
           << ", last fragment built has L1ID = 0x" << last_built_L1ID
           << ", last L1A received has L1ID = 0x" << last_received_L1ID,
        ERS_EMPTY,
        ((uint32_t)expected_L1ID) ((uint32_t)last_built_L1ID) ((uint32_t)last_received_L1ID))

/*! \class swrod::ROBFragmentError
 *  This issue is used to report a ROB fragment that has problems
 */
ERS_DECLARE_ISSUE_BASE(swrod, ROBFragmentError, Exception,
        "ROB fragment with L1ID = 0x" << std::hex << L1ID << " for the ROB 0x"
        << ROB_id << " has errors. Total number of bad fragments for this ROB is "
        << std::dec << total_errors
        << ". Please check the status word in the ROB header for details.",
        ERS_EMPTY,
        ((uint32_t)ROB_id) ((uint32_t)L1ID) ((uint64_t)total_errors))

/*! \class swrod::ROBRemovalFailed
 *  This issue is used to report failure of a ROB re enabling attempt
 */
ERS_DECLARE_ISSUE_BASE(swrod, ROBRemovalFailed, Exception,
        "Failed to disable ROB 0x" << std::hex << ROB_id << " because " << reason,
        ERS_EMPTY,
        ((uint32_t)ROB_id) ((std::string)reason))

/*! \class swrod::ROBRecoveryFailed
 *  This issue is used to report failure of a ROB re enabling attempt
 */
ERS_DECLARE_ISSUE_BASE(swrod, ROBRecoveryFailed, Exception,
        "Failed to re enable ROB 0x" << std::hex << ROB_id << " because " << reason,
        ERS_EMPTY,
        ((uint32_t)ROB_id) ((std::string)reason))

/*! \class swrod::LinkRecoveryFailed
 *  This issue is used to report failure of an e-link re enabling attempt
 */
ERS_DECLARE_ISSUE_BASE(swrod, LinkRecoveryFailed, Exception,
        "Failed to re enable e-link 0x" << std::hex << elink << " because " << reason,
        ERS_EMPTY,
        ((uint64_t)elink) ((std::string)reason))

/*! \class swrod::HltClearIssue
 *  Reports problems with processing HLT clear requests
 */
ERS_DECLARE_ISSUE_BASE(swrod, HltClearIssue, Exception,
        "Failed to clear " << count << " events, for ROB 0x" << std::hex << rob
         << ", first failed l1 ID 0x"<< first << ", last failed l1 ID 0x" << last << std::dec,
        ERS_EMPTY,
        ((unsigned int)count) ((unsigned int)rob) ((unsigned int)first) ((unsigned int)last))

 /*! \class swrod::HltClearIssue
  *  Reports missing HLT clear messages
  */
ERS_DECLARE_ISSUE_BASE(swrod, HltMissedClearIssue, Exception,
        "Missed " << count << " clear messages ",
        ERS_EMPTY,
        ((unsigned int)count))

 /*! \class swrod::HltBadRobIssue
  *  Reports HLT request for ROB we don't serve
  */
ERS_DECLARE_ISSUE_BASE(swrod, HltBadRobReqIssue, Exception,
        "Request for wrong ROB 0x" << std::hex << rob << std::dec,
        ERS_EMPTY,
        ((unsigned int)rob))

/*! \class swrod::PrepRunIssue
 * Reports failure to read run parameters from IS
 */
ERS_DECLARE_ISSUE_BASE(swrod, PrepRunIssue, Exception,
        "Failed to get runParams from IS",
        ERS_EMPTY,
        ERS_EMPTY)

/*! \class swrod::FileWritingIssue
 * Reports problems writing data to file
 */
ERS_DECLARE_ISSUE_BASE(swrod, FileWritingIssue, Exception,
        "write failed",
        ERS_EMPTY,
        ERS_EMPTY)

/*! \class swrod::FileWritingForcedIssue
 * Reports problems writing data to file
 */
ERS_DECLARE_ISSUE_BASE(swrod, FileWritingForcedIssue, Exception,
        "Ignoring recording disabled state and writing file anyway",
        ERS_EMPTY,
        ERS_EMPTY)

/*! \class swrod::FileStartIssue
 * Reports problems initialising data writer
 */
ERS_DECLARE_ISSUE_BASE(swrod, FileStartIssue, Exception,
        "DataWriter initialization failed",
        ERS_EMPTY,
        ERS_EMPTY)

/*! \class swrod::MsgCastIssue
 * Reports problems casting asyncmsg to expected message type
 */
ERS_DECLARE_ISSUE_BASE(swrod, MsgCastIssue, Exception,
        "Failed to cast message to "  << reason,
        ERS_EMPTY,
        ((std::string)reason))

/*! \class swrod::MsgTypeIssue
 * Reports problems with received asyncmsg of unknown message type
 */
ERS_DECLARE_ISSUE_BASE(swrod, MsgTypeIssue, Exception,
        "Unknown message type 0x" << std::hex << what << std::dec,
        ERS_EMPTY,
        ((unsigned int)what))

/*! \class swrod::FragmentOverwriteIssue
 * Reports problems with indexing fragments that already exist in the index
 */
ERS_DECLARE_ISSUE_BASE(swrod, FragmentOverwriteIssue, Exception,
        "Overwriting an existing fragment collection, ROB 0x" << std::hex << rob
        << " L1Id 0x" << l1id << std::dec,
        ERS_EMPTY,
        ((unsigned int) rob) ((unsigned int) l1id))

/*! \class swrod::FragmentNotFoundIssue
 * Reports problems with fragments that are not found in the index
 */
ERS_DECLARE_ISSUE_BASE(swrod, FragmentNotFoundIssue, Exception,
       "Fragment not found for L1Id 0x" << std::hex << l1id << std::dec,
       ERS_EMPTY,
       ((unsigned int) l1id))


/*! \class swrod::RobDisabledIssue
 * Reports attempts to use a disabled ROB
 */
ERS_DECLARE_ISSUE_BASE(swrod, RobDisabledIssue, Exception,
       action << " for disabled ROB 0x" << std::hex << rob << std::dec,
       ERS_EMPTY,
       ((std::string) action) ((unsigned int) rob))

 /*! \class swrod::BadRobIssue
  *  Reports insertion attempt for ROB we don't serve
  */
ERS_DECLARE_ISSUE_BASE(swrod, BadRobIssue, Exception,
        "Attempt to insert unknown ROB 0x" << std::hex << rob << std::dec,
        ERS_EMPTY,
        ((unsigned int)rob))

 /*! \class swrod::UnknownRobUidIssue
  *  Reports attempt to lookup ROB we don't serve
  */
ERS_DECLARE_ISSUE_BASE(swrod, UnknownRobUidIssue, Exception,
       "Unknown ROB UID: " << rob,
       ERS_EMPTY,
       ((std::string)rob))

 /*! \class swrod::BadCommandIssue
  *  Reports failure of RC command
  */
ERS_DECLARE_ISSUE_BASE(swrod, BadCommandIssue, Exception,
       "Command "  << cmd << " failed ",
       ERS_EMPTY,
       ((std::string)cmd))
       /*! \class swrod::BadConfigurationException
        *  This issue is used to report problems with SW ROD configuration
        */

 /*! \class swrod::SynchronisationException
  *  Reports failure of RC command
  */
ERS_DECLARE_ISSUE_BASE(swrod, SynchronisationException, Exception,
       message,
       ERS_EMPTY,
       ((std::string)message))

#endif /* SWROD_EXCEPTIONS_H_ */
