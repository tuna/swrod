/*
 * ROBFragmentProvider.h
 *
 *  Created on: Jun 11, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTPROVIDER_H_
#define SWROD_ROBFRAGMENTPROVIDER_H_

#include <memory>

#include <swrod/ROBFragment.h>

namespace swrod {
    class ROBFragmentConsumer;

    /**
     * This class defines an interface for a supplier of ROB fragments and provides default
     * implementation of this interface. This implementation maintains a list of ROB fragment
     * consumers by adding a new consumer to the end of the list.
     */
    class ROBFragmentProvider {
    public:
        ROBFragmentProvider() :
            m_function([](const std::shared_ptr<swrod::ROBFragment> &){})
        {}

        virtual ~ROBFragmentProvider() = default;

        /**
         * Adds a new ROB Fragment Consumer to this provider.
         * Default implementation adds the given consumer to the end of the consumers list.
         *
         * @param[in] consumer A new ROB fragment consumer.
         */
        virtual void addConsumer(
                const std::shared_ptr<ROBFragmentConsumer> & consumer);

        /**
         * Default implementation passes the given ROB fragment to the first consumer in the list.
         *
         * @param[in] fragment ROB fragment to be passed to the consumers.
         */
        virtual void forwardROBFragment(
                const std::shared_ptr<swrod::ROBFragment> & fragment) {
            m_function(fragment);
       }

    protected:
        std::shared_ptr<ROBFragmentConsumer> m_consumer;
        std::function<void(const std::shared_ptr<swrod::ROBFragment> &)> m_function;
    };
}

#endif /* SWROD_ROBFRAGMENTPROVIDER_H_ */
