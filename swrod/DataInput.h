/*
 * DataInput.hpp
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_DATAINPUT_H_
#define SWROD_DATAINPUT_H_

#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>

#include <swrod/Component.h>
#include <swrod/InputLinkId.h>
#include <swrod/detail/InputCallback.h>
#include <swrod/detail/tsl/robin_map.h>

namespace swrod {
    /**
     * This is an abstract class that defines an interface for receiving input data by SW ROD.
     * An implementation of this interface has to pass all data it receives to the subscribers
     * by calling the protected DataInput::dataReceived() function. An implementation of
     * this interface must always call this function from a single thread or serialise the
     * calls if they are done from different threads.
     * In addition an implementation of this interface has to able to execute a given action
     * in a synchronous way with respect to the data handling threads.
     * This should be done by overriding the DataInput::actionsPending() virtual function, that
     * is called when a new action is requested to be executed. When this happens the implementation
     * of this function shall be accountable for execution of the DataInput::executeActions() function
     * from the same thread that is used for the DataInput::dataReceived() function invocations.
     */
    class DataInput : public Component {
    public:
        typedef detail::DataCallback DataCallback;
        typedef std::function<void(void)> Action;

        virtual ~DataInput() = default;

        const std::string & getName() const override {
            static std::string s("DataInput");
            return s;
        }

        /**
         * This function can be used to execute a given action in the context of
         * the data handling thread. This is necessary to avoid using locks in the data
         * handling thread that may otherwise result in significant performance
         * penalty.
         *
         * @param action The action to be executed in the context of the data handling thread.
         */
        void executeSynchronousAction(const Action & action);

        /**
         * This function is called when input data has to be re synchronised with the
         * TTC system.
         * @param ECR_value - the value of ECR counter that has to be used by the input system
         */
        virtual void synchronise(uint8_t ECR_value) {};

        /**
         * Associates the given callback function with the given link and calls
         * DataInput::subscribeToFelix(). If a callback had been already set for the given
         * link the function will throw swrod::SubscriptionException exception.
         *
         * @param[in] link The ID of the input link.
         * @param[in] callback This function will be called for any data received
         *      from the given input link.
         * @throw swrod::SubscriptionException This exception is thrown if a callback is
         *      already registered with the given link.
         */
        void subscribe(const InputLinkId & link, const DataCallback & callback);

        /**
         * Removes the callback that had been previously set for the given input link.
         * If no callback is associated with the given input link the function immediately
         * returns, otherwise calls the DataInput::unsubscribeFromFelix() function.
         *
         * @param[in] link The ID of the input link.
         */
        void unsubscribe(const InputLinkId & link);

    protected:

        /**
         * This function is called when there are actions scheduled for execution
         * in the context of the data receiving thread. In response to this call an
         * implementation of the DataInput interface has to call DataInput::executeActions()
         * function as soon as possible in the context of the data receiving thread.
         */
        virtual void actionsPending() = 0;

        /**
         * An implementation of the DataInput interface shall override this function.
         * This function is called by the DataInput::subscribe() and is responsible for
         * establishing subscription for the given input link.
         *
         * @param[in] link_id The ID of the input link to be subscribed.
         */
        virtual void subscribeToFelix(const InputLinkId & link_id) = 0;

        /**
         * An implementation of the DataInput interface shall override this function.
         * This function is called by the DataInput::unsubscribe() and is responsible
         * for removing subscription that had been previously established for the given
         * input link.
         *
         * @param[in] link_id The ID of the input link to be unsubscribed.
         */
        virtual void unsubscribeFromFelix(const InputLinkId & link_id) = 0;

        /**
         * This function can be overridden to translate communication transport error status
         * to the corresponding ROB fragment header error status. Default implementation
         * returns zero.
         *
         * @param status - communication transport status
         * @return ROB fragment header status
         */
        virtual uint32_t translateStatus(uint8_t status) const noexcept {
            return 0;
        }

        /**
         * An implementation of the DataInput interface shall provide input data by
         * calling this function.
         *
         * @param link input link ID for which the data has been received
         * @param data pointer to the beginning of the data
         * @param size data size
         * @param status error status that is given by the data source
         */
        void dataReceived(InputLinkId link, const uint8_t * data, uint32_t size, uint8_t status) {
#define SWROD_UNLIKELY(x) __builtin_expect(!!(x),0)

            if (m_last_id != link) {
                m_last_it = m_callbacks.find(link);
                if (SWROD_UNLIKELY(m_last_it == m_callbacks.end())) {
                    ERS_DEBUG(5, "No callback was found for 0x"
                            << std::hex << link << " input link");
                    m_last_id = InputLinkId(-1);
                    return;
                }
                m_last_id = link;
            }

            ERS_DEBUG(5, "Data packet of " << size << " bytes received for the 0x"
                    << std::hex << link << " input link");

            m_last_it->second(data, size, status, !status ? 0 : translateStatus(status));
        }

        /**
         * This function executes all pending actions. If there are no actions pending
         * it does nothing. It shall be called by an implementation of DataInput interface
         * in the context of the data receiving thread in response to a DataInput::actionsPending()
         * function invocation.
         */
        void executeActions();

        std::vector<InputLinkId> subscribedLinks() const {
            std::vector<InputLinkId> links;
            for (auto & c : m_callbacks) {
                links.push_back(c.first);
            }
            return links;
        }

    private:
        struct Hash {
            size_t operator()(InputLinkId x) const noexcept {
                x = ((x >> 17) ^ x) * 0x45d9f3b;
                return (x >> 17) ^ x;
            }
        };

        typedef tsl::robin_map<InputLinkId, DataCallback, Hash> Callbacks;
        typedef std::queue<Action> Actions;

        Callbacks m_callbacks;
        std::mutex m_actions_mutex;
        std::condition_variable m_actions_condition;
        Actions m_actions;

        InputLinkId m_last_id = InputLinkId(-1);
        Callbacks::iterator m_last_it;
    };
}

#endif /* SWROD_DATAINPUT_H_ */
