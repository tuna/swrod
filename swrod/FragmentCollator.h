// -*- c++ -*-
#ifndef FRAGMENT_COLLATOR_H
#define FRAGMENT_COLLATOR_H

#include <map>
#include <vector>
#include <memory>
#include <set>
#include <mutex>
#include <condition_variable>

#include "swrod/ROBFragment.h"

namespace swrod {
   namespace helper {
      //! Helper class to collate ROBfragments belonging to the same L1 ID
      class FragmentCollator {
      public:
         FragmentCollator();
         void expected(unsigned int exp) {m_expectedFragsPerL1=exp;};
         unsigned int expected() const {return m_expectedFragsPerL1;};
         void maxIndex(unsigned int size) {m_maxIndexSize=size;};
         void close();
         void reset();
         bool insert(std::shared_ptr<ROBFragment> frag);
         void clear(unsigned int L1Id);
         std::vector<unsigned int> clear(std::vector<unsigned int>& L1IdVec);
         void enable(unsigned int robId);
         void disable(unsigned int robId);
         std::vector<unsigned int> latestL1() const;
         unsigned int lowestL1() const {
            if (m_fragMap.size()) {
               return m_fragMap.begin()->first;
            }
            else {
               return 0;
            }
         }
         const std::vector<std::shared_ptr<ROBFragment>>& get(
            unsigned int l1Id);
         std::vector<std::shared_ptr<ROBFragment>> get(
            unsigned int l1Id,std::set<unsigned int>* robs);

         std::vector<std::shared_ptr<ROBFragment>> take(
            unsigned int l1Id);

         unsigned int size() const {return m_fragMap.size();};
         void dump() const;
      private:
         struct Record {
            std::vector<std::shared_ptr<ROBFragment>> m_frag;
            std::set<unsigned int> m_source;
         };
         std::vector<std::shared_ptr<ROBFragment>> m_emptyEvent;
         std::map<unsigned int,Record> m_fragMap;

         unsigned int m_expectedFragsPerL1;
         std::set<unsigned int> m_disabledRobs;
         std::mutex m_mapMutex;
         std::map<unsigned int,unsigned int> m_latestL1Id;
         unsigned int m_maxIndexSize;
         bool m_closed;
         std::condition_variable m_condition;
      };
   }
}
#endif
