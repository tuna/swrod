/*
 * ROBFragment.h
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENT_H_
#define SWROD_ROBFRAGMENT_H_

#include <string.h>

#include <cstdint>
#include <cstring>
#include <functional>
#include <memory>
#include <mutex>
#include <vector>

#include <swrod/GBTChunk.h>

namespace swrod {

    /**
     * This class contains information for a fully built ROB fragment.
     */
    class ROBFragment {
    public:

        /**
         * This class implements memory management for ROB fragment data and provides API
         * for data manipulation.
         */
        class DataBlock {
        public:

            /**
             * Creates a new object that owns the given memory block. When this object is destroyed
             * it deletes the memory block using the custom deleter given to this constructor.
             *
             * @param memory_block A pointer to the memory block that will be owned by the new object.
             * @param memory_size A size of the memory block in 4-byte words.
             * @param data_size A size of the data that is already present in the given memory block in 4-byte words.
             * @param deleter A function that will be used to delete the memory block. It will be called
             *                  by the destructor of the new object.
             */
            DataBlock(uint32_t memory_block[] = nullptr, uint32_t memory_size = 0, uint32_t data_size = 0,
                    const std::function<void(uint32_t[])> & deleter = [](uint32_t d[]){delete[] d;}) :
                m_memory_block(memory_block, deleter),
                m_memory_size(memory_size),
                m_data_size(data_size)
            { }

            /**
             * Copies the given header and data chunk to the internal buffer of the data block if the buffer
             * has enough free space. Otherwise does nothing.
             *
             * @param header chunk header to be copied to the buffer in front of the data chunk.
             * @param data data chunk to be copied to the data block's internal buffer
             * @param size number of bytes to copy
             * @return true if there was enough space in the buffer to accommodate the given header and data chunk,
             * false otherwise. If false is returned the data block's internal buffer remains in the same state
             * as it was before the function invocation.
             */
            bool append(GBTChunk::Header & header, const void * data, uint16_t size) {
                static_assert(not (sizeof(GBTChunk::Header) % sizeof(uint32_t)));

                //Calculate size in 4-byte words
                header.m_size = (size + sizeof(GBTChunk::Header) + 3) >> 2;
                if (header.m_size > freeSpaceSize()) {
                    return false;
                }

                // Set potential padding bytes to zero
                *(dataEnd() + header.m_size - 1) = 0;

                memcpy(dataEnd(), &header, sizeof(GBTChunk::Header));
                memcpy(dataEnd() + (sizeof(GBTChunk::Header)>>2), data, size);
                m_data_size += header.m_size;
                return true;
            }

            /**
             * Copies the given data chunk to the data block internal buffer. If N is the amount
             * of free space in the buffer and N is smaller than the given data chunk size then
             * only the first N bytes of the data chunk will be copied.
             *
             * @param data data chunk to be copied to the data block buffer
             * @param size number of bytes to copy
             * @return the number of copied bytes
             */
            uint32_t append(const void * data, uint32_t size) {
                uint32_t word_size = (size + 3)>>2;
                if (word_size > freeSpaceSize()) {
                    word_size = freeSpaceSize();
                    size = word_size<<2;
                }

                // Set potential padding bytes to zero
                *(dataEnd() + word_size - 1) = 0;
                memcpy(dataEnd(), data, size);
                m_data_size += word_size;
                return size;
            }

            /**
             * Returns a pointer to the beginning of the memory block.
             * @return pointer to the beginning of data
             */
            uint32_t* dataBegin() const {
                return m_memory_block.get();
            }

            /**
             * Returns a pointer to the end of data that is present in the
             * memory block. This pointer points to the first byte after the last one used by the data.
             * If no data is present the function returns the same pointer as dataBegin()
             * @return end of data pointer
             */
            uint32_t* dataEnd() const {
                return m_memory_block.get() + m_data_size;
            }

            /**
             * Returns size of the data that is present in the memory block in 4-byte words. This size is equal
             * to dataEnd() - dataBegin().
             * @return data size
             */
            uint32_t dataSize() const {
                return m_data_size;
            }

            /**
             * Returns size of the free space in the memory block in 4-byte words.
             * This size is equal to memorySize() - dataSize()
             * @return free space size
             */
            uint32_t freeSpaceSize() const {
                return m_memory_size - m_data_size;
            }

            /**
             * Returns size of the memory block in bytes.
             * @return memory block size
             */
            uint32_t memorySize() const {
                return m_memory_size;
            }

            /**
             * Sets data size to zero.
             */
            void reset() {
                m_data_size = 0;
            }

        private:
            typedef std::unique_ptr<uint32_t[], std::function<void(uint32_t[])>> MemoryBlock;

            MemoryBlock m_memory_block;
            uint32_t m_memory_size;
            uint32_t m_data_size;
        };

        /**
         * Constructs a new ROB data fragment with the given data and parameters.
         *
         * @param source_id The ROB ID.
         * @param l1id The extended L1 ID.
         * @param bcid The bunch crossing ID.
         * @param trigger_type The trigger type.
         * @param rob_status ROB header status word.
         * @param missed_packets The number of missed packets in this fragment.
         * @param corrupt_packets The number of corrupt packets in this fragment.
         * @param data The data payload of this fragment.
         */
        ROBFragment(uint32_t source_id, uint32_t l1id, uint16_t bcid, uint32_t trigger_type,
                uint32_t rob_status, uint16_t missed_packets, uint16_t corrupt_packets,
                std::vector<DataBlock> && data,
                bool rod_header_present = false);

        /**
         * Constructs a new empty ROB data fragment with the given parameters.
         *
         * @param source_id The ROB ID.
         * @param l1id The extended L1 ID.
         * @param status_word Usually contains the code of the error that prevents proper building of this event.
         */
        ROBFragment(uint32_t source_id, uint32_t l1id, uint32_t status_word);

        /**
         * Sets a function that will be executed exactly once when serialize() function is called
         * for the first time.
         *
         * @param pp the function to be applied to this fragment before serialization
         */
        void setPreProcessor(const std::function<void (ROBFragment &)> & pp);

        /**
         * Returns the value of the ROB header status word.
         *
         * @returns the value of the status word from the ROB fragment header
         */
        uint32_t statusWord() const {
            return m_rob_header[6];
        }

        /**
         * This function puts the content of the fragment into a number of memory chunks
         * that can be used to send it over network or to write it to a file.
         *
         * @param run_number The current run number.
         * @param status_word This value will be set to the second status word of the ROB header.
         * @returns The event represented as a number of memory blocks.
         */
        std::vector<std::pair<uint8_t*, uint32_t>> serialize(uint32_t run_number, uint32_t status_word = 0);

        const uint32_t m_source_id;
        const uint32_t m_l1id;
        const uint16_t m_bcid;
        const uint32_t m_trigger_type;
        const uint16_t m_missed_packets;
        const uint16_t m_corrupted_packets;
        const bool m_rod_header_present;

        uint32_t m_detector_type;
        uint16_t m_rod_minor_version;
        bool m_status_front;
        std::vector<uint32_t> m_status_words;
        std::vector<DataBlock> m_data;

    private:
        std::mutex m_mutex;
        uint32_t m_rob_header[9];
        uint32_t m_rod_header[9];
        uint32_t m_rod_trailer[3];
        std::function<void ()> m_pp;
    };
}

#endif /* SWROD_ROBFRAGMENT_H_ */
