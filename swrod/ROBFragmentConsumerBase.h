/*
 * ROBFragmentConsumerBase.h
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTCONSUMERBASE_H_
#define SWROD_ROBFRAGMENTCONSUMERBASE_H_

#include <memory>
#include <tbb/concurrent_queue.h>

#include <swrod/ROBFragmentConsumer.h>
#include <swrod/detail/ThreadPool.h>

namespace swrod {

    /**
     * This class provides an efficient configurable implementation of the ROBFragmentConsumer interface,
     * that uses a given number of worker threads for data processing.
     * Deriving a new class from the ROBFragmentConsumerBase is a recommended way to implement
     * a custom ROB fragment consumer.
     */
    class ROBFragmentConsumerBase : public ROBFragmentConsumer {
    public:
        typedef std::function<void(const std::shared_ptr<ROBFragment> &)> UserFunction;

        /**
         * Defines a policy of passing ROB fragments to the next consumer.
         */
        enum class ForwardingPolicy {
            None,               ///< Does not pass incoming ROB fragments to the next consumer
            Immediate,          ///< Pass incoming ROB fragment to the next consumer immediately upon receiving
            AfterProcessing     ///< Pass incoming ROB fragments after finishing local processing
        };

        /**
         * Defines what to do with the incoming ROB fragments when the internal input queue is full.
         */
        enum class QueuingPolicy {
            Drop,       ///< Drop ROB fragments if the input queue if full
            Wait        ///< Wait until a place becomes available in the queue
        };

        /**
         * Constructs a new ROB fragment consumer that will create the given number of worker threads that
         * will execute the same function.
         *
         * @param[in] function Processing function to be executed.
         * @param[in] queuing_policy Fragment queueing policy.
         * @param[in] forwarding_policy Fragment forwarding policy.
         * @param[in] workers_number The number of worker threads.
         * @param[in] queue_size Size of the internal queue. Default value -1 means unlimited.
         * @param[in] name The ID of the new object. If the new consumer returns a non-null pointer to
         *      IS object from the getStatistics() function then this ID must be unique in the scope of
         *      the current SW ROD application.
         * @param[in] cpu_set Can be used to limit CPU cores where the pool threads will be executed.
         * CPU range can contain numbers separated by commas and may include ranges.
         * For example: 0,5,7,9-11. Empty value implies no limits.
         */
        explicit ROBFragmentConsumerBase(
                const UserFunction & function = [](const std::shared_ptr<ROBFragment> &){},
                QueuingPolicy queuing_policy = QueuingPolicy::Wait,
                ForwardingPolicy forwarding_policy = ForwardingPolicy::Immediate,
                uint16_t workers_number = 1,
                int32_t queue_size = -1,
                const std::string & name = "Consumer",
                const std::string & cpu_set = "");

        /**
         * Constructs a new ROB fragment consumer that will use the given number of worker threads
         * to execute the given processing function.
         *
         * @param[in] config Configuration object that contains parameters taken from the
         *      SwRodFragmentConsumer configuration class
         * @param[in] function The processing function to be executed.
         * @param[in] queuing_policy Fragment queueing policy.
         * @param[in] forwarding_policy Fragment forwarding policy.
         * @param[in] name The ID of the new object. If the new consumer returns a non-null pointer to
         *      IS object from the getStatistics() function then this ID must be unique in the scope of
         *      the current SW ROD application.
         */
        explicit ROBFragmentConsumerBase(
                const boost::property_tree::ptree& config,
                const UserFunction & function = [](const std::shared_ptr<ROBFragment> &){},
                QueuingPolicy queuing_policy = QueuingPolicy::Wait,
                ForwardingPolicy forwarding_policy = ForwardingPolicy::Immediate,
                const std::string & name = "Consumer");

        /**
         * Constructs a new ROB fragment consumer creating a new worker thread for every processing function
         * in the given functions vector.
         *
         * @param[in] config Configuration object that contains parameters taken from the
         *      SwRodFragmentConsumer configuration class
         * @param[in] functions A vector of processing function to be executed. Each function
         *      will be executed in a dedicated worker thread.
         * @param[in] queuing_policy Fragment queueing policy, default is -1, which means unlimited.
         * @param[in] forwarding_policy Fragment forwarding policy.
         * @param[in] name The ID of the new object. If the new consumer returns a non-null pointer to
         *      IS object from the getStatistics() function then this ID must be unique in the scope of
         *      the current SW ROD application.
         */
        explicit ROBFragmentConsumerBase(
                const boost::property_tree::ptree& config,
                const std::vector<UserFunction> & functions,
                QueuingPolicy queuing_policy = QueuingPolicy::Wait,
                ForwardingPolicy forwarding_policy = ForwardingPolicy::Immediate,
                const std::string & name = "Consumer");

        /**
         * Returns the name that was given to the object constructor. This name
         * must be unique to support publishing of monitoring statistics to IS.
         *
         * @return The object name.
         */
        const std::string& getName() const override {
            return m_threads.getName();
        }

        /**
         * This function puts the given ROB fragment into internal queue and returns. The actual
         * processing is done by the worker threads, which are constantly polling this queue for
         * new fragments.
         * @param fragment
         */
        void insertROBFragment(const std::shared_ptr<ROBFragment> & fragment) override;

        /**
         * Called when a new data taking session is started.
         * @param[in] run_params The new run parameters.
         */
        void runStarted(const RunParams & run_params) override;

        /**
         * Called when the current data taking session is terminated.
         */
        void runStopped() override;

    protected:
        void run(const UserFunction & user_function, const bool & active);

    protected:
        typedef tbb::concurrent_bounded_queue<std::shared_ptr<ROBFragment>> FragmentQueue;

        const ForwardingPolicy m_forwarding;
        const QueuingPolicy m_queuing;
        const bool m_flush_buffer;
        bool m_running;
        FragmentQueue m_fragments_queue;
        detail::ThreadPool m_threads;
    };
}

#endif /* SWROD_ROBFRAGMENTCONSUMERBASE_H_ */
