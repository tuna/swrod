/*
 * ROBFragmentConsumer.h
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTCONSUMER_H_
#define SWROD_ROBFRAGMENTCONSUMER_H_

#include <memory>

#include <swrod/Component.h>
#include <swrod/InputLinkId.h>
#include <swrod/ROBFragment.h>
#include <swrod/ROBFragmentProvider.h>

namespace swrod {

    /**
     * An abstract class that defines an interface for a ROB fragment consumer implementation.
     *
     * It is not recommended to use this class directly, but to derive a ROB Fragment Consumer
     * implementation from the ROBFragmentConsumerBase class.
     */
    class ROBFragmentConsumer : public Component,
                                public ROBFragmentProvider {
    public:
        virtual ~ROBFragmentConsumer() = default;

        /**
         * This function is used to pass fully built ROB fragments to this consumer.
         * A consumer has to process the given fragment as fast as possible and pass it to the
         * next consumer in the chain using the ROBFragmentProvider::forwardROBFragment() function.
         * It is crucially important for the overall SW ROD performance to return from this function
         * as fast as possible as otherwise this may create back pressure to the caller of this
         * function (e.g. the previous consumer in the chain) and may decrease the overall event
         * processing rate of the SW ROD application.
         * It is strongly recommended not to inherit the ROBFragmentConsumer class directly, but
         * to derive a custom consumer implementation from the ROBFragmentConsumerBase class. That
         * class provides configurable implementation of the ROBFragmentConsumer interface, which
         * is simple yet efficient.
         *
         * @param[in] fragment ROB fragment to be processed.
         */
        virtual void insertROBFragment(const std::shared_ptr<ROBFragment> & fragment) = 0;

        /**
         * Called to inform this consumer that the given input link has been disabled.
         *
         * @param[in] link_id The link that has been disabled.
         */
        virtual void linkDisabled(const InputLinkId & link_id) { }

        /**
         * Called to inform this consumer that the given input link has been re enabled.
         *
         * @param[in] link_id The link that has been enabled.
         */
        virtual void linkEnabled(const InputLinkId & link_id) { }

        /**
         * Called to inform this consumer object that the given ROB has been disabled.
         *
         * @param ROB_id The ID of the isabled ROB.
         */
        virtual void ROBDisabled(uint32_t ROB_id) { }

        /**
         * Called to inform this consumer object that the given ROB has been re enabled.
         *
         * @param ROB_id The ID of the re enabled ROB.
         */
        virtual void ROBEnabled(uint32_t ROB_id) { }
    };
}

#endif /* SWROD_ROBFRAGMENTCONSUMER_H_ */
