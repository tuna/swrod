/*
 * L1AInputHandler.h
 *
 *  Created on: Mar 3, 2021
 *      Author: kolos
 */

#ifndef SWROD_L1AINPUTHANDLER_H_
#define SWROD_L1AINPUTHANDLER_H_

#include <memory>
#include <vector>

#include <swrod/DataInput.h>
#include <swrod/DataInputHandler.h>
#include <swrod/ROBFragmentBuilder.h>

namespace swrod {
    class Core;

    /**
     * This class defines an interface and provides a default implementation
     * of a L1 Accept data handler.
     */
    class L1AInputHandler: public virtual DataInputHandler {
    public:

        /**
         * Creates a new L1 Accept handler using the given configuration.
         *
         * @param config A new object configuration.
         * @param core a reference to swrod::Core object
         */
        L1AInputHandler(const boost::property_tree::ptree & config, const Core & core);

        virtual ~L1AInputHandler() = default;

        /**
         * This function can be used to register a ROBFragmentBuilder instance to receive
         * notifications about L1 Accept data that this object receives.
         * The L1A data will be passed to the fragment builder via the
         * ROBFragmentBuilder::l1aReceived() function.
         *
         * @param builder fragment builder that has to be notified about L1 Accept received data
         */
        virtual void subscribe(const std::shared_ptr<ROBFragmentBuilder> & builder) {
            m_builders.push_back(builder);
        }

    protected:
        void notify(const L1AInfo & l1a_info) const {
            std::for_each(m_builders.begin(), m_builders.end(),
                    [&l1a_info](auto & it){ it->l1aReceived(l1a_info); } );
        }

    protected:
        std::vector<std::shared_ptr<ROBFragmentBuilder>> m_builders;
        std::shared_ptr<DataInput> m_data_input;
    };
}

#endif /* SWROD_L1AINPUTHANDLER_H_ */
