/*
 * rdma_sender.cpp
 *
 *  Created on: Jan 19, 2018
 *      Author: kolos
 */

#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include <csignal>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <string>
#include <thread>
#include <vector>

#include <boost/program_options.hpp>

#include <ers/ers.h>

#include <swrod/detail/Barrier.h>
#include <swrod/detail/L1AMessage.h>
#include <swrod/detail/NetioPacketHeader.h>
#include "core/RDMABase.h"

using namespace swrod::detail;

const uint16_t BCID_MASK = 0x0fff;

class RDMASender: public swrod::RDMABase {
public:
    RDMASender(
            swrod::test::Barrier & barrier, int32_t id, uint32_t ecr_interval,
            const std::string & host, uint32_t port,
            uint32_t buffer_size, uint32_t packet_size, uint32_t pileup = 1,
            const std::vector<uint64_t> & channels = std::vector<uint64_t>()) :
            m_barrier(barrier),
            m_packet_size(packet_size + 12),
            m_pileup(pileup),
            m_buffer_size(buffer_size),
            m_ecr_interval(ecr_interval),
            m_channels(channels),
            m_evt_thread(std::bind(&RDMASender::runEventLoop, this))
    {
        addrinfo *addr;

        std::string p = std::to_string(port);
        TEST_NZ(getaddrinfo(host.c_str(), p.c_str(), NULL, &addr));
        TEST_NZ(rdma_resolve_addr(m_comm_id, NULL, addr->ai_addr, 1000 /*milliseconds*/));
        freeaddrinfo(addr);

        m_packet = new uint8_t[m_packet_size];
        memset(m_packet, 0x00, m_packet_size);
    }

    RDMASender(const RDMASender& ) = delete;
    RDMASender& operator=(const RDMASender& ) = delete;

    virtual ~RDMASender() {
        stopEventLoop();
        m_evt_thread.join();
    }

    void on_pre_conn() override
    {
        const size_t N = 10;

        m_buffers_mrs.resize(N);
        for (size_t i = 0; i < N; ++i) {
            m_buffers_mrs[i] = ibv_reg_mr(get_pd(), new uint8_t[m_buffer_size], m_buffer_size,
                    IBV_ACCESS_LOCAL_WRITE);
        }

        m_message_mrs.resize(N);
        m_messages.resize(N);
        for (size_t i = 0; i < N; ++i) {
            m_message_mrs[i] = ibv_reg_mr(get_pd(), &m_messages[i], sizeof(message),
                    IBV_ACCESS_LOCAL_WRITE);
            post_receive(m_message_mrs[i]);
        }
    }

    void on_completion(ibv_wc &wc) override
    {
        if (wc.opcode & IBV_WC_RECV) {
            ibv_mr * mgr = (ibv_mr *) wc.wr_id;
            message m = *((message*) mgr->addr);
            if (m.id == MSG_ACK) {
                post_receive(mgr);
                m_total_acknowledged += m_chunks_to_acknowledge;
                m_chunks_sent -= m_chunks_to_acknowledge;
                send_next_chunk();
            } else if (m.id == MSG_READY) {
                post_receive(mgr);
                std::cout << "READY message received\n";
                for (size_t i = 0; i < m_buffers_mrs.size(); ++i) {
                    m_free_mgrs.push(m_buffers_mrs[i]);
                }
                send_next_chunk();
            } else if (m.id == MSG_DONE) {
                std::cout << "DONE message received, disconnecting\n";
                rdma_disconnect(m_comm_id);
                return;
            }
        } else if (wc.opcode == IBV_WC_SEND) {
            ibv_mr * mgr = (ibv_mr *) wc.wr_id;
            m_free_mgrs.push(mgr);
            send_next_chunk();
        }
    }

    void on_connect() override {
    }

    void on_disconnect() override {
    }

    void send_next_chunk() {
        while (m_chunks_sent < m_max_chunks_to_send) {
            if (m_free_mgrs.empty()) {
                break;
            }
            ibv_mr * mgr = m_free_mgrs.front();
            m_free_mgrs.pop();

            fillBuffer((uint8_t*) mgr->addr);

            send_remote(mgr);
            ++m_chunks_sent;
            ++m_total_sent;
        }
    }

    virtual void fillBuffer(uint8_t * buffer) {
        uint8_t * const END = buffer + m_buffer_size - m_packet_size - 4;

        uint8_t * msg = buffer;

        while (true) {
            while (m_current_channel < m_channels.size()) {
                while (m_pileup_cnt < m_pileup) {
                    memcpy(msg, m_packet, m_packet_size);
                    *(uint32_t*)msg = m_packet_size;
                    *(uint64_t*)(msg + 4) = m_channels[m_current_channel];
                    *(uint32_t*)(msg + 12) = (m_l1id + m_pileup_cnt);
                    *(uint16_t*)(msg + 18) = (m_l1id + m_pileup_cnt) & BCID_MASK;
                    *(uint32_t*)(msg + 20) = 0xffff; // L1ID bit mask
                    ++m_total_count;
                    msg += m_packet_size;
                    ++m_pileup_cnt;
                    if (msg > END) {
                        *(uint32_t*)msg = 0;
                        m_total_size += m_buffer_size;
                        return;
                    }
                }
                m_pileup_cnt = 0;
                ++m_current_channel;
            }
            m_current_channel = 0;

            m_count += m_pileup;
            if ((m_count % m_sync_interval) == 0) {
                m_barrier.wait();
            }

            if (m_count % m_ecr_interval == 0) {
                m_l1id = 0;
            } else {
                m_l1id += m_pileup;
            }
        }
    }

    uint64_t getTotalCount() const {
        return m_total_count;
    }

    uint64_t getTotalSize() const {
        return m_total_size;
    }

protected:
    swrod::test::Barrier & m_barrier;
    const uint32_t m_sync_interval = 1000;
    const uint32_t m_packet_size;
    const uint32_t m_pileup;
    const uint32_t m_buffer_size;
    const uint64_t m_ecr_interval;
    uint32_t m_current_channel = 0;
    uint32_t m_pileup_cnt = 0;
    uint32_t m_l1id = 0;
    uint32_t m_chunks_sent = 0;
    uint32_t m_total_sent = 0;
    uint32_t m_total_acknowledged = 0;
    uint64_t m_total_count = 0;
    uint64_t m_total_size = 0;
    uint64_t m_count = 0;

    std::vector<uint64_t> m_channels;
    std::vector<ibv_mr*> m_buffers_mrs;
    std::vector<ibv_mr*> m_message_mrs;
    std::vector<message> m_messages;
    uint8_t * m_packet;
    std::queue<ibv_mr*> m_free_mgrs;
    std::thread m_evt_thread;
};

class L1ASender : public RDMASender {
public:
    L1ASender(swrod::test::Barrier & barrier, uint32_t id, uint32_t ecr_interval,
            const std::string & host, uint32_t port, uint32_t buffer_size, uint32_t channel) :
        RDMASender(barrier, id, ecr_interval, host, port, buffer_size, sizeof(L1APacket))
    {
        m_l1a_packet.header.size = sizeof(m_l1a_packet);
        m_l1a_packet.header.fid = channel;
    }

    void fillBuffer(uint8_t * buffer) override {
        uint8_t * const END = buffer + m_buffer_size - sizeof(m_l1a_packet) - 4;

        uint8_t * msg = buffer;

        while (true) {
            if (!m_synchronised) {
                m_barrier.wait();
                m_synchronised = true;
            }

            memcpy(msg, (uint8_t*) &m_l1a_packet, sizeof(m_l1a_packet));
            msg += sizeof(m_l1a_packet);

            ++m_count;

            if (m_count % m_ecr_interval == 0) {
                m_l1a_packet.message.l1id = 0;
                m_l1a_packet.message.bcid = 0;
                m_l1a_packet.message.ecr++;
            } else {
                m_l1a_packet.message.l1id++;
                m_l1a_packet.message.bcid = m_l1a_packet.message.l1id & BCID_MASK;
            }

            if ((m_count % m_sync_interval) == 0) {
                *((uint32_t*) msg) = 0;
                m_total_size += (msg - buffer);
                m_synchronised = false;
                return;
            }

            if (msg > END) {
                *((uint32_t*) msg) = 0;
                m_total_size += (msg - buffer);
                return;
            }
        }
    }

private:
    struct L1APacket {
        struct {
            uint32_t size;
            uint64_t fid;
        } __attribute__((packed)) header;
        L1AMessage message;
    } __attribute__((packed));

    L1APacket m_l1a_packet;
    bool m_synchronised = true;
};

int signalReceived = 0;
void signal_handler(int signal) {
    signalReceived = signal;
}

using namespace boost::program_options;

int main(int ac, char *av[]) {
    options_description description("Options");

    description.add_options()("help,h", "produce help message")
        ("port,P", value<uint32_t>()->default_value(12345), "port number")
        ("single-port,S", "use the same port number for all sending threads")
        ("l1a-elink,a", value<uint64_t>()->default_value(7777), "L1A elink id")
        ("first-elink,f", value<uint64_t>()->default_value(1), "first elink id")
        ("comm-buffer-size,u", value<uint32_t>()->default_value(100000), "communication buffer size in bytes")
        ("elinks,e", value<uint16_t>()->default_value(192), "e-links number per worker thread")
        ("packet-size,s", value<uint16_t>()->default_value(40), "fragment size")
        ("pileup,k", value<uint32_t>()->default_value(1), "pileup")
        ("ecr-interval,l", value<uint32_t>()->default_value(500000), "ECR interval")
        ("host,H", value<std::string>()->default_value("127.0.0.1"), "send data to this IP")
        ("workers,w", value<uint16_t>()->default_value(1), "worker threads number");

    variables_map arguments;
    try {
        store(parse_command_line(ac, av, description), arguments);
        notify(arguments);
    } catch (error & ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "Test application for the 'swrod' package" << std::endl;
        description.print(std::cout);
        return 0;
    }

    uint16_t elinks_per_worker = arguments["elinks"].as<uint16_t>();
    uint16_t workers_number = arguments["workers"].as<uint16_t>();
    uint16_t packet_size = arguments["packet-size"].as<uint16_t>();
    uint32_t pileup = arguments["pileup"].as<uint32_t>();
    uint32_t ecr_interval = arguments["ecr-interval"].as<uint32_t>();
    uint32_t buffer_size = arguments["comm-buffer-size"].as<uint32_t>();
    uint64_t l1a_elink_id = arguments["l1a-elink"].as<uint64_t>();
    uint64_t first_elink_id = arguments["first-elink"].as<uint64_t>();
    uint32_t port_number = arguments["port"].as<uint32_t>();
    std::string host_name = arguments["host"].as<std::string>();
    bool single_port = arguments.count("single-port");

    std::signal(SIGINT, signal_handler);

    swrod::test::Barrier barrier(workers_number + (l1a_elink_id ? 1 : 0));
    std::vector<std::shared_ptr<RDMASender>> senders;

    for (uint16_t i = 0; i < workers_number; ++i) {

        std::vector<uint64_t> elinks;
        for (size_t e = 0; e < elinks_per_worker; ++e) {
            uint64_t fid = (i<<24) + ((e/8)<<16) + ((e%8)<<8) + first_elink_id;
            elinks.push_back(fid);
        }

        std::shared_ptr<RDMASender> s(new RDMASender(barrier, i, ecr_interval,
                host_name, port_number, buffer_size, packet_size, pileup, elinks));
        senders.push_back(s);
        if (!single_port) {
            ++port_number;
        }
    }

    std::shared_ptr<L1ASender> l1a_sender;
    if (l1a_elink_id) {
        l1a_sender.reset(new L1ASender(barrier, -1, ecr_interval, host_name,
                      port_number, buffer_size, l1a_elink_id));
    }

    uint64_t old_size = 0;
    uint64_t old_count = 0;

    while (!signalReceived) {
        usleep(1000000);

        struct timeval tv;
        gettimeofday(&tv, NULL);
        time_t curtime = tv.tv_sec;
        char buffer[16];
        strftime(buffer, 16, "%T.", localtime(&curtime));

        uint64_t size = std::accumulate(senders.begin(), senders.end(), (uint64_t) 0,
                [](uint64_t r, auto & s) {return r + s->getTotalSize();});
        uint64_t count = std::accumulate(senders.begin(), senders.end(), (uint64_t) 0,
                [](uint64_t r, auto & s) {return r + s->getTotalCount();});

        std::cout << buffer << std::setw(6) << std::setfill('0') << tv.tv_usec << " : " << size
                << " bytes sent, message rate = " << (count - old_count) / 1000000.
                << " MHz, throughput = " << (size - old_size) / 1e6 << " MB/s" << std::endl;
        old_count = count;
        old_size = size;
    }

    barrier.deactivate();
    senders.clear();

    return 0;
}

