/*
 * FastL1AGenerator.h
 *
 *  Created on: Apr 7, 2021
 *      Author: kolos
 */

#ifndef TEST_CORE_FASTL1AGENERATOR_H_
#define TEST_CORE_FASTL1AGENERATOR_H_

#include <condition_variable>
#include <memory>
#include <mutex>

#include <boost/property_tree/ptree.hpp>

#include <swrod/DataInput.h>
#include <swrod/detail/WorkerThread.h>
#include <swrod/detail/Barrier.h>

namespace swrod {
    class Core;

    namespace test {
        class FastL1AGenerator: public DataInput {
        public:
            static std::shared_ptr<FastL1AGenerator> instance();

            FastL1AGenerator(const boost::property_tree::ptree & config, const Core & core);

            ~FastL1AGenerator();

            void actionsPending() override { }

            void subscribeToFelix(const InputLinkId & link) override {
                m_link = link;
            }

            void unsubscribeFromFelix(const InputLinkId & link) override { }

            uint32_t getECRInterval() const {
                return m_ECR_interval;
            }

            uint32_t getSyncInterval() const {
                return m_sync_interval;
            }

            void synchronize() {
                m_barrier.wait();
            }

            void runStarted(const RunParams & rp) override {
                DataInput::runStarted(rp);
                m_barrier.reactivate();
                m_thread.start();
            }

            void runStopped() override {
                DataInput::runStopped();
                m_barrier.deactivate();
                m_thread.stop();
            }

        private:
            void run(const bool & active);

        private:
            const uint32_t m_ECR_interval;
            const uint32_t m_sync_interval;
            detail::WorkerThread m_thread;
            test::Barrier m_barrier;
            InputLinkId m_link;
        };
    }
}

#endif /* TEST_CORE_FASTL1AGENERATOR_H_ */
