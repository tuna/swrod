/*
 *  RDMABase.cpp
 */

#include <sys/poll.h>

#include "RDMABase.h"

#include <ers/ers.h>

using namespace swrod;

const int timeout_ms = 500;

void rc_die(const char *reason) {
    perror(reason);
    exit(EXIT_FAILURE);
}

void RDMABase::poll_cq() {
    ibv_cq *cq;
    ibv_wc wc;
    void * tmp;

    m_poller_stopped = false;

    while (!m_poller_stopped) {
        if (poll_fd(m_comp_channel->fd) <= 0) {
            continue;
        }
        TEST_NZ(ibv_get_cq_event(m_comp_channel, &cq, &tmp));
        TEST_NZ(ibv_req_notify_cq(cq, 0));

        while (ibv_poll_cq(cq, 1, &wc) == 1) {
            if (wc.status == IBV_WC_SUCCESS)
                on_completion(wc);
            else {
                ERS_LOG("poll_cq: status is " <<wc.status);
                exit(1);
            }
        }
        ibv_ack_cq_events(cq, 1);
    }
}

void RDMABase::post_receive(ibv_mr * mr) {
    struct ibv_recv_wr wr, *bad_wr = NULL;
    struct ibv_sge sge;

    memset(&wr, 0, sizeof(wr));

    wr.wr_id = (uintptr_t) mr;
    wr.sg_list = &sge;
    wr.num_sge = 1;

    sge.addr = (uintptr_t) mr->addr;
    sge.length = mr->length;
    sge.lkey = mr->lkey;

    TEST_NZ(ibv_post_recv(m_qp, &wr, &bad_wr));
}

void RDMABase::send_remote(ibv_mr * mr) {
    struct ibv_send_wr wr, *bad_wr = NULL;
    struct ibv_sge sge;

    memset(&wr, 0, sizeof(wr));

    wr.wr_id = (uintptr_t) mr;
    wr.opcode = IBV_WR_SEND;
    wr.send_flags = IBV_SEND_SIGNALED;

    wr.sg_list = &sge;
    wr.num_sge = 1;

    sge.addr = (uintptr_t) mr->addr;
    sge.length = mr->length;
    sge.lkey = mr->lkey;

    TEST_NZ(ibv_post_send(m_qp, &wr, &bad_wr));
}

void RDMABase::build_connection(rdma_cm_id * id) {
    m_context = id->verbs;

    TEST_Z(m_pd = ibv_alloc_pd(m_context));
    TEST_Z(m_comp_channel = ibv_create_comp_channel(m_context));
    TEST_Z(m_cq = ibv_create_cq(m_context, m_queue_length, NULL, m_comp_channel, 0));
    TEST_NZ(ibv_req_notify_cq(m_cq, 0));

    ibv_qp_init_attr qp_attr;
    build_qp_attr(&qp_attr);

    TEST_NZ(rdma_create_qp(id, m_pd, &qp_attr));
    m_qp = id->qp;
}

void RDMABase::build_params(rdma_conn_param *params) {
    memset(params, 0, sizeof(*params));

    params->initiator_depth = params->responder_resources = 1;
    params->retry_count = 255;
    params->rnr_retry_count = 255;
}

void RDMABase::build_qp_attr(ibv_qp_init_attr *qp_attr) {
    memset(qp_attr, 0, sizeof(*qp_attr));

    qp_attr->send_cq = m_cq;
    qp_attr->recv_cq = m_cq;
    qp_attr->qp_type = IBV_QPT_RC;

    qp_attr->cap.max_send_wr = m_queue_length;
    qp_attr->cap.max_recv_wr = m_queue_length;
    qp_attr->cap.max_send_sge = 1;
    qp_attr->cap.max_recv_sge = 1;
}

void RDMABase::runEventLoop() {
    rdma_conn_param cm_params;
    build_params(&cm_params);
    m_stopped = false;

    while (!m_stopped) {

        if (poll_fd(m_ec->fd) <= 0) {
            continue;
        }

        rdma_cm_event *event = 0;
        if (rdma_get_cm_event(m_ec, &event)) {
            break;
        }
        rdma_cm_event_type eid = event->event;
        rdma_cm_id * cm_id = event->id;
        if (eid == RDMA_CM_EVENT_ADDR_RESOLVED) {
            build_connection(cm_id);
            on_pre_conn();
            TEST_NZ(rdma_resolve_route(cm_id, timeout_ms));
            m_poller_thread = std::thread(std::bind(&RDMABase::poll_cq, this));
        } else if (eid == RDMA_CM_EVENT_ROUTE_RESOLVED) {
            TEST_NZ(rdma_connect(cm_id, &cm_params));
        } else if (eid == RDMA_CM_EVENT_CONNECT_REQUEST) {
            build_connection(cm_id);
            on_pre_conn();
            TEST_NZ(rdma_accept(cm_id, &cm_params));
            m_poller_thread = std::thread(std::bind(&RDMABase::poll_cq, this));
        } else if (eid == RDMA_CM_EVENT_ESTABLISHED) {
            on_connect();
        } else if (eid == RDMA_CM_EVENT_DISCONNECTED) {
            stop_poller();
            rdma_destroy_qp(cm_id);
            ibv_destroy_cq(m_cq);
            ibv_destroy_comp_channel(m_comp_channel);
            ibv_dealloc_pd(m_pd);
            on_disconnect();
        } else {
            rc_die("unknown event\n");
        }
        rdma_ack_cm_event(event);
    }
}

int RDMABase::poll_fd(int fd) {
    struct pollfd pfd[2] = {{ m_event_fd, POLLIN, 0 }, {fd, POLLIN | POLLOUT, 0}};
    int ret = poll(pfd, 2, timeout_ms);
    if (ret == -1 || ret == 0) {
        return ret;
    }
    if ((pfd[0].revents & POLLIN)) {
        unsigned long v;
        eventfd_read(m_event_fd, &v);
        signal_received();
    }

    if ((pfd[1].revents & POLLIN) || (pfd[1].revents & POLLOUT)) {
        return 1;
    }
    return 0;
}

