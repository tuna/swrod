/*
 * ROBFragmentCounter.cpp
 *
 *  Created on: Jun 19, 2019
 *      Author: kolos
 */

#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>

#include <chrono>
#include <mutex>

#include "ROBFragmentCounter.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    std::mutex g_screen_mutex;

    Factory<ROBFragmentConsumer>::Registrator __reg__(
            "ROBFragmentCounter",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<ROBFragmentCounter>(config, core);
            });
}

ROBFragmentCounter::ROBFragmentCounter(
        const boost::property_tree::ptree & config, const Core & core) :
                m_period(config.get<uint32_t>("Period", 1000)),
                m_elapsed(0),
                m_offset(config.get<uint32_t>("Offset", 1)),
                m_ROB_id(config.get<uint32_t>("RobConfig.Id", 0)),
                m_show_average(config.get<uint32_t>("Mode", 1) == 2),
                m_running(false)
{ }

ROBFragmentCounter::~ROBFragmentCounter() {
    runStopped();
}

void ROBFragmentCounter::runStarted(const RunParams & ) {
    if (!m_running) {
        m_running = true;
        m_print_thread = std::thread(std::bind(&ROBFragmentCounter::run, this));
    }
}

void ROBFragmentCounter::runStopped() {
    if (m_running) {
        m_running = false;
        m_timer.notify_one();
        m_print_thread.join();
    }
}

void ROBFragmentCounter::print() {
    static const uint32_t width = 5;

    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    while (m_history.size() > (w.ws_col - 30)/width) {
        m_history.pop_front();
    }

    std::unique_lock<std::mutex> g_lock(g_screen_mutex);
    // move cursor to the appropriate line and clear it
    std::cout << "\033[" << (w.ws_row - m_offset) << ";1H" << "\033[K";
    std::cout << "ROB " << std::setw(2) << m_ROB_id << " rates (kHz): ";
    for (std::deque<uint32_t>::iterator it = m_history.begin(); it != m_history.end(); ++it) {
        std::cout << std::setw(width) << std::right << *it;
    }
    // move cursor to the low-right corner
    std::cout << "\033[" << w.ws_row << ";" << w.ws_col << "H" << std::flush;
}

void ROBFragmentCounter::run() {
    std::mutex mutex;

    uint32_t cnt = 0;
    while (m_show_average && not m_counter) {
        std::unique_lock<std::mutex> lock(mutex);
        m_timer.wait_for(lock, std::chrono::milliseconds(1));
        if (++cnt == m_period) {
            m_history.push_back(0);
            print();
            cnt = 0;
        }
    }

    while (m_running) {
        std::unique_lock<std::mutex> lock(mutex);
        if (std::cv_status::no_timeout == m_timer.wait_for(lock,
                std::chrono::milliseconds(m_period))) {
            continue;
        }

        uint32_t rate = (m_counter - m_last_counter)/m_period;
        m_last_counter = m_counter;
        if (rate) {
            m_elapsed += m_period;
        }
        uint32_t average_rate = m_elapsed ? m_counter/m_elapsed : 0;
        m_history.push_back(m_show_average ? average_rate : rate);
        print();
    }
}
