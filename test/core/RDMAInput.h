/*
 * RDMAInput.hpp
 *
 *  Created on: Jul 30, 2018
 *      Author: kolos
 */

#ifndef SWROD_RDMAINPUT_HPP_
#define SWROD_RDMAINPUT_HPP_

#include <set>

#include <boost/property_tree/ptree.hpp>

#include <swrod/DataInput.h>
#include <swrod/detail/WorkerThread.h>

#include "RDMABase.h"

namespace swrod {
    class Core;

    namespace test {

        class RDMAInput: public DataInput, public RDMABase {
        public:
            RDMAInput(const boost::property_tree::ptree & config, const Core& core);

            ~RDMAInput();

            void actionsPending() override {
                signal();
            }

            void subscribeToFelix(const InputLinkId & link) override {
                m_links.insert(link);
            }

            void unsubscribeFromFelix(const InputLinkId & link) override {
                m_links.erase(link);
            }

            void runStarted(const RunParams & ) override;

            uint32_t translateStatus(uint8_t status) const noexcept override;

        protected:
            void run(const bool & active);

            void bufferReceived(uint8_t * buffer, uint32_t size);

            void signal_received() override {
                executeActions();
            }

            void on_pre_conn() override;

            void on_connect() override;

            void on_completion(ibv_wc &wc) override;

            void on_disconnect() override;

        private:
            const uint32_t m_buffer_size;
            const uint32_t m_port;
            std::set<InputLinkId> m_links;

            uint16_t m_channels_number = 0;
            uint64_t m_count = 0, m_rounds = 0, m_sync_rate = 0;
            uint64_t m_chunks_received = 0;
            uint64_t m_req_posted = 0;
            uint64_t m_ack_posted = 0;
            uint64_t m_req_complete = 0;
            message m_message;
            std::vector<ibv_mr*> m_buffers_mrs;
            ibv_mr * m_message_mr;
            detail::WorkerThread m_thread;
        };
    }
}

#endif /* SWROD_RDMAINPUT_HPP_ */
