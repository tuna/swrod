/*
 * FastDataGenerator.h
 *
 *  Created on: Apr 7, 2021
 *      Author: kolos
 */

#ifndef TEST_CORE_FASTDATAGENERATOR_H_
#define TEST_CORE_FASTDATAGENERATOR_H_

#include <boost/property_tree/ptree.hpp>

#include <swrod/DataInput.h>
#include <swrod/detail/WorkerThread.h>

namespace swrod {
    namespace test {
        class FastDataGenerator : public DataInput {
        public:
            FastDataGenerator(
                    const boost::property_tree::ptree & config);

            ~FastDataGenerator();

            void actionsPending() override {
                executeActions();
            }

            void runStarted(const RunParams & rp) override {
                DataInput::runStarted(rp);
                m_thread.start();
            }

            void runStopped() override {
                DataInput::runStopped();
                m_thread.stop();
            }

            void subscribeToFelix(const InputLinkId & link) override;

            void unsubscribeFromFelix(const InputLinkId & link) override;

        private:
            void run(const bool & active);

        private:
            const uint16_t m_packet_size;
            const uint32_t m_pileup;
            const uint32_t m_l1id_bit_mask;

            std::vector<InputLinkId> m_links;
            detail::WorkerThread m_thread;
        };
    }
}

#endif /* TEST_CORE_FASTDATAGENERATOR_H_ */
