/*
 * FastL1AGenerator.cpp
 *
 *  Created on: Apr 7, 2021
 *      Author: kolos
 */

#include <ers/ers.h>

#include "FastL1AGenerator.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/detail/L1AMessage.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace swrod::test;
using namespace boost::property_tree;

namespace {
    std::mutex mutex;
    std::shared_ptr<FastL1AGenerator> l1aGen;

    Factory<DataInput>::Registrator __reg__(
            "FastL1A",
            [](const boost::property_tree::ptree& config, const Core& core) {
                std::unique_lock<std::mutex> lock(mutex);
                if (!l1aGen) {
                    l1aGen.reset(new FastL1AGenerator(config, core));
                }
                return l1aGen;
            });

    uint32_t countInputs(const ptree& config) {
        uint32_t inputs = 0;
        for (const ptree::value_type & m : PTREE_GET_CHILD(config, "Modules")) {
            inputs += PTREE_GET_VALUE(m.second, uint32_t, "WorkersNumber");
        }
        return inputs;
    }
}

std::shared_ptr<FastL1AGenerator> FastL1AGenerator::instance() {
    ERS_ASSERT_MSG(l1aGen, "FastL1AGenerator singleton does not exist");
    return l1aGen;
}

FastL1AGenerator::FastL1AGenerator(const ptree& config, const Core & core) :
    m_ECR_interval(config.get<uint32_t>("EcrInterval")),
    m_sync_interval(config.get<uint32_t>("SyncInterval")),
    m_thread(std::bind(&FastL1AGenerator::run, this, std::placeholders::_1),
            "FastL1A", config.get<std::string>("CPU", "")),
    m_barrier(countInputs(core.getConfiguration()) + 1),
    m_link(0)
{
    ;
}

FastL1AGenerator::~FastL1AGenerator() {
    runStopped();
}

void FastL1AGenerator::run(const bool& active) {
    detail::L1AMessage message;

    synchronize();

    uint64_t counter = 0;
    while (true) {
        message.bcid = message.l1id;
        dataReceived(m_link, (uint8_t*)&message, sizeof(message), 0);
        message.l1id++;

        ++counter;
        if ((counter % m_sync_interval) == 0) {
            synchronize();
            if (!active) {
                break;
            }
        }
        if ((counter % m_ECR_interval) == 0) {
            message.ecr++;
            message.l1id = 0;
            ERS_DEBUG(2, "Generating ECR with L1ID = 0x" << std::hex
                    << message.ext_l1id);
        }
    }
}
