/*
 * ROBFragmentShredder.cpp
 *
 *  Created on: Jul 16, 2019
 *      Author: kolos
 */

#include <boost/format.hpp>

#include "ROBFragmentShredder.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    Factory<ROBFragmentConsumer>::Registrator __reg__(
            "ROBFragmentShredder",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<ROBFragmentShredder>(config, core);
            });
            
    std::string toHexString(uint32_t v) {
        return (boost::format("ROB-%x-shredder") % v).str();
    }
}

ROBFragmentShredder::ROBFragmentShredder(
        const boost::property_tree::ptree& config, const Core& core) :
        ROBFragmentConsumerBase(config,
                [](auto &){},
                ROBFragmentConsumerBase::QueuingPolicy::Wait,
                ROBFragmentConsumerBase::ForwardingPolicy::AfterProcessing,
                config.count("RobConfig")
                            ? toHexString(config.get<uint32_t>("RobConfig.Id"))
                            : std::string("ROB-shredder"))
{
}
