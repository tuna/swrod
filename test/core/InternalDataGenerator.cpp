/*
 * InternalDataGenerator.cpp
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#include <tuple>
#include <vector>

#include <ers/ers.h>

#include "InternalDataGenerator.h"

#include <swrod/Factory.h>

using namespace swrod;
using namespace swrod::test;
using namespace boost::property_tree;

namespace {
    Factory<DataInput>::Registrator __reg__(
            "InternalData",
            [](const ptree& config, const Core& ) {
                return std::make_shared<InternalDataGenerator>(config);
            });
}

InternalDataGenerator::InternalDataGenerator(const boost::property_tree::ptree& config) :
    m_packet_size(config.get<uint16_t>("PacketSize")),
    m_pileup(config.get<uint16_t>("Pileup", 1)),
    m_l1id_bit_mask(config.get<uint32_t>("L1idBitMask", -1)),
    m_total_links_per_ROB(config.get<uint32_t>("TotalFullModeLinks", 1)),
    m_ROB_number(config.get<uint32_t>("ROBsPerModule", 1)),
    m_workers_number(config.get<uint32_t>("WorkersNumber", 1)),
    m_worker_index(config.get<uint32_t>("WorkerIndex", 0)),
    m_full_mode(config.get<bool>("IsFullMode", false)),
    m_packets_counter(0),
    m_triggers_counter(0),
    m_running(false),
    m_actions_pending(0),
    m_thread(m_full_mode
            ? std::bind(&InternalDataGenerator::runFullMode, this, std::placeholders::_1)
            : std::bind(&InternalDataGenerator::runGBTMode, this, std::placeholders::_1),
             "InternalData", config.get<std::string>("CPU", ""))
{
    m_packet = new uint8_t[m_packet_size];
    memset(m_packet, 0, m_packet_size);
    *((uint16_t*)m_packet) = 1; // good checksum value
    *(((uint32_t*)m_packet) + 2) = m_l1id_bit_mask;

    m_l1a_gen = InternalL1AGenerator::instance();
    m_ttc_id = m_l1a_gen->acquireTTCId();

    m_thread.start();
}

InternalDataGenerator::~InternalDataGenerator() {
    m_thread.stop();
    delete[] m_packet;
}

void InternalDataGenerator::subscribeToFelix(const InputLinkId& link) {
    m_links.push_back(link);
}

void InternalDataGenerator::unsubscribeFromFelix(const InputLinkId& link) {
    m_links.erase(std::remove(m_links.begin(), m_links.end(), link), m_links.end());
}

void InternalDataGenerator::runStarted(const RunParams & rp) {
    std::unique_lock lock(m_mutex);
    DataInput::runStarted(rp);
    m_packets_counter = 0;
    m_triggers_counter = 0;

    m_links_per_ROB = m_links.size() / m_ROB_number;

    uint32_t links_per_worker = m_total_links_per_ROB / m_workers_number;
    uint32_t remain = m_total_links_per_ROB % m_workers_number;
    if (remain >= m_worker_index) {
        m_trigger_range.first = m_worker_index * (links_per_worker + 1);
        links_per_worker += !!(remain - m_worker_index);
    }
    else {
        m_trigger_range.first = m_worker_index * links_per_worker + remain;
    }
    m_trigger_range.second = m_trigger_range.first + links_per_worker - 1;

    ERS_LOG("Generate packets for [" << m_trigger_range.first << "; "
            << m_trigger_range.second << "] range");

    beforeStart();
    m_running = true;

    // This is necessary if SW ROD is configured to work in data-driven mode
    // in which case it will not call runStarted for the Internal L1A generator
    // but this is required to have data generators working
    m_l1a_gen->startGeneration(rp);
}

void InternalDataGenerator::runStopped() {
    std::unique_lock lock(m_mutex);

    m_running = false;
    m_condition.wait(lock, [this]{ return !m_triggers_counter; });
    DataInput::runStopped();
    afterStop();

    // This is necessary if SW ROD is configured to work in data-driven mode
    m_l1a_gen->stopGeneration();
}

void InternalDataGenerator::runFullMode(const bool& active) {
    while (active) {
        if (m_actions_pending) {
            executeActions();
            --m_actions_pending;
        }

        if (!m_running) {
            {
                std::unique_lock lock(m_mutex);
                if (m_triggers_counter) {
                    ERS_DEBUG(1, "Triggers sent = " << m_triggers_counter
                            << " packets sent = " << m_packets_counter);
                    m_triggers_counter = 0;
                    m_packets_counter = 0;
                    m_condition.notify_one();
                }
            }
            usleep(10);
            continue;
        }

        uint32_t l1id;
        uint16_t bcid;
        if (!m_l1a_gen->getTTCInformation(m_ttc_id, l1id, bcid)) {
            usleep(10);
            continue;
        }

        uint64_t trigger = (m_triggers_counter++ % m_total_links_per_ROB);
        if (m_links.empty()) {
            continue;
        }

        if (trigger >= m_trigger_range.first && trigger <= m_trigger_range.second) {
            uint32_t offset = trigger - m_trigger_range.first;
            for (uint32_t i = 0; i < m_ROB_number; ++i) {
                uint32_t link = i*m_links_per_ROB + offset;
                generatePacket(m_links[link % m_links.size()],
                        l1id & m_l1id_bit_mask, bcid);
                ++m_packets_counter;
            }
        }
    }
}

void InternalDataGenerator::runGBTMode(const bool& active) {
    std::vector<std::tuple<uint32_t, uint16_t>> ttc(m_pileup, std::make_tuple(0xffffffff, 0xffff));
    size_t pos = 0;

    while (active) {
        if (m_actions_pending) {
            executeActions();
            --m_actions_pending;
        }

        if (!m_running) {
            {
                std::unique_lock lock(m_mutex);
                if (m_triggers_counter) {
                    ERS_DEBUG(1, "Triggers sent = " << m_triggers_counter
                            << " packets sent = " << m_packets_counter);
                    m_triggers_counter = 0;
                    m_packets_counter = 0;
                    m_condition.notify_one();
                }
            }
            usleep(10);
            continue;
        }

        while (pos < ttc.size()) {
            if (!m_l1a_gen->getTTCInformation(
                    m_ttc_id, std::get<0>(ttc[pos]), std::get<1>(ttc[pos]))) {
                usleep(10);
                break;
            }
            ++pos;
        }

        if (pos != ttc.size()) {
            continue;
        } else {
            pos = 0;
        }

        for (auto link : m_links) {
            for (auto & t : ttc) {
                auto [l1id, bcid] = t;
                generatePacket(link, l1id & m_l1id_bit_mask, bcid);
            }
            ++m_packets_counter;
        }
        ++m_triggers_counter;
    }
}

void InternalDataGenerator::generatePacket(InputLinkId link, uint32_t l1id, uint16_t bcid) {
    *(uint32_t*)m_packet = l1id;
    *(uint16_t*)(m_packet + 6) = bcid;
    dataReceived(link, m_packet, m_packet_size, 0);
}
