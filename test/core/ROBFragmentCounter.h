/*
 * ROBFragmentCounter.h
 *
 *  Created on: May 8, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTCOUNTER_H_
#define SWROD_ROBFRAGMENTCOUNTER_H_

#include <atomic>
#include <condition_variable>
#include <deque>
#include <mutex>
#include <thread>

#include <boost/property_tree/ptree.hpp>

#include <swrod/ROBFragmentConsumer.h>

namespace swrod {
    class Core;

    class ROBFragmentCounter : public ROBFragmentConsumer {
    public:

        ROBFragmentCounter(const boost::property_tree::ptree & config, const Core & core);

        ~ROBFragmentCounter();

        const std::string & getName() const override {
            static std::string s("FragmentCounter");
            return s;
        }

        void insertROBFragment(const std::shared_ptr<ROBFragment> & fragment) override {
            ++m_counter;
            forwardROBFragment(fragment);
        }

        uint64_t getFragmentsNumber() const {
            return m_counter;
        }

        void runStarted(const RunParams & run_params) override;

        void runStopped() override;

    private:
        void print();

        void run();

    private:
        std::atomic<uint64_t> m_counter = 0;
        uint64_t m_last_counter = 0;
        std::deque<uint32_t> m_history;
        uint32_t m_period;
        uint32_t m_elapsed;
        uint32_t m_offset;
        uint32_t m_ROB_id;
        bool m_show_average;
        bool m_running;
        std::condition_variable m_timer;
        std::thread m_print_thread;
    };
}

#endif /* SWROD_ROBFRAGMENTCOUNTER_H_ */
