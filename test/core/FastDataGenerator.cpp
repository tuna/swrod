/*
 * FastDataGenerator.cpp
 *
 *  Created on: Apr 7, 2021
 *      Author: kolos
 */

#include <ers/ers.h>

#include "FastDataGenerator.h"
#include "FastL1AGenerator.h"

#include <swrod/Factory.h>

using namespace swrod;
using namespace swrod::test;

namespace {
    Factory<DataInput>::Registrator __reg__(
            "FastData",
            [](const boost::property_tree::ptree& config, const Core& ) {
                return std::make_shared<FastDataGenerator>(config);
            });
}

FastDataGenerator::FastDataGenerator(const boost::property_tree::ptree& config) :
    m_packet_size(config.get<uint16_t>("PacketSize")),
    m_pileup(config.get<uint32_t>("Pileup", 1)),
    m_l1id_bit_mask(config.get<uint32_t>("L1idBitMask", -1)),
    m_thread(std::bind(&FastDataGenerator::run, this, std::placeholders::_1),
             "FastData", config.get<std::string>("CPU", ""))
{
    ERS_ASSERT_MSG(not config.get<bool>("IsFullMode", false),
            "'FastData' generator can't be used in FULL mode. Please use 'InternalData' generator instead.");
}

FastDataGenerator::~FastDataGenerator() {
    runStopped();
}

void FastDataGenerator::subscribeToFelix(const InputLinkId& link) {
    m_links.push_back(link);
}

void FastDataGenerator::unsubscribeFromFelix(const InputLinkId& link) {
    m_links.erase(std::remove(m_links.begin(), m_links.end(), link), m_links.end());
}

void FastDataGenerator::run(const bool& active) {
    static const uint16_t BCID_MASK = 0x0fff;

    uint8_t * data = new uint8_t[m_packet_size];
    memset(data, 0, m_packet_size);
    *((uint16_t*)data) = 1; // good checksum value
    *(((uint32_t*)data) + 2) = m_l1id_bit_mask;

    uint64_t counter = 0;
    uint32_t l1id = 0;

    std::shared_ptr<FastL1AGenerator> l1aGen = FastL1AGenerator::instance();
    const uint32_t sync_rate = l1aGen->getSyncInterval();
    const uint32_t ecr_interval = l1aGen->getECRInterval();

    l1aGen->synchronize();

    while (active) {
        uint32_t diff = (sync_rate - (counter % sync_rate));
        uint32_t pileup = std::min(diff, m_pileup);
        diff = (ecr_interval - (counter % ecr_interval));
        pileup = std::min(diff, pileup);

        for (auto link : m_links) {
            for (uint16_t p = 0; p < pileup; ++p) {
                *(uint32_t*)data = (l1id + p) & m_l1id_bit_mask;
                *(uint16_t*)(data + 6) = (l1id + p) & BCID_MASK;
                dataReceived(link, data, m_packet_size, 0);
            }
        }

        l1id += pileup;
        counter += pileup;
        if (counter % sync_rate == 0) {
            // synchronize with l1a generator
            l1aGen->synchronize();
        }
        if (counter % ecr_interval == 0) {
            l1id = ((l1id >> 24) + 1) << 24;
        }
    }

    delete[] data;
}
