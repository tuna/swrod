/*
 * RDMAInput.cpp
 *
 *  Created on: Dec 19, 2017
 *      Author: kolos
 */

#include "RDMAInput.h"

#include <assert.h>
#include <arpa/inet.h>
#include <errno.h>
#include <limits.h>
#include <malloc.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <rdma/rdma_verbs.h>

#include <iostream>

#include <ers/ers.h>

#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/exceptions.h>


using namespace swrod;
using namespace swrod::test;
using namespace boost::property_tree;

namespace {
    Factory<DataInput>::Registrator __reg__(
            "RDMAInput",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<RDMAInput>(config, core);
            });
}

RDMAInput::RDMAInput(const boost::property_tree::ptree & config, const Core& core)
try :   m_buffer_size(config.get<uint32_t>("BufferPageSize", 65536)),
        m_port(config.get<uint32_t>("Port", 12345)),
        m_thread(std::bind(&RDMAInput::run, this, std::placeholders::_1),
                "RDMAInput", config.get<std::string>("CPU", ""))
{
    m_thread.start();
}
catch (ptree_error & ex) {
    throw BadConfigurationException(ERS_HERE, ex.what(), ex);
}

RDMAInput::~RDMAInput() {
    stopEventLoop();
}

void RDMAInput::run(const bool & )
{
    runEventLoop();
}

void RDMAInput::runStarted(const RunParams & rp) {
    DataInput::runStarted(rp);

    struct sockaddr_in6 addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin6_family = AF_INET6;
    addr.sin6_port = htons(m_port);

    TEST_NZ(rdma_bind_addr(m_comm_id, (sockaddr * )&addr));
    TEST_NZ(rdma_listen(m_comm_id, 10));
}

void RDMAInput::bufferReceived(uint8_t * buffer, uint32_t size)
{
    uint8_t * const END = buffer + size;
    for (uint8_t * p = buffer; p < END;) {
        uint32_t size = *(uint32_t*)p;
        if (!size) {
            break;
        }
        dataReceived(*(uint64_t*)(p + 4), p + 12, size - 12, 0);
        p += size;
        ++m_count;
    }
}

void RDMAInput::on_pre_conn()
{
    m_buffers_mrs.resize(m_queue_length);

    for (size_t i = 0; i < m_queue_length; ++i) {
        TEST_Z(
            m_buffers_mrs[i] = ibv_reg_mr(get_pd(), new uint8_t[m_buffer_size],
                    m_buffer_size, IBV_ACCESS_LOCAL_WRITE));
        post_receive(m_buffers_mrs[i]);
    }

    m_message_mr = ibv_reg_mr(get_pd(), &m_message, sizeof(message),
        IBV_ACCESS_LOCAL_WRITE);
}

void RDMAInput::on_connect()
{
    m_message.id = MSG_READY;
    send_remote(m_message_mr);
}

void RDMAInput::on_completion(ibv_wc &wc)
{
    if (wc.opcode & IBV_WC_RECV) {
        ++m_req_complete;

        uint32_t size = wc.byte_len;

        ibv_mr * buffer = (ibv_mr *) wc.wr_id;
        bufferReceived((uint8_t*) buffer->addr, size);
        post_receive(buffer);

        if (++m_chunks_received % m_chunks_to_acknowledge == 0) {
            m_message.id = MSG_ACK;
            send_remote(m_message_mr);
            ++m_ack_posted;
        }
    }
}

void RDMAInput::on_disconnect() {
}

uint32_t RDMAInput::translateStatus(uint8_t status) const noexcept {
    if (!status) {
        return 0;
    }
    return status;
}
