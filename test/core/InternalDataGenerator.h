/*
 * InternalDataGenerator.h
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#ifndef SRC_INPUT_INTERNALDATAGENERATOR_H_
#define SRC_INPUT_INTERNALDATAGENERATOR_H_

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <vector>

#include <boost/property_tree/ptree.hpp>

#include <swrod/DataInput.h>
#include <swrod/detail/WorkerThread.h>

#include "InternalL1AGenerator.h"

namespace swrod {
    namespace test {
        class InternalDataGenerator : public DataInput {
        public:
            InternalDataGenerator(
                    const boost::property_tree::ptree & config);

            virtual ~InternalDataGenerator();

            void actionsPending() override {
                ++m_actions_pending;
            }

            void runStarted(const RunParams &) override;

            void runStopped() override;

            void subscribeToFelix(const InputLinkId & link) override;

            void unsubscribeFromFelix(const InputLinkId & link) override;

            uint32_t translateStatus(uint8_t s) const noexcept override {
                return s;
            }

        protected:
            virtual void generatePacket(InputLinkId link, uint32_t l1id, uint16_t bcid);

            virtual void beforeStart() { }

            virtual void afterStop() { }

        private:
            void runGBTMode(const bool & active);

            void runFullMode(const bool & active);

        protected:
            const uint16_t m_packet_size;
            const uint16_t m_pileup;
            const uint32_t m_l1id_bit_mask;
            const uint32_t m_total_links_per_ROB;
            const uint32_t m_ROB_number;
            const uint32_t m_workers_number;
            const uint32_t m_worker_index;
            const bool m_full_mode;
            std::mutex m_mutex;
            std::condition_variable m_condition;
            uint64_t m_packets_counter;
            uint64_t m_triggers_counter;
            bool m_running;
            std::atomic<uint32_t> m_actions_pending;
            std::shared_ptr<InternalL1AGenerator> m_l1a_gen;
            uint32_t m_ttc_id;
            std::pair<uint32_t, uint32_t> m_trigger_range;
            uint32_t m_links_per_ROB;
            uint8_t * m_packet;
            std::vector<InputLinkId> m_links;
            detail::WorkerThread m_thread;
        };
    }
}

#endif /* SRC_INPUT_INTERNALDATAGENERATOR_H_ */
