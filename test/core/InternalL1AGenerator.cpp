/*
 * InternalL1AGenerator.cpp
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#include <ers/ers.h>

#include "InternalL1AGenerator.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>

using namespace swrod;
using namespace swrod::test;
using namespace boost::property_tree;

namespace {
    std::mutex mutex;
    std::shared_ptr<InternalL1AGenerator> l1aGen;

    Factory<DataInput>::Registrator __reg__(
        "InternalL1A",
        [](const boost::property_tree::ptree& config, const Core& ) {
            std::unique_lock<std::mutex> lock(mutex);
            if (!l1aGen || !config.get<bool>("IsSingleton", false)) {
                l1aGen.reset(new InternalL1AGenerator(config));
            }
            return l1aGen;
        });
}

std::shared_ptr<InternalL1AGenerator> InternalL1AGenerator::instance() {
    std::unique_lock<std::mutex> lock(mutex);
    if (!l1aGen) {
        ERS_LOG("InternalL1AGenerator singleton does not exist, creating default one");
        l1aGen.reset(new InternalL1AGenerator(boost::property_tree::ptree()));
    }
    return l1aGen;
}

InternalL1AGenerator::InternalL1AGenerator(const ptree& config) :
    m_events_number(config.get<uint64_t>("EventsNumber", -1)),
    m_drop_mask(config.get<uint32_t>("DropMask", -1)),
    m_ttc_queue_size(config.get<uint32_t>("TTCQueueSize", 10000)),
    m_ECR_interval(config.get<uint32_t>("EcrInterval", 10000)),
    m_ttc_id(0),
    m_counter(0),
    m_last_l1id(-1),
    m_link(-1),
    m_running(false),
    m_paused(false),
    m_thread(std::bind(&InternalL1AGenerator::run, this, std::placeholders::_1),
            "InternalL1A", config.get<std::string>("CPU", ""))
{
    ;
}

InternalL1AGenerator::~InternalL1AGenerator() {
    if (m_paused) {
        m_paused = false;
        m_condition.notify_one();
    }
    runStopped();
}

void InternalL1AGenerator::reset() {
    m_counter = 0;
    m_last_l1id = uint32_t(-1);
}

bool InternalL1AGenerator::getTTCInformation(
        uint32_t ttc_id, uint32_t &l1id, uint16_t &bcid) {
    static const uint16_t BCID_MASK = 0x0fff;

    ERS_ASSERT(ttc_id < m_ttc_queues.size());

    if (!m_ttc_queues[ttc_id].second.try_pop(l1id)) {
        return false;
    }

    bcid = l1id & BCID_MASK;
    return true;
}

void InternalL1AGenerator::synchronise() {
    while (true) {
        uint32_t size = 0;
        for (auto &q : m_ttc_queues) {
            size += q.second.size();
        }
        if (!size) {
            break;
        }
    }
}

uint32_t InternalL1AGenerator::pause(bool synch) {
    std::unique_lock lock(m_mutex);
    m_paused = true;

    if (synch) {
        synchronise();
    }

    m_ttc_id = 0;
    ERS_LOG(
            "Trigger has been paused, current counter = " << m_counter
            << " last L1ID = 0x" << std::hex << m_last_l1id);

    return m_last_l1id;
}

void InternalL1AGenerator::resume() {
    std::unique_lock lock(m_mutex);
    if (!m_paused) {
        return;
    }

    m_message.ecr++;
    m_message.l1id = 0;
    m_paused = false;
    m_condition.notify_one();
    ERS_LOG("Trigger has been resumed, current counter = " << m_counter
            << " first l1id = 0x" << std::hex << m_message.ext_l1id);
}

void InternalL1AGenerator::runStarted(const RunParams &rp) {
    if (!m_running && !m_ttc_queues.empty()) {
        DataInput::runStarted(rp);
        m_counter = 0;
        m_message.ecr = m_paused ? 0xff : 0x00;
        m_message.l1id = 0;
        for (auto &q : m_ttc_queues) {
            q.second.clear();
        }
        m_running = true;
        m_thread.start();
    }
}

void InternalL1AGenerator::runStopped() {
    if (m_running) {
        m_running = false;
        if (m_paused) {
            m_condition.notify_one();
        }
        m_thread.stop();
        m_ttc_id = 0;
        DataInput::runStopped();
    }
}

void InternalL1AGenerator::startGeneration(const RunParams &rp) {
    if (m_link == uint64_t(-1)) {
        runStarted(rp);
    }
}

void InternalL1AGenerator::stopGeneration() {
    if (m_link == uint64_t(-1)) {
        runStopped();
    }
}

void InternalL1AGenerator::run(const bool &active) {

    while (m_counter < m_events_number) {
        if (!active && m_events_number == uint64_t(-1)) {
            break;
        }

        std::unique_lock lock(m_mutex);
        m_condition.wait(lock, [this]() {
            return !m_paused || !m_running;
        });

        m_message.bcid = m_message.l1id & 0x0fff;
        if (m_link != uint64_t(-1)) {
            if ((m_message.l1id & m_drop_mask) != m_drop_mask) {
                dataReceived(m_link, (uint8_t*) &m_message, sizeof(m_message), 0);
            }
        }
        m_last_l1id = m_message.ext_l1id;

        for (auto &q : m_ttc_queues) {
            q.first = false;
        }

        uint32_t to_be_sent = m_ttc_queues.size();
        while (to_be_sent) {
            for (auto &q : m_ttc_queues) {
                if (!q.first) {
                    q.first = q.second.try_push(m_last_l1id);
                    to_be_sent -= q.first;
                }
            }
            if (!active && m_events_number == uint64_t(-1)) {
                return;
            }
            if (to_be_sent) {
                usleep(10);
            }
        }

        if ((++m_counter % m_ECR_interval) == 0) {
            m_message.ecr++;
            m_message.l1id = 0;
            ERS_DEBUG(3, "Generating ECR: " << (uint16_t)m_message.ecr);
        } else {
            m_message.l1id++;
        }
    }

    synchronise();
}
