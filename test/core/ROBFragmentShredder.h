/*
 * ROBFragmentShredder.h
 *
 *  Created on: Jul 16, 2019
 *      Author: kolos
 */

#ifndef TEST_CORE_ROBFRAGMENTSHREDDER_H_
#define TEST_CORE_ROBFRAGMENTSHREDDER_H_

#include <boost/property_tree/ptree.hpp>

#include <swrod/ROBFragmentConsumerBase.h>

namespace swrod {
    class Core;

    class ROBFragmentShredder : public ROBFragmentConsumerBase {
    public:
        ROBFragmentShredder(const boost::property_tree::ptree & config, const Core & core);

        const std::string & getName() const override {
            static std::string s("FragmentShredder");
            return s;
        }
    };
}

#endif /* TEST_CORE_ROBFRAGMENTSHREDDER_H_ */
