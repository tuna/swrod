/*
 * InternalL1AGenerator.h
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#ifndef TEST_INTERNALL1AGENERATOR_H_
#define TEST_INTERNALL1AGENERATOR_H_

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>

#include <boost/property_tree/ptree.hpp>
#include <tbb/concurrent_queue.h>

#include <swrod/DataInput.h>
#include <swrod/detail/L1AMessage.h>
#include <swrod/detail/WorkerThread.h>
#include <swrod/detail/Barrier.h>

namespace swrod {
    class Core;

    namespace test {
        class InternalL1AGenerator: public DataInput {
        public:
            static std::shared_ptr<InternalL1AGenerator> instance();

            InternalL1AGenerator(const boost::property_tree::ptree & config);

            ~InternalL1AGenerator();

            uint32_t acquireTTCId() {
                uint32_t id = m_ttc_id++;
                if (id >= m_ttc_queues.size()) {
                    m_ttc_queues.emplace_back(false, L1IdQueue());
                    m_ttc_queues.rbegin()->second.set_capacity(m_ttc_queue_size);
                }
                ERS_ASSERT(id < m_ttc_queues.size());
                return id;
            }

            bool getTTCInformation(uint32_t ttc_id, uint32_t & l1id, uint16_t & bcid);

            uint32_t pause(bool synch = false);

            void reset();

            void resume();

            void actionsPending() override { }

            void runStarted(const RunParams &) override;

            void runStopped() override;

            void setECRInterval(uint32_t ECR_interval) {
                m_ECR_interval = ECR_interval;
            }

            void startGeneration(const RunParams & rp);

            void stopGeneration();

            void subscribeToFelix(const InputLinkId & link) override {
                m_link = link;
            }

            void unsubscribeFromFelix(const InputLinkId & link) override {
                m_link = uint64_t(-1);
            }

            void waitForTriggersNumber(uint32_t triggers) {
                uint64_t c = m_counter;
                while ((m_counter - c ) < triggers && m_counter != m_events_number) {
                    std::this_thread::yield();
                }
            }

        private:
            void run(const bool & active);

            void synchronise();

        private:
            typedef tbb::concurrent_bounded_queue<uint32_t> L1IdQueue;

            const uint64_t m_events_number;
            const uint32_t m_drop_mask;
            const uint32_t m_ttc_queue_size;
            uint32_t m_ECR_interval;
            std::atomic<uint32_t> m_ttc_id;
            uint64_t m_counter;
            uint32_t m_last_l1id;
            InputLinkId m_link;
            bool m_running;
            bool m_paused;

            std::mutex m_mutex;
            std::condition_variable m_condition;

            detail::L1AMessage m_message;
            std::vector<std::pair<bool,L1IdQueue>> m_ttc_queues;
            detail::WorkerThread m_thread;
        };
    }
}

#endif /* TEST_INTERNALL1AGENERATOR_H_ */
