/*
 * ClearMessage.h
 *
 *  Created on: Mar 1, 2018
 *      Author: kolos
 */

#ifndef SWROD_HLT_CLEARMESSAGE_HPP_
#define SWROD_HLT_CLEARMESSAGE_HPP_

#include <asyncmsg/Message.h>

namespace swrod {
namespace hlt {
class ClearMessage: public daq::asyncmsg::OutputMessage
{
public:
    enum _id_ { TYPE_ID = 0x00DCDF10 };

    explicit ClearMessage(std::uint32_t transactionId,
                          const std::vector<std::uint32_t> & l1ids) :
            m_transaction_id(transactionId),
            m_l1_ids(l1ids)
    {
        ;
    }

    std::uint32_t typeId() const override
    {
        return TYPE_ID;
    }

    std::uint32_t transactionId() const override
    {
        return m_transaction_id;
    }

    void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override
    {
        buffers.emplace_back(m_l1_ids.data(), m_l1_ids.size() * sizeof(std::uint32_t));
    }

private:
    std::uint32_t m_transaction_id;
    std::vector<std::uint32_t> m_l1_ids;
};
}
}

#endif /* SWROD_HLT_CLEARMESSAGE_HPP_ */
