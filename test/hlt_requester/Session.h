/*
 * Session.h
 *
 *  Created on: Jan 26, 2018
 *      Author: kolos
 */

#ifndef SWROD_HLT_SESSION_H_
#define SWROD_HLT_SESSION_H_

#include <assert.h>
#include <cstdint>

#include <memory>

#include <ers/ers.h>

#include <asyncmsg/Error.h>
#include <asyncmsg/Message.h>
#include <asyncmsg/Session.h>

#include "ResponseMessage.h"

namespace swrod {
    ERS_DECLARE_ISSUE(hlt, SessionError,
            "Problem communicating with " << remoteName << " (" << remoteEndpoint << ") while " << action,
            ((std::string) remoteName) ((std::string) remoteEndpoint) ((std::string) action))

    ERS_DECLARE_ISSUE(hlt, SystemError,
            message << " " << "(" << category << ": " << number << ")",
            ((std::string) message) ((std::string) category) ((int) number))

    namespace hlt {
        class Session: public daq::asyncmsg::Session {
        public:

            Session(boost::asio::io_service& ioService) :
                daq::asyncmsg::Session(ioService) {
            }

            void onOpen() override
            {
                ERS_LOG("Connection to " << remoteName() << " opened.");
                asyncReceive();
            }

            void onOpenError(const boost::system::error_code& error) override
            {
                abort(ERS_HERE, "opening connection",
                        SystemError(ERS_HERE, error.message(), error.category().name(),
                                error.value()));
            }

            void onClose() override
            {
                ERS_LOG("Connection to " << remoteName() << " closed.");
            }

            void onCloseError(const boost::system::error_code& error) override
            {
                // Ignore errors caused by calling asyncClose() twice
                if (error != daq::asyncmsg::Error::SESSION_NOT_OPEN) {
                    ERS_LOG("Error closing hlt::Session");
                }
            }

            std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(uint32_t typeId,
                    uint32_t transactionId, uint32_t size) override
                    {
                if (typeId == ResponseMessage::TYPE_ID) {
                    return std::make_unique<ResponseMessage>(transactionId, size);
                } else {
                    ERS_LOG("receiving unknown message: tr_id = " << transactionId
                            << " type = " << std::hex << typeId << " size = " << size);
                    return std::unique_ptr<daq::asyncmsg::InputMessage>();
                }
            }

            void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> ) override
            {
            }

            void onReceiveError(const boost::system::error_code& error,
                    std::unique_ptr<daq::asyncmsg::InputMessage> message) override {
                abort(ERS_HERE, "receiving bad message",
                        SystemError(ERS_HERE, error.message(), error.category().name(),
                                error.value()));
            }

            void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> ) override {
            }

            void onSendError(const boost::system::error_code& error,
                    std::unique_ptr<const daq::asyncmsg::OutputMessage> ) override
            {
                abort(ERS_HERE, "sending output message",
                        SystemError(ERS_HERE, error.message(), error.category().name(),
                                error.value()));
            }

        protected:

            void abort(const ers::Context& context, const std::string& action,
            const std::exception& cause) {
                ers::error(SessionError(context, remoteName(),
                        remoteEndpoint().address().to_string(), action, cause));

                if (state() == State::OPEN) {
                    asyncClose();
                }
                ::abort();
            }
        };
    }
}

#endif /* SWROD_HLT_SESSION_H_ */
