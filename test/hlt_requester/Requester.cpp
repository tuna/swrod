/*
 * Requester.cpp
 *
 *  Created on: Mar 11, 2021
 *      Author: kolos
 */
#include <regex>

#include <felix/felix_client_properties.h>
#include <felix/felix_client_status.h>

#include "Requester.h"

using namespace swrod::hlt;
using namespace std::chrono;
namespace arg = std::placeholders;

Requester::Requester(std::vector<std::shared_ptr<RequesterSession>> data_sessions,
        std::shared_ptr<ClearSession> clear_session, uint32_t l1_rate,
        uint32_t l1id_reset_interval, uint32_t eb_request_fraction,
        uint32_t l2_request_fraction, bool apply_correction, uint64_t l1a_fid,
        const std::string & interface) :
        m_data_sessions(data_sessions),
        m_clear_session(clear_session),
        m_l1_rate(l1_rate),
        m_l1id_reset_interval(l1id_reset_interval),
        m_eb_request_fraction(eb_request_fraction),
        m_l2_request_fraction(l2_request_fraction),
        m_apply_correction(apply_correction),
        m_counter(0),
        m_eb_sent(0),
        m_l2_sent(0)
{
    if (!l1a_fid) {
        m_running = true;
        m_thread = std::thread(std::bind(&Requester::run, this));
    } else {
        m_felix.reset(new felix_proxy::ClientThread({
            std::bind(&Requester::netioMessageReceived, this,
                    arg::_1, arg::_2, arg::_3, arg::_4),
            nullptr, nullptr, nullptr,
            {
                {std::string(FELIX_CLIENT_LOCAL_IP_OR_INTERFACE), interface},
                {std::string(FELIX_CLIENT_BUS_DIR), "."},
                {std::string(FELIX_CLIENT_BUS_GROUP_NAME), "FELIX"},
                {std::string(FELIX_CLIENT_BUS_INTERFACE), "ZSYS_INTERFACE"},
                {std::string(FELIX_CLIENT_TIMEOUT), "10000"},
            }
        }));
        m_felix->subscribe(l1a_fid);
    }
}

Requester::~Requester() {
    m_running = false;
    m_thread.join();
}

void Requester::send(uint32_t l1id) {
    bool send_eb = (m_counter * m_eb_request_fraction / 100) > m_eb_sent;
    bool send_l2 = (m_counter * m_l2_request_fraction / 100) > m_l2_sent;

    if (send_l2) {
        m_data_sessions[m_l2_sent % m_data_sessions.size()]->sendL2Request(l1id);
        ++m_l2_sent;
    }
    if (send_eb) {
        m_data_sessions[m_eb_sent % m_data_sessions.size()]->sendEBRequest(l1id);
        ++m_eb_sent;
    }
    else if (not send_l2) {
        m_data_sessions[m_counter % m_data_sessions.size()]->skipL1ID(l1id);
    }
    ++m_counter;
}

void Requester::run()
{
    const uint64_t rate_checkup_period = 10000;
    const microseconds target_time = microseconds(
        1000000*rate_checkup_period/m_l1_rate);
    const microseconds threshold = microseconds(5);
    uint32_t l1id = 0;

    auto start = high_resolution_clock::now();

    while (m_running) {
        send(l1id);

        if (m_counter % m_l1id_reset_interval == 0) {
            l1id = ((l1id >> 24) + 1) << 24;
        } else {
            ++l1id;
        }

        if (m_counter % rate_checkup_period == 0) {
            auto end = high_resolution_clock::now();
            microseconds time_to_wait = target_time -
                duration_cast<microseconds>(end - start);
            if (time_to_wait > threshold) {
                usleep(time_to_wait.count());
            }
            start = high_resolution_clock::now();
            if (m_apply_correction) {
                m_clear_session->adjustRates();
            }
        }
    }
}

void Requester::netioMessageReceived(uint64_t link, const uint8_t * data,
        uint32_t size, uint8_t status) {
    uint32_t l1id = *((uint32_t*)((uint8_t*)data + 4));
    send(l1id);
}
