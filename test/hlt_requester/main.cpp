/*
 * hlt_requester main.cpp
 *
 *  Created on: Jan 19, 2018
 *      Author: kolos
 */

#include <cstdint>
#include <sys/time.h>

#include <chrono>
#include <csignal>
#include <iostream>
#include <iomanip>
#include <mutex>
#include <numeric>
#include <string>
#include <thread>
#include <vector>

#include <boost/program_options.hpp>

#include <asyncmsg/NameService.h>
#include <ipc/core.h>
#include "Requester.h"
#include "RequesterSession.h"

using namespace std::chrono;
using namespace swrod::hlt;

volatile std::sig_atomic_t signal_status = 0;

void signal_handler(int s) {
    signal_status = s;
}

using namespace boost::program_options;

int main(int ac, char *av[])
{
    try {
        IPCCore::init(ac, av);
    }
    catch(daq::ipc::Exception & ex) {
        ers::fatal(ex);
        return 1;
    }

    options_description description("Options");

    description.add_options()("help,h", "produce help message")
        ("felix-interface,i", value<std::string>()->default_value("eth0"),
            "FELIX data receiving interface name")
        ("l1a-elink,a", value<uint64_t>()->default_value(0),
            "FID of L1A e-link, 0 for using internally generated L1IDs")
        ("correction,A",
            "adjust request rate to the processing speed of the data provider")
        ("port,P", value<uint32_t>()->default_value(9000),
            "port number to request data")
        ("clear-port,Q", value<uint32_t>()->default_value(9001),
            "port number to send clears")
        ("first,f", value<uint32_t>()->default_value(1),
            "first ROB id")
        ("clear-group-size,c", value<uint32_t>()->default_value(100),
            "group size for clear requests")
        ("EB-request-fraction,B", value<uint32_t>()->default_value(50),
            "fraction of fragments to request (%)")
        ("L2-request-fraction,L", value<uint32_t>()->default_value(0),
            "fraction of fragments to request (%)")
        ("L2-robs-number,l", value<uint32_t>()->default_value(0),
                "number of robs for L2 requests")
        ("robs-number,r", value<uint32_t>()->default_value(1),
            "number of robs to request")
        ("ecr-interval,E", value<uint32_t>()->default_value(500000),
            "ECR interval")
        ("l1id-rate,R", value<uint32_t>()->default_value(100000),
            "target l1id rate (Hz)")
        ("host,H", value<std::string>()->default_value("127.0.0.1"),
            "IP address to request data")
        ("partition,p", value<std::string>(),
            "partition name")
        ("application,s", value<std::string>(),
            "Dataflow application name")
        ("network,n", value<std::string>()->default_value("0.0.0.0/0.0.0.0"),
            "data network in a form of 'ip/mask'")
        ("data-sessions,D", value<uint32_t>()->default_value(1),
            "number of data requester sessions")
        ("clear-sessions,C", value<uint32_t>()->default_value(1),
            "number of clear sending sessions");

    variables_map arguments;
    try {
        store(parse_command_line(ac, av, description), arguments);
        notify(arguments);
    } catch (error & ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "Test application for the 'swrod' package" << std::endl;
        description.print(std::cout);
        return 0;
    }

    uint32_t robs_number = arguments["robs-number"].as<uint32_t>();
    uint32_t l2_robs_number = arguments["L2-robs-number"].as<uint32_t>();
    uint32_t data_sessions_number = arguments["data-sessions"].as<uint32_t>();
    uint32_t clear_sessions_number = arguments["clear-sessions"].as<uint32_t>();
    uint32_t ecr_interval = arguments["ecr-interval"].as<uint32_t>();
    uint32_t l1id_rate = arguments["l1id-rate"].as<uint32_t>();
    uint32_t first_rob_id = arguments["first"].as<uint32_t>();
    uint32_t clear_group_size = arguments["clear-group-size"].as<uint32_t>();
    uint32_t eb_request_fraction = arguments["EB-request-fraction"].as<uint32_t>();
    uint32_t l2_request_fraction = arguments["L2-request-fraction"].as<uint32_t>();
    uint32_t port_number = arguments["port"].as<uint32_t>();
    uint32_t clear_port_number = arguments["clear-port"].as<uint32_t>();
    uint64_t l1a_fid = arguments["l1a-elink"].as<uint64_t>();
    std::string interface = arguments["felix-interface"].as<std::string>();
    std::string host_name = arguments["host"].as<std::string>();

    std::unique_ptr<daq::asyncmsg::NameService> name_service;
    if (arguments.count("partition") && arguments.count("application") && arguments.count("network")) {
        name_service.reset(new daq::asyncmsg::NameService(
                arguments["partition"].as<std::string>(), {arguments["network"].as<std::string>()}));
    }

    std::vector<uint32_t> rob_ids(robs_number);
    for (uint32_t i = 0; i < robs_number; ++i) {
        rob_ids[i] = first_rob_id + i;
    }

    std::vector<uint32_t> l2_rob_ids(l2_robs_number);
    for (uint32_t i = 0; i < l2_robs_number; ++i) {
        l2_rob_ids[i] = first_rob_id + i;
    }

    auto clear_endpoint = boost::asio::ip::tcp::endpoint(
            boost::asio::ip::address::from_string(host_name), clear_port_number);
    if (name_service) {
        try {
            clear_endpoint = name_service->resolve("CLEAR_" + arguments["application"].as<std::string>());
            std::cout << "Resolved 'clear' endpoint = " << clear_endpoint << std::endl;
        } catch (ers::Issue & ex) {
            ers::fatal(ex);
            return 1;
        }
    }

    std::shared_ptr<ClearSession> clear_session =
        std::make_shared<ClearSession>(clear_group_size);

    for (uint32_t i = 0; i < clear_sessions_number; ++i) {
        std::shared_ptr<RequesterSession> session = std::make_shared<RequesterSession>();
        session->connect("CLEAR_SENDER", clear_endpoint);
        clear_session->addSession(session);
    }

    auto endpoint = boost::asio::ip::tcp::endpoint(
            boost::asio::ip::address::from_string(host_name), port_number);
    if (name_service) {
        try {
            endpoint = name_service->resolve(arguments["application"].as<std::string>());
            std::cout << "Resolved 'data' endpoint = " << endpoint << std::endl;
        } catch (ers::Issue & ex) {
            ers::fatal(ex);
            return 1;
        }
    }

    std::vector<std::shared_ptr<RequesterSession>> data_sessions;
    data_sessions.reserve(data_sessions_number);
    for (uint32_t i = 0; i < data_sessions_number; ++i) {
        std::shared_ptr<RequesterSession> session =
            std::make_shared<RequesterSession>(rob_ids, clear_session, l2_rob_ids);

        session->connect("HLT_EB_REQUESTOR", endpoint);
        data_sessions.push_back(session);
    }

    uint64_t old_eb_request_count = 0;
    uint64_t old_l2_request_count = 0;
    uint64_t old_eb_response_count = 0;
    uint64_t old_l2_response_count = 0;
    uint64_t old_clears_count = 0;

    std::cout << std::fixed << std::setprecision(1);

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    std::unique_ptr<Requester> requester;

    try {
        requester = std::make_unique<Requester>(
                data_sessions, clear_session, l1id_rate, ecr_interval,
                eb_request_fraction, l2_request_fraction, arguments.count("correction"),
                l1a_fid, interface);
    }
    catch (ers::Issue & ex) {
        ers::fatal(ex);
        return 1;
    }

    while (signal_status == 0) {
        usleep(1000000);

        struct timeval tv;
        gettimeofday(&tv, NULL);
        time_t curtime = tv.tv_sec;
        char buffer[16];
        strftime(buffer, 16, "%T.", localtime(&curtime));

        uint64_t eb_request_count = std::accumulate(
            data_sessions.begin(), data_sessions.end(),
            (uint64_t)0, [](uint64_t r, std::shared_ptr<RequesterSession> s){
                return r + s->getRequestCounter();
            });
        uint64_t l2_request_count = std::accumulate(
            data_sessions.begin(), data_sessions.end(),
            (uint64_t)0, [](uint64_t r, std::shared_ptr<RequesterSession> s){
                return r + s->getL2RequestCounter();
            });
        uint64_t eb_response_count = std::accumulate(
            data_sessions.begin(), data_sessions.end(),
            (uint64_t)0, [](uint64_t r, std::shared_ptr<RequesterSession> s){
                return r + s->getResponseCounter();
            });
        uint64_t l2_response_count = std::accumulate(
            data_sessions.begin(), data_sessions.end(),
            (uint64_t)0, [](uint64_t r, std::shared_ptr<RequesterSession> s){
                return r + s->getL2ResponseCounter();
            });

        uint64_t clears_count = clear_session->getClearsCounter();

        std::cout << buffer << std::setw(6) << std::setfill('0') << tv.tv_usec << " : "
            << "EB rates = " << (eb_request_count - old_eb_request_count) / 1000. << "/"
                             << (eb_response_count - old_eb_response_count) / 1000. << " kHz, "
            << "L2 rates = " << (l2_request_count - old_l2_request_count) / 1000. << "/"
                             << (l2_response_count - old_l2_response_count) / 1000. << " kHz, "
            << "clears rate = " << (clears_count - old_clears_count) / 1000. << " kHz"
            << std::endl;
        old_eb_request_count = eb_request_count;
        old_l2_request_count = l2_request_count;
        old_eb_response_count = eb_response_count;
        old_l2_response_count = l2_response_count;
        old_clears_count = clears_count;
    }

    return 0;
}

