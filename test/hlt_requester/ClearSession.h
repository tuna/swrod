/*
 * ClearSession.h
 *
 *  Created on: Jan 26, 2018
 *      Author: kolos
 */

#ifndef SWROD_HLT_CLEARSESSION_H_
#define SWROD_HLT_CLEARSESSION_H_

#include <assert.h>
#include <cstdint>

#include <memory>
#include <queue>
#include <map>
#include <vector>

#include "ClearMessage.h"
#include "Session.h"

namespace swrod {
    namespace hlt {

        class ClearSession {
        public:

            ClearSession(uint32_t clear_group_size = 100) :
                    m_clear_group_size(clear_group_size) {
                ;
            }

            void addSession(std::shared_ptr<Session> session) {
                m_sessions.push_back(session);
            }

            uint64_t getClearsCounter() const {
                return m_clears_counter;
            }

            void l2Sent(uint32_t l1id) {
                std::unique_lock<std::mutex> lock(m_mutex);
                m_l2_index_map[l1id] = m_index++;
            }

            void ebSent(uint32_t l1id) {
                std::unique_lock<std::mutex> lock(m_mutex);
                m_eb_index_map[l1id] = m_index++;
            }

            void skipL1ID(uint32_t l1id) {
                std::unique_lock<std::mutex> lock(m_mutex);
                m_skipped.push({l1id, m_index++});
            }

            void receivedL1ID(uint32_t l1id, bool is_l2) {
                std::unique_lock<std::mutex> lock(m_mutex);

                uint64_t index;
                if (is_l2) {
                    auto i = m_l2_index_map.find(l1id);
                    ERS_ASSERT (i != m_l2_index_map.end());
                    index = i->second;
                    m_l2_index_map.erase(i);
                } else {
                    auto i = m_eb_index_map.find(l1id);
                    ERS_ASSERT(i != m_eb_index_map.end());
                    index = i->second;
                    m_eb_index_map.erase(i);
                }

                m_clear_l1ids.push_back(l1id);
                if (m_clear_l1ids.size() == m_clear_group_size) {
                    sendClears();
                }

                while (not m_skipped.empty()) {
                    Record & r = m_skipped.front();
                    if (r.m_index < index) {
                        m_clear_l1ids.push_back(r.m_l1id);
                        m_skipped.pop();
                        if (m_clear_l1ids.size() == m_clear_group_size) {
                            sendClears();
                        }
                    } else {
                        break;
                    }
                }
            }

            void adjustRates() {
                static const size_t threshold = 10000;
                static const size_t timeout = 100000;
                size_t counter = 0;
                while ((m_eb_index_map.size() > threshold || m_l2_index_map.size() > threshold)
                        && counter++ < timeout) {
                    usleep(10);
                }
            }

        private:
            void sendClears() {
                std::unique_ptr<ClearMessage> message(
                        new ClearMessage(m_transaction_id, m_clear_l1ids));
                m_sessions[m_transaction_id++ % m_sessions.size()]->asyncSend(std::move(message));
                m_clears_counter += m_clear_l1ids.size();
                m_clear_l1ids.clear();
            }

        private:
            struct Record {
                uint32_t m_l1id;
                uint64_t m_index;
            };

            typedef std::map<uint32_t, uint64_t> Index;

            const uint32_t m_clear_group_size;
            uint32_t m_transaction_id = 0;
            uint64_t m_clears_counter = 0;
            uint64_t m_index = 0;
            std::mutex m_mutex;
            Index m_eb_index_map;
            Index m_l2_index_map;
            std::queue<Record> m_skipped;
            std::vector<uint32_t> m_clear_l1ids;
            std::vector<std::shared_ptr<Session>> m_sessions;
        };
    }
}

#endif /* SWROD_HLT_CLEARSESSION_H_ */
