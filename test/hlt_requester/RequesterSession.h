/*
 * RequesterSession.h
 *
 *  Created on: Jan 26, 2018
 *      Author: kolos
 */

#ifndef SWROD_HLT_REQUESTERSESSION_H_
#define SWROD_HLT_REQUESTERSESSION_H_

#include <assert.h>
#include <atomic>
#include <cstdint>

#include <memory>
#include <unordered_map>
#include <vector>

#include "ClearSession.h"
#include "IOService.h"
#include "RequestMessage.h"
#include "Session.h"

using namespace swrod::hlt;

namespace swrod {
    namespace hlt {

        class RequesterSession: public IOService, public Session {
        public:
            RequesterSession() :
                    Session(this->getService())
            {}

            RequesterSession(const std::vector<uint32_t> & rob_ids,
                    std::shared_ptr<ClearSession> clear_session,
                    const std::vector<uint32_t> l2_rob_ids = std::vector<uint32_t>(),
                    bool is_l2 = false) :
                    Session(this->getService()),
                    m_rob_ids(rob_ids),
                    m_is_l2(is_l2),
                    m_transaction_id(0),
                    m_request_counter(0),
                    m_response_counter(0),
                    m_clear_session(clear_session)
            {
                if (l2_rob_ids.empty()) {
                    return;
                }

                m_l2_session.reset(new RequesterSession(
                        l2_rob_ids, clear_session, std::vector<uint32_t>(), true));
            }

            void connect(const std::string & name,
                    const boost::asio::ip::tcp::endpoint & endpoint) {
                asyncOpen(name, endpoint);

                while (state() != daq::asyncmsg::Session::State::OPEN) {
                    usleep(1000);
                }

                if (m_l2_session) {
                    m_l2_session->connect("HLT_L2_REQUESTOR", endpoint);
                }
            }

            uint64_t getL2RequestCounter() const {
                return (m_l2_session ? m_l2_session->getRequestCounter() : 0);
            }

            uint64_t getL2ResponseCounter() const {
                return (m_l2_session ? m_l2_session->getResponseCounter() : 0);
            }

            uint64_t getRequestCounter() const {
                return m_request_counter;
            }

            uint32_t getResponseCounter() const {
                return m_response_counter;
            }

            void sendL2Request(uint32_t l1id) {
                ERS_ASSERT(m_l2_session);
                m_clear_session->l2Sent(l1id);
                m_l2_session->sendRequest(l1id);
            }

            void sendEBRequest(uint32_t l1id) {
                m_clear_session->ebSent(l1id);
                sendRequest(l1id);
            }

            void skipL1ID(uint32_t l1id) {
                m_clear_session->skipL1ID(l1id);
            }

        private:
            void sendRequest(uint32_t l1id) {
                ++m_request_counter;
                std::unique_ptr<RequestMessage> message(
                        new RequestMessage(m_transaction_id++, l1id, m_rob_ids));
                asyncSend(std::move(message));
            }

            void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override {
                if (message->typeId() == ResponseMessage::TYPE_ID) {
                    ++m_response_counter;
                    ResponseMessage* r =
                                    static_cast<ResponseMessage*>(message.get());

                    unsigned int l1id = *(r->m_received_data + 14);
                    m_clear_session->receivedL1ID(l1id, m_is_l2);
                } else {
                    ERS_ASSERT(0);
                }
            }

            void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> message) override {
                asyncReceive(); // we want to get a response
            }

        private:
            const std::vector<uint32_t> m_rob_ids;
            const bool m_is_l2 = false;
            mutable std::mutex m_mutex;
            std::atomic<uint32_t> m_transaction_id = 0;
            std::atomic<uint64_t> m_request_counter = 0;
            std::atomic<uint64_t> m_response_counter = 0;

            std::shared_ptr<ClearSession> m_clear_session;
            std::shared_ptr<RequesterSession> m_l2_session;
        };
    }
}

#endif /* SWROD_HLT_REQUESTERSESSION_H_ */
