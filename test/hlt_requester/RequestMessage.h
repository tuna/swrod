/*
 * RequestMessage.h
 *
 *  Created on: Mar 1, 2018
 *      Author: kolos
 */

#ifndef SWROD_HLT_REQUESTMESSAGE_H_
#define SWROD_HLT_REQUESTMESSAGE_H_

#include <asyncmsg/Message.h>

namespace swrod {
    namespace hlt {
        struct RequestMessage: public daq::asyncmsg::OutputMessage {

            enum _id_ { TYPE_ID = 0x00DCDF20 };

            RequestMessage(uint32_t transactionId, uint32_t l1id,
                    const std::vector<uint32_t>& rob_ids) :
                    m_transaction_id(transactionId), m_l1id(l1id), m_rob_ids(rob_ids) {
            }

            uint32_t typeId() const override
            {
                return TYPE_ID;
            }

            uint32_t transactionId() const override
            {
                return m_transaction_id;
            }

            void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override
            {
                buffers.emplace_back(static_cast<const void*>(&m_l1id), sizeof(m_l1id));
                buffers.emplace_back(m_rob_ids.data(), m_rob_ids.size() * sizeof(uint32_t));
            }

            uint32_t m_transaction_id;
            uint32_t m_l1id;
            std::vector<uint32_t> m_rob_ids;
        };
    }
}

#endif /* SWROD_HLT_REQUESTMESSAGE_H_ */
