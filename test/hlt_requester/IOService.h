/*
 * IOService.h
 *
 *  Created on: Jan 31, 2018
 *      Author: kolos
 */

#ifndef SWROD_HLT_IOSERVICE_HPP_
#define SWROD_HLT_IOSERVICE_HPP_

#include <memory>
#include <thread>

#include <boost/asio/io_service.hpp>

namespace swrod {
namespace hlt {

struct IOService
{
    IOService(uint32_t concurrency_hint=1) :
        m_io_service(new boost::asio::io_service(concurrency_hint)),
        m_io_work(new boost::asio::io_service::work(*m_io_service)),
        m_thread([this](){ m_io_service->run(); })
    {
        ;
    }

    ~IOService()
    {
        m_io_service->stop();
        m_thread.join();
    }

    boost::asio::io_service& getService() {
        return *m_io_service;
    }

private:
    std::unique_ptr<boost::asio::io_service> m_io_service;
    std::unique_ptr<boost::asio::io_service::work> m_io_work;
    std::thread m_thread;
};
}}


#endif /* SWROD_HLT_IOSERVICE_HPP_ */
