/*
 * ResponseMessage.h
 *
 *  Created on: Mar 1, 2019
 *      Author: kolos
 */

#ifndef SWROD_HLT_RESPONSEMESSAGE_H_
#define SWROD_HLT_RESPONSEMESSAGE_H_

#include <assert.h>

#include <memory>

#include <asyncmsg/Message.h>

namespace swrod {
    namespace hlt {
        struct ResponseMessage: public daq::asyncmsg::InputMessage {

            enum _id_ { TYPE_ID = 0x00DCDF21 };

            ResponseMessage(uint32_t transactionId, const uint32_t message_size) :
                    m_transaction_id(transactionId), m_received_size(message_size) {
                m_received_data = new uint32_t[message_size / sizeof(uint32_t) + 1];
            }

            ResponseMessage(const ResponseMessage& ) = delete;
            ResponseMessage& operator=(const ResponseMessage& ) = delete;

            virtual ~ResponseMessage() {
                delete[] m_received_data;
            }

            uint32_t typeId() const override
            {
                return TYPE_ID;
            }

            uint32_t transactionId() const override
            {
                return m_transaction_id;
            }

            void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override
            {
                buffers.emplace_back(m_received_data, m_received_size);
            }

            const uint32_t m_transaction_id;
            const uint32_t m_received_size;
            uint32_t* m_received_data;
        };
    }
}

#endif /* SWROD_HLT_RESPONSEMESSAGE_H_ */
