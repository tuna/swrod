/*
 * swrod_test.cpp
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#include <sys/resource.h>
#include <csignal>
#include <iostream>
#include <memory>

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <TSystem.h>

#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/infodictionary.h>
#include "monsvc/PublishingController.h"
#include "monsvc/ConfigurationRules.h"

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/Factory.h>

using namespace boost::program_options;
using namespace boost::property_tree;
using namespace swrod;

volatile std::sig_atomic_t signal_status = 0;

void signal_handler(int s) {
    signal_status = s;
}

void publishStatistics(Core & core) {
    std::string server(core.getConfiguration().get<std::string>("ISServerName"));
    std::string app(core.getConfiguration().get<std::string>("Application.Name"));
    ISInfoDictionary dict(core.getConfiguration().get<std::string>("Partition.Name"));

    for (auto b : core.getBuilders()) {
       auto stats = b->getStatistics();
       if (stats) {
           try {
               dict.checkin(server + ".swrod." + app + "." + b->getName(), *stats);
           } catch (daq::is::Exception& ex) {
               ers::debug(ex, 0);
           }
       }
    }

    for (auto c : core.getConsumers()) {
       auto stats = c->getStatistics();
       if (stats) {
           try {
               dict.checkin(server + "." + c->getName(), *stats);
           } catch (daq::is::Exception& ex) {
               ers::debug(ex, 0);
           }
       }
    }
}

int main(int argc, char ** argv)
{
    // Always enable core file production
    rlimit core_limit = { RLIM_INFINITY, RLIM_INFINITY };
    setrlimit( RLIMIT_CORE, &core_limit );

    // Disable ROOT signal handlers as otherwise no core file will be produced in case of a crash
    if (gSystem) {
        gSystem->ResetSignals();
    }

    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::Exception & ex) {
        ers::fatal(ex);
        return 1;
    }

    options_description description("Options");

    description.add_options()("help,h", "produce help message")
        ("port,P", value<uint32_t>()->default_value(12345),
            "port number")
        ("elinks-number,e", value<uint16_t>()->default_value(192),
            "elinks number per ROB")
        ("comm-buffer-size,u", value<uint32_t>()->default_value(100000),
            "communication protocol data buffer size in bytes")
        ("comm-buffers-num,U", value<uint32_t>()->default_value(32),
            "communication protocol buffers number")
        ("buffer-size,b", value<uint32_t>()->default_value(100000),
            "input buffer maximum size in terms of number of packets per elink")
        ("buffer-init-size,B", value<uint32_t>()->default_value(64),
            "input buffer initial size in terms of number of packets per elink")
        ("l1a-elink,a", value<uint64_t>()->default_value(7777),
            "L1A elink id, set it to zero to run in data-driven mode")
        ("l1a-per-module,A",
            "Use one L1A input per module")
        ("first-elink,f", value<uint64_t>()->default_value(1),
            "first link id")
        ("packet-size,s", value<uint16_t>()->default_value(40),
            "packet size for internal data generator")
        ("pileup,k", value<uint16_t>()->default_value(1),
            "pileup for internal data generator")
        ("data-input-type,i", value<std::string>()->default_value("FastData"),
            "input type: FastData, InternalData, TestData, NetioInput, NetioNextInput, FelixInput, RDMAInput")
        ("l1a-input-type,I", value<std::string>()->default_value("FastL1A"),
            "input type: FastL1A, InternalL1A, NetioInput, NetioNextInput, FelixInput, RDMAInput")
        ("netio-backend,z", value<std::string>()->default_value("posix"),
            "Netio backed: posix or fi_verbs")
        ("netio-network-interface,n", value<std::string>()->default_value("127.0.0.1"),
            "Netio network interface for receiving data")
        ("fragment-builder-CPUs,Q", value<std::string>()->default_value(""),
                "CPU cores for fragment building threads. Empty string means no restrictions.")
        ("fragment-consumer-CPUs,Z", value<std::string>()->default_value(""),
                 "CPU cores for fragment consumer threads. Empty string means no restrictions.")
        ("netio-use-callback,u",
            "use NetIO callback mode for reading data")
        ("felixbus-dir,d", value<std::string>()->default_value("/tmp"),
            "Directory to be used by felixbus")
        ("event-builder-type,T", value<std::string>()->default_value("GBTModeBuilder"),
            "input type: FullModeBuilder, GBTModeBuilder")
        ("samplers-number,L", value<uint16_t>()->default_value(0),
            "number of event sampling threads")
        ("show-rates,V", value<uint16_t>()->default_value(0),
            "display event building rates: 0 - nothing, 1 - instantaneous, 2 - average")
        ("publish-stats,F",
             "publish statistics to DF IS server")
        ("verbose,v",
              "print ERS LOG messages")
        ("partition-name,p", value<std::string>()->default_value("initial"),
            "partition name")
        ("robs-number,r", value<uint16_t>()->default_value(1),
            "number of ROBs")
        ("proc-number,N", value<uint16_t>()->default_value(0),
            "number of custom processing threads")
        ("ecr-interval,E", value<uint32_t>()->default_value(500000),
            "ECR interval for internal data generator")
        ("sync-interval,S", value<uint32_t>()->default_value(10000),
            "sync interval for internal data generator")
        ("config-out-file,C", value<std::string>(),
            "dump configuration to this JSON file")
        ("config-in-file,c", value<std::string>(),
            "read configuration from this JSON file")
        ("out-dir,D", value<std::string>(),
            "put data-file to this directory")
        ("run-number,R", value<uint32_t>()->default_value(11111),
            "run number to be used for the output file name")
        ("workers-number,w", value<uint16_t>()->default_value(1),
            "number of data reader threads per input module")
        ("robs-per-module,g", value<uint16_t>()->default_value(1),
            "number of ROBs per input module")
        ("builders-number,W", value<uint16_t>()->default_value(1),
            "number of fragment builder threads per ROB")
        ("HLT-request-processing,H", value<uint16_t>()->default_value(0),
            "number of HLT request processing threads")
        ("HLT-network,G", value<std::string>()->default_value("0.0.0.0/0.0.0.0"),
            "HLT data network in a form of 'ip/mask'")
        ("shredders-per-ROB,X", value<uint16_t>()->default_value(0),
            "number of data destruction threads per ROB")
        ("shredders-number,Y", value<uint16_t>()->default_value(1),
                "number of global data destruction threads");

    variables_map arguments;
    try {
        store(parse_command_line(argc, argv, description), arguments);
        notify(arguments);
    } catch (error & ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "Test application for the 'swrod' package" << std::endl;
        description.print(std::cout);
        return 0;
    }

    if (not arguments.count("verbose")) {
        setenv("TDAQ_ERS_LOG", "null", 1);
    }

    std::string data_input_type = arguments["data-input-type"].as<std::string>();
    std::string l1a_input_type = arguments["l1a-input-type"].as<std::string>();
    std::string event_builder_type = arguments["event-builder-type"].as<std::string>();
    std::string partition_name = arguments["partition-name"].as<std::string>();
    std::string netio_backend = arguments["netio-backend"].as<std::string>();
    std::string netio_network = arguments["netio-network-interface"].as<std::string>();
    std::string felixbus_dir = arguments["felixbus-dir"].as<std::string>();
    std::string HLT_network = arguments["HLT-network"].as<std::string>();
    std::string cpu_1 = arguments["fragment-builder-CPUs"].as<std::string>();
    std::string cpu_2 = arguments["fragment-consumer-CPUs"].as<std::string>();
    uint64_t first_link_id = arguments["first-elink"].as<uint64_t>();
    uint64_t l1a_elink_id = arguments["l1a-elink"].as<uint64_t>();
    uint16_t links_number = arguments["elinks-number"].as<uint16_t>();
    uint16_t robs_number = arguments["robs-number"].as<uint16_t>();
    uint16_t workers_number = arguments["workers-number"].as<uint16_t>();
    uint16_t robs_per_module = arguments["robs-per-module"].as<uint16_t>();
    uint16_t builders_number = arguments["builders-number"].as<uint16_t>();
    uint16_t shredders_number = arguments["shredders-number"].as<uint16_t>();
    uint16_t shredders_per_ROB = arguments["shredders-per-ROB"].as<uint16_t>();
    uint16_t proc_number = arguments["proc-number"].as<uint16_t>();
    uint16_t samplers_number = arguments["samplers-number"].as<uint16_t>();
    uint16_t HLT_threads_number = arguments["HLT-request-processing"].as<uint16_t>();
    uint16_t packet_size = arguments["packet-size"].as<uint16_t>();
    uint16_t pileup = arguments["pileup"].as<uint16_t>();
    uint16_t show_rates = arguments["show-rates"].as<uint16_t>();
    uint32_t ecr_interval = arguments["ecr-interval"].as<uint32_t>();
    uint32_t sync_interval = arguments["sync-interval"].as<uint32_t>();
    uint32_t comm_buffer_size = arguments["comm-buffer-size"].as<uint32_t>();
    uint32_t comm_buffers_num = arguments["comm-buffers-num"].as<uint32_t>();
    uint32_t buffer_size = arguments["buffer-size"].as<uint32_t>();
    uint32_t buffer_init_size = arguments["buffer-init-size"].as<uint32_t>();
    uint32_t port = arguments["port"].as<uint32_t>();
    uint32_t run_number = arguments["run-number"].as<uint32_t>();

    std::string app_name(getenv("TDAQ_APPLICATION_NAME")
            ? getenv("TDAQ_APPLICATION_NAME") : "swrod_test");

    //////////////////////////////////////////////////////////
    // Prepare Core configuration
    //////////////////////////////////////////////////////////
    ptree configuration;

    if (arguments.count("config-in-file")) {
        try {
            read_json(arguments["config-in-file"].as<std::string>(), configuration);
            if (configuration.count("Partition")) {
                partition_name = configuration.get<std::string>("Partition.Name");
            }
            if (configuration.count("Application")) {
                app_name = configuration.get<std::string>("Application.Name");
            }
        } catch (json_parser::json_parser_error & ex) {
            std::cerr << ex.what() << std::endl;
            return 1;
        }
    }
    else {
        configuration.put("DataFlowParameters.MulticastAddress", "tcp:");
        configuration.add("DataFlowParameters.networks.network", HLT_network);

        configuration.add("Partition.Name", partition_name);
        configuration.add("Application.FullStatisticsInterval", 1);
        configuration.add("Application.Name", app_name);
        configuration.add("ISServerName", "DF");

        ptree plugins;
        ptree plugin;
        plugin.put("LibraryName", "libswrod_core_test.so");
        plugins.add_child("Plugin", plugin);
        plugin.put("LibraryName", "libswrod_core_unit_test.so");
        plugins.add_child("Plugin", plugin);
        plugin.put("LibraryName", "libswrod_core_impl.so");
        plugins.add_child("Plugin", plugin);
        configuration.add_child("Plugins", plugins);

        ptree input;
        input.add("Type", data_input_type);
        input.add("PacketSize", packet_size);
        input.add("Pileup", pileup);
        input.add("BufferPageSize", comm_buffer_size);
        input.add("BufferPages", comm_buffers_num);
        input.add("UseCallback", arguments.count("netio-use-callback"));
        input.add("Port", port);
        input.add("Backend", netio_backend);
        input.add("DataNetwork", netio_network);
        input.add("FelixBusTimeout", 1000);
        input.add("FelixBusInterface", "ZSYS_INTERFACE");
        input.add("FelixBusGroupName", "FELIX");
        input.add("FelixBusDirectory", felixbus_dir);
        if (event_builder_type == "FullModeBuilder") {
            input.add("IsFullMode", true);
            input.add("TotalFullModeLinks", links_number);
            input.put("ROBsPerModule", robs_per_module);
        }
        configuration.add_child("InputMethod", input);

        uint32_t l1a_port = port + robs_number*workers_number;
        ptree l1a_input;
        l1a_input.add("Type", l1a_input_type);
        l1a_input.add("EcrInterval", ecr_interval);
        l1a_input.add("SyncInterval", sync_interval);
        l1a_input.add("BufferPageSize", comm_buffer_size);
        l1a_input.add("BufferPages", comm_buffers_num);
        l1a_input.add("Port", l1a_port);
        l1a_input.add("Backend", netio_backend);
        l1a_input.add("DataNetwork", netio_network);
        l1a_input.add("TTCQueueSize", 100);
        l1a_input.add("UseCallback", arguments.count("netio-use-callback"));
        l1a_input.add("FelixBusTimeout", 1000);
        l1a_input.add("FelixBusInterface", "ZSYS_INTERFACE");
        l1a_input.add("FelixBusGroupName", "FELIX");
        l1a_input.add("FelixBusDirectory", felixbus_dir);

        ptree l1a_config;
        l1a_config.add("Type", "L1AInputHandler");
        l1a_config.add("Link", l1a_elink_id);
        l1a_config.add("CPU", cpu_1);
        l1a_config.add_child("InputMethod", l1a_input);

        if (l1a_elink_id != 0) {
            configuration.add_child("L1AHandler", l1a_config);
        }

        uint16_t modules_number = robs_number / robs_per_module;

        for (uint16_t g = 0; g < modules_number; ++g) {
            ptree module;

            input.put("Port", port + g);
            module.add_child("InputMethod", input);

            module.add("WorkersNumber", workers_number);
            module.add("CPU", cpu_1);
            if (arguments.count("l1a-per-module")) {
                l1a_config.put("Link", l1a_elink_id + g);
                l1a_config.put("CPU", cpu_1);
                l1a_config.put("InputMethod.Port", l1a_port + g);
                module.add_child("L1AHandler", l1a_config);
            }

            for (uint16_t r = 0; r < robs_per_module; ++r) {
                ptree rob;
                uint32_t ROB_id = g * robs_per_module + r;
                rob.add("Id", ROB_id);
                rob.add("CPU", cpu_1);

                ptree event_builder;
                event_builder.add("Type", event_builder_type);
                event_builder.add("BuildersNumber", builders_number);
                event_builder.add("MaxMessageSize", packet_size + 4);
                event_builder.add("BufferSize", buffer_size);
                event_builder.add("MinimumBufferSize", buffer_init_size);
                event_builder.add("L1AWaitTimeout", 1000);
                event_builder.add("ResynchTimeout", 1000);
                event_builder.add("RecoveryDepth", 10);
                event_builder.add("FlushBufferAtStop", true);
                event_builder.add("RODHeaderPresent", false);
                event_builder.add("DropCorruptedPackets", false);
                event_builder.add("ReadyQueueSize", 100000);
                rob.add_child("FragmentBuilder", event_builder);

                ptree data_channel;
                data_channel.add("Tag", (uint64_t)12345);
                for (size_t e = 0; e < links_number; ++e) {
                    ptree link;
                    uint64_t fid = (ROB_id<<24) + ((e/8)<<16) + ((e%8)<<8) + first_link_id;
                    link.add("FelixId", fid);
                    link.add("DetectorResourceId", (uint32_t)fid);
                    data_channel.add_child("Contains.InputLink", link);
                }

                ptree custom_proc;
                custom_proc.add("LibraryName", "libswrod_test_plugin.so");
                custom_proc.add("TrigInfoExtractor", "testTriggerInfoExtractor");
                custom_proc.add("DataIntegrityChecker", "testDataIntegrityChecker");
                custom_proc.add("ProcessorFactory", "createTestCustomProcessor");
                data_channel.add_child("CustomLib", custom_proc);
                rob.add_child("Contains", data_channel);

                ptree consumers;
                if (proc_number) {
                    ptree custom_processing;
                    custom_processing.add("Type", "ROBFragmentProcessor");
                    custom_processing.add("WorkersNumber", proc_number);
                    custom_processing.add("QueueSize", 100000);
                    custom_processing.add("DeferProcessing", false);
                    custom_processing.add("FlushBufferAtStop", false);
                    custom_processing.add("CPU", cpu_1);
                    consumers.add_child("Consumer", custom_processing);
                }

                if (HLT_threads_number) {
                    ptree hlt;
                    hlt.add("Type", "HLTRequestHandler");
                    hlt.add("IgnoreClearXId", true);
                    hlt.add("DataRequestTimeout", 1000000);
                    hlt.add("ClearTimeout", 1000000);
                    hlt.add("IOServices", 1);
                    hlt.add("DataServerThreads", HLT_threads_number);
                    hlt.add("MaxIndex", 500000);
                    hlt.add("FlushBufferAtStop", true);
                    hlt.add("QueueSize", 100000);
                    hlt.add("CPU", cpu_2);
                    consumers.add_child("Consumer", hlt);
                }

                if (shredders_per_ROB) {
                    ptree shredder;
                    shredder.add("Type", "ROBFragmentShredder");
                    shredder.add("WorkersNumber", shredders_per_ROB);
                    shredder.add("FlushBufferAtStop", true);
                    shredder.add("QueueSize", 100000);
                    shredder.add("CPU", cpu_2);
                    consumers.add_child("Consumer", shredder);
                }

                if (show_rates) {
                    ptree counter;
                    counter.add("Offset", robs_number - ROB_id - 1);
                    counter.add("Mode", show_rates);
                    counter.add("Type", "ROBFragmentCounter");
                    counter.add("QueueSize", 100000);
                    counter.add("FlushBufferAtStop", true);
                    consumers.add_child("Consumer", counter);
                }

                rob.add_child("Consumers", consumers);
                module.add_child("ROBs.ROB", rob);
            }
            configuration.add_child("Modules.Module", module);
        }


        ptree consumers;
        if (samplers_number) {
            ptree sampler;
            sampler.add("Type", "EventSampler");
            sampler.add("QueueSize", 100000);
            sampler.add("EventBufferSize", 100);
            sampler.add("WorkersNumber", samplers_number);
            sampler.add("SampleAll", false);
            sampler.add("SamplingRatio", 10);
            sampler.add("MaximumChannels", 10);
            sampler.add("FlushBufferAtStop", true);
            sampler.add("CPU", cpu_2);
            consumers.add_child("Consumer", sampler);
        }

        if (arguments.count("out-dir")) {
            ptree robs;
            for (size_t i = 0; i < robs_number; ++i) {
                ptree rob;
                rob.add("Id", i);
                robs.add_child("ROB", rob);
            }
            ptree file_writer;
            file_writer.add("Type", "FileWriter");
            file_writer.add("swrod.name", "swrod_test");
            file_writer.add("QueueSize", 1024);
            file_writer.add("IgnoreRecordingEnable", true);
            file_writer.add("FlushBufferAtStop", true);
            file_writer.add("EventStorage.DirectoryToWrite",
                    arguments["out-dir"].as<std::string>());
            file_writer.add("EventStorage.MaxEventsPerFile", 0);
            file_writer.add("EventStorage.MaxMegaBytesPerFile", 2560);
            file_writer.add_child("ROBs", robs);
            consumers.add_child("Consumer", file_writer);
        }

        if (shredders_number) {
            ptree shredder;
            shredder.add("Type", "ROBFragmentShredder");
            shredder.add("WorkersNumber", shredders_number);
            shredder.add("FlushBufferAtStop", false);
            shredder.add("QueueSize", 100000);
            shredder.add("CPU", cpu_2);
            consumers.add_child("Consumer", shredder);
        }

        configuration.add_child("Consumers", consumers);
    }

    if (arguments.count("config-out-file")) {
        try {
            write_json(arguments["config-out-file"].as<std::string>(), configuration);
        } catch (json_parser::json_parser_error & ex) {
            std::cerr << ex.what() << std::endl;
            return 1;
        }
    }

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    std::unique_ptr<Core> core;
    try {
        core.reset(new Core(configuration));
        core->connectToFelix();

        IPCPartition partition(partition_name);
        std::shared_ptr<monsvc::PublishingController> pc;

        if (arguments.count("publish-stats")) {
            pc.reset(new monsvc::PublishingController(partition, app_name));
            pc->add_configuration_rule(
               *monsvc::ConfigurationRule::from(
                       "Histogramming:.*/=>oh:(10,1,Histogramming," + app_name + ")"));
            pc->start_publishing();
        }

        RunParams rp;
        rp.recording_enabled = true;
        rp.run_number = run_number;
        core->runStarted(rp);

        while (signal_status == 0) {
            usleep(1000000);
            if (arguments.count("publish-stats")) {
                publishStatistics(*core.get());
            }
        }

        std::cout << "Received signal " << signal_status << std::endl;

        if (pc) {
            pc->stop_publishing();
        }
        core->runStopped();
        core->disconnectFromFelix();
    } catch (const ers::Issue & ex) {
        ers::fatal(ex);
        return 1;
    }
}
