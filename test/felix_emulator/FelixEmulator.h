/* -*- c++ -*-
 *  FelixEmulator.h
 *
 *  Created on: Mar 24, 2020
 */

#ifndef _SWROD_FELIXEMULATOR_H_
#define _SWROD_FELIXEMULATOR_H_

#include <memory>
#include <string>

#include <swrod/ROBFragmentBuilderBase.h>

#include "DataPublisher.h"

namespace swrod {
    class FelixEmulator : public ROBFragmentBuilderBase {
    public:
        FelixEmulator(const boost::property_tree::ptree& robConfig, const Core & core) :
            ROBFragmentBuilderBase(robConfig, core,
                [](const L1AInfo &){},
                [&](const InputLinkVector & links, const std::shared_ptr<DataInput> & input) {
                    return new DataPublisher(
                            "ROB-" + robConfig.get<std::string>("Id"),
                            links, robConfig, input);
                })
        { }

        void disable() override {}

        void disableLinks(std::vector<InputLinkId> & ) override {}
    };
}

#endif
