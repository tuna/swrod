/*
 * NetioSocket.h
 *
 *  Created on: Mar 24, 2020
 *      Author: kolos
 */

#ifndef _SWROD_NEIOSOCKET_H_
#define _SWROD_NEIOSOCKET_H_

#include <atomic>
#include <vector>

#include <boost/property_tree/json_parser.hpp>

#include <netio/netio.hpp>
#include <swrod/detail/NetioPacketHeader.h>

namespace swrod {

    class NetioSocket {
    public:
        NetioSocket(uint32_t page_size = 1048576, uint32_t n_pages = 128) :
                m_port(allocatePort()),
                m_total_count(0),
                m_total_size(0),
                m_header({0, 0, 0}),
                m_message({(const uint8_t*)&m_header, nullptr}),
                m_sizes({sizeof(m_header), 0}),
                m_context("posix"),
                m_socket(&m_context, m_port, createNetioConfig(page_size, n_pages)),
                m_io_thread([this]() { m_context.event_loop()->run_forever(); })
        { }

        ~NetioSocket() {
            m_context.event_loop()->stop();
            m_io_thread.join();
        }

        uint32_t getPort() {
            return m_port;
        }

        uint64_t getTotalCount() const {
            return m_total_count;
        }

        uint64_t getTotalSize() const {
            return m_total_size;
        }

        void publish(InputLinkId link, const uint8_t * data, uint32_t size) {
            m_header.size = size;
            m_header.elink_id = link;
            m_message[1] = data;
            m_sizes[1] = size;
            netio::message msg(m_message, m_sizes);
            m_socket.publish(link, msg);
            ++m_total_count;
            m_total_size += size;
        }

    private:
        static uint32_t readFromEnvironment(const char *name, uint32_t default_value) {
            uint32_t value = default_value;
            const char *env = ::getenv(name);
            if (env) {
                sscanf(env, "%u", &value);
            }
            return value;
        }

        static uint32_t allocatePort() {
            static std::atomic<uint32_t> port(
                    readFromEnvironment("SWROD_FELIX_EMULATOR_PORT", 33333));
            return port++;
        }

        static netio::sockcfg createNetioConfig(uint32_t page_size, uint32_t n_pages) {
            netio::sockcfg cfg = netio::sockcfg::cfg();
            cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION, n_pages);
            cfg(netio::sockcfg::PAGESIZE, page_size);
            cfg(netio::sockcfg::FLUSH_INTERVAL_MILLISECS, 1000);
            return cfg;
        }

    private:
        const uint32_t m_port;
        uint64_t m_total_count;
        uint64_t m_total_size;
        detail::NetioPacketHeader m_header;
        std::vector<const uint8_t*> m_message;
        std::vector<size_t> m_sizes;

        netio::context m_context;
        netio::publish_socket m_socket;
        std::thread m_io_thread;
    };
}

#endif /* _SWROD_NEIOSOCKET_H_ */
