/*
 * L1AcceptPublisher.h
 *
 *  Created on: Mar 24, 2020
 *      Author: kolos
 */

#ifndef FELIX_EMULATOR_L1ACCEPTPUBLISHER_H_
#define FELIX_EMULATOR_L1ACCEPTPUBLISHER_H_

#include <string>

#include <TriggerCommander/MasterTrigger.h>
#include <TriggerCommander/CommandedTrigger.h>
#include <RunControl/Common/OnlineServices.h>

#include <swrod/L1AInputHandler.h>

#include "test/core/InternalL1AGenerator.h"
#include "DataPublisher.h"

namespace swrod {
    class L1AcceptPublisher: public L1AInputHandler,
                             public DataPublisher,
                             public daq::trigger::MasterTrigger {
    public:
        L1AcceptPublisher(const boost::property_tree::ptree & config, const Core & core) :
            L1AInputHandler(config, core),
            DataPublisher("Trigger",
                        {{config.get<size_t>("Link"), config.get<size_t>("Link")}},
                        config, m_data_input),
                m_trigger(new daq::trigger::CommandedTrigger(
                   daq::rc::OnlineServices::instance().getIPCPartition(),
                   daq::rc::OnlineServices::instance().applicationName(),
                   this))
        {
            test::InternalL1AGenerator::instance()->pause();
        }

        ~L1AcceptPublisher() {
            m_trigger->_destroy();
        }

        uint32_t hold(const std::string &, bool extendedL1ID) override {
            ERS_LOG("Putting trigger on hold and retrieving last generated L1ID");
            return test::InternalL1AGenerator::instance()->pause();
        }


        void resume(const std::string &) override {
            ERS_LOG("Resuming trigger");
            test::InternalL1AGenerator::instance()->resume();
        }

        void increaseLumiBlock(uint32_t ) override { }

        void setPrescales(uint32_t , uint32_t ) override { }

        void setL1Prescales(uint32_t ) override { }

        void setHLTPrescales(uint32_t ) override { }

        void setLumiBlockInterval(uint32_t ) override { }

        void setMinLumiBlockLength(uint32_t ) override { }

        void setBunchGroup(uint32_t bg) override { }

        void setConditionsUpdate(uint32_t folderIndex, uint32_t lb) override { }

    private:
        daq::trigger::CommandedTrigger * m_trigger;
    };
}

#endif /* FELIX_EMULATOR_L1ACCEPTPUBLISHER_H_ */
