/*
 * netio_subscriber.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#include <string.h>

#include <cstdint>
#include <dlfcn.h>

#include <csignal>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <regex>
#include <string>
#include <thread>
#include <vector>

#include <boost/program_options.hpp>

#include <felixbus/bus.hpp>
#include <felixbus/elinktable.hpp>
#include <felixbus/felixtable.hpp>

#include <netio/netio.hpp>

using namespace netio;

typedef std::tuple<uint32_t, uint32_t, uint16_t> (*TriggerInfoExtractor)(const uint8_t *);

TriggerInfoExtractor info_extractor;

netio::sockcfg createNetioConfig(uint32_t page_size, uint32_t buffer_pages,
        netio::msg_rcvd_cb_t callback) {
    netio::sockcfg cfg = netio::sockcfg::cfg();
    cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION, buffer_pages);
    cfg(netio::sockcfg::PAGESIZE, page_size);
    cfg(netio::sockcfg::CALLBACK, (uint64_t) callback);
    return cfg;
}

bool subscribeToFelix(felix::bus::FelixTable & felix_table,
        felix::bus::ElinkTable & elink_table,
        netio::subscribe_socket & socket, const uint32_t& link) {
    std::string uuid = elink_table.getFelixId(link);
    std::string address = felix_table.getAddress(uuid);

    std::regex regex("tcp://([^:]+):([0-9]+)");
    std::smatch match_result;

    if (std::regex_match(address, match_result, regex) && match_result.size() == 3) {
        std::string host = match_result[1].str();
        uint32_t port = std::stoi(match_result[2].str());
        netio::endpoint endpoint(host, port);
        socket.subscribe(link, endpoint);
        std::clog << "Subscribed to '" << address << "' address for link = "
                << link << " uuid = '" << uuid << "'" << std::endl;
        return true;
    } else {
        std::clog << "ERROR: Subscription to '" << address << "' address for link = "
                << link << " uuid = '" << uuid << "' has FAILED" << std::endl;
        return false;
    }
}

struct ELink {
    ELink(uint32_t size = 0) : m_last_data_packet(size, 0) {
    }

    uint64_t m_counter = 0;
    uint32_t m_last_l1id = ~0;
    uint16_t m_last_bcid = ~0;
    std::vector<uint8_t> m_last_data_packet;
};

struct L1A {
    uint32_t m_l1id = ~0;
    uint16_t m_bcid = ~0;
};

const uint32_t c_l1a_buffer_size = 1000000;
std::vector<L1A> l1as(c_l1a_buffer_size);
std::vector<ELink> elinks;
ELink l1a_elink(100);

bool l1a_subscribed = false;
bool stopped = false;
bool verbose = false;
std::mutex mutex;

void dump(const uint8_t * data, uint16_t size)
{
    std::clog << std::hex << std::setfill('0');
    for (uint16_t i = 0; i < size; ++i) {
        std::clog << std::setw(2) << (int)data[i] << " ";
    }
    std::clog << std::dec << std::endl;
}

void l1aMessageReceived(uint8_t* data, size_t size, void*) {
    uint32_t l1id = *((uint32_t*)(data + 12));
    uint16_t bcid = *((uint16_t*)(data + 10)) & 0xfff;
    uint32_t linkid = *((uint32_t*)(data+4));

    if (verbose) {
        std::cout << "Got L1A packet from e-link 0x" << std::hex << linkid
                << ": packet #" << std::dec << l1a_elink.m_counter << std::hex
                << " with [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "]" << std::endl;
    }

    uint32_t expected_l1id = l1a_elink.m_last_l1id + 1;
    if (expected_l1id != l1id) {
        expected_l1id = (l1a_elink.m_last_l1id & 0xff000000) + 0x01000000;
        if (expected_l1id != l1id) {
            std::unique_lock lock(mutex);
            std::clog << "Error for l1A e-link 0x" << std::hex << linkid
                    << ": packet #" << std::dec << l1a_elink.m_counter << std::hex
                    << " has [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "], "
                    << " last [L1ID;BCID] = [0x" << l1a_elink.m_last_l1id << ";0x"
                    << l1a_elink.m_last_bcid << "]"
                    << std::endl;
            std::clog << "current packet : ";
            dump(data, size);
            std::clog << "previous packet: ";
            dump(&l1a_elink.m_last_data_packet[0], *((uint16_t*)&l1a_elink.m_last_data_packet[0]));
        }
    }
    l1as[l1a_elink.m_counter%c_l1a_buffer_size].m_l1id = l1id;
    l1as[l1a_elink.m_counter%c_l1a_buffer_size].m_bcid = bcid;
    memcpy(&l1a_elink.m_last_data_packet[0], data, size);
    l1a_elink.m_last_l1id = l1id;
    l1a_elink.m_last_bcid = bcid;
    ++l1a_elink.m_counter;
}

void dataMessageReceived(uint8_t* data, size_t size, void*) {
    uint32_t linkid = *((uint32_t*)(data+4));
    ELink & el = elinks[linkid];
    L1A & l1a = l1as[el.m_counter%c_l1a_buffer_size];

    try {
        auto [l1id, l1id_mask, bcid] = info_extractor(data + 8);

        if (verbose) {
            std::cout << "Got data packet from e-link 0x" << std::hex << linkid
                    << ": packet #" << std::dec << el.m_counter << std::hex
                    << " with [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "]" << std::endl;
        }

        while (el.m_counter >= l1a_elink.m_counter) {
            if (stopped) {
                return;
            }
            usleep(5);
        }
        if ((l1a.m_l1id & l1id_mask) != l1id
                || (bcid != 0xffff && bcid != l1a.m_bcid)) {
            std::unique_lock lock(mutex);
            std::clog << "Error for e-link 0x" << std::hex << linkid
                    << ": packet #" << std::dec << el.m_counter << std::hex
                    << " has [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "], "
                    << " last [L1ID;BCID] = [0x" << el.m_last_l1id << ";0x"
                    << el.m_last_bcid << "], "
                    << " L1A packet [L1ID;BCID] = [0x" << l1a.m_l1id << ";0x"
                    << l1a.m_bcid << "]" << std::endl;
            std::clog << "current packet : ";
            dump(data, size);
            std::clog << "previous packet: ";
            dump(&el.m_last_data_packet[0], *((uint16_t*)&el.m_last_data_packet[0]));
        }

        el.m_last_l1id = l1a.m_l1id;
        el.m_last_bcid = bcid;
        memcpy(&el.m_last_data_packet[0], data, size);
        el.m_counter++;
    }
    catch (std::exception & ex) {
        std::unique_lock lock(mutex);
        std::clog << "Got exception '" << ex.what() << "'"
                << " for e-link 0x" << std::hex << linkid
                << ": packet #" << std::dec << el.m_counter << std::hex
                << " last [L1ID;BCID] = [0x" << el.m_last_l1id << ";0x"
                << el.m_last_bcid << "], "
                << " L1A packet [L1ID;BCID] = [0x" << l1a.m_l1id << ";0x"
                << l1a.m_bcid << "]" << std::endl;
        std::clog << "current packet : ";
        dump(data, size);
        std::clog << "previous packet: ";
        dump(&el.m_last_data_packet[0], *((uint16_t*)&el.m_last_data_packet[0]));
    }
}

void dataMessageReceivedNoTTC(uint8_t* data, size_t size, void*) {
    uint32_t linkid = *((uint32_t*)(data+4));
    ELink & el = elinks[linkid];

    try {
        auto [l1id, l1id_mask, bcid] = info_extractor(data + 8);

        if (verbose) {
            std::cout << "Got data packet from e-link 0x" << std::hex << linkid
                    << ": packet #" << std::dec << el.m_counter << std::hex
                    << " with [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "]" << std::endl;
        }

        uint32_t expected_l1id = el.m_last_l1id + 1;
        if ((expected_l1id & l1id_mask) != l1id) {
            expected_l1id = (el.m_last_l1id & 0xff000000) + 0x01000000;
            if ((expected_l1id & l1id_mask) != l1id) {
                std::unique_lock lock(mutex);
                std::clog << "Error for e-link 0x" << std::hex << linkid
                        << ": packet #" << std::dec << el.m_counter << std::hex
                        << " has [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "], "
                        << " last [L1ID;BCID] = [0x" << el.m_last_l1id << ";0x"
                        << el.m_last_bcid << "]"
                        << std::endl;
                std::clog << "current packet : ";
                dump(data, size);
                std::clog << "previous packet: ";
                dump(&el.m_last_data_packet[0], *((uint16_t*)&el.m_last_data_packet[0]));
                expected_l1id = (expected_l1id & ~l1id_mask) | l1id;
            }
        }

        el.m_last_l1id = expected_l1id;
        el.m_last_bcid = bcid;
        memcpy(&el.m_last_data_packet[0], data, size);
        el.m_counter++;
    }
    catch (std::exception & ex) {
        std::unique_lock lock(mutex);
        std::clog << "Got exception '" << ex.what() << "'"
                << " for e-link 0x" << std::hex << linkid
                << ": packet #" << std::dec << el.m_counter << std::hex
                << " last [L1ID;BCID] = [0x" << el.m_last_l1id << ";0x"
                << el.m_last_bcid << "]" << std::endl;
        std::clog << "current packet : ";
        dump(data, size);
        std::clog << "previous packet: ";
        dump(&el.m_last_data_packet[0], *((uint16_t*)&el.m_last_data_packet[0]));
    }
}

volatile std::sig_atomic_t signal_status = 0;

void signal_handler(int s) {
    signal_status = s;
}

using namespace boost::program_options;

int main(int ac, char *av[])
{
    std::vector<uint32_t> eids;

    options_description description("Options");

    description.add_options()("help,h", "produce help message")
        ("netio-page-size,n", value<uint32_t>()->default_value(1048576), "Netio page size")
        ("netio-pages-number,m", value<uint32_t>()->default_value(32), "Netio pages number")
        ("plugin-lib,l", value<std::string>()->required(), "SW ROD plugin library name")
        ("packet-size,s", value<uint32_t>()->default_value(10000), "maximum packet size")
        ("trigger-extractor-name,t", value<std::string>()->required(), "Name of the trigger info extractor function")
        ("l1a-elink,a", value<uint32_t>()->default_value(0), "ID of L1A e-link, 0 for no L1A")
        ("verbose,v", "print information for any incoming packet")
        ("elinks,e", value<std::vector<uint32_t>>(&eids)->multitoken()->zero_tokens(),
                "List of e-links to subscribe");

    variables_map arguments;
    try {
        store(parse_command_line(ac, av, description), arguments);
        notify(arguments);
    } catch (error & ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "Test application for the 'swrod' package" << std::endl;
        description.print(std::cout);
        return 0;
    }

    uint32_t netio_page_size = arguments["netio-page-size"].as<uint32_t>();
    uint32_t netio_pages_number = arguments["netio-pages-number"].as<uint32_t>();
    uint32_t l1a_elink = arguments["l1a-elink"].as<uint32_t>();
    uint32_t packet_size = arguments["packet-size"].as<uint32_t>();
    std::string plugin = arguments["plugin-lib"].as<std::string>();
    std::string trigger_info_extractor_name = arguments["trigger-extractor-name"].as<std::string>();
    verbose = arguments.count("verbose");

    void * handle = dlopen(plugin.c_str(), RTLD_LAZY | RTLD_GLOBAL);

    if (!handle) {
        std::clog << "Can not load " << plugin << " shared library: " << dlerror() << std::endl;
        return 1;
    }

    void * fptr = dlsym(handle, trigger_info_extractor_name.c_str());
    if (!fptr) {
        std::clog << "Can not resolve " << trigger_info_extractor_name
                << " function: " << dlerror() << std::endl;
        return 1;
    }
    info_extractor = reinterpret_cast<decltype(info_extractor)>(fptr);

    felix::bus::Bus felix_bus;
    felix::bus::FelixTable felix_table;
    felix::bus::ElinkTable elink_table;
    felix_bus.setVerbose(false);
    felix_bus.setZyreVerbose(false);

    felix_bus.connect();
    felix_bus.subscribe("FELIX", &felix_table);
    felix_bus.subscribe("ELINKS", &elink_table);

    sleep(2);

    netio::context data_context("posix");
    netio::subscribe_socket data_socket(&data_context,
            createNetioConfig(netio_page_size, netio_pages_number,
                    l1a_elink ? &dataMessageReceived : &dataMessageReceivedNoTTC));
    std::thread data_thread([&data_context]() { data_context.event_loop()->run_forever();});

    int errors = 0;
    if(!eids.empty()) {
        uint32_t max = *std::max_element(eids.begin(), eids.end());
        elinks.resize(max+1);

        for (auto e : eids) {
            elinks[e] = ELink(packet_size);
            if (!subscribeToFelix(felix_table, elink_table, data_socket, e)) {
                ++errors;
                break;
            }
        }
    }

    netio::context l1a_context("posix");
    netio::subscribe_socket l1a_socket(&l1a_context,
            createNetioConfig(netio_page_size, netio_pages_number, &l1aMessageReceived));
    std::thread l1a_thread([&l1a_context]() { l1a_context.event_loop()->run_forever();});

    if (l1a_elink) {
        if (!subscribeToFelix(felix_table, elink_table, l1a_socket, l1a_elink)) {
            ++errors;
        } else {
            l1a_subscribed = true;
        }
    }

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    while (!errors && signal_status == 0) {
        sleep(1);
    }

    stopped = true;

    l1a_context.event_loop()->stop();
    l1a_thread.join();

    data_context.event_loop()->stop();
    data_thread.join();

    return errors;
}
