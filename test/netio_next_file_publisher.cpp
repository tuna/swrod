/*
 * netio_next_NSW_publisher.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#include <sched.h>
#include <time.h>

#include <csignal>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <boost/program_options.hpp>

#include <felixbus/bus.hpp>
#include <felixbus/elinktable.hpp>
#include <felixbus/felixtable.hpp>

#include "netio/netio.h"

#include <EventStorage/pickDataReader.h>
#include <eformat/FullEventFragment.h>

#include <swrod/detail/Barrier.h>
#include <swrod/detail/L1AMessage.h>

using namespace swrod::detail;
using namespace eformat::read;

class NetioPublisher {
public:

    NetioPublisher(bool is_L1A_thread, std::vector<ROBFragment> & data,
            swrod::test::Barrier &barrier, const std::string &hostname, uint32_t port,
            uint32_t page_size, uint32_t buffer_pages,uint32_t ecr_interval,
            uint32_t synch_interval, uint32_t subscriptions, int32_t cpu) :
            m_is_L1A_thread(is_L1A_thread),
            m_subscriptions_number(subscriptions),
            m_ecr_interval(ecr_interval),
            m_synch_interval(synch_interval),
            m_cpu(cpu),
            m_data(data),
            m_barrier(barrier)
{
        netio_init(&m_context);
        m_context.evloop.cb_init = nullptr;

        struct netio_buffered_socket_attr attr;
        attr.num_pages = buffer_pages;
        attr.pagesize = page_size;
        attr.watermark = attr.pagesize * 0.9;

        netio_publish_socket_init(&m_socket, &m_context, hostname.c_str(), port, &attr);
        m_socket.cb_subscribe = &NetioPublisher::on_subscribe;
        m_socket.cb_connection_closed = &NetioPublisher::on_disconnect;
        m_socket.usr = this;

        netio_signal_init(&m_context.evloop, &m_signal);

        m_thread = std::thread(std::bind(&NetioPublisher::run, this));
    }

    ~NetioPublisher() {
        netio_terminate(&m_context.evloop);
        m_thread.join();
    }

    uint64_t getTotalCount() const {
        return m_total_count;
    }

    uint64_t getTotalSize() const {
        return m_total_size;
    }

private:
    static int send(netio_subscription &sub, uint8_t *data, uint32_t size) {
        struct iovec iov[2];
        iov[0].iov_base = &sub.tag;
        iov[0].iov_len = sizeof(netio_tag_t);
        iov[1].iov_base = data;
        iov[1].iov_len = size;

        return netio_buffered_sendv((netio_buffered_send_socket*) sub.socket, iov, 2);
    }

    static void on_subscribe(struct netio_publish_socket *socket, netio_tag_t tag, void *addr,
            size_t addr_len) {
        NetioPublisher *object = (NetioPublisher*) socket->usr;
        object->subscribed();
    }

    static void on_disconnect(struct netio_publish_socket *socket) {
        NetioPublisher *object = (NetioPublisher*) socket->usr;
        object->disconnected();
    }

    static void c_GBT_flood(void *ptr) {
        NetioPublisher *object = (NetioPublisher*) ptr;
        object->GBT_flood();
    }
    static void s_GBT_flood(netio_publish_socket *ptr) {
        NetioPublisher *object = (NetioPublisher*) ptr->usr;
        object->GBT_flood();
    }

    static void c_floodL1A(void *ptr) {
        NetioPublisher *object = (NetioPublisher*) ptr;
        object->L1A_flood();
    }
    static void s_floodL1A(netio_publish_socket *ptr) {
        NetioPublisher *object = (NetioPublisher*) ptr->usr;
        object->L1A_flood();
    }

    void subscribed() {
        if (++m_subscriptions != m_subscriptions_number) {
            return;
        }

        if (!m_running) {
            std::cout << "received " << m_subscriptions
                    << " subscription(s), start publishing for ["
                    << m_socket.subscription_table.subscriptions[0].tag << ";"
                    << m_socket.subscription_table.subscriptions[m_subscriptions_number - 1].tag
                    << "] channels" << std::endl;

            m_barrier.reactivate();
            m_total_count = 0;
            m_total_size = 0;
            m_current_subscriber = 0;
            m_current_event = 0;
            m_current_buf_start = 0;
            m_current_buf_end = 0;
            m_l1a_packet = L1APacket();

            m_socket.cb_buffer_available =
                    m_is_L1A_thread ? &NetioPublisher::s_floodL1A : &NetioPublisher::s_GBT_flood;
            m_socket.usr = this;

            netio_signal_init(&m_context.evloop, &m_signal);
            m_signal.cb =
                    m_is_L1A_thread ? &NetioPublisher::c_floodL1A : &NetioPublisher::c_GBT_flood;
            m_signal.data = this;

            m_barrier.wait();
            m_running = true;
            netio_signal_fire(&m_signal);
        }
    }

    void disconnected() {
        if (m_running) {
            std::cout << "stop publishing" << std::endl;
            m_subscriptions = 0;
            m_running = false;
            m_barrier.deactivate();
        }
    }

    void run() {
        if (m_cpu >= 0) {
            cpu_set_t pmask;
            CPU_ZERO(&pmask);
            CPU_SET(m_cpu, &pmask);
            int s = sched_setaffinity(0, sizeof(pmask), &pmask);
            if (s != 0) {
                std::clog << "Setting affinity to CPU " << m_cpu << " failed with error = " << s
                        << std::endl;
            }
        }
        std::ostringstream out;
        out << "pub[" << m_subscriptions_number << "]";
        std::string s = out.str();
        pthread_setname_np(pthread_self(), s.c_str());

        netio_run(&m_context.evloop);
    }

    void GBT_flood();

    void FULL_flood();

    void L1A_flood();

private:
    struct L1APacket {
        uint8_t header = 0;
        L1AMessage message;
        uint64_t counter = 0;
    } __attribute__((packed));

    static const uint16_t BCID_MASK = 0x0fff;
    const bool m_is_L1A_thread;
    const uint32_t m_subscriptions_number;
    const uint32_t m_ecr_interval;
    const uint32_t m_synch_interval;
    const int32_t m_cpu;
    std::vector<ROBFragment> m_data;

    swrod::test::Barrier &m_barrier;
    netio_context m_context;
    netio_publish_socket m_socket;
    netio_signal m_signal;

    uint32_t m_current_subscriber = 0;
    uint32_t m_current_event = 0;
    const uint32_t * m_current_buf_start = 0;
    const uint32_t * m_current_buf_end = 0;
    uint32_t m_subscriptions = 0;
    uint64_t m_total_count = 0;
    uint64_t m_total_size = 0;
    bool m_running = false;

    L1APacket m_l1a_packet;
    std::thread m_thread;
};

void NetioPublisher::GBT_flood() {
    while (m_running) {
        for (; m_current_event < m_data.size(); ++m_current_event) {
            m_current_buf_start = m_current_buf_start
                    ? m_current_buf_start
                    : m_data[m_current_event].rod_data();
            m_current_buf_end = m_current_buf_end
                    ? m_current_buf_end
                    : (m_current_buf_start + m_data[m_current_event].rod_ndata());

            while (m_current_buf_start < m_current_buf_end) {
                uint32_t psize = *m_current_buf_start & 0xffff;
                *(uint32_t*)(m_current_buf_start + 1) = 0;

                int ret = send(m_socket.subscription_table.subscriptions[m_current_subscriber],
                        ((uint8_t*)m_current_buf_start) + 7, psize * 4 - 7);

                if (ret != NETIO_STATUS_OK) {
                    netio_buffered_publish_flush(&m_socket, 0, NULL);
                    netio_signal_fire(&m_signal);
                    return;
                }

                m_total_size += psize*4;
                ++m_total_count;
                m_current_buf_start += psize;
                ++m_current_subscriber;
            }
            m_current_subscriber = 0;
            m_current_buf_start = m_current_buf_end = 0;

            if ((++m_l1a_packet.counter % m_synch_interval) == 0) {
                netio_buffered_publish_flush(&m_socket, 0, NULL);
                m_barrier.wait();
            }
        }
        m_current_event = 0;
    }
}

void NetioPublisher::L1A_flood() {
    while (m_running) {

        for (; m_current_event < m_data.size(); ++m_current_event) {
            int ret = send(m_socket.subscription_table.subscriptions[0],
                    (uint8_t*) &m_l1a_packet, sizeof(m_l1a_packet));

            if (ret != NETIO_STATUS_OK) {
                netio_buffered_publish_flush(&m_socket, 0, NULL);
                netio_signal_fire(&m_signal);
                return;
            }

            m_l1a_packet.message.l1id++;
            m_l1a_packet.message.bcid = m_l1a_packet.message.l1id & BCID_MASK;
            if (m_l1a_packet.message.l1id == m_ecr_interval) {
                m_l1a_packet.message.l1id = 0;
                m_l1a_packet.message.ecr++;
            }

            if ((++m_l1a_packet.counter % m_synch_interval) == 0) {
                netio_buffered_publish_flush(&m_socket, 0, NULL);
                m_barrier.wait();
            }
        }
        m_current_event = 0;
    }
}

volatile std::sig_atomic_t signal_status = 0;

void signal_handler(int s) {
    signal_status = s;
}

using namespace boost::program_options;

int main(int ac, char *av[]) {
    options_description description("Options");

    description.add_options()("help,h", "produce help message")
            ("file,f",value<std::string>()->required(), "ATLAS raw file")
            ("port,P",value<uint32_t>()->default_value(12345), "port number")
            ("l1a-elink,a",value<uint64_t>()->default_value(7777), "L1A elink id, 0 for no L1A")
            ("l1a-subscribers,A",value<uint32_t>()->default_value(1), "Number of L1A e-link subscribers to expect")
            ("sync-interval,S",value<uint32_t>()->default_value(10000), "Synchronisation interval for data and TTC packets")
            ("host,H",value<std::string>()->default_value("127.0.0.1"), "Network interface for publishing")
            ("ecr-interval,E",value<uint32_t>()->default_value(500000), "ECR interval")
            ("ttc-netio-page-size,t", value<uint32_t>()->default_value(32000), "Netio page size for TTC-to-Host e-link in bytes")
            ("netio-page-size,u",value<uint32_t>()->default_value(256000), "Netio page for data e-links size in bytes")
            ("netio-pages-number,U", value<uint32_t>()->default_value(32), "Netio pages number")
            ("workers,w", value<uint32_t>()->default_value(1), "worker threads per ROB")
            ("cpu-affinity,C", "attach worker threads to specific CPU cores");

    variables_map arguments;
    try {
        store(parse_command_line(ac, av, description), arguments);
        notify(arguments);
    } catch (error &ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "Test application for the 'swrod' package" << std::endl;
        description.print(std::cout);
        return 0;
    }

    uint32_t ecr_interval = arguments["ecr-interval"].as<uint32_t>();
    uint32_t workers_number = arguments["workers"].as<uint32_t>();
    uint32_t sync_interval = arguments["sync-interval"].as<uint32_t>();
    uint64_t l1a_elink_id = arguments["l1a-elink"].as<uint64_t>();
    uint32_t l1a_subscribers = arguments["l1a-subscribers"].as<uint32_t>();
    uint32_t port_number = arguments["port"].as<uint32_t>();
    std::string host = arguments["host"].as<std::string>();
    std::string file = arguments["file"].as<std::string>();
    uint32_t ttc_netio_page_size = arguments["ttc-netio-page-size"].as<uint32_t>();
    uint32_t netio_page_size = arguments["netio-page-size"].as<uint32_t>();
    uint32_t netio_pages_number = arguments["netio-pages-number"].as<uint32_t>();
    bool cpu_affinity = arguments.count("cpu-affinity");

    felix::bus::FelixTable felixTable;
    felix::bus::ElinkTable elinkTable;

    std::vector<std::shared_ptr<NetioPublisher>> workers;
    uint32_t cpu = 1;

    DataReader * data_reader = pickDataReader(file);
    if (!data_reader || !data_reader->good()) {
        std::cerr << "Cannot read '" << file << "' data file."
                << std::endl;
        return 2;
    }

    std::vector<unsigned int*> data_buffer;
    std::map<uint32_t, std::vector<ROBFragment>> data;
    while (data_reader->good()) {
        try {
            char *buf;
            unsigned int size = 0;

            DRError err = data_reader->getData(size, &buf);
            if (err != EventStorage::DROK) {
                std::cerr << "Bad data file '" << file << "' is given" << std::endl;
                return 2;
            }
            FullEventFragment frag((unsigned int*)buf);
            frag.check();
            data_buffer.push_back((unsigned int*)buf);

            std::vector<ROBFragment> robs;
            frag.robs(robs);

            for (auto & r : robs) {
                data[r.rob_source_id()].push_back(r);
            }

        } catch (ers::Issue &ex) {
            ers::fatal(ex);
            return 2;
        }
    }

    if (data.empty()) {
        std::cerr << "Data file '" << file << "' contains no data" << std::endl;
        return 2;
    }

    swrod::test::Barrier barrier(workers_number * data.size() + (l1a_elink_id ? 1 : 0));

    for (auto & rob : data) {
        std::string uuid = felixTable.addFelix(
                "tcp://" + host + ":" + std::to_string(port_number), false, true,
                netio_pages_number, netio_page_size);

        const uint32_t * start = rob.second[0].rod_data();
        const uint32_t * end = start + rob.second[0].rod_ndata();
        uint32_t nlinks = 0;
        while (start < end) {
            uint32_t psize = *start & 0xffff;
            uint64_t fid = *(start + 1);
            elinkTable.addElink(fid, uuid);
            start += psize;
            ++nlinks;
        }

        workers.push_back(
                std::make_shared<NetioPublisher>(false, rob.second, barrier, host,
                        port_number++, netio_page_size, netio_pages_number,
                        ecr_interval, sync_interval, nlinks, cpu_affinity ? cpu++ : -1));
    }

    if (l1a_elink_id) {
        std::string uuid = felixTable.addFelix("tcp://" + host + ":" + std::to_string(port_number),
                false, true, netio_pages_number, ttc_netio_page_size);

        elinkTable.addElink(l1a_elink_id, uuid);
        workers.push_back(
                std::make_shared<NetioPublisher>(true, data.begin()->second, barrier, host,
                        port_number++, ttc_netio_page_size, netio_pages_number,
                        ecr_interval, sync_interval, l1a_subscribers, cpu_affinity ? cpu++ : -1));
    }

    felix::bus::Bus bus;
    bus.connect();
    bus.publish("FELIX", felixTable);
    bus.publish("ELINKS", elinkTable);

    uint64_t old_size = 0;
    uint64_t old_count = 0;

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    while (signal_status == 0) {
        usleep(1000000);

        struct timeval tv;
        gettimeofday(&tv, NULL);
        time_t curtime = tv.tv_sec;
        char buffer[16];
        strftime(buffer, 16, "%T.", localtime(&curtime));

        uint64_t size = std::accumulate(workers.begin(), workers.end(), (uint64_t) 0,
                [](uint64_t r, auto &s) {
                    return r + s->getTotalSize();
                });
        uint64_t count = std::accumulate(workers.begin(), workers.end(), (uint64_t) 0,
                [](uint64_t r, auto &s) {
                    return r + s->getTotalCount();
                });

        std::cout << buffer << std::setw(6) << std::setfill('0') << tv.tv_usec << " : " << size
                << " bytes sent, message rate = " << (count - old_count) / 1e6
                << " MHz, throughput = " << (size - old_size) / 1e6 << " MB/s" << std::endl;
        old_count = count;
        old_size = size;
    }

    barrier.deactivate();
    workers.clear();

    return 0;
}
