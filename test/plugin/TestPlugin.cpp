/*
 * TestPlugin.cpp
 *
 *  Created on: May 29, 2019
 *      Author: kolos
 */

#include <tuple>

#include <boost/property_tree/ptree.hpp>

#include <ers/ers.h>
#include <is/infoT.h>
#include <RunControl/Common/RunControlCommands.h>

#include <swrod/exceptions.h>
#include <swrod/CustomProcessor.h>
#include <swrod/GBTChunk.h>

#include "ProcessorStatistics.h"

extern "C"
std::tuple<uint32_t, uint32_t, uint16_t>
testTriggerInfoExtractor(const uint8_t * data, uint32_t size) {
    uint32_t mask = *(uint32_t*)(data + 8);
    if (!mask) {
        throw swrod::Exception(ERS_HERE, "Data fragment corrupted - L1ID bit-width == 0");
    }
    return std::tuple(
            *(uint32_t*)data & mask,
            mask,
            *(uint16_t*)(data + 6));
}

extern "C"
std::optional<bool> testDataIntegrityChecker(const uint8_t * data, uint32_t size) {
    uint16_t checksum = *(uint16_t*)(data + 4);
    if (checksum) {
        return checksum == 1 ? true : false;
    }
    return {};
}

class TestCustomProcessor: public swrod::CustomProcessor {
public:
    explicit TestCustomProcessor(uint64_t tag) {
        m_tag = tag;
    }

    ISInfo * getStatistics() override {
        return &m_stats;
    }

    void userCommand(const daq::rc::UserCmd & cmd) override {
        ERS_LOG("Command '" << cmd.commandName() << "' received");
    }

    void linkEnabled(const swrod::InputLinkId & link) override {
    }

    void linkDisabled(const swrod::InputLinkId & link) override {
    }

    void processROBFragment(swrod::ROBFragment & fragment) override {
        fragment.m_status_words.push_back(0x0a);
        fragment.m_status_words.push_back(0x0b);
        fragment.m_status_words.push_back(0x0c);
        fragment.m_rod_minor_version = 15;
        fragment.m_detector_type = 0x222;
        fragment.m_status_front = false;

        if (!m_tag) {
            return;
        }

        for (auto & block : fragment.m_data) {
            swrod::GBTChunk::Header header{0, 0, 0, 0xffffffff};

            if (!block.append(header, &m_tag, sizeof(m_tag))) {
                // The memory block is not large enough to accommodate extra data
                // Allocate a new memory block of sufficient size, copy the
                // original data there and add the custom tag at the end

                uint32_t size = block.dataSize() +
                        ((sizeof(m_tag) + sizeof(swrod::GBTChunk::Header))>>2);
                uint32_t * data = new uint32_t[size];
                memcpy(data, block.dataBegin(), block.dataSize());

                block = swrod::ROBFragment::DataBlock(data, size, block.dataSize());
                block.append(header, &m_tag, sizeof(m_tag));
            }
        }

        m_stats.m_fragments_processed++;
        m_stats.m_L1ID_distribution.add(fragment.m_l1id);
    }

private:
    ProcessorStatistics m_stats;
    uint64_t m_tag;
};

extern "C"
swrod::CustomProcessor * createTestCustomProcessor(
        const boost::property_tree::ptree & config) {

    using namespace boost::property_tree;

#ifndef ERS_NO_DEBUG
    ERS_DEBUG(1, "DataChannel contains "
            << config.get_child("Contains").count("InputLink") << " e-links");

    for (const ptree::value_type & it : config.get_child("Contains")) {
        ERS_DEBUG(2, "[" << it.second.get<uint64_t>("FelixId") << ":"
                << it.second.get<uint32_t>("DetectorResourceId") << "]");
    }
#endif

    return new TestCustomProcessor(config.get<uint32_t>("Tag", 0));
}
