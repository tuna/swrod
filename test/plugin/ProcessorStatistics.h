/*
 * ProcessorStatistics.h
 *
 *  Created on: Oct 24, 2019
 *      Author: kolos
 */

#ifndef TEST_PLUGIN_PROCESSORSTATISTICS_H_
#define TEST_PLUGIN_PROCESSORSTATISTICS_H_

class Distribution : public ISInfo {
public:

    static const ISType & type() {
        static const ISType type_ = Distribution().ISInfo::type();
        return type_;
    }

    Distribution() :
            ISInfo("Distribution") {
        initialize();
    }

    ~Distribution() {
    }

    void add(uint32_t v) {
        m_value[v/m_bin_width]++;
    }

    const uint32_t m_bins_number = 10;
    const uint32_t m_bin_width = uint32_t(-1)/m_bins_number;
    std::vector<uint64_t> m_value;

protected:
    void publishGuts(ISostream & out) override {
        out << m_value;
    }

    void refreshGuts(ISistream & in) override {
        in  >> m_value;
    }

private:
    void initialize() {
        m_value.resize(m_bins_number, 0);
    }
};

class ProcessorStatistics : public ISInfo {
public:

    static const ISType & type() {
        static const ISType type_ = ProcessorStatistics().ISInfo::type();
        return type_;
    }

    ProcessorStatistics() :
            ISInfo("ProcessorStatistics") {
        initialize();
    }

    ~ProcessorStatistics() {
    }

    uint64_t m_fragments_processed;
    uint64_t m_errors;
    Distribution m_L1ID_distribution;

protected:

    void publishGuts(ISostream & out) override {
        out << m_fragments_processed << m_errors << m_L1ID_distribution;
    }

    void refreshGuts(ISistream & in) override {
        in >> m_fragments_processed >> m_errors >> m_L1ID_distribution;
    }

private:
    void initialize() {
        m_fragments_processed = m_errors = 0;
    }
};

#endif /* TEST_PLUGIN_PROCESSORSTATISTICS_H_ */
