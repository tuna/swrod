/*
 * memcpy_test.cpp
 *
 *  Created on: Nov 18, 2021
 *      Author: kolos
 */

#include <chrono>
#include <cstdint>
#include <cstring>
#include <functional>
#include <iostream>
#include <queue>

#include <swrod/detail/tsl/robin_map.h>
#include <swrod/detail/concurrentqueue.h>

__attribute__((noinline))
uint32_t extract_l1id(const char * p, uint32_t size) {
    if (size) {
        return *(uint32_t*)p;
    }
    return 0;
}

struct Input {
    typedef std::function<void(uint64_t , const char * , uint32_t , uint32_t)> Callback;

    Input (Callback cb, uint32_t chunk_size) :
        m_chunk_size(chunk_size), m_cb(cb) {
        m_buffer = new char[m_buffer_size];
        memset(m_buffer, 'x', m_buffer_size);
    }

    void subscribe(uint64_t fid) {
        m_fids.push_back(fid);
    }

    void run(uint32_t triggers_number) {
        char * p = m_buffer;
        char * end = p + m_buffer_size - m_chunk_size;
        for (uint32_t l1id = 0; l1id < triggers_number; ++l1id) {
            for (auto f : m_fids) {
                *(uint32_t*)p = l1id;
                m_cb(f, p, m_chunk_size, 0);
                if (p < end) {
                    p += m_chunk_size;
                } else {
                    p = m_buffer;
                }
            }
        }
    }

private:
    const uint32_t m_chunk_size;
    const uint32_t m_buffer_size = 256000;
    std::vector<uint64_t> m_fids;
    Callback m_cb;
    char * m_buffer;
};

namespace args = std::placeholders;

typedef uint32_t (*ExtractL1IdFunction)(const char * , uint32_t );

struct Builder {
    struct Fragment {
        char * m_data;
        uint32_t m_chunk_counter = 0;
    };

    Builder(uint32_t fids_number, uint32_t chunk_size, uint32_t buffer_size,
            ExtractL1IdFunction extractor) :
        m_fids_number(fids_number),
        m_buffer_size(buffer_size),
        m_fragment_size(fids_number * chunk_size),
        m_input(std::bind(&Builder::receive, this, args::_1, args::_2, args::_3, args::_4),
                chunk_size),
        m_extractor(extractor)
    {
        for (uint32_t i = 0; i < m_fids_number; ++i) {
            uint64_t fid = i*i*i + i;
            m_input.subscribe(fid);
            m_bookkeeper.insert(std::make_pair(fid, 0));
        }
        m_buffer.resize(m_buffer_size);
        for (auto & b : m_buffer) {
            b.m_data = new char[m_fragment_size];
        }
    }

    void run(uint32_t triggers_number) {
        m_input.run(triggers_number);
    }

    void receive(uint64_t fid, const char * data, uint32_t size, uint32_t status) {
        uint32_t l1id = m_extractor(data, size);
        Bookkeeper::iterator it = m_bookkeeper.find(fid);
        if (it != m_bookkeeper.end()) {
            it.value()++;
        } else {
            std::cerr << fid << " is not found" << std::endl;
        }
        Fragment & f = m_buffer[l1id % m_buffer_size];
        memcpy(f.m_data + f.m_chunk_counter * size, data, size);
        m_bytes += size;
        if (++f.m_chunk_counter == m_fids_number) {
            fragmentReady(f);
            f.m_chunk_counter = 0;
        }
    }

    void fragmentReady(Fragment & f) {
        m_ready.push(Fragment(f));
        f.m_data = new char[m_fragment_size];
        f.m_chunk_counter = 0;
        ++m_fragments;
        if (m_ready.size() > 10000) {
            delete [] m_ready.front().m_data;
            m_ready.pop();
        }
    }

    uint64_t getBytes() const {
        return m_bytes;
    }

    uint64_t getChunks() const {
        uint64_t chunks = 0;
        auto it = m_bookkeeper.begin();
        for ( ; it != m_bookkeeper.end(); ++it) {
            chunks += it->second;
        }
        return chunks;
    }

    uint64_t getFragments() const {
        return m_fragments;
    }

private:
    struct Hash {
        size_t operator()(uint64_t x) const noexcept {
            x = ((x >> 17) ^ x) * 0x45d9f3b;
            return (x >> 17) ^ x;
        }
    };

    typedef tsl::robin_map<uint64_t, uint64_t, Hash> Bookkeeper;

    const uint32_t m_fids_number;
    const uint32_t m_buffer_size;
    const uint32_t m_fragment_size;
    Input m_input;
    std::vector<Fragment> m_buffer;
    std::queue<Fragment> m_ready;
    Bookkeeper m_bookkeeper;
    ExtractL1IdFunction m_extractor;
    uint64_t m_bytes = 0;
    uint64_t m_fragments = 0;
};

using namespace std::chrono;
int main(int argc, char ** argv) {

    Builder b(12, 500, 10000, extract_l1id);

    time_point<system_clock> start(system_clock::now());

    uint32_t iterations = argc > 1 ? std::stoi(argv[1]) : 1;
    for (uint32_t i = 0; i < iterations; ++i) {
        b.run(1000000);
    }

    time_point<std::chrono::system_clock> stop(system_clock::now());

    uint64_t total = duration_cast<milliseconds>(stop - start).count()/iterations;

    std::cout << b.getFragments()/iterations << " fragments (" << b.getChunks()/1e6/iterations
            << " MChunks) of total size "
            << b.getBytes()/1e9/iterations << " GB have been built in "
            << total << " milliseconds" << std::endl;

}
