/*
 * DataInputHandlerBase.cpp
 *
 *  Created on: Nov 6, 2019
 *      Author: kolos
 */

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/DataInputHandlerBase.h>

namespace swrod {
    template<class T> extern
    std::ostream& operator<<(std::ostream &out, const std::vector<T> &v);
}

using namespace swrod;
using namespace boost::property_tree;

DataInputHandlerBase::DataInputHandlerBase(const std::string & id, const InputLinkVector & links,
        const ptree & config, const std::shared_ptr<DataInput> & input, detail::InputCallback cb) :
    m_id(id),
    m_links_number(links.size()),
    m_callback(cb),
    m_input(input)
{
    for (auto & ll : links) {
        m_links.emplace_back(ll.first, ll.second);
    }
}

DataInputHandlerBase::~DataInputHandlerBase() {
    ;
}

void DataInputHandlerBase::runStarted(const RunParams & run_params) {
    ERS_LOG("Run #" << run_params.run_number << " is being started");

    if (m_running) {
        ERS_LOG("Data input handler is already running");
        return;
    }

    for (auto & l : m_links) {
        l.reset();
    }
    m_running = true;
    m_input->runStarted(run_params);

    ERS_LOG("Start of run #" << run_params.run_number << " has been processed");
}

void DataInputHandlerBase::runStopped() {
    ERS_LOG("Run is being stopped");

    if (!m_running) {
        ERS_LOG("Data input handler '" << m_id << "' is not running");
        return;
    }

    m_input->runStopped();
    m_running = false;

    ERS_LOG("End of run has been processed");
}

void DataInputHandlerBase::subscribeToFelix() {
    ERS_LOG("Subscribing to all input links");

    for (Link & link : m_links) {
        if (link.m_enabled) {
            m_input->subscribe(link.m_fid, m_callback.bind(link));
            ERS_DEBUG(1, "Subscribed to the 0x" << std::hex << link.m_fid << " input link");
        }
    }

    ERS_LOG("Subscribed to " << m_links.size() << " input links");
}

void DataInputHandlerBase::unsubscribeFromFelix() {
    ERS_LOG("Unsubscribing from all input links");

    for (Link & link : m_links) {
        if (link.m_enabled) {
            m_input->unsubscribe(link.m_fid);
            ERS_DEBUG(1, "Unsubscribed from the 0x" << std::hex << link.m_fid << " input link");
        }
    }

    ERS_LOG("Unsubscribed from " << m_links.size() << " input links");
}

void DataInputHandlerBase::disable() {
    ERS_LOG("Disable '" << m_id << "' input handler");

    m_input->executeSynchronousAction([this]{
        unsubscribeFromFelix();
        disabled();
    });

    ERS_LOG("'" << m_id << "' input handler has been disabled");
}

void DataInputHandlerBase::disableLinks(std::vector<InputLinkId> & link_ids) {
    ERS_LOG("Disable links " << link_ids << " for '" << m_id << "' input handler");

    m_input->executeSynchronousAction([this, &link_ids]{
        auto id = link_ids.begin();
        while (id != link_ids.end()) {
            InputLinks::iterator link = std::find_if(m_links.begin(), m_links.end(),
                    [&id](Link & link) { return link.m_fid == *id; });
            if (link == m_links.end()) {
                ERS_LOG("Link 0x" << std::hex << *id << " is not handled by this worker thread");
                ++id;
                continue;
            }

            m_input->unsubscribe(link->m_fid);
            link->m_enabled = false;
            --m_links_number;
            linkDisabled(*link);

            ERS_LOG("Link 0x" << std::hex << *id << " has been disabled, total number of links is "
                    << std::dec << m_links_number);
            id = link_ids.erase(id);
        }
    });

    ERS_LOG("Links " << link_ids << " have not been disabled");
}

void DataInputHandlerBase::resynchAfterRestart(uint32_t lastL1ID) {
    ERS_LOG("Resynch '" << m_id << "' worker with last L1ID = 0x" << std::hex << lastL1ID);

    for (Link & link : m_links) {
        link.m_expected_l1id = (lastL1ID & 0xff000000) + 0x01000000;
        ERS_DEBUG(2, "Input link " << std::hex << link.m_fid << " has been re synchronised");
    }

    ERS_LOG("Worker '" << m_id << "' has been resynchronised");
}

void DataInputHandlerBase::enable(uint32_t lastL1ID, uint64_t triggersNumber) {
    ERS_LOG("Enable worker '" << m_id << "' input handler"
            << " with last L1ID = 0x" << std::hex << lastL1ID
            << " triggers counter = " << std::dec << triggersNumber);

    for (Link & link : m_links) {
        if (link.m_enabled) {
            link.m_packets_counter = triggersNumber;
            link.m_expected_l1id = (lastL1ID & 0xff000000) + 0x01000000;
            m_input->subscribe(link.m_fid, m_callback.bind(link));
            ERS_DEBUG(2, "Subscribed to the " << std::hex << link.m_fid << " input link");
        }
    }
    enabled(triggersNumber);

    ERS_LOG("Input handler '" << m_id << "' has been re enabled");
}

void DataInputHandlerBase::enableLinks(std::vector<InputLinkId> & link_ids,
        uint32_t lastL1ID, uint64_t triggersNumber) {

    ERS_LOG("Enable links " << link_ids << " for '" << m_id << "' input handler"
            << " with last L1ID = 0x" << std::hex << lastL1ID
            << " triggers number = " << std::dec << triggersNumber);

    auto id = link_ids.begin();
    while (id != link_ids.end()) {
        InputLinks::iterator link = std::find_if(m_links.begin(), m_links.end(),
                [&id](Link & link) { return link.m_fid == *id; });
        if (link == m_links.end()) {
            ERS_LOG("Link 0x" << std::hex << *id << " is not handled by this worker thread");
            ++id;
            continue;
        }

        try {
            m_input->subscribe(link->m_fid, m_callback.bind(*link));
            link->m_enabled = true;
            link->m_packets_counter = triggersNumber;
            link->m_expected_l1id = (lastL1ID & 0xff000000) + 0x01000000;
            ++m_links_number;

            linkEnabled(*link);
            ERS_LOG("Link 0x" << std::hex << *id
                    << " has been enabled, expected L1ID = 0x" << link->m_expected_l1id
                    << " packets counter = " << std::dec << link->m_packets_counter
                    << " total number of links is " << m_links_number);
            id = link_ids.erase(id);
        }
        catch (swrod::Exception & ex) {
            ers::error(LinkRecoveryFailed(ERS_HERE, link->m_fid, "subscription failed", ex));
            ++id;
        }
    }

    ERS_LOG("Links " << link_ids << " have not been enabled");
}
