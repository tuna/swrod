/*
 * ROBFragmentProvider.cpp
 *
 *  Created on: Jun 20, 2019
 *      Author: kolos
 */

#include <swrod/ROBFragmentConsumer.h>
#include <swrod/ROBFragmentProvider.h>

using namespace swrod;

void ROBFragmentProvider::addConsumer(
        const std::shared_ptr<ROBFragmentConsumer> & consumer) {
    if (m_consumer) {
        if (m_consumer != consumer) {
            m_consumer->addConsumer(consumer);
        }
    } else {
        m_consumer = consumer;
        m_function = std::bind(&ROBFragmentConsumer::insertROBFragment,
                m_consumer, std::placeholders::_1);
    }
}
