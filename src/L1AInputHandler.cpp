/*
 * L1AInputHandler.cpp
 *
 *  Created on: Mar 3, 2021
 *      Author: kolos
 */

#include <swrod/DataInput.h>
#include <swrod/Factory.h>
#include <swrod/L1AInputHandler.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    std::shared_ptr<DataInput> createInput(const ptree &config, const Core & core) {
        ptree inConfig(PTREE_GET_CHILD(config, "InputMethod"));
        inConfig.put("CPU", PTREE_GET_VALUE(config, std::string, "CPU"));
        return Factory<DataInput>::instance().create(
                PTREE_GET_VALUE(inConfig, std::string, "Type"), inConfig, core);
    }
}

L1AInputHandler::L1AInputHandler(
        const boost::property_tree::ptree &config, const Core & core) :
            m_data_input(createInput(config, core))
{
}
