/*
 * DataInput.cpp
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#include <swrod/exceptions.h>
#include <swrod/DataInput.h>

using namespace swrod;

void DataInput::subscribe(const InputLinkId &link, const DataCallback &callback) {
    if (m_callbacks.find(link) != m_callbacks.end()) {
        throw SubscriptionException(ERS_HERE, link, "another subscription already exists");
    }
    m_callbacks.insert(std::make_pair(link, callback));
    subscribeToFelix(link);
    ERS_DEBUG(2, "Subscribed to the 0x" << std::hex << link << " input link");
}

void DataInput::unsubscribe(const InputLinkId &link) {
    auto it = m_callbacks.find(link);
    if (it != m_callbacks.end()) {
        m_callbacks.erase(it->first);
        unsubscribeFromFelix(link);
        ERS_DEBUG(2, "Unsubscribed from the 0x" << std::hex << link << " input link");

        if (m_last_id == link) {
            m_last_id = InputLinkId(-1);
        }
    } else {
        ERS_LOG("No subscription found for the 0x" << std::hex << link << " input link");
    }
}

void DataInput::executeActions() {
    ERS_DEBUG(1, "Executing actions");
    std::unique_lock lock(m_actions_mutex);
    while (!m_actions.empty()) {
        Action action = m_actions.front();
        action();
        m_actions.pop();
    }
    m_actions_condition.notify_one();
    ERS_DEBUG(1, "Actions have been executed");
}

void DataInput::executeSynchronousAction(const Action &action) {
    ERS_DEBUG(1, "Executing synchronous actions");

    std::unique_lock lock(m_actions_mutex);
    m_actions.push(action);

    lock.unlock();
    actionsPending();
    lock.lock();

    ERS_DEBUG(1, "Waiting for synchronous actions execution");
    // Wait before completion
    m_actions_condition.wait(lock, [this]() {
        return m_actions.empty();
    });

    ERS_DEBUG(1, "Done");
}
