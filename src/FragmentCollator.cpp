#include<iostream>

#include "swrod/FragmentCollator.h"
#include "swrod/ROBFragment.h"
#include "swrod/exceptions.h"

using namespace swrod;
using namespace swrod::helper;

FragmentCollator::FragmentCollator(): m_expectedFragsPerL1(0),
                                      m_maxIndexSize(0),m_closed(false){
}

void FragmentCollator::close(){
   m_closed=true;
   m_condition.notify_all();
}

void FragmentCollator::reset(){
   std::lock_guard<std::mutex> lock{m_mapMutex};
   std::cout << "FragmentCollator::reset() Clearing " << m_fragMap.size()
             << " entries from fragment map" << std::endl;
   m_fragMap.clear();
   m_closed=false;
}

bool FragmentCollator::insert(std::shared_ptr<ROBFragment> fragment) {
   std::unique_lock<std::mutex> lock{m_mapMutex};
   unsigned int sourceId=fragment->m_source_id;
   if (m_disabledRobs.find(sourceId)!=m_disabledRobs.end()) {
      return false;
   }

   unsigned int l1Id=fragment->m_l1id;

   auto iter=m_fragMap.find(l1Id);
   if (iter==m_fragMap.end()) {
      // Check that we are allowed to add another entry
      if (m_maxIndexSize>0 && m_fragMap.size()>=m_maxIndexSize) {
         // Wait for condition variable set by clear
         //std::cout << "Index full waiting for space...";std::cout.flush();
         m_condition.wait(lock,
                          [this](){
                             return (m_fragMap.size()<m_maxIndexSize || m_closed);
                          }
            );
         //std::cout << "space availble, updating index" << std::endl;
      }
      m_fragMap[l1Id]=Record();
      iter=m_fragMap.find(l1Id);
   }

   if (iter->second.m_source.find(sourceId)!=iter->second.m_source.end()) {
      ers::warning(FragmentOverwriteIssue(ERS_HERE,sourceId,l1Id));
      iter->second.m_frag.clear();
      iter->second.m_source.clear();
   }
   iter->second.m_frag.push_back(fragment);
   iter->second.m_source.insert(sourceId);

   unsigned int partsExpected=m_expectedFragsPerL1;
   for (auto rob : m_disabledRobs) {
      if (iter->second.m_source.find(rob)!=iter->second.m_source.end()) {
         partsExpected++;
      }
   }

   bool complete=(iter->second.m_frag.size()>=partsExpected);
   m_latestL1Id[sourceId]=l1Id;

   return complete;
}

std::vector<unsigned int> FragmentCollator::latestL1() const {
   std::vector<unsigned int> result;
   for (auto l1 : m_latestL1Id) {
      result.push_back(l1.second);
   }
   return result;
}

void FragmentCollator::clear(unsigned int l1Id) {
   std::unique_lock<std::mutex> lock{m_mapMutex};
   auto iter=m_fragMap.find(l1Id);
   if (iter!=m_fragMap.end()) {
      m_fragMap.erase(iter);
   }
   lock.unlock();
   m_condition.notify_one();
}

std::vector<unsigned int> FragmentCollator::clear(std::vector<unsigned int>& l1IdVec) {
   std::vector<unsigned int> failures;
   std::unique_lock<std::mutex> lock{m_mapMutex};
   for (auto l1Id : l1IdVec) {
      auto iter=m_fragMap.find(l1Id);
      if (iter!=m_fragMap.end()) {
         m_fragMap.erase(iter);
      }
      else {
         failures.push_back(l1Id);
      }
   }
   lock.unlock();
   m_condition.notify_all();
   return failures;
}


void FragmentCollator::enable(unsigned int robId){
   std::lock_guard<std::mutex> lock{m_mapMutex};
   m_disabledRobs.erase(robId);
   m_expectedFragsPerL1++;
}


void FragmentCollator::disable(unsigned int robId){
   std::lock_guard<std::mutex> lock{m_mapMutex};
   m_disabledRobs.insert(robId);
   m_expectedFragsPerL1--;
}


const std::vector<std::shared_ptr<ROBFragment>>&
FragmentCollator::get(unsigned int l1Id){
   std::lock_guard<std::mutex> lock{m_mapMutex};
   auto mapIter=m_fragMap.find(l1Id);
   if (mapIter!=m_fragMap.end()) {
      return mapIter->second.m_frag;
   }
   else {
      return m_emptyEvent;
   }
}

std::vector<std::shared_ptr<ROBFragment>>
FragmentCollator::get(unsigned int l1Id,std::set<unsigned int>* robs){
   std::vector<std::shared_ptr<ROBFragment>> result;

   std::lock_guard<std::mutex> lock{m_mapMutex};
   auto mapIter=m_fragMap.find(l1Id);
   if (mapIter!=m_fragMap.end()) {
      for (auto frag: mapIter->second.m_frag){
         auto robIter=robs->find(frag->m_source_id);
         if (robIter!=robs->end()){
            result.push_back(frag);
            robs->erase(robIter);
         }
      }
   }
   return result;
}

std::vector<std::shared_ptr<ROBFragment>>
FragmentCollator::take(unsigned int l1Id){
   std::lock_guard<std::mutex> lock{m_mapMutex};
   auto mapIter=m_fragMap.find(l1Id);
   std::vector<std::shared_ptr<ROBFragment>> fragVec;
   if (mapIter!=m_fragMap.end()) {
      fragVec=std::move(mapIter->second.m_frag);
      m_fragMap.erase(mapIter);
   }
   return fragVec;
}

void FragmentCollator::dump() const {
   std::cout << "FragmentCollator: " << m_fragMap.size()
             << " events in map\n";
   for (auto iter : m_fragMap) {
      std::cout << " event " << iter.first
                << ": " << iter.second.m_source.size() << " parts\n";
   }
}
