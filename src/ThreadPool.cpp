/*
 * ThreadPool.cpp
 *
 *  Created on: Jul 16, 2019
 *      Author: kolos
 */

#include <ers/ers.h>

#include <swrod/detail/ThreadPool.h>

using namespace swrod::detail;

ThreadPool::ThreadPool(uint16_t num,
        const ThreadFunction & function,
        const std::string& name, const std::string & cpu_set) :
        m_name(name)
{
    for (uint16_t i = 0; i < num; ++i) {
        m_workers.push_back(std::make_unique<WorkerThread>(function, m_name, cpu_set));
    }
}

ThreadPool::ThreadPool(const std::vector<ThreadFunction> & functions,
        const std::string & name, const std::string & cpu_set) :
        m_name(name)
{
    for (auto & f : functions) {
        m_workers.push_back(std::make_unique<WorkerThread>(f, m_name, cpu_set));
    }
}

ThreadPool::~ThreadPool() {
    stop();
}

void ThreadPool::start() {
    ERS_DEBUG(1, "Starting the '" << m_name << "' thread pool");
    for (auto & w : m_workers) {
        w->start();
    }
    ERS_DEBUG(1, "The '" << m_name << "' thread pool has been started");
}

void ThreadPool::stop() {
    ERS_DEBUG(1, "Stopping the '" << m_name << "' thread pool");
    for (auto & w : m_workers) {
        w->stop();
    }
    ERS_DEBUG(1, "The '" << m_name << "' thread pool has been stopped");
}
