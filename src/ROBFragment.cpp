/*
 * ROBFragment.cpp
 *
 *  Created on: Jun 4, 2019
 *      Author: kolos
 */

#include <iostream>
#include <numeric>

#include <eformat/Status.h>
#include <eformat/HeaderMarker.h>
#include <eformat/Version.h>
#include <eformat/checksum.h>

#include <swrod/ROBFragment.h>

using namespace swrod;

namespace {
    template <class T, uint32_t N>
    constexpr uint32_t c_array_size(const T (&array)[N]) noexcept
    {
        return N;
    }
}

ROBFragment::ROBFragment(uint32_t source_id, uint32_t l1id, uint16_t bcid, uint32_t trigger_type,
    uint32_t rob_status, uint16_t missed_packets, uint16_t corrupted_packets,
    std::vector<ROBFragment::DataBlock> && data, bool rod_header_present) :
    m_source_id(source_id), m_l1id(l1id), m_bcid(bcid),
    m_trigger_type(trigger_type),
    m_missed_packets(missed_packets), m_corrupted_packets(corrupted_packets),
    m_rod_header_present(rod_header_present),
    m_detector_type(0), m_rod_minor_version(0),
    m_status_front(true),
    m_data(std::move(data)),
    m_rob_header{
        eformat::ROB,
        0, // sizeof this event, to be set later
        c_array_size(m_rob_header),
        eformat::DEFAULT_VERSION,
        m_source_id,
        2, // number of status words is always 2
        rob_status,
        0, // this status word will be set by the serialise function
        eformat::NO_CHECKSUM
    },
    m_rod_header{
        eformat::ROD,
        c_array_size(m_rod_header),
        eformat::DEFAULT_ROD_VERSION,
        m_source_id,
        0, // run number, to be set later
        m_l1id,
        m_bcid,
        m_trigger_type,
        0 // detector type, to be set later
    },
    m_rod_trailer{
        0, // number of status words, to be set later
        0, // number of data words, to be set later
        eformat::STATUS_FRONT
    },
    m_pp([](){})
{ }

ROBFragment::ROBFragment(uint32_t source_id, uint32_t l1id, uint32_t status_word) :
    m_source_id(source_id), m_l1id(l1id), m_bcid(-1),
    m_trigger_type(0), m_missed_packets(0), m_corrupted_packets(0),
    m_rod_header_present(false),
    m_detector_type(0),
    m_rod_minor_version(0),
    m_status_front(true),
    m_rob_header{
        eformat::ROB,
        0, // sizeof event, to be set later
        c_array_size(m_rob_header),
        eformat::DEFAULT_VERSION,
        m_source_id,
        2, // number of status words is always 2
        status_word,
        0, // this status word will be set by the serialise function
        eformat::NO_CHECKSUM
    },
    m_rod_header{
        eformat::ROD,
        c_array_size(m_rod_header),
        eformat::DEFAULT_ROD_VERSION,
        m_source_id,
        0, // run number, to be set later
        m_l1id,
        m_bcid,
        m_trigger_type,
        0 // detector type, to be set later
    },
    m_rod_trailer{
        0, // number of status words, to be set later
        0, // number of data words, to be set later
        eformat::STATUS_FRONT
    },
    m_pp([](){})
{ }

void ROBFragment::setPreProcessor(const std::function<void (ROBFragment &)> & pp) {
    std::unique_lock lock(m_mutex);
    m_pp = [pp, this](){
        pp(*this);
        m_pp = [](){};
    };
}

std::vector<std::pair<uint8_t*, uint32_t>>
ROBFragment::serialize(uint32_t run_number, uint32_t status_word) {
    std::unique_lock lock(m_mutex);

    m_pp();

    if (status_word) {
        m_rob_header[7] = status_word;
    }

    m_rod_header[2] |= m_rod_minor_version;
    m_rod_header[4] = run_number;
    m_rod_header[8] = m_detector_type;

    std::vector<std::pair<uint8_t*, uint32_t>> result;
    result.reserve(m_data.size() + (m_rod_header_present ? 3 : 5));

    ///////////////////////////////////////////////////////////////////////
    // serialize ROB header
    ///////////////////////////////////////////////////////////////////////
    uint32_t size = c_array_size(m_rob_header);
    result.push_back(std::pair<uint8_t*, uint32_t>(
            (uint8_t*)&m_rob_header[0], sizeof(m_rob_header)));

    ///////////////////////////////////////////////////////////////////////
    // skip ROB status
    // serialize ROD header if it is not already present in the data
    ///////////////////////////////////////////////////////////////////////
    if (!m_rod_header_present) {
        result.push_back(std::pair<uint8_t*, uint32_t>(
                (uint8_t*)m_rod_header, sizeof(m_rod_header)));
        size += c_array_size(m_rod_header);
    }

    ///////////////////////////////////////////////////////////////////////
    // serialize ROD status
    ///////////////////////////////////////////////////////////////////////
    if (m_status_front && !m_status_words.empty()) {
        m_rod_trailer[0] = m_status_words.size();
        m_rod_trailer[2] = eformat::STATUS_FRONT;
        result.push_back(std::pair<uint8_t*, uint32_t>(
                (uint8_t*)&m_status_words[0],
                m_status_words.size() * sizeof(uint32_t)));
        size += m_status_words.size();
    }

    ///////////////////////////////////////////////////////////////////////
    // serialize data
    ///////////////////////////////////////////////////////////////////////
    uint32_t data_size = 0;
    for (const auto & b : m_data) {
        result.push_back(std::make_pair(
                (uint8_t*)b.dataBegin(),
                b.dataSize() * sizeof(uint32_t)));
        data_size += b.dataSize();
    }
    m_rod_trailer[1] = data_size;
    size += data_size;

    ///////////////////////////////////////////////////////////////////////
    // serialize ROD status
    ///////////////////////////////////////////////////////////////////////
    if (!m_status_front && !m_status_words.empty()) {
        m_rod_trailer[0] = m_status_words.size();
        m_rod_trailer[2] = eformat::STATUS_BACK;
        result.push_back(std::pair<uint8_t*, uint32_t>(
                (uint8_t*)&m_status_words[0],
                m_status_words.size() * sizeof(uint32_t)));
        size += m_status_words.size();
    }

    ///////////////////////////////////////////////////////////////////////
    // serialize ROD trailer if it is not already present in the data
    ///////////////////////////////////////////////////////////////////////
    if (!m_rod_header_present) {
        result.push_back(std::pair<uint8_t*, uint32_t>(
                (uint8_t*)m_rod_trailer, sizeof(m_rod_trailer)));
        size += c_array_size(m_rod_trailer);
    }

    m_rob_header[1] = size;
    return result;
}
