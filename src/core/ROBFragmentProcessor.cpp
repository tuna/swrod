/*
 * ROBFragmentProcessor.cpp
 *
 *  Created on: May 29, 2019
 *      Author: kolos
 */
#include <boost/format.hpp>

#include <is/infoany.h>
#include <is/infodynany.h>
#include <is/types.h>

#include <swrod/Core.h>
#include <swrod/CustomProcessingFramework.h>
#include <swrod/Factory.h>
#include <swrod/detail/ptree.h>

#include "ROBFragmentProcessor.h"

using namespace swrod;
using namespace boost::property_tree;

namespace {
    #define SUM(T,t)    case ISType::t : \
                            if (from.isAttributeArray()) \
                                add(to.getAttributeValue<std::vector<T>>(i), from); \
                            else \
                                add(to.getAttributeValue<T>(i), from); \
                            break;
    #define SUM_SEPARATOR
    #define SUM_OBJECT_TYPE  ISInfoDynAny

    void addToFrom(ISInfoDynAny & to, ISInfoAny & from);

    OWLDate & operator+=(OWLDate & to, const OWLDate & ) {
        return to;
    }

    OWLTime & operator+=(OWLTime & to, const OWLTime & ) {
        return to;
    }

    ISInfoDynAny & operator+=(ISInfoDynAny & to, const ISInfoDynAny & from) {
        ISInfoAny any;
        any <<= from;
        addToFrom(to, any);
        return to;
    }

    template <class T>
    std::vector<T> & operator+=(std::vector<T> & to, const std::vector<T> & from) {
        if (to.size() == from.size()) {
            for (size_t i = 0; i < to.size(); ++i) {
                to[i] += from[i];
            }
        }
        else {
            to.insert(to.end(), from.begin(), from.end());
        }
        return to;
    }

    template <>
    std::vector<bool> & operator+=(std::vector<bool> & to, const std::vector<bool> & from) {
        if (to.size() == from.size()) {
            for (size_t i = 0; i < to.size(); ++i) {
                to[i] = to[i] && from[i];
            }
        }
        else {
            to.insert(to.end(), from.begin(), from.end());
        }
        return to;
    }

    template <class T>
    void add(T & to, ISInfoAny & from) {
        T v;
        from >> v;
        to += v;
    }

    template <>
    void add(bool & to, ISInfoAny & from) {
        bool v;
        from >> v;
        to = to && v;
    }

    template <>
    void add(ISInfoDynAny & to, ISInfoAny & from) {
        ISInfoDynAny v(IPCPartition(), from.getAttributeInfoType());
        from >> v;
        to += v;
    }

    template <>
    void add(std::vector<ISInfoDynAny> & to, ISInfoAny & from) {
        std::vector<ISInfoDynAny> v(
                std::allocator<ISInfoDynAny>(IPCPartition(), from.getAttributeInfoType()));
        from >> v;
        to += v;
    }

    void addToFrom(ISInfoDynAny & to, ISInfoAny & from) {
        for (size_t i = 0; i < from.countAttributes(); ++i) {
            switch (from.getAttributeType()) {
                IS_TYPES(SUM)
                default : ERS_ASSERT(false);
            }
        }
    }
}

namespace {
    Factory<ROBFragmentConsumer>::Registrator __reg__(
            "ROBFragmentProcessor",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<ROBFragmentProcessor>(config, core);
            });
}

ProcessorPool::ProcessorPool(const boost::property_tree::ptree & config, const Core& core) {
    uint32_t ROB_id = PTREE_GET_VALUE(config, uint32_t, "RobConfig.Id");
    uint32_t workers_number = PTREE_GET_VALUE(config, uint32_t, "WorkersNumber");
    bool defer_processing = PTREE_GET_VALUE(config, bool, "DeferProcessing");

    m_processors.reserve(workers_number);
    m_functions.reserve(workers_number);
    for (uint32_t i = 0; i < workers_number; ++i) {
        m_processors.push_back(std::make_shared<LockableProcessor>(
                        core.getProcessingFramework().createCustomProcessor(ROB_id, config)));

        auto proc = *m_processors.rbegin();
        if (defer_processing) {
            m_functions.push_back([proc](const std::shared_ptr<ROBFragment> & f) {
                f->setPreProcessor([proc](ROBFragment & ff) {
                    (*proc)->processROBFragment(ff);
                });
            });
        } else {
            m_functions.push_back([proc](const std::shared_ptr<ROBFragment> & f) {
                proc->processor().processROBFragment(*f);
            });
        }
    }
}

ROBFragmentProcessor::ROBFragmentProcessor(
        const boost::property_tree::ptree & config, const Core & core) :
    ProcessorPool(config, core),
    ROBFragmentConsumerBase(config, getFunctions(),
        ROBFragmentConsumerBase::QueuingPolicy::Wait,
        ROBFragmentConsumerBase::ForwardingPolicy::AfterProcessing,
        (boost::format("ROB-%x-processor") %
                PTREE_GET_VALUE(config, std::string, "RobConfig.Id")).str()),
    m_unique_id((boost::format("ROB-%08x-processor") %
            PTREE_GET_VALUE(config, std::string, "RobConfig.Id")).str())
{
}

ISInfo * ROBFragmentProcessor::getStatistics()  {
    if (m_processors.size() == 1) {
        return (*m_processors[0])->getStatistics();
    }
    m_statistics = ISInfoDynAny();
    for (auto & proc : m_processors) {
        ISInfo * i = (*proc)->getStatistics();
        if (!i) {
            continue;
        }
        if (!m_statistics.getAttributesNumber()) {
            m_statistics = ISInfoDynAny(IPCPartition(), i->type());
        }
        if (m_statistics.type() == i->type()) {
            ISInfoAny any;
            any <<= *i;
            addToFrom(m_statistics, any);
        }
    }
    return m_statistics.getAttributesNumber() ? &m_statistics : nullptr;
}
