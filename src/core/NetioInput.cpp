/*
 * NetioInput.cpp
 *
 *  Created on: Jun 14, 2019
 *      Author: kolos
 */

#include "NetioInput.h"

#include <regex>

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/detail/NetioPacketHeader.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    Factory<DataInput>::Registrator __reg__("NetioInput",
            [](const boost::property_tree::ptree& config, const Core& ) {
                return std::make_shared<NetioInput>(config);
            });

    netio::sockcfg createNetioConfig(const boost::property_tree::ptree& config,
            netio::msg_rcvd_cb_t callback, void * usr_data) {
        netio::sockcfg cfg = netio::sockcfg::cfg();
        cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION,
                PTREE_GET_VALUE(config, uint32_t, "BufferPages"));
        cfg(netio::sockcfg::PAGESIZE,
                PTREE_GET_VALUE(config, uint32_t, "BufferPageSize"));
        if (config.count("ZeroCopy")) {
            cfg(netio::sockcfg::ZERO_COPY);
        }
        if (callback != nullptr) {
            cfg(netio::sockcfg::CALLBACK, (uint64_t) callback);
            cfg(netio::sockcfg::CALLBACK_DATA, (uint64_t) usr_data);
        }
        return cfg;
    }
}

NetioInput::NetioInput(const boost::property_tree::ptree& config)
try : m_use_callback(PTREE_GET_VALUE(config, bool, "UseCallback")),
      m_timeout_ms(PTREE_GET_VALUE(config, int32_t, "FelixBusTimeout")),
      m_felix_bus(PTREE_GET_VALUE(config, std::string, "FelixBusGroupName"),
                  PTREE_GET_VALUE(config, std::string, "FelixBusInterface")),
      m_context(PTREE_GET_VALUE(config, std::string, "Backend")),
      m_signal(m_context.event_loop(), std::bind(&NetioInput::signalHandler, this, std::placeholders::_1), 0),
      m_socket(&m_context,
              createNetioConfig(config, m_use_callback ? &NetioInput::netioMessageReceived : nullptr, this)),
      m_thread(std::bind(&NetioInput::run, this, std::placeholders::_1),
                "NetioInput",
                PTREE_GET_VALUE(config, std::string, "CPU")),
      m_read_thread(std::bind(&NetioInput::read, this, std::placeholders::_1),
              "NetioInput",
              PTREE_GET_VALUE(config, std::string, "CPU"))
{
    m_felix_bus.connect();
    m_felix_bus.subscribe("FELIX", &m_felix_table);
    m_felix_bus.subscribe("ELINKS", &m_elink_table);

    m_thread.start();
    m_read_thread.start();
}
catch (ptree_error & ex) {
    throw BadConfigurationException(ERS_HERE, ex.what(), ex);
}

NetioInput::~NetioInput() {
    m_context.event_loop()->stop();
    m_thread.stop();
    m_read_thread.stop();
}

void NetioInput::actionsPending() {
    ERS_DEBUG(1, "Fire signal");
    m_signal.fire();
}

void NetioInput::signalHandler(void *) {
    ERS_DEBUG(1, "Signal handler invoked");
    executeActions();
    ERS_DEBUG(1, "Actions executed");
}

void NetioInput::netioMessageReceived(uint8_t* data, size_t size, void* usr_data) {
    ERS_DEBUG(5, "message with size = " << size << " received");

    NetioInput * ptr = reinterpret_cast<NetioInput*>(usr_data);
    ptr->dataReceived(*((uint32_t*)(data + 4)),
            data + sizeof(detail::NetioPacketHeader),
            size - sizeof(detail::NetioPacketHeader),
            *((uint16_t*)(data + 2)));
}

void NetioInput::subscribeToFelix(const InputLinkId& link) {
    std::string uuid = m_elink_table.getFelixId(link, m_timeout_ms);
    std::string address = m_felix_table.getAddress(uuid);

    std::regex regex("tcp://([^:]+):([0-9]+)");
    std::smatch match_result;

    if (std::regex_match(address, match_result, regex) && match_result.size() == 3) {
        std::string host = match_result[1].str();
        uint32_t port = std::stoi(match_result[2].str());
        netio::endpoint endpoint(host, port);
        try {
            m_socket.subscribe(link, endpoint);
            m_subscriptions[link] = endpoint;
            ERS_DEBUG(1, "Subscribed to '" << address << "' address for link = 0x"
                    << std::hex << link << " uuid = '" << uuid << "'");
        } catch (std::exception & ex) {
            throw SubscriptionException(ERS_HERE, link, ex.what(), ex);
        }
    } else {
        throw BadAddressException(ERS_HERE, address, link, uuid);
    }
}

void NetioInput::unsubscribeFromFelix(const InputLinkId& link) {
    auto it = m_subscriptions.find(link);
    if (it != m_subscriptions.end()) {
        try {
            m_socket.unsubscribe(it->first, it->second);
            ERS_DEBUG(1, "Unsubscribed from link = 0x" << std::hex << link);
        } catch (std::exception & ex) {
            throw Exception(ERS_HERE, "Can not unsubscribe", ex);
        }
        m_subscriptions.erase(it);
    }
}

void NetioInput::run(const bool& ) {
    ERS_DEBUG(1, "Starting netio event loop");
    try {
        m_context.event_loop()->run_forever();
    } catch (std::exception & ex) {
        ers::fatal(Exception(ERS_HERE, "Event loop failure", ex));
    }
    ERS_DEBUG(1, "Netio event loop has been stopped");
}

void NetioInput::read(const bool& active) {
    if (m_use_callback) {
        return;
    }

    netio::message msg;
    while (active) {
        m_socket.recv(msg);
        const netio::message::fragment * f = msg.fragment_list();
        const uint8_t * data = f->data[0];
        dataReceived(*((uint32_t*)(data + 4)),
                data + sizeof(detail::NetioPacketHeader),
                f->size[0] - sizeof(detail::NetioPacketHeader),
                *((uint16_t*)(data + 2)));
    }
}
