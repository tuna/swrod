// -*- c++ -*-
#ifndef HLTREQUESTMANAGER_H
#define HLTREQUESTMANAGER_H

#include <vector>

#include "ROSDescriptorNP/RequestManager.h"

namespace ROS{
   class RequestDescriptor;
}
namespace swrod{
   class HLTRequestHandler;
   class HLTRequestManager : public ROS::RequestManager {
   public: 
      HLTRequestManager(HLTRequestHandler* handler,
                        unsigned int, unsigned int, unsigned int,
                        unsigned int maxPendigClears=20000);
      virtual ~HLTRequestManager();

      virtual std::vector<unsigned int> pollLevel1() final;
      virtual void doRequest(ROS::RequestDescriptor* desc) final;
      virtual void doClears(std::vector<unsigned int>& level1IdList) final;

      unsigned int latestL1Available(){
         return m_availableL1;
      };
   private:
      HLTRequestHandler* m_requestHandler;
   };
}
#endif
