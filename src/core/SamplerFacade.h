/*
 * SamplerFacade.h
 *
 *  Created on: Apr 23, 2020
 *      Author: kolos
 */

#ifndef SAMPLERFACADE_H_
#define SAMPLERFACADE_H_

#include <sys/uio.h>

#include <chrono>
#include <functional>
#include <mutex>
#include <set>
#include <thread>

#include <boost/thread/shared_mutex.hpp>

#include <emon/EventChannel.h>
#include <emon/EventSampler.h>
#include <emon/PushSampling.h>
#include <emon/SamplingAddress.h>

namespace swrod {
    template <class Event>
    class SamplerFacade {
    public:
        SamplerFacade( const IPCPartition & partition,
                            const emon::SamplingAddress & address,
                            size_t max_channels,
                            bool sample_all = false)
            :   m_sample_all(sample_all),
                m_sampler(partition, address, new SamplingFactory(*this), max_channels) {
        }

        bool pushEvent(Event & event) {
            boost::shared_lock lock(m_mutex);
            bool r = true;
            for (auto channel : m_channels) {
                r = r && channel->pushEvent(event);
            }
            return r;
        }

        bool isActive() const {
            return not m_channels.empty();
        }

        bool isSampleAll() const {
            return m_sample_all;
        }

    protected:
        virtual ~SamplerFacade() {}

        virtual void sampleEvent(Event & , emon::EventChannel &) = 0;

        virtual void samplingStarted() { }

        virtual void samplingStopped() { }

    private:

        class SamplingChannel: public emon::PushSampling {
        public:

            SamplingChannel(emon::EventChannel * channel, SamplerFacade & parent) :
                m_terminated(false),
                m_channel(channel),
                m_parent(parent)
            {
                m_parent.channelCreated(this);
            }

            ~SamplingChannel() {
                m_terminated = true;
                m_parent.channelDestroyed(this);
            }

            bool pushEvent(Event & event) {
                std::unique_lock lock(m_mutex);
                while (!m_channel->readyToSendEvent()) {
                    if (!m_parent.m_sample_all || m_terminated) {
                        return false;
                    }
                    usleep(10);
                }
                m_parent.sampleEvent(event, *m_channel);
                return true;
            }

        private:
            std::mutex m_mutex;
            bool m_terminated;
            emon::EventChannel * m_channel;
            SamplerFacade & m_parent;
        };

        class SamplingFactory: public emon::PushSamplingFactory {
        public:
            explicit SamplingFactory(SamplerFacade & parent) : m_parent(parent) {
            }

            emon::PushSampling * startSampling(const emon::SelectionCriteria & criteria,
                    emon::EventChannel * channel) override {
                return new SamplingChannel(channel, m_parent);
            }
        private:
            SamplerFacade & m_parent;
        };


    private:
        void channelCreated(SamplingChannel * channel) {
            boost::unique_lock lock(m_mutex);
            if (m_channels.empty()) {
                samplingStarted();
            }
            m_channels.insert(channel);
            ERS_DEBUG(1, "Sampling channel created, number of active channels = "
                    << m_channels.size());
        }

        void channelDestroyed(SamplingChannel * channel) {
            boost::unique_lock lock(m_mutex);
            m_channels.erase(channel);
            if (m_channels.empty()) {
                samplingStopped();
            }
            ERS_DEBUG(1, "Sampling channel destroyed, number of active channels = "
                    << m_channels.size());
        }

    private:
        typedef std::set<SamplingChannel*> Channels;

        const bool m_sample_all;
        boost::shared_mutex m_mutex;   // Boost shared_mutex is guaranteed to be fair while std:: one is not
        Channels m_channels;
        emon::EventSampler m_sampler;
    };
}

#endif /* SAMPLERFACADE_H_ */
