/*
 * FelixInput.cpp
 *
 *  Created on: Jun 14, 2019
 *      Author: kolos
 */
#include <sstream>

#include "FelixInput.h"

#include <felix/felix_client_properties.h>
#include <felix/felix_client_status.h>

#include <swrod/exceptions.h>
#include <swrod/Factory.h>
#include <swrod/ROBStatus.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;
namespace arg = std::placeholders;

namespace {
    Factory<DataInput>::Registrator __reg__("FelixInput",
            [](const boost::property_tree::ptree& config, const Core& ) {
                return std::make_shared<FelixInput>(config);
            });
}

FelixInput::FelixInput(const boost::property_tree::ptree& config)
try : m_felix({
        std::bind(&FelixInput::dataReceived, this, arg::_1, arg::_2, arg::_3, arg::_4),
        nullptr, nullptr, nullptr,
        {
            {std::string(FELIX_CLIENT_LOCAL_IP_OR_INTERFACE),
                    PTREE_GET_VALUE(config, std::string, "DataNetwork")},
            {std::string(FELIX_CLIENT_BUS_DIR),
                    PTREE_GET_VALUE(config, std::string, "FelixBusDirectory")},
            {std::string(FELIX_CLIENT_BUS_GROUP_NAME),
                    PTREE_GET_VALUE(config, std::string, "FelixBusGroupName")},
            {std::string(FELIX_CLIENT_BUS_INTERFACE),
                    PTREE_GET_VALUE(config, std::string, "FelixBusInterface")},
            {std::string(FELIX_CLIENT_TIMEOUT),
                    PTREE_GET_VALUE(config, std::string, "FelixBusTimeout")},
            {std::string("thread_affinity" /*FELIX_CLIENT_THREAD_AFFINITY*/),
                    PTREE_GET_VALUE(config, std::string, "CPU")}
        }
    })
{
    ;
}
catch (std::exception & ex) {
    throw BadConfigurationException(ERS_HERE, ex.what(), ex);
}

FelixInput::~FelixInput() {
    ;
}

void FelixInput::actionsPending() {
    ERS_DEBUG(1, "Request actions execution");
    m_felix.exec(std::bind(&FelixInput::executeActions, this));
    ERS_DEBUG(1, "Actions executed");
}

void FelixInput::synchronise(uint8_t ECR_value) {
    using FCTE = FelixClientThreadExtension;

    std::vector<FCTE::Reply> replies;
    FCTE::Status status = FCTE::OK;

    try {
        ERS_LOG("Sending ECR_RESET command with ECR counter = 0x" << std::hex << (uint16_t)ECR_value);
        status = m_felix.send_cmd(subscribedLinks(), FCTE::ECR_RESET,
                std::vector<std::string>(1, std::to_string(ECR_value)), replies);

        ERS_LOG("send_cmd returns " << status << " status");
    } catch(felix_proxy::NotImplemented & ex) {
        ers::error(ex);
        return;
    }

    if (status != FCTE::OK) {
        std::ostringstream out;
        out << "Setting ECR counter to 0x" << std::hex << (uint16_t)ECR_value << " failed:";
        for (auto & r : replies) {
            out << "\ne-link 0x" << std::hex << r.ctrl_fid << " - " << r.message;
        }
        throw swrod::SynchronisationException(ERS_HERE, out.str());
    }
}

void FelixInput::subscribeToFelix(const InputLinkId& link) {
    try {
        ERS_DEBUG(1, "Subscribing to 0x" << std::hex << link << " e-link");
        m_felix.subscribe(link);
        ERS_DEBUG(1, "Subscribed to 0x" << std::hex << link << " e-link");
    } catch (std::exception & ex) {
        throw SubscriptionException(ERS_HERE, link, ex.what(), ex);
    }
}

void FelixInput::unsubscribeFromFelix(const InputLinkId& link) {
    try {
        ERS_DEBUG(1, "Unsubscribing from 0x" << std::hex << link << " e-link");
        m_felix.unsubscribe(link);
        ERS_DEBUG(1, "Unsubscribed from 0x" << std::hex << link << " e-link");
    } catch (std::exception & ex) {
        throw Exception(ERS_HERE, "Can not unsubscribe", ex);
    }
}

uint32_t FelixInput::translateStatus(uint8_t status) const noexcept {
    ERS_DEBUG(5, "Translating 0x" << std::hex << status << " status");

    if (!status) {
        return 0;
    }
    uint32_t r = 0;
    if ( status & (FELIX_STATUS_FW_TRUNC | FELIX_STATUS_SW_TRUNC)) {
        r |= ROBStatus::Specific::Truncated;
        r |= ROBStatus::Specific::TxError;
        r |= ROBStatus::Generic::BufferOverflow;
        if (status & FELIX_STATUS_FW_TRUNC) {
            r |= ROBStatus::Specific::Transmission::FirmwareTrunc;
        }
        if (status & FELIX_STATUS_SW_TRUNC) {
            r |= ROBStatus::Specific::Transmission::SoftwareTrunc;
        }
    }
    if ( status & (FELIX_STATUS_FW_CRC | FELIX_STATUS_FW_MALF | FELIX_STATUS_SW_MALF)) {
        r |= ROBStatus::Specific::TxError;
        r |= ROBStatus::Generic::CorruptData;
        if (status & FELIX_STATUS_FW_CRC) {
            r |= ROBStatus::Specific::Transmission::CRCError;
        }
        if (status & FELIX_STATUS_SW_MALF) {
            r |= ROBStatus::Specific::Transmission::SoftwareMalf;
        }
        if (status & FELIX_STATUS_FW_MALF) {
            r |= ROBStatus::Specific::Transmission::FirmwareMalf;
        }
    }

    ERS_DEBUG(4, "Status 0x" << std::hex << status << " has been translated to 0x" << r);

    return r;
}
