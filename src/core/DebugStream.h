/*
 * DebugStream.h
 *
 *  Created on: Jun 18, 2019
 *      Author: kolos
 */

#ifndef SRC_CORE_DEBUGSTREAM_H_
#define SRC_CORE_DEBUGSTREAM_H_

#include <boost/property_tree/ptree.hpp>

#include <tbb/concurrent_queue.h>

#include "SamplerFacade.h"

#include <swrod/ROBFragment.h>
#include <swrod/ROBFragmentConsumer.h>
#include <swrod/detail/EventHeader.h>
#include <swrod/detail/ThreadPool.h>

namespace swrod {
    class Core;

    struct Event {
        std::shared_ptr<ROBFragment> m_fragment;
        detail::EventHeader m_header;
        std::vector<struct iovec> m_serialized;
    };

    class DebugStream: public ROBFragmentConsumer,
                        public SamplerFacade<Event>
    {
    public:
        DebugStream(const boost::property_tree::ptree & robConfig,
                const Core & core);

        const std::string & getName() const override {
            static std::string s("DebugStream");
            return s;
        }

        void insertROBFragment(const std::shared_ptr<ROBFragment> & fragment) override;

        void runStarted(const RunParams & run_params) override;

        void runStopped() override;

    protected:
        void sampleEvent(Event & event, emon::EventChannel & channel) override;

        void samplingStarted() override {}

        void samplingStopped() override {}

    private:
        void run(const bool & active);

    private:
        typedef tbb::concurrent_bounded_queue<std::shared_ptr<ROBFragment>> FragmentQueue;

        uint32_t m_run_number;
        bool m_running;
        FragmentQueue m_fragment_queue;
        detail::ThreadPool m_threads;
    };
}

#endif /* SRC_CORE_DEBUGSTREAM_H_ */
