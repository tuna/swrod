/*
 * ROBFragmentProcessor.h
 *
 *  Created on: May 29, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTPROCESSOR_H_
#define SWROD_ROBFRAGMENTPROCESSOR_H_

#include <memory>
#include <mutex>

#include <is/infodynany.h>

#include <swrod/ROBFragmentConsumerBase.h>
#include <swrod/CustomProcessor.h>

namespace swrod {
    class Core;

    class ProcessorPool {
        protected:
            ProcessorPool(const boost::property_tree::ptree & config, const Core& core);

        protected:
            const std::vector<ROBFragmentConsumerBase::UserFunction> & getFunctions() {
                return m_functions;
            }

            struct LockedPtr : public std::unique_lock<std::mutex> {
                LockedPtr(std::mutex & m, CustomProcessor * p) :
                    std::unique_lock<std::mutex>(m), m_ptr(p) {
                }

                CustomProcessor * operator->() const {
                    return m_ptr;
                }

            private:
                CustomProcessor * m_ptr;
            };

            struct LockableProcessor {
                LockableProcessor(std::unique_ptr<CustomProcessor> && p) :
                    m_processor(std::move(p)) {}

                LockedPtr operator->() const {
                    return LockedPtr(m_mutex, m_processor.get());
                }

                CustomProcessor & processor() const {
                    return *m_processor;
                }

            private:
                mutable std::mutex m_mutex;
                std::unique_ptr<CustomProcessor> m_processor;
            };

            std::vector<std::shared_ptr<LockableProcessor>> m_processors;
            std::vector<ROBFragmentConsumerBase::UserFunction> m_functions;
    };

    class ROBFragmentProcessor : public ProcessorPool,
                                 public ROBFragmentConsumerBase {
    public:
        ROBFragmentProcessor(const boost::property_tree::ptree & config, const Core& core);

        const std::string& getName() const override {
            return m_unique_id;
        }

        void linkDisabled(const InputLinkId & link_id) {
            for_each(m_processors.begin(), m_processors.end(),
                    [&link_id](auto & w){ (*w)->linkDisabled(link_id); });
        }

        void linkEnabled(const InputLinkId & link_id) {
            for_each(m_processors.begin(), m_processors.end(),
                    [&link_id](auto & w){ (*w)->linkEnabled(link_id); });
        }

        void runStarted(const RunParams& run_params) override {
            for_each(m_processors.begin(), m_processors.end(),
                    [&run_params](auto & w){ (*w)->runStarted(run_params); });
            ROBFragmentConsumerBase::runStarted(run_params);
        }

        void runStopped() override {
            ROBFragmentConsumerBase::runStopped();
            for_each(m_processors.begin(), m_processors.end(),
                    [](auto & w){ (*w)->runStopped(); });
        }

        void userCommand(const daq::rc::UserCmd & cmd) override {
            for_each(m_processors.begin(), m_processors.end(),
                    [&cmd](auto & w){ (*w)->userCommand(cmd); });
        }

        ISInfo * getStatistics() override;

    private:
        const std::string m_unique_id;
        ISInfoDynAny m_statistics;
    };
}

#endif /* SWROD_ROBFRAGMENTPROCESSOR_H_ */
