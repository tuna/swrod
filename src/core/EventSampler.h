/*
 * EventSampler.h
 *
 *  Created on: Jun 18, 2019
 *      Author: kolos
 */

#ifndef SRC_CORE_EVENTSAMPLER_H_
#define SRC_CORE_EVENTSAMPLER_H_

#include <boost/property_tree/ptree.hpp>

#include <tbb/concurrent_queue.h>

#include "SamplerFacade.h"

#include <swrod/ROBFragment.h>
#include <swrod/ROBFragmentConsumer.h>
#include <swrod/detail/Collator.h>
#include <swrod/detail/EventHeader.h>
#include <swrod/detail/ThreadPool.h>

namespace swrod {
    class Core;

    struct SerializedEvent {
        Event m_event;
        detail::EventHeader m_header;
        std::vector<struct iovec> m_serialized;
    };

    class EventSampler: public ROBFragmentConsumer,
                        public SamplerFacade<SerializedEvent>
    {
    public:
        EventSampler(const boost::property_tree::ptree & robConfig,
                const Core & core);

        const std::string & getName() const override {
            static std::string s("EventSampler");
            return s;
        }

        void insertROBFragment(const std::shared_ptr<ROBFragment> & fragment) override;

        void runStarted(const RunParams & run_params) override;

        void runStopped() override;

        void ROBDisabled(uint32_t ROB_id) override;

        void ROBEnabled(uint32_t ROB_id) override;

    protected:
        void sampleEvent(SerializedEvent & event, emon::EventChannel & channel) override;

        void samplingStarted() override;

        void samplingStopped() override;

    private:
        bool match(Event & event, const emon::SelectionCriteria & criteria) const;

        void run(const bool & active);

        void eventReady(Event && event);

    private:
        typedef tbb::concurrent_bounded_queue<Event> EventQueue;

        const uint32_t m_sampling_ratio;
        uint32_t m_run_number;
        bool m_running;
        EventQueue m_event_queue;
        Collator m_collator;
        detail::ThreadPool m_threads;
    };
}

#endif /* SRC_CORE_EVENTSAMPLER_H_ */
