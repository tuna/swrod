/*
 * L1AInputHandler.h
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#ifndef SWROD_DEFAULTL1AINPUTHANDLER_H_
#define SWROD_DEFAULTL1AINPUTHANDLER_H_

#include <boost/property_tree/ptree.hpp>

#include <swrod/DataInput.h>
#include <swrod/DataInputHandlerBase.h>
#include <swrod/ROBFragmentBuilder.h>
#include <swrod/L1AInputHandler.h>

namespace swrod {
    class Core;

    class DefaultL1AInputHandler:   public L1AInputHandler,
                                    public DataInputHandlerBase {
    public:
        DefaultL1AInputHandler(const boost::property_tree::ptree & config, const Core & core);

        void resynchAfterRestart(uint32_t lastL1ID) override;

        void runStarted(const RunParams & run_params) override {
            m_expected_l1id = m_counter = 0;
            DataInputHandlerBase::runStarted(run_params);
        }

        void dataReceived(const uint8_t * data, uint32_t size, uint8_t status, uint32_t rob_status);

    private:
        uint32_t m_expected_l1id;
        uint64_t m_counter;
    };
}

#endif /* SWROD_DEFAULTL1AINPUTHANDLER_H_ */
