// -*- c++ -*-
#ifndef HLTDATABUFFER_H
#define HLTDATABUFFER_H

#include <atomic>
#include <string>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <thread>
#include <vector>

#include <boost/property_tree/ptree.hpp>

#include "tbb/concurrent_hash_map.h"
#include "tbb/concurrent_queue.h"

#include "swrod/ROBFragmentConsumer.h"

#include "swrod/HLTRequestStatistics.h"
#include "rc/RunParams.h"

class ISInfo;

namespace swrod{
   class Core;
   class HLTRequestManager;
   class HLTRequestHandler;

   typedef tbb::concurrent_hash_map<unsigned int,
                                    std::shared_ptr<ROBFragment> > FragmentIndex;

   class HLTDataBuffer : public ROBFragmentConsumer {
   public:
      HLTDataBuffer(unsigned int robId,
                    unsigned int maxIndex,
                    std::shared_ptr<HLTRequestHandler> requestHandler,
                    std::shared_ptr<HLTRequestManager> requestManager);

      virtual ~HLTDataBuffer();

      const std::string & getName() const final {
         return m_consumerName;
      };

      void insertROBFragment(const std::shared_ptr<ROBFragment>& fragment) final;

      void ROBDisabled(unsigned int robId) final;

      void ROBEnabled(unsigned int robId) final;

      void runStarted(const RunParams& runPars) final;

      void runStopped() final;

      ISInfo* getStatistics() final;

      void clear(std::shared_ptr<std::vector<unsigned int> > l1Ids);
      void resetStats();
      unsigned int latestL1Available(){return m_statistics.latestL1Available;}
      unsigned int size(){return m_index.size();};

      bool find(FragmentIndex::const_accessor& evAcc, unsigned int level1Id){
         return m_index.find(evAcc,level1Id);
      }

      void requested(unsigned int l1) {
         m_requestsComplete++;
         m_latestRequested=l1;
      }
      void mayCome() {m_mayComeFragments++;}
      void lost() {m_lostFragments++;}
      void discarded() {m_discardedFragments++;}
   private:
      std::string m_consumerName;

      unsigned int m_robId;
      unsigned int m_maxIndex;

      std::shared_ptr<HLTRequestHandler> m_requestHandler;
      std::shared_ptr<HLTRequestManager> m_requestManager;
      HLTRequestStatistics m_statistics;
      HLTRequestStatistics m_lastStats;
      std::chrono::time_point<std::chrono::system_clock> m_lastStatsTime;
      unsigned long m_lastTotalRobs;
      FragmentIndex m_index;

      tbb::concurrent_bounded_queue<std::shared_ptr<std::vector<unsigned int> > > m_clearQueue;
      std::shared_ptr<std::thread> m_clearThread;
      void doClears();

      std::mutex m_mutex;
      std::condition_variable m_condition;
      bool m_active;
      bool m_disabled;
      std::atomic<unsigned int> m_latestRequested;
      std::atomic<unsigned long> m_requestsComplete;
      std::atomic<unsigned int> m_mayComeFragments;
      std::atomic<unsigned int> m_lostFragments;
      std::atomic<unsigned int> m_discardedFragments;
   };
}
#endif
