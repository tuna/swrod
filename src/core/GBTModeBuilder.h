/*
 * GBTModeBuilder.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_GBTMODEBUILDER_H_
#define SWROD_GBTMODEBUILDER_H_

#include <type_traits>

#include <swrod/exceptions.h>
#include <swrod/ROBFragmentBuilderBase.h>
#include "GBTModeWorker.h"

namespace swrod {
    class Core;

    template <bool ReceiveTTC>
    class GBTModeBuilder : public ROBFragmentBuilderBase {
    public:
        GBTModeBuilder(const boost::property_tree::ptree& robConfig, const Core & core) :
            ROBFragmentBuilderBase(robConfig, core,
                std::bind(&GBTModeBuilder<ReceiveTTC>::handleL1A<ReceiveTTC>, this, std::placeholders::_1),
                [&](const InputLinkVector & links, const std::shared_ptr<DataInput> & input) {
                    return new GBTModeWorker<ReceiveTTC>(links, robConfig, core, input, *this);
                })
        { }

        template <bool TTC = ReceiveTTC>
        typename std::enable_if<TTC>::type sliceReady(
                uint64_t index, ROBFragment::DataBlock && data,
                uint32_t l1id, uint16_t bcid, uint32_t status,
                uint16_t missedPackets, uint16_t corruptedPackets);

        template <bool TTC = ReceiveTTC>
        typename std::enable_if<!TTC>::type sliceReady(
                uint64_t index, ROBFragment::DataBlock && data,
                uint32_t l1id, uint16_t bcid, uint32_t status,
                uint16_t missedPackets, uint16_t corruptedPackets);

        bool getTriggerInfo(const uint64_t & index, uint32_t & l1id, uint16_t & bcid) const;

    private:
        template <bool TTC = ReceiveTTC>
        typename std::enable_if<TTC>::type handleL1A(const L1AInfo& l1a) {
            ERS_DEBUG(4, "L1A info with l1id = 0x" << std::hex << l1a.m_l1id << " received");

            FragmentAssembler::accessor a;
            if (!m_fragment_assembler.insert(a, l1a.m_index)) {
                // was in the map already
                a->second.m_l1id = l1a.m_l1id;
                if (l1a.m_bcid != 0xffff) {
                    if (a->second.m_bcid != 0xffff && a->second.m_bcid != l1a.m_bcid) {
                        ers::error(BCIDMismatch(ERS_HERE, a->second.m_l1id, a->second.m_bcid, l1a.m_bcid));
                        a->second.m_bcid = l1a.m_bcid;
                    }
                }
                a->second.m_trigger_type = l1a.m_trigger_type;
                if (a->second.m_data.size() == m_workers.size()) {
                    fragmentReady(a);
                }
            } else {
                a->second.m_index = l1a.m_index;
                a->second.m_l1id = l1a.m_l1id;
                a->second.m_bcid = l1a.m_bcid;
                a->second.m_trigger_type = l1a.m_trigger_type;
                a->second.m_data.reserve(m_workers.size());
            }
        }

        template <bool TTC = ReceiveTTC>
        typename std::enable_if<!TTC>::type handleL1A(const L1AInfo& l1a) {
        }
    };

    template <bool ReceiveTTC>
    template <bool TTC>
    typename std::enable_if<TTC>::type GBTModeBuilder<ReceiveTTC>::sliceReady(
            uint64_t index, ROBFragment::DataBlock && data,
            uint32_t l1id, uint16_t bcid, uint32_t status,
            uint16_t missedPackets, uint16_t corruptedPackets)
    {
        FragmentAssembler::accessor a;
        if (!m_fragment_assembler.insert(a, index)) {
            // was in the map already
            if (a->second.m_bcid != bcid) {
                if (bcid != 0xffff) {
                    if (a->second.m_bcid != 0xffff) {
                        ers::error(BCIDMismatch(ERS_HERE, a->second.m_l1id, a->second.m_bcid, bcid));
                    }
                    a->second.m_bcid = bcid;
                }
            }
            a->second.m_data.push_back(std::move(data));
            a->second.m_status |= status;
            a->second.m_missed_packets += missedPackets;
            a->second.m_corrupted_packets += corruptedPackets;
            if (a->second.m_data.size() == m_workers.size() && a->second.m_l1id != uint32_t(-1)) {
                fragmentReady(a);
            }
        } else {
            a->second.m_index = index;
            a->second.m_missed_packets = missedPackets;
            a->second.m_corrupted_packets += corruptedPackets;
            a->second.m_bcid = bcid;
            a->second.m_status = status;
            a->second.m_data.reserve(m_workers.size());
            a->second.m_data.push_back(std::move(data));
        }
    }

    template <bool ReceiveTTC>
    template <bool TTC>
    typename std::enable_if<!TTC>::type GBTModeBuilder<ReceiveTTC>::sliceReady(
            uint64_t index, ROBFragment::DataBlock && data,
            uint32_t l1id, uint16_t bcid, uint32_t status,
            uint16_t missedPackets, uint16_t corruptedPackets)
    {
        FragmentAssembler::accessor a;
        if (!m_fragment_assembler.insert(a, index)) {
            // was in the map already
            a->second.m_data.push_back(std::move(data));
            a->second.m_status |= status;
            a->second.m_missed_packets += missedPackets;
            a->second.m_corrupted_packets += corruptedPackets;
       } else {
            a->second.m_index = index;
            a->second.m_missed_packets = missedPackets;
            a->second.m_corrupted_packets = corruptedPackets;
            a->second.m_l1id = l1id;
            a->second.m_bcid = bcid;
            a->second.m_status = status;
            a->second.m_data.reserve(m_workers.size());
            a->second.m_data.push_back(std::move(data));
        }
        if (a->second.m_data.size() == m_workers.size()) {
            fragmentReady(a);
        }
    }

    template <bool ReceiveTTC>
    bool GBTModeBuilder<ReceiveTTC>::getTriggerInfo(
            const uint64_t & index, uint32_t & l1id, uint16_t & bcid) const {
        uint64_t start = 0;
        do {
            FragmentAssembler::const_accessor a;
            if (m_fragment_assembler.find(a, index)) {
                if (a->second.m_l1id != uint32_t(-1)) {
                    l1id = a->second.m_l1id;
                    bcid = a->second.m_bcid;
                    return true;
                }
            } else {
                auto now = []() {
                    return std::chrono::duration_cast<std::chrono::milliseconds>(
                           std::chrono::system_clock::now().time_since_epoch()).count();
                };

                if (!start) {
                    start = now();
                } else {
                    auto elapsed = now() - start;
                    if (elapsed > m_l1a_wait_timeout) {
                        ers::error(BilatedL1AException(ERS_HERE, index, m_l1a_wait_timeout));
                        return false;
                    }
                }
            }
        } while (m_running);

        ERS_DEBUG(3, "Run is being stopped");
        return false;
    }
}

#endif /* SWROD_GBTMODEBUILDER_H_ */
