/*
 * GBTModeBuilder.cpp
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#include "GBTModeBuilder.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>

using namespace swrod;

namespace {
    Factory<ROBFragmentBuilder>::Registrator __reg__(
            "GBTModeBuilder",
            [](const boost::property_tree::ptree& config, const Core& core) {
                if (config.count("L1AHandler")) {
                    return std::shared_ptr<ROBFragmentBuilder>(
                            new GBTModeBuilder<true>(config, core));
                } else {
                    return std::shared_ptr<ROBFragmentBuilder>(
                            new GBTModeBuilder<false>(config, core));
                }
            });
}
