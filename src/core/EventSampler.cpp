/*
 * EventSampler.cpp
 *
 *  Created on: Jun 18, 2019
 *      Author: kolos
 */

#include "EventSampler.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    Factory<ROBFragmentConsumer>::Registrator __reg__("EventSampler",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<EventSampler>(config, core);
            });

    template<typename TIterator, typename TOp>
    inline bool matchAny(TIterator first, TIterator last, TOp unary_op) {
        for (; first != last; ++first) {
            if (unary_op(*first)) {
                return true;
            }
        }
        return false;
    }
}

EventSampler::EventSampler(const boost::property_tree::ptree & config,
        const Core & core)
try : SamplerFacade<SerializedEvent>(
        IPCPartition(
                PTREE_GET_VALUE(core.getConfiguration(), std::string, "Partition.Name")),
        emon::SamplingAddress("SWROD",
                PTREE_GET_VALUE(core.getConfiguration(), std::string, "Application.Name")),
        PTREE_GET_VALUE(config, uint32_t, "MaximumChannels"),
        PTREE_GET_VALUE(config, bool, "SampleAll")),
      m_sampling_ratio(PTREE_GET_VALUE(config, uint16_t, "SamplingRatio")),
      m_run_number(-1),
      m_running(false),
      m_collator(core.getBuilders().size(),
                  std::bind(&EventSampler::eventReady, this, std::placeholders::_1),
                  PTREE_GET_VALUE(config, uint32_t, "QueueSize"),
                  PTREE_GET_VALUE(config, std::string, "CPU")),
      m_threads(PTREE_GET_VALUE(config, uint16_t, "WorkersNumber"),
                  std::bind(&EventSampler::run, this, std::placeholders::_1),
                  "EventSampler",
                  PTREE_GET_VALUE(config, std::string, "CPU"))
{
    m_event_queue.set_capacity(PTREE_GET_VALUE(config, uint32_t, "EventBufferSize"));
}
catch (ptree_error & ex) {
    throw BadConfigurationException(ERS_HERE, ex.what(), ex);
}

void EventSampler::sampleEvent(SerializedEvent & event, emon::EventChannel & channel) {

    if (not match(event.m_event, channel.getCriteria())) {
        return;
    }

    if (event.m_serialized.empty()) {;
        event.m_serialized.reserve(64);
        event.m_serialized.push_back(
                iovec({&event.m_header, event.m_header.m_header_size * sizeof(uint32_t)}));

        uint32_t size = 0;
        for (auto & fragment : event.m_event) {
           std::vector<std::pair<uint8_t*, uint32_t>> data =
                   fragment->serialize(m_run_number);
           for (auto & slice : data) {
               event.m_serialized.push_back(iovec({slice.first, slice.second}));
               size += slice.second;
           }
        }

        size /= sizeof(uint32_t);
        event.m_header.m_event_size = size + event.m_header.m_header_size;
        event.m_header.m_payload_size = size;
        event.m_header.m_l1id = event.m_event[0]->m_l1id;
        event.m_header.m_bcid = event.m_event[0]->m_bcid;
        event.m_header.m_l1_trigger_type = event.m_event[0]->m_trigger_type;
        event.m_header.m_run_number = m_run_number;
    }

    channel.pushEvent(&event.m_serialized[0], event.m_serialized.size());

    ERS_DEBUG(3, "Event with l1id = " << event.m_event[0]->m_l1id
            << " was sent to the '" << channel.getCriteria() << "' sampling channel");
}

void EventSampler::samplingStarted() {
    ;
}

void EventSampler::samplingStopped() {
    m_collator.clear();
    m_event_queue.clear();
}

bool EventSampler::match(Event & event, const emon::SelectionCriteria & criteria) const {
    ERS_ASSERT_MSG(!event.empty(), "Event must contain at least one ROB fragment");

    if (not criteria.m_lvl1_trigger_type.m_ignore
            && not matchAny(event.begin(), event.end(), [&](auto & rob) {
                return criteria.m_lvl1_trigger_type.m_value == rob->m_trigger_type;
            }))
    {
        ERS_DEBUG(3, "LVL1 Trigger Type does not match");
        return false;
    }
    if (not criteria.m_status_word.m_ignore
            && not matchAny(event.begin(), event.end(), [&](auto & rob) {
                return rob->m_status_words.end() != std::find(
                        rob->m_status_words.begin(),
                        rob->m_status_words.end(),
                        criteria.m_status_word.m_value);
            }))
    {
        ERS_DEBUG(3, "Status Word does not match");
        return false;
    }

    ERS_DEBUG(3, "Event with l1id = " << event[0]->m_l1id
            << " matches '" <<  criteria << "' selection criteria");

    return true;
}

void EventSampler::insertROBFragment(const std::shared_ptr<ROBFragment>& fragment) {
    forwardROBFragment(fragment);

    // If this code is used then sampling activation, that can happen at any moment,
    // may lead to appearance of dangling ROB fragments in the Collator. Getting rid
    // of them would be very complicated as fragments arrival is not L1Id-ordered.
    // Such fragments may corrupt collated events after the wrap up of L1Id.
    //
    // if (not isActive() || not m_running) {
    //     return;
    // }

    if (not (fragment->m_l1id % m_sampling_ratio)) {
        m_collator.insertROBFragment(fragment);
    }
}

void EventSampler::eventReady(Event && event) {
    if (not isActive() || not m_running) {
        return;
    }

    if (isSampleAll()) {
        m_event_queue.push(event);
    } else {
        m_event_queue.try_push(event);
    }
}

void EventSampler::run(const bool & active) {
    while (active) {
        SerializedEvent se;
        if (not m_event_queue.try_pop(se.m_event)) {
            usleep(10);
            continue;
        }
        pushEvent(se);
    }
}

void EventSampler::runStarted(const RunParams & run_params) {
    ERS_DEBUG(1, "starting sampling thread");

    m_run_number = run_params.run_number;
    m_running = true;
    m_collator.runStarted(run_params);
    m_threads.start();

    ERS_DEBUG(1, "done");
}

void EventSampler::runStopped() {
    ERS_DEBUG(1, "stopping sampling thread");

    m_running = false;
    m_collator.runStopped();
    m_collator.clear();
    m_event_queue.clear();
    m_threads.stop();

    ERS_DEBUG(1, "done");
}

void EventSampler::ROBDisabled(uint32_t ROB_id) {
    m_collator.ROBDisabled(ROB_id);
    ERS_LOG("ROB '" << ROB_id << "' was disabled");
}

void EventSampler::ROBEnabled(uint32_t ROB_id) {
    m_collator.ROBEnabled(ROB_id);
    ERS_LOG("ROB '" << ROB_id << "' was enabled");
}
