/*
 * GBTModeWorker.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_GBTMODEWORKER_H_
#define SWROD_GBTMODEWORKER_H_

#include <memory>
#include <utility>
#include <type_traits>

#include <swrod/exceptions.h>
#include <swrod/DataInput.h>
#include <swrod/GBTChunk.h>
#include <swrod/InputLinkId.h>
#include <swrod/ROBFragmentWorkerBase.h>
#include <swrod/ROBStatus.h>
#include <swrod/detail/CircularBuffer.h>
#include <swrod/detail/MemoryPool.h>

namespace swrod {
    class Core;
    template <bool ReceiveTTC> class GBTModeBuilder;

    template <bool ReceiveTTC>
    class GBTModeWorker: public ROBFragmentWorkerBase {
    public:
        GBTModeWorker(const InputLinkVector & links,
                const boost::property_tree::ptree & robConfig,
                const Core & core, const std::shared_ptr<DataInput> & input,
                GBTModeBuilder<ReceiveTTC>& builder);

        void runStarted(const RunParams & run_params) override;

        void runStopped() override;

    private:
        void enabled(uint64_t triggersCounter) override;

        void linkDisabled(const Link & link) override;

    private:
        struct Slice {
            explicit Slice(ROBFragment::DataBlock && block = ROBFragment::DataBlock()) :
                    m_block(std::move(block)) {
                reset();
            }

            Slice(Slice &&) = default;
            Slice & operator=(Slice &&) = default;

            void reset() {
                m_links_counter = 0;
                m_packets_missed = 0;
                m_packets_corrupted = 0;
                m_l1id = uint32_t(-1);
                m_bcid = uint16_t(-1);
                m_status = 0;
                m_block.reset();
            }

            void reset(ROBFragment::DataBlock && block) {
                std::swap(m_block, block);
                reset();
            }

            ROBFragment::DataBlock m_block;
            uint32_t m_l1id;
            uint32_t m_status;
            uint16_t m_links_counter;
            uint16_t m_bcid;
            uint16_t m_packets_missed;
            uint16_t m_packets_corrupted;
        };

        void handleBufferOverflow();

        bool handleMissedPackets(Link & link, uint32_t l1id, uint32_t l1id_mask, uint16_t bcid);

        void handlePacketError(const uint8_t * packet, uint32_t size, Link & link, Slice & slice,
                uint32_t l1id, uint32_t l1id_mask, uint16_t bcid);

        void dataReceived(Link & link, const uint8_t* data, uint32_t size, uint8_t status, uint32_t rob_status);

        void sliceReady(uint32_t status = 0);

        template <bool TTC = ReceiveTTC>
        typename std::enable_if<TTC>::type getSeed(
                Slice & slice, Link & link, uint32_t l1id, uint32_t l1id_mask, uint16_t bcid) {
            if (slice.m_l1id == uint32_t(-1)) {
                if (!m_builder.getTriggerInfo(link.m_packets_counter, slice.m_l1id, slice.m_bcid)) {
                    getSeedFromLink(slice, link, l1id, l1id_mask, bcid);
                }
            }
            else if ((slice.m_l1id & l1id_mask) == l1id && slice.m_bcid == 0xffff) {
                slice.m_bcid = bcid;
            }
        }

        void getSeedFromLink(Slice & slice, Link & link, uint32_t l1id,
                uint32_t l1id_mask, uint16_t bcid) {
            if ((link.m_expected_l1id & l1id_mask) == l1id) {
                slice.m_l1id = link.m_expected_l1id;
                slice.m_bcid = bcid;
            } else if (((((link.m_expected_l1id>>24)+1)<<24) & l1id_mask) == l1id) {
                // This may be an ECR
                slice.m_l1id = ((link.m_expected_l1id>>24)+1)<<24;
                slice.m_bcid = bcid;
            }
        }

        template <bool TTC = ReceiveTTC>
        typename std::enable_if<!TTC>::type getSeed(
                Slice & slice, Link & link, uint32_t l1id, uint32_t l1id_mask, uint16_t bcid) {
            if (slice.m_l1id == uint32_t(-1)) {
                getSeedFromLink(slice, link, l1id, l1id_mask, bcid);
            }
            else if ((slice.m_l1id & l1id_mask) == l1id && slice.m_bcid == 0xffff) {
                slice.m_bcid = bcid;
            }
        }

        static void setCorruptStatus(CorruptPacketException & ex,
                uint8_t & packet_status, uint32_t & fragment_status) {
            fragment_status = ROBStatus::Generic::CorruptData;
            fragment_status |= ROBStatus::Specific::TxError;
            if (ex.get_l1id() != (ex.get_l1a_l1id() & ex.get_l1id_mask())) {
                packet_status |= GBTChunk::Status::L1IdMismatch;
                fragment_status |= ROBStatus::Generic::L1IDCheckFailed;
            }
            if (ex.get_bcid() != ex.get_l1a_bcid()) {
                packet_status |= GBTChunk::Status::BCIdMismatch;
                fragment_status |= ROBStatus::Generic::BCIDCheckFailed;
            }
            if (ex.get_crc_failed()) {
                packet_status |= GBTChunk::Status::CRCError;
            }
        }

    private:
        const uint32_t m_maximum_buffer_size;
        const uint32_t m_initial_buffer_size;
        const uint32_t m_recovery_depth;
        const bool m_flush_buffer;
        const bool m_drop_corrupted;
        uint64_t m_buffer_overflow_counter;
        GBTModeBuilder<ReceiveTTC> & m_builder;
        detail::MemoryPool m_memory_pool;
        detail::CircularBuffer<Slice> m_buffer;
    };

    template <bool ReceiveTTC>
    GBTModeWorker<ReceiveTTC>::GBTModeWorker(const InputLinkVector & links,
            const boost::property_tree::ptree & robConfig, const Core & core,
            const std::shared_ptr<DataInput> & input, GBTModeBuilder<ReceiveTTC>& builder) :
            ROBFragmentWorkerBase(links, robConfig, core, input,
                    detail::InputCallback::make<
                        GBTModeWorker<ReceiveTTC>, Link,
                        &GBTModeWorker<ReceiveTTC>::dataReceived>(*this)),
            m_maximum_buffer_size(
                    PTREE_GET_VALUE(robConfig, uint32_t, "FragmentBuilder.BufferSize")),
            m_initial_buffer_size(
                    PTREE_GET_VALUE(robConfig, uint32_t, "FragmentBuilder.MinimumBufferSize")),
            m_recovery_depth(
                    PTREE_GET_VALUE(robConfig, uint32_t, "FragmentBuilder.RecoveryDepth")),
            m_flush_buffer(
                    PTREE_GET_VALUE(robConfig, bool, "FragmentBuilder.FlushBufferAtStop")),
            m_drop_corrupted(
                    PTREE_GET_VALUE(robConfig, bool, "FragmentBuilder.DropCorruptedPackets")),
            m_buffer_overflow_counter(0),
            m_builder(builder),
            m_memory_pool(m_links_number * (m_max_message_size
                    + sizeof(GBTChunk::Header) + 3 /* for 4-byte alignment */)),
            m_buffer(m_maximum_buffer_size, m_initial_buffer_size,
                    [this](){return Slice(m_memory_pool.alloc());},
                    std::bind(&GBTModeWorker<ReceiveTTC>::handleBufferOverflow, this))
    { }


    template <bool ReceiveTTC>
    void GBTModeWorker<ReceiveTTC>::handleBufferOverflow() {
        if (!(m_buffer_overflow_counter++ % m_buffer.size())) {
            for (auto & link : m_links) {
                if (link.m_packets_counter < m_buffer.head()) {
                    ers::error(NoDataReceived(ERS_HERE, link.m_fid, link.m_packets_counter,
                            m_buffer_overflow_counter));
                }
            }
        }
        sliceReady(ROBStatus::Generic::DataTimedout);
    }

    template <bool ReceiveTTC>
    bool GBTModeWorker<ReceiveTTC>::handleMissedPackets(Link & link,
            uint32_t l1id, uint32_t l1id_mask, uint16_t bcid) {

        uint32_t packets_missed = 0;
        Link original(link);
        for (uint32_t step = 0; step < m_recovery_depth; ++step) {
            if (!m_running) {
                return false;
            }
            Slice & slice = m_buffer[link.m_packets_counter];
            getSeed(slice, link, l1id, l1id_mask, bcid);

            ERS_LOG("Link 0x" << std::hex << link.m_fid
                    << " data error: seed is set to 0x" << slice.m_l1id
                    << std::dec << " counter = " << link.m_packets_counter);

            if ((slice.m_l1id & l1id_mask) != l1id
                    || (bcid != 0xffff && slice.m_bcid != 0xffff && bcid != slice.m_bcid)) {
                ERS_LOG("Link 0x" << std::hex << link.m_fid << " data error:"
                        << " L1ID = 0x" << l1id << " and BCID 0x" << bcid
                        << " do not match current ROB L1ID = 0x" << slice.m_l1id
                        << " and BCID = 0x" << slice.m_bcid
                        << " expected L1ID = 0x" << link.m_expected_l1id
                        << " packets missed = " << std::dec << link.m_packets_missed
                        << " packets counter = " << link.m_packets_counter);

                link.m_packets_missed++;
                link.m_packets_counter++;
                if (slice.m_l1id != uint32_t(-1)) {
                    link.m_expected_l1id = slice.m_l1id + 1;
                } else {
                    link.m_expected_l1id++;
                }
                ++packets_missed;
                continue;
            }

            // Packet match has been found!
            link.m_expected_l1id = slice.m_l1id + 1;
            ERS_LOG("Link 0x" << std::hex << link.m_fid << " data error:"
                    << " the issue has been recovered, "
                    << " matched l1id = 0x" << slice.m_l1id
                    << " matched bcid = 0x" << slice.m_bcid
                    << " next expected L1ID = 0x" << link.m_expected_l1id
                    << std::dec << " missed packets = " << packets_missed
                    << " total packets missed = "<< link.m_packets_missed);

            for (uint32_t i = original.m_packets_counter; i < link.m_packets_counter; ++i) {
                Slice & ss = m_buffer[i];
                ss.m_packets_missed++;
                ss.m_links_counter++;
                if (ss.m_links_counter == m_links_number) {
                    sliceReady();
                }
            }
            return true;
        }

        link.m_expected_l1id = original.m_expected_l1id;
        link.m_packets_missed = original.m_packets_missed;
        link.m_packets_counter = original.m_packets_counter;

        if (m_running) {
            ERS_LOG("Link 0x" << std::hex << link.m_fid
                    << " data error: recovery attempt has failed, packet counter = "
                    << std::dec << link.m_packets_counter);
        }

        return false;
    }

    template <bool ReceiveTTC>
    void GBTModeWorker<ReceiveTTC>::handlePacketError(
            const uint8_t * packet, uint32_t size, Link & link, Slice & slice,
            uint32_t l1id, uint32_t l1id_mask, uint16_t bcid) {
        std::optional<bool> integrity = m_data_integrity_checker(packet, size);
        ERS_LOG("Link 0x" << std::hex << link.m_fid << " data error:"
                << " L1ID bit mask = 0x" << l1id_mask
                << " L1ID received = 0x" << l1id
                << " BCID received = 0x" << bcid
                << " current ROB L1ID = 0x" << slice.m_l1id
                << " current ROB BCID = 0x" << slice.m_bcid
                << " expected l1id = 0x" << link.m_expected_l1id
                << " packets counter = " << std::dec << link.m_packets_counter
                << " data integrity check returns " <<
                (integrity ? (integrity.value() ? "good" : "bad") : "undefined"));

        if (!integrity.value_or(true)) {
            ERS_LOG("Link 0x" << std::hex << link.m_fid << " data error:"
                    << " packet will be " << (m_drop_corrupted ? "dropped" : "accepted"));
            throw CorruptPacketException(ERS_HERE, link.m_fid, link.m_packets_counter,
                    l1id_mask, l1id, bcid, slice.m_l1id, slice.m_bcid, true);
        }

        ERS_LOG("Link 0x" << std::hex << link.m_fid << " data error: packet integrity has been confirmed");

        if (((link.m_expected_l1id - 1) & l1id_mask) == l1id && link.m_last_bcid == bcid) {
            ERS_LOG("Link 0x" << std::hex << link.m_fid
                    << " data error: duplicate packet with L1ID = 0x" << l1id << " is dropped");
            throw DuplicatePacketException(ERS_HERE, link.m_fid, link.m_packets_counter,
                    l1id, bcid);
        }

        bool l1id_matched = (slice.m_l1id & l1id_mask) == l1id;
        bool bcid_matched = bcid != 0xffff && slice.m_bcid != 0xffff && bcid == slice.m_bcid;
        if (l1id_matched != bcid_matched) {
            // This is either L1ID mismatch or BCID mismatch but not both.
            throw CorruptPacketException(ERS_HERE, link.m_fid, link.m_packets_counter,
                    l1id_mask, l1id, bcid, slice.m_l1id, slice.m_bcid, false);
        }
        else {
            if (!handleMissedPackets(link, l1id, l1id_mask, bcid)) {
                throw CorruptPacketException(ERS_HERE, link.m_fid, link.m_packets_counter,
                        l1id_mask, l1id, bcid, slice.m_l1id, slice.m_bcid, false);
            }
        }
    }

    template <bool ReceiveTTC>
    void GBTModeWorker<ReceiveTTC>::dataReceived(
            Link & link, const uint8_t* data, uint32_t size, uint8_t status, uint32_t rob_status) {

        ERS_DEBUG(5, "Data packet for the link 0x" << std::hex << link.m_fid
                << " is received, size = " << std::dec << size
                << " packet counter = " << link.m_packets_counter);

#define SWROD_UNLIKELY(x) __builtin_expect(!!(x),0)

        if (SWROD_UNLIKELY(not m_running)) {
            link.m_packets_dropped++;
            ers::error(DroppedPacketException(ERS_HERE, link.m_fid,
                    link.m_packets_counter, link.m_packets_dropped,
                    SpuriousPacketException(ERS_HERE, link.m_fid)));
            return;
        }

        uint8_t packet_status = GBTChunk::Status::Ok;
        uint16_t bcid = 0xffff;
        Slice * slice = 0;

        try {
            slice = &m_buffer[link.m_packets_counter];

            uint32_t l1id, l1id_mask;
            std::tie(l1id, l1id_mask, bcid) = m_trigger_info_extractor(data, size);

            getSeed(*slice, link, l1id, l1id_mask, bcid);

            ERS_DEBUG(4, "Seed is set: L1ID = 0x"
                    << std::hex << slice->m_l1id << " BCID = 0x" << slice->m_bcid
                    << std::dec << " counter = " << link.m_packets_counter
                    << " link is 0x" << std::hex << link.m_fid
                    << " expected l1id 0x" << std::hex << link.m_expected_l1id );

            if (SWROD_UNLIKELY(
                    (slice->m_l1id & l1id_mask) != l1id
                    || (bcid != slice->m_bcid && bcid != 0xffff && slice->m_bcid != 0xffff))) {
                handlePacketError(data, size, link, *slice, l1id, l1id_mask, bcid);

                // Packets counter may have been changed by the recovery procedure
                slice = &m_buffer[link.m_packets_counter];
            }
        }
        catch (BufferUnderflowException & ex) {
            link.m_packets_dropped++;
            link.m_packets_counter++;
            ers::error(DroppedPacketException(ERS_HERE,
                    link.m_fid, link.m_packets_counter, link.m_packets_dropped, ex));
            return;
        }
        catch (DuplicatePacketException & ex) {
            link.m_packets_dropped++;
            ers::error(DroppedPacketException(ERS_HERE,
                    link.m_fid, link.m_packets_counter, link.m_packets_dropped, ex));
            return;
        }
        catch (CorruptPacketException & ex) {
            link.m_packets_corrupted++;
            if (m_drop_corrupted) {
                link.m_packets_dropped++;
                link.m_packets_counter++;
                ers::error(DroppedPacketException(ERS_HERE,
                        link.m_fid, link.m_packets_counter, link.m_packets_dropped, ex));
                slice->m_packets_missed++;
                if (++slice->m_links_counter == m_links_number) {
                    sliceReady();
                }
                return;
            }
            slice->m_packets_corrupted++;
            setCorruptStatus(ex, packet_status, slice->m_status);
            ers::error(DataCorruptionException(ERS_HERE,
                    link.m_fid, link.m_packets_counter, link.m_packets_corrupted, ex));
        }
        catch (Exception & ex) {
            // Exception was thrown by the custom TTC information extraction function
            // The packet is unconditionally dropped
            link.m_packets_corrupted++;
            link.m_packets_dropped++;
            ers::error(DroppedPacketException(ERS_HERE,
                    link.m_fid, link.m_packets_counter, link.m_packets_dropped, ex));
            return;
        }

        GBTChunk::Header header{0, status, packet_status, link.m_resource_id};

#define LIKELY(x) __builtin_expect(!!(x), 1)
        if (LIKELY(slice->m_block.append(header, data, size))) {
            slice->m_status |= rob_status;
            link.m_last_bcid = bcid;
        }
        else {
            link.m_packets_dropped++;
            slice->m_status |= ROBStatus::Specific::Truncated;
            slice->m_status |= ROBStatus::Generic::BufferOverflow;
            slice->m_packets_missed++;
            ers::error(DroppedPacketException(
                    ERS_HERE, link.m_fid, link.m_packets_counter, link.m_packets_dropped,
                    ScarceBufferException(ERS_HERE, link.m_fid, link.m_packets_counter, size)));
        }

        if (LIKELY(slice->m_l1id != uint32_t(-1))) {
            link.m_expected_l1id = slice->m_l1id + 1;
        } else {
            link.m_expected_l1id++;
        }

        if (++slice->m_links_counter == m_links_number) {
            sliceReady();
        }

        link.m_packets_counter++;
    }

    template <bool ReceiveTTC>
    void GBTModeWorker<ReceiveTTC>::sliceReady(uint32_t status) {
        uint64_t index = m_buffer.head();
        Slice slice(std::move(m_buffer.pop()));
        slice.m_status |= status;

        ERS_DEBUG(4, "ROB's " << m_ROB_id << " slice for l1id = 0x" << std::hex
                << slice.m_l1id << " is ready, slice index = " << std::dec << index
                << " slice size = " << slice.m_block.dataSize());

        uint16_t packets_missed = slice.m_packets_missed + (m_links_number - slice.m_links_counter);
        if (packets_missed) {
            slice.m_status |= ROBStatus::Specific::ShortFragment;
            slice.m_status |= ROBStatus::Generic::CorruptData;
        }

        m_builder.sliceReady(index, std::move(slice.m_block),
                slice.m_l1id, slice.m_bcid, slice.m_status, packets_missed, slice.m_packets_corrupted);
    }

    template <bool ReceiveTTC>
    void GBTModeWorker<ReceiveTTC>::runStarted(const RunParams & run_params) {
        ERS_DEBUG(1, "Starting data processing");

        m_buffer.reset();
        DataInputHandlerBase::runStarted(run_params);

        ERS_DEBUG(1, "Data processing has been started");
    }

    template <bool ReceiveTTC>
    void GBTModeWorker<ReceiveTTC>::runStopped() {
        ERS_DEBUG(1, "Stopping data processing");

        DataInputHandlerBase::runStopped();

        if (!m_flush_buffer) {
            for (auto & link : m_links) {
                ERS_DEBUG(2, "Emptying input buffer that has "
                        << (link.m_packets_counter - m_buffer.head()) << " partially build fragments");
                while (m_buffer.head() < link.m_packets_counter) {
                    sliceReady();
                }
            }
        }

        ERS_DEBUG(1, "Data processing has been stopped");
    }

    template <bool ReceiveTTC>
    void GBTModeWorker<ReceiveTTC>::enabled(uint64_t triggersCounter) {
        m_buffer.reset(triggersCounter);
    }

    template <bool ReceiveTTC>
    void GBTModeWorker<ReceiveTTC>::linkDisabled(const Link & disabled_link) {
        // Decrease the counter of collected chunks for the slices that already
        // contain the just disabled input link
        for (uint64_t i = m_buffer.head(); i < disabled_link.m_packets_counter; ++i) {
            m_buffer[i].m_links_counter--;
        }

        // Check the slices that don't contain the just disabled link
        // If they already contain the m_links_number of chunks
        // then report them as complete and pass to the fragment builder

        if (m_buffer.head() < disabled_link.m_packets_counter) {
            // The above mentioned condition is guaranteed to be not meet
            return;
        }

        uint64_t processed = 0;
        for (Link & link : m_links) {
            processed = link.m_packets_counter;
            for (uint64_t i = m_buffer.head(); i < processed; ++i) {
                if (m_buffer[i].m_links_counter == m_links_number) {
                    sliceReady();
                } else {
                    break;
                }
            }
        }
    }
}

#endif /* SWROD_GBTMODEWORKER_H_ */
