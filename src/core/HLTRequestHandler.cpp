#include <algorithm>
#include <cstdint>
#include <pthread.h>
#include <sched.h>

#include <boost/property_tree/ptree.hpp>

#include "swrod/Core.h"
#include "swrod/Factory.h"
#include "swrod/exceptions.h"

#include <swrod/detail/WorkerThread.h>

#include "HLTRequestHandler.h"
#include "HLTDataBuffer.h"

#include "HLTRequestManager.h"
#include "HLTRequestDescriptor.h"
#include "HLTDataServer.h"
#include "SwrodOutputMsg.h"

#include "ipc/partition.h"
#include "asyncmsg/asyncmsg.h"
#include "asyncmsg/NameService.h"

#include "monsvc/MonitoringService.h"              // for MonitoringService

#include "ers/ers.h"

#include "TH1D.h"

using namespace boost::property_tree;
using namespace swrod;


std::weak_ptr<HLTRequestHandler> HLTRequestHandler::s_instance;
std::mutex HLTRequestHandler::s_mutex;

std::shared_ptr<HLTRequestHandler> HLTRequestHandler::instance(const ptree& config,
                                                               const ptree& coreConfig){
   std::lock_guard<std::mutex> lock(s_mutex);
   //std::cout << "HLTRequestHandler::instance(...):";
   std::shared_ptr<HLTRequestHandler> sp=s_instance.lock();
   if (!sp) {
      ERS_LOG ("s_instance not set, creating new instance");
      sp.reset(new HLTRequestHandler(config, coreConfig));
      s_instance=sp;
   }
   std::cout << std::endl;
   return sp;
}

HLTRequestHandler::HLTRequestHandler(const ptree& config,
                                     const ptree& coreConfig)
try :
   m_active(false),
      m_nChannels(0),
      m_nameService(0),
      m_runNumber(0),
      m_lastClearXId(0),
      m_monitoringService(monsvc::MonitoringService::instance()),
      m_dataRequestsReceived(0),
      m_clearRequestsReceived(0),
      m_clearRequestsMissed(0),
      m_clearRequestsDuplicated(0),
      m_badRobRequests(0) {
      ERS_LOG("constructor starting");
   m_maxIndex=config.get<unsigned int>("MaxIndex");

   m_ignoreClearXId=config.get<bool>("IgnoreClearXId");
   m_requestTimeOut=
      config.get<unsigned int>("DataRequestTimeout");
   unsigned int clearTimeout=
      config.get<unsigned int>("ClearTimeout");
   //std::cout << "HLTRequestHandler Creating HLTRequestManager\n";
   m_requestManager.reset(new HLTRequestManager(this,
                                                m_requestTimeOut,//conditionTimeout,
                                                m_requestTimeOut,
                                                clearTimeout));

   float maxTime=1.5*m_requestTimeOut;
   if (maxTime==0) {
      maxTime=500000;
   }
   m_timeHistogram=m_monitoringService.register_object(
      "/EXPERT/requestLatency",
      new TH1D("requestLatency", "Request latency; latency #mu s", 500, 0, maxTime),
      true);
   auto nRobs=0;
   for (auto mod: coreConfig.get_child("Modules")){
      nRobs+=mod.second.get_child("ROBs").size();
   }
   float maxSize=nRobs*2500;
   m_sizeHistogram=m_monitoringService.register_object(
      "/EXPERT/dataSize",
      new TH1D("dataSize","data size (32bit words); size (32bit words)", 500, 0, maxSize),
      true);
   m_channelsHistogram=m_monitoringService.register_object(
      "/EXPERT/nChannels",
      new TH1D("nChannels",
               "Number of channels in data request; n channels requested",
               nRobs, 0.5, nRobs+0.5),
      true);

   bool standalone=config.get<bool>("environment.standalone",false);

   if (!standalone) {
      std::vector<std::string> networks;
      for (auto tree : coreConfig.get_child("DataFlowParameters.networks")) {
         networks.push_back(tree.second.data());
      }
      auto partName=coreConfig.get<std::string>("Partition.Name");
      auto ipcPartition=IPCPartition(partName.c_str());
      m_nameService=new daq::asyncmsg::NameService(ipcPartition,
                                                   networks);
   }

   auto cpuSet=config.get<std::string>("CPU");
   auto cpus=swrod::detail::WorkerThread::parseCpuRange(cpuSet);
   if (!cpus.empty()) {
      cpu_set_t pmask;
      CPU_ZERO(&pmask);
      for (int cpu : cpus) {
         CPU_SET(cpu, &pmask);
      }
      auto status=sched_setaffinity(0, sizeof(pmask), &pmask);
      if (status != 0) {
         ERS_LOG("Setting affinity to CPU rage [" << cpuSet
                 << "] failed with error = " << status);
      }
   }

   auto nIOServices=config.get<unsigned int>("IOServices");
   auto myName=coreConfig.get<std::string>("Application.Name");
   m_dataIoServices.reset(new std::vector<boost::asio::io_service>(nIOServices));
   m_dataServer=std::make_shared<HLTDataServer>(*m_dataIoServices,
                                                myName,
                                                m_nameService,
                                                this);
   auto nThreads=config.get<unsigned int>("DataServerThreads");
   m_dataServer->configure(nThreads);
   //std::cout << "HLTRequestHandler finished configuring data server\n";std::cout.flush();

   int service=m_dataServer->getIoService();
   std::shared_ptr<HLTDataServerSession> session=
      std::make_shared<HLTDataServerSession>(
         (*m_dataIoServices)[service],
         this);
   m_dataServer->asyncAccept(session);

   std::string mcAddress=coreConfig.get<std::string>("DataFlowParameters.MulticastAddress");
   std::string proto=mcAddress.substr(0,mcAddress.find(":"));
   if (!proto.empty()) {
      mcAddress=mcAddress.substr(mcAddress.find(":")+1);
   }
   m_clearIoService.reset(new std::vector<boost::asio::io_service>(1));
   if (mcAddress.empty() || proto=="tcp") {
      ERS_LOG("creating unicast ClearSession");
      m_clearServer=std::make_shared<HLTDataServer>(*m_clearIoService,
                                                    myName,
                                                    m_nameService,
                                                    this);
      m_clearServer->configure(1,"CLEAR_");
      m_clearServer->asyncAccept(std::make_shared<HLTDataServerSession>(
                                    (*m_clearIoService)[0],this));
   }
   else {
      ERS_LOG ("creating multicast ClearSession");
      auto sPos=mcAddress.find("/");
      std::string mcInterface;
      if (sPos!=std::string::npos) {
         mcInterface=mcAddress.substr(sPos+1);
      }
      mcAddress=mcAddress.substr(0,sPos);
      m_clearSession=std::make_shared<ClearSession>((*m_clearIoService)[0],
                                                    mcAddress,
                                                    mcInterface,
                                                    this);
      m_clearSession->asyncReceive();
   }
   ERS_LOG("constructor Completed");
}
catch (ptree_error& err) {
   throw(BadConfigurationException(ERS_HERE,err.what()));
}


HLTRequestHandler::~HLTRequestHandler(){
   ERS_LOG ("destructor entered");
   m_dataServer->unconfigure();
   m_dataServer.reset();

   if (m_clearServer) {
      m_clearServer->unconfigure();
      m_clearServer.reset();
   }

   if (m_clearSession) {
      m_clearSession->close();
      m_clearSession.reset();
   }
   if (m_clearIoService) {
      for (unsigned int service=0;service<m_clearIoService->size();service++) {
         m_clearIoService->at(service).stop();
//         m_clearIoService->at(service).reset();
      }
   }

   if (m_nameService!=0) {
      delete m_nameService;
      m_nameService=0;
   }

   m_timeHistogram.lock();
   TH1D* hist=m_timeHistogram.get();
   m_monitoringService.remove_object(hist);
   m_timeHistogram.unlock();

   m_sizeHistogram.lock();
   hist=m_sizeHistogram.get();
   m_monitoringService.remove_object(hist);
   m_sizeHistogram.unlock();

   m_channelsHistogram.lock();
   hist=m_channelsHistogram.get();
   m_monitoringService.remove_object(hist);
   m_channelsHistogram.unlock();
}



std::shared_ptr<HLTDataBuffer>
HLTRequestHandler::addBuffer(uint32_t robId,
                             const boost::property_tree::ptree& config) {
   ERS_LOG("Adding ROB ID: " << std::hex << robId << std::dec);
   m_robs.insert(robId);
   m_channelIndex[robId]=m_dataBuffer.size();
   m_nChannels++;
   m_disabled.push_back(false);
   auto sp=std::make_shared<HLTDataBuffer>(robId,
                                           m_maxIndex,
                                           s_instance.lock(),
                                           m_requestManager);
   m_dataBuffer.push_back(sp);
   return sp;
}


void HLTRequestHandler::resetStats(){
   m_timeHistogram->Reset();
   m_sizeHistogram->Reset();
   m_channelsHistogram->Reset();

   m_dataRequestsReceived=0;
   m_clearRequestsReceived=0;
   m_clearRequestsMissed=0;
   m_clearRequestsDuplicated=0;
   m_badRobRequests=0;
   m_totalRobsRequested=0;
}

std::vector<unsigned int> HLTRequestHandler::latestL1()const {
   std::vector<unsigned int> latest;
   latest.reserve(m_nChannels);
   ERS_ASSERT (m_disabled.size()==m_dataBuffer.size());
   for (unsigned int chanIndex=0;chanIndex<m_nChannels;chanIndex++) {
      if (!m_disabled[chanIndex]){
         auto buf=m_dataBuffer[chanIndex].lock();
         if (buf) {
            latest.push_back(buf->latestL1Available());
         }
      }
   }
   return latest;
}

void HLTRequestHandler::ROBEnabled(unsigned int robId){
   auto iter=m_channelIndex.find(robId);
   if (iter==m_channelIndex.end()) {
      ers::error(BadRobIssue(ERS_HERE,robId));
      return;
   }
   m_disabled[iter->second]=false;
}
void HLTRequestHandler::ROBDisabled(unsigned int robId){
   auto iter=m_channelIndex.find(robId);
   if (iter==m_channelIndex.end()) {
      ers::error(BadRobIssue(ERS_HERE,robId));
      return;
   }
   m_disabled[iter->second]=true;
}

void HLTRequestHandler::runStarted(const RunParams& runPars){
   if (!m_active) {
      m_runNumber=runPars.run_number;
      resetStats();
      std::fill(m_disabled.begin(),m_disabled.end(),false);
      m_requestManager->start();
      m_active=true;
   }
}

void HLTRequestHandler::runStopped(){
   if (m_active) {
      m_active=false;
      m_requestManager->stop();
   }
}

void HLTRequestHandler::processDataRequest(unsigned int level1Id,
                                           std::vector<unsigned int>* robs,
                                           std::shared_ptr<daq::asyncmsg::Session> session,
                                           unsigned int transactionId) {
   m_dataRequestsReceived++;
   m_totalRobsRequested+=robs->size();
   //std::cout << "HLTRequestHandler::processDataRequest()\n";std::cout.flush();

   auto desc=new HLTRequestDescriptor();
   desc->initialise(level1Id,
                    0,
                    session,
                    transactionId);

   auto endIter=m_robs.end();
   auto requestRobs=desc->channels();
   for (auto rob: *robs) {
      if (m_robs.find(rob)!=endIter) {
         requestRobs->insert(rob);
      }
      else {
         m_badRobRequests++;
         ers::error(HltBadRobReqIssue(ERS_HERE,rob));
      }
   }

   if (requestRobs->size()) {
      m_requestManager->addRequest(desc);
   }
}

void HLTRequestHandler::processDataRequest(HLTRequestDescriptor* desc){
   //std::cout << "HLTRequestHandler::processDataRequest(desc) \n";std::cout.flush();
   auto level1Id=desc->level1Id();
   auto robs=desc->channels();
   std::vector<std::shared_ptr<ROBFragment>> myFrags;
   myFrags.reserve(robs->size());

   for (auto robIter : *robs) {
      auto chanIndex=m_channelIndex[robIter];
      FragmentIndex::const_accessor evAcc;
      auto buf=m_dataBuffer[chanIndex].lock();
      if (buf && buf->find(evAcc,level1Id)) {
         myFrags.push_back(evAcc->second);
         buf->requested(level1Id);
      }
      else {
         unsigned int status;
         if (m_disabled[chanIndex]) {
            status=0x80000000;
            if (buf) {buf->discarded();}
         }
         else {
            auto now=std::chrono::system_clock::now();
            unsigned int elapsed=std::chrono::duration_cast<std::chrono::microseconds>
               (now-desc->requestTime()).count();
            if (elapsed<m_requestTimeOut) {
               status=0x20000000;
               if (buf) {buf->lost();}
            }
            else {
               status=0x40000000;
               if (buf) {buf->mayCome();}
            }
         }
         myFrags.emplace_back(new ROBFragment(robIter, level1Id, status));
      }
      evAcc.release();
   }

   std::unique_ptr<SwrodOutputMsg> message(
      new SwrodOutputMsg(desc,
                         m_runNumber,
                         m_requestManager->latestL1Available(),
                         myFrags));
   auto dest=desc->destination();
   auto session=dest.lock();
   if (session) {
      session->asyncSend(std::move(message));
   }
   // else {
   //    std::cout << "Message size: " << message->size() << std::endl;
   //    std::vector<boost::asio::const_buffer> buffers;
   //    message->toBuffers(buffers);
   //    std::cout << "Message buffers:\n" << std::hex ;
   //    for (auto buf : buffers) {
   //       const unsigned int* tPtr=(const unsigned int*) buf.data();
   //       for (unsigned int word=0;word<buf.size()/4;word++) {
   //          std::cout << " " << tPtr[word];
   //       }
   //    }
   //    std::cout << std::dec << std::endl;
   // }
}

void HLTRequestHandler::finalise(HLTRequestDescriptor* descriptor){
   auto elapsed=std::chrono::system_clock::now()-descriptor->requestTime();
   m_timeHistogram->Fill(std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count());
   m_sizeHistogram->Fill(descriptor->size()/4); // /4 for sixe in 32bit words
   m_channelsHistogram->Fill(descriptor->nChannelsRequested());

   delete descriptor;
}

void HLTRequestHandler::processClearRequest(std::vector<unsigned int>& l1Ids,
                                            unsigned int transactionId) {
   if (!m_ignoreClearXId && m_clearRequestsReceived!=0) {
      // Check transactionId is one more than last time
      int xidIncrement=transactionId-m_lastClearXId;
      if (xidIncrement>1) {
         m_clearRequestsMissed+=xidIncrement-1;
         ers::warning(HltMissedClearIssue(ERS_HERE, xidIncrement-1));
      }
      else if ((xidIncrement < 1 && transactionId != 0) //It is not a HLTSV restart
               || (transactionId == 0 && m_lastClearXId < 10)) { //It could be a HLTSV restart
         m_clearRequestsDuplicated++;
         return;
      }
   }
   m_lastClearXId=transactionId;
   m_clearRequestsReceived++;
   if (m_active) {
      m_requestManager->addClears(l1Ids);
   }
}

void HLTRequestHandler::clear(std::vector<unsigned int>& l1Ids){
   // move l1 ids into shared-ptr in case buffer returns before clear completes
   auto sharedL1=std::make_shared<std::vector<unsigned int> >();
   sharedL1->swap(l1Ids);
   for (auto buffer : m_dataBuffer) {
      auto buf=buffer.lock();
      if (buf) {
         buf->clear(sharedL1);
      }
   }
}


