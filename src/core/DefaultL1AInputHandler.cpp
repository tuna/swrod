/*
 * L1AInputHandler.cpp
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#include <ers/ers.h>
#include <src/core/DefaultL1AInputHandler.h>

#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/detail/L1AMessage.h>
#include <swrod/exceptions.h>
#include <swrod/detail/ptree.h>


using namespace swrod;
using namespace swrod::detail;
using namespace boost::property_tree;

namespace {
    Factory<L1AInputHandler>::Registrator __reg__(
            "L1AInputHandler",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_unique<DefaultL1AInputHandler>(config, core);
            });
}

DefaultL1AInputHandler::DefaultL1AInputHandler(
        const boost::property_tree::ptree & config, const Core & core) :
        L1AInputHandler(config, core),
        DataInputHandlerBase("L1A",
            {{PTREE_GET_VALUE(config, size_t, "Link"), PTREE_GET_VALUE(config, size_t, "Link")}},
            config, m_data_input,
            detail::InputCallback::make<DefaultL1AInputHandler,
                        &DefaultL1AInputHandler::dataReceived>(*this)),
    m_expected_l1id(0),
    m_counter(0)
{
}

void DefaultL1AInputHandler::resynchAfterRestart(uint32_t lastL1ID) {
    m_expected_l1id = (lastL1ID & 0xff000000) + 0x01000000;
}

void DefaultL1AInputHandler::dataReceived(const uint8_t* data, uint32_t size, uint8_t status, uint32_t rob_status) {
    L1AMessage & l1a = *(L1AMessage*)data;

    ERS_DEBUG(4, "L1A message received: l1id = 0x" << std::hex
            << l1a.ext_l1id << " bcid = 0x" << l1a.bcid);

    if (not m_running) {
        if (not m_links.empty()) {
            auto & link = *m_links.begin();
            link.m_packets_dropped++;
            ers::error(DroppedPacketException(ERS_HERE, link.m_fid,
                    m_counter + link.m_packets_counter, link.m_packets_dropped,
                    SpuriousPacketException(ERS_HERE, link.m_fid)));
        }
        return;
    }

    if (m_expected_l1id != l1a.ext_l1id) {
        if (l1a.l1id) {
            ers::error(L1ABrokenException(ERS_HERE, m_counter, m_expected_l1id, l1a.ext_l1id));
            // check if some packets were missed right after ECR
            uint8_t expected_ecr = m_expected_l1id>>24;
            if (l1a.ecr > expected_ecr || (!l1a.ecr && 0xff == expected_ecr)) {
                m_expected_l1id = l1a.ext_l1id & 0xff000000;
            }

            if (l1a.ext_l1id < m_expected_l1id) {
                ERS_LOG("L1Accept with L1ID = 0x" << std::hex << l1a.ext_l1id
                        << " will be ignored as it is smaller than expected L1ID = 0x" << m_expected_l1id);
                return;
            }

            while (m_expected_l1id < l1a.ext_l1id) {
                notify(L1AInfo(0xffff, 0xffff, m_expected_l1id++, m_counter++));
            }
        } else {
            // this is a change due to ECR
            uint8_t expected_ecr = (m_expected_l1id>>24) + 1;
            if (l1a.ecr != expected_ecr) {
                ers::warning(L1ABrokenException(ERS_HERE, m_counter, m_expected_l1id, l1a.ext_l1id));
            }
            m_expected_l1id = l1a.ext_l1id;
        }
    }

    notify(L1AInfo(l1a.bcid, l1a.trigger_type, m_expected_l1id++, m_counter++));
}
