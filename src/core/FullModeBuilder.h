/*
 * FullModeBuilder.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_FULLMODEBUILDER_H_
#define SWROD_FULLMODEBUILDER_H_

#include <type_traits>

#include <swrod/ROBFragmentBuilderBase.h>
#include "FullModeWorker.h"

namespace swrod {

    template <bool ReceiveTTC>
    class FullModeBuilder : public ROBFragmentBuilderBase {
    public:
        FullModeBuilder(const boost::property_tree::ptree& robConfig, const Core & core) :
            ROBFragmentBuilderBase(robConfig, core,
                std::bind(&FullModeBuilder<ReceiveTTC>::handleL1A<ReceiveTTC>, this, std::placeholders::_1),
                [&](const InputLinkVector & links, const std::shared_ptr<DataInput> & input) {
                    return new FullModeWorker<ReceiveTTC>(links, robConfig, core, input, *this);
                },
                PTREE_GET_VALUE(robConfig, bool, "FragmentBuilder.RODHeaderPresent"))
        { }

        template <bool TTC = ReceiveTTC>
        typename std::enable_if<TTC>::type dataReceived(const DataInputHandlerBase::Link & link,
                uint32_t l1id, uint16_t bcid, uint32_t status,
                ROBFragment::DataBlock && data);

        template <bool TTC = ReceiveTTC>
        typename std::enable_if<!TTC>::type dataReceived(const DataInputHandlerBase::Link & link,
                uint32_t l1id, uint16_t bcid, uint32_t status,
                ROBFragment::DataBlock && data);

        bool isRODHeaderPresent() const {
            return m_ROD_header_present;
        }

    private:
        template <bool TTC = ReceiveTTC>
        typename std::enable_if<TTC>::type handleL1A(const L1AInfo& l1a) {
            ERS_DEBUG(4, "L1A info with l1id = 0x" << std::hex << l1a.m_l1id << " received");

            FragmentAssembler::accessor a;
            if (!m_fragment_assembler.insert(a, l1a.m_l1id)) {
                // was in the map already
                a->second.m_index = l1a.m_index;
                a->second.m_l1id = l1a.m_l1id;
                a->second.m_trigger_type = l1a.m_trigger_type;
                if (a->second.m_data.empty()) {
                    a->second.m_bcid = l1a.m_bcid;
                    ERS_LOG("Duplicate L1A packet with L1ID = 0x" << std::hex << l1a.m_l1id << " received");
                    return;
                }
                else if (a->second.m_bcid != l1a.m_bcid) {
                    ers::error(BCIDMismatch(ERS_HERE, a->second.m_l1id, a->second.m_bcid, l1a.m_bcid));
                    if (l1a.m_bcid != 0xffff) {
                        a->second.m_bcid = l1a.m_bcid;
                    }
                }
                fragmentReady(a);
            } else {
                a->second.m_index = l1a.m_index;
                a->second.m_l1id = l1a.m_l1id;
                a->second.m_bcid = l1a.m_bcid;
                a->second.m_trigger_type = l1a.m_trigger_type;
                a->second.m_data.reserve(1);
            }
        }

        template <bool TTC = ReceiveTTC>
        typename std::enable_if<!TTC>::type handleL1A(const L1AInfo& l1a) {
        }
    };

    template <bool ReceiveTTC>
    template <bool TTC>
    typename std::enable_if<TTC>::type FullModeBuilder<ReceiveTTC>::dataReceived(
            const DataInputHandlerBase::Link & link, uint32_t l1id, uint16_t bcid,
            uint32_t status, ROBFragment::DataBlock && data) {
        ERS_DEBUG(5, "Data fragment with l1id = 0x" << std::hex << l1id << " received");

        FragmentAssembler::accessor a;
        if (!m_fragment_assembler.insert(a, l1id)) {
            // was in the map already
            if (!a->second.m_data.empty()) {
                ERS_LOG("Duplicate data packet with L1ID = 0x" << std::hex << l1id
                        << " will be ignored");
                throw DuplicatePacketException(ERS_HERE, link.m_fid,
                        link.m_packets_counter, l1id, bcid);
            }
            if (a->second.m_bcid != bcid) {
                ers::error(BCIDMismatch(ERS_HERE, l1id, bcid, a->second.m_bcid));
                if (a->second.m_bcid == 0xffff) {
                    a->second.m_bcid = bcid;
                }
            }
            a->second.m_status = status;
            a->second.m_data.push_back(std::move(data));
            fragmentReady(a);
        } else {
            a->second.m_data.push_back(std::move(data));
            a->second.m_l1id = l1id;
            a->second.m_bcid = bcid;
            a->second.m_status = status;
        }
    }

    template <bool ReceiveTTC>
    template <bool TTC>
    typename std::enable_if<!TTC>::type FullModeBuilder<ReceiveTTC>::dataReceived(
            const DataInputHandlerBase::Link & link, uint32_t l1id, uint16_t bcid,
            uint32_t status, ROBFragment::DataBlock && data) {
        ERS_DEBUG(4, "Data fragment with l1id = 0x" << std::hex << l1id << " received");

        FragmentAssembler::accessor a;
        if (!m_fragment_assembler.insert(a, l1id)) {
            // was in the map already
            ERS_LOG("Duplicate data packet with L1ID = 0x" << std::hex << l1id
                    << " will be ignored");
            throw DuplicatePacketException(ERS_HERE, link.m_fid,
                    link.m_packets_counter, l1id, bcid);
        } else {
            a->second.m_l1id = l1id;
            a->second.m_bcid = bcid;
            a->second.m_status = status;
            a->second.m_trigger_type = 0;
            a->second.m_data.push_back(std::move(data));
            fragmentReady(a);
       }
    }
}

#endif /* SWROD_FULLMODEBUILDER_H_ */
