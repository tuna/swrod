/*
 * NetioNextInput.h
 *
 *  Created on: Feb 14, 2020
 *      Author: kolos
 */

#ifndef SRC_CORE_NETIONEXTINPUT_H_
#define SRC_CORE_NETIONEXTINPUT_H_

#include <map>

#include <boost/property_tree/ptree.hpp>

#include <felixbus/bus.hpp>
#include <felixbus/elinktable.hpp>
#include <felixbus/felixtable.hpp>
#include <felix/felix_client_status.h>

#include <netio/netio.h>

#include <swrod/DataInput.h>
#include <swrod/detail/WorkerThread.h>

namespace swrod {
    class Core;

    class NetioNextInput : public DataInput {
    public:
        NetioNextInput(const boost::property_tree::ptree & config);

        ~NetioNextInput();

        void actionsPending() override;

        void subscribeToFelix(const InputLinkId & link) override;

        void unsubscribeFromFelix(const InputLinkId & link) override;

        uint32_t translateStatus(uint8_t status) const noexcept override;

    private:
        static void netioMessageReceived(
                netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size) {
            NetioNextInput * ptr = (NetioNextInput*)socket->usr;
            ERS_DEBUG(5, "Received " << size << " bytes packet for 0x" << std::hex << tag << " elink");
            ptr->dataReceived(tag, (uint8_t*)data + 1, size - 1, *(uint8_t*)data);
        }

        static void signalHandler(void *);

        void run(const bool & active);

    private:
        typedef std::map<std::string, netio_subscribe_socket> SocketMap;

        const uint32_t m_timeout_ms;
        const std::string m_interface;
        felix::bus::Bus m_felix_bus;
        felix::bus::FelixTable m_felix_table;
        felix::bus::ElinkTable m_elink_table;
        netio_context m_context;
        netio_signal m_signal;
        SocketMap m_sockets;
        detail::WorkerThread m_thread;
    };
}

#endif /* SRC_CORE_NETIONEXTINPUT_H_ */
