// -*- c++ -*-
#ifndef HLT_DATA_SERVER_H
#define HLT_DATA_SERVER_H

#include <boost/asio/io_service.hpp>   // for io_service
#include <memory>                      // for unique_ptr, shared_ptr
#include <string>                      // for string
#include <vector>                      // for vector

#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/ClearSession.h"

namespace daq{
   namespace asyncmsg{
      class InputMessage;
      class OutputMessage;
      class NameService;
      class Session;
   }
}

namespace swrod{
   class HLTRequestHandler;

   class HLTDataServer: public ROS::DataServer {
   public:
      HLTDataServer(std::vector<boost::asio::io_service>& ioService,
                    const std::string& iomName,
                    daq::asyncmsg::NameService* nameService,
                    HLTRequestHandler* hltRequestHandler);
      virtual ~HLTDataServer();
   private:
      virtual void onAccept(std::shared_ptr<daq::asyncmsg::Session>) noexcept;
      HLTRequestHandler* m_hltRequestHandler;
   };


   class HLTDataServerSession: public ROS::DataServerSession {

   public:
      HLTDataServerSession(boost::asio::io_service& ioService,
                           HLTRequestHandler* hltRequestHandler);
      virtual ~HLTDataServerSession();
   private:
      void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) final;
      void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage>  message)
         noexcept final;
      HLTRequestHandler* m_hltRequestHandler;
   };


   class ClearSession: public ROS::ClearSession {
   public:
      ClearSession(boost::asio::io_service& ioService,
                   const std::string& mcAddress,
                   const std::string& mcInterface,
                   HLTRequestHandler* hltRequestHandler):
         ROS::ClearSession(ioService, mcAddress, mcInterface),
         m_hltRequestHandler(hltRequestHandler){
      };
      virtual ~ClearSession() {};
   private:
      void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) final;
      HLTRequestHandler* m_hltRequestHandler;
   };
}
#endif
