#include <cstdint>
#include <sstream>
#include <boost/property_tree/ptree.hpp>

#include "HLTDataBuffer.h"
#include "HLTRequestHandler.h"
#include "HLTRequestManager.h"

#include "swrod/Core.h"
#include "swrod/Factory.h"
#include "swrod/exceptions.h"

#include "ers/ers.h"

using namespace boost::property_tree;
using namespace swrod;


namespace {
   Factory<ROBFragmentConsumer>::Registrator
   __reg__("HLTRequestHandler",
           [](const boost::property_tree::ptree& config,
              const Core& core) {
              auto server=HLTRequestHandler::instance(config,
                                                      core.getConfiguration());
              return server->addBuffer(config.get_child("RobConfig").get<uint32_t>("Id"), config);
           });
}



HLTDataBuffer::HLTDataBuffer(unsigned int robId,
                             unsigned int maxIndex,
                             std::shared_ptr<HLTRequestHandler> requestHandler,
                             std::shared_ptr<HLTRequestManager> requestManager)
   : m_robId(robId),
     m_maxIndex(maxIndex),
     m_requestHandler(requestHandler),
     m_requestManager(requestManager),
     m_active(false),
     m_disabled(false) {
   std::ostringstream name;
   name << "HLTDataBuffer" 
        << std::hex << std::setw(8) << std::setfill('0') << robId;
   m_consumerName=name.str();
}

HLTDataBuffer::~HLTDataBuffer(){
   ERS_LOG("destructor entered");
   m_active=false;
   if (m_clearThread) {
      m_clearQueue.abort();
      if (m_clearThread->joinable()) {
         m_clearThread->join();
      }
   }
   ERS_LOG("destructor completed");
}

void HLTDataBuffer::insertROBFragment(
   const std::shared_ptr<ROBFragment>& fragment){
   forwardROBFragment(fragment); 

   if (!m_active) {
      ERS_LOG("insertROBFragment while run not active, returning");
      return;
   }
   unsigned int l1Id=fragment->m_l1id;

   if (m_disabled) {
      ers::warning(RobDisabledIssue(ERS_HERE,
                                    "HLTRequestHandler::insertROBFragment",
                                    m_robId));
      return;
   }

   if (m_index.size()>=m_maxIndex) {
      std::unique_lock lock(m_mutex);
      m_condition.wait(lock,
                       [this](){
                          return (m_index.size()<m_maxIndex || !m_active);
                       }
         );
   }
   m_statistics.latestL1Available=l1Id;

   FragmentIndex::accessor evAcc;
   if (!m_index.find(evAcc,l1Id)) {
      m_index.insert(evAcc,std::pair(l1Id,fragment));
   }
   else {
      evAcc->second=fragment;
      ers::warning(FragmentOverwriteIssue(ERS_HERE,m_robId,l1Id));
   }
}

void HLTDataBuffer::ROBEnabled(unsigned int robId){
   if (robId==m_robId){
      m_disabled=false;
      m_requestHandler->ROBEnabled(robId);
   }
}
void HLTDataBuffer::ROBDisabled(unsigned int robId){
   if (robId==m_robId){
      m_disabled=true;
      m_requestHandler->ROBDisabled(robId);
   }
}
void HLTDataBuffer::runStarted(const RunParams& runPars){
   resetStats();
   m_requestHandler->runStarted(runPars);
   m_active=true;

   m_clearThread.reset(new std::thread(&HLTDataBuffer::doClears,this));
   pthread_setname_np(m_clearThread->native_handle(),"ClearThread");
}
void HLTDataBuffer::runStopped(){
   m_active=false;
   m_requestHandler->runStopped();
   m_condition.notify_all();

   m_clearQueue.abort();
   if (m_clearThread->joinable()) {
      m_clearThread->join();
   }
   m_clearQueue.clear();
   // Empty the index of any events that have not received a Clear
   m_index.clear();
}

void HLTDataBuffer::clear(std::shared_ptr<std::vector<unsigned int> > l1Ids){
   m_clearQueue.push(l1Ids);
}
void HLTDataBuffer::doClears(){
   ERS_LOG("thread starting for ROB " << m_robId);
   std::shared_ptr<std::vector<unsigned int> > l1Ids;
   while (m_active) {
      try {
         m_clearQueue.pop(l1Ids);
      }
      catch (tbb::user_abort& abortException) {
         break;
      }
      unsigned int fragsCleared=0;
      std::vector<unsigned int> failures;
      for (auto level1Id :*l1Ids) {
         if (m_index.erase(level1Id)) {
            fragsCleared++;
         }
         else{
            failures.push_back(level1Id);
         }
      }
      if (fragsCleared) {
         m_condition.notify_all();
      }
      unsigned int nfails=failures.size();
      if (nfails!=0 && !m_disabled) {
         // Don't issue warning if channel is disabled
         ers::warning(HltClearIssue(ERS_HERE,nfails,m_robId,
                                    failures[0],failures[nfails-1]));
      }
      // but count regardless
      m_statistics.fragmentsCleared+=fragsCleared;
      m_statistics.failedClears+=nfails;
   }
   ERS_LOG("thread stopping for ROB " << m_robId);
}


ISInfo* HLTDataBuffer::getStatistics() {
   if (!m_active) {
      return 0;
   }
   m_statistics.indexSize=m_index.size();

   m_statistics.dataRequestsReceived=m_requestHandler->dataRequestsReceived();
   m_statistics.clearRequestsReceived=m_requestHandler->clearRequestsReceived();
   m_statistics.clearRequestsMissed=m_requestHandler->clearRequestsMissed();
   m_statistics.clearRequestsDuplicated=m_requestHandler->clearRequestsDuplicated();
   m_statistics.badROBs=m_requestHandler->badRobRequestsReceived();
   m_statistics.clearsPending=m_requestManager->clearsPending();
   m_statistics.clearsTimedOut=m_requestManager->clearsTimedOut();
   m_statistics.lateClears=m_requestManager->lateClears();
   m_statistics.requestsTimedOut=m_requestManager->timedOut();
   m_statistics.requestsPending=m_requestManager->pending();

   m_statistics.latestL1Requested=m_latestRequested;
   m_statistics.requestsComplete=m_requestsComplete;
   m_statistics.mayComeFragments=m_mayComeFragments;
   m_statistics.lostFragments=m_lostFragments;
   m_statistics.discardedFragments=m_discardedFragments;

   auto now=std::chrono::system_clock::now();
   auto elapsed=now-m_lastStatsTime;
   float seconds=(float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6;
   if (seconds>0) {
      float l1Interval=m_statistics.fragmentsCleared-m_lastStats.fragmentsCleared;
      m_statistics.level1Rate=l1Interval/seconds;
      float reqInterval=m_statistics.dataRequestsReceived-m_lastStats.dataRequestsReceived;
      m_statistics.requestRate=reqInterval/seconds;

      unsigned long totalRobs=m_requestHandler->totalRobsRequested();
      unsigned long robInterval=totalRobs-m_lastTotalRobs;
      if (reqInterval!=0) {
         m_statistics.meanRequestSize=(float)robInterval/(float)reqInterval;
      }
      else {
         m_statistics.meanRequestSize=0;
      }
      m_lastTotalRobs=totalRobs;
   }

   m_lastStats.dataRequestsReceived=m_statistics.dataRequestsReceived;
   m_lastStats.clearRequestsMissed=m_statistics.clearRequestsMissed;
   m_lastStats.fragmentsCleared=m_statistics.fragmentsCleared;
   m_lastStatsTime=now;

   return &m_statistics;
}

void HLTDataBuffer::resetStats(){

   m_statistics.fragmentsCleared=0;
   m_statistics.failedClears=0;
   m_statistics.indexSize=m_index.size();
   m_statistics.dataRequestsReceived=0;
   m_statistics.requestsComplete=0;

   m_requestsComplete=0;
   m_mayComeFragments=0;
   m_lostFragments=0;
   m_discardedFragments=0;

   m_statistics.latestL1Available=0;
   m_statistics.lostFragments=0;
   m_statistics.mayComeFragments=0;
   m_statistics.discardedFragments=0;


   m_statistics.latestL1Requested=0;
   m_statistics.badROBs=0;

   m_lastStats.clearRequestsReceived=0;
   m_lastStats.dataRequestsReceived=0;
   m_lastStats.clearRequestsMissed=0;

   m_lastStats.fragmentsCleared=m_statistics.fragmentsCleared;

   m_lastTotalRobs=0;

   m_lastStatsTime=std::chrono::system_clock::now();
}
