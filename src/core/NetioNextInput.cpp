/*
 * NetioNextInput.cpp
 *
 *  Created on: Feb 14, 2020
 *      Author: kolos
 */

#include "NetioNextInput.h"
#include <regex>

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/ROBStatus.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    Factory<DataInput>::Registrator __reg__(
            "NetioNextInput",
            [](const boost::property_tree::ptree& config, const Core& ) {
                return std::make_shared<NetioNextInput>(config);
            });
}

NetioNextInput::NetioNextInput(const boost::property_tree::ptree& config)
try : m_timeout_ms(PTREE_GET_VALUE(config, int32_t, "FelixBusTimeout")),
      m_interface(PTREE_GET_VALUE(config, std::string, "DataNetwork")),
      m_felix_bus(PTREE_GET_VALUE(config, std::string, "FelixBusGroupName"),
                  PTREE_GET_VALUE(config, std::string, "FelixBusInterface")),
      m_thread(std::bind(&NetioNextInput::run, this, std::placeholders::_1),
              "NetioNextInput", PTREE_GET_VALUE(config, std::string, "CPU"))
{
    m_felix_bus.connect();
    m_felix_bus.subscribe("FELIX", &m_felix_table);
    m_felix_bus.subscribe("ELINKS", &m_elink_table);

    netio_init(&m_context);
    m_context.evloop.cb_init = nullptr;
    m_signal.cb = &NetioNextInput::signalHandler;
    m_signal.data = this;

    netio_signal_init(&m_context.evloop, &m_signal);

    m_thread.start();
}
catch (ptree_error & ex) {
    throw BadConfigurationException(ERS_HERE, ex.what(), ex);
}

NetioNextInput::~NetioNextInput() {
    netio_terminate(&m_context.evloop);
    m_thread.stop();
}

void NetioNextInput::signalHandler(void * ptr) {
    NetioNextInput * self = reinterpret_cast<NetioNextInput*>(ptr);
    self->executeActions();
}

void NetioNextInput::actionsPending() {
    netio_signal_fire(&m_signal);
}

void NetioNextInput::subscribeToFelix(const InputLinkId& link) {
    std::string uuid = m_elink_table.getFelixId(link, m_timeout_ms);
    std::string address = m_felix_table.getAddress(uuid);

    std::regex regex("tcp://([^:]+):([0-9]+)");
    std::smatch match_result;

    if (!std::regex_match(address, match_result, regex) || match_result.size() != 3) {
        throw BadAddressException(ERS_HERE, address, link, uuid);
    }

    std::string host = match_result[1].str();
    uint32_t port = std::stoi(match_result[2].str());

    auto it = m_sockets.find(address);
    if (it == m_sockets.end()) {
        struct netio_buffered_socket_attr attr;
        attr.num_pages = m_felix_table.getNetioPages(uuid);
        attr.pagesize = m_felix_table.getNetioPageSize(uuid);
        attr.watermark = attr.pagesize * 0.9;

        it = m_sockets.insert(std::make_pair(address, netio_subscribe_socket())).first;

        netio_subscribe_socket_init(&it->second, &m_context, &attr, m_interface.c_str(), host.c_str(), port);
        it->second.usr = this;
        it->second.recv_socket.cb_connection_established = nullptr;
        it->second.cb_msg_received = &NetioNextInput::netioMessageReceived;
    }

    ERS_DEBUG(1, "Subscribing to '" << address << "' for link = " << link << " uuid = '" << uuid << "'\n\n");

    netio_subscribe_socket* socket = &it->second;
    executeSynchronousAction([socket, link](){netio_subscribe(socket, link);});

    ERS_DEBUG(1, "Subscribed to '" << address << "' for link = " << link << " uuid = '" << uuid << "'\n\n");
}

void NetioNextInput::unsubscribeFromFelix(const InputLinkId& link) {
    ;
}

uint32_t NetioNextInput::translateStatus(uint8_t status) const noexcept {
    ERS_DEBUG(5, "Translating 0x" << std::hex << status << " status");

    if (!status) {
        return 0;
    }
    uint32_t r = 0;
    if ( status & (FELIX_STATUS_FW_TRUNC | FELIX_STATUS_SW_TRUNC)) {
        r |= ROBStatus::Specific::Truncated;
        r |= ROBStatus::Specific::TxError;
        r |= ROBStatus::Generic::BufferOverflow;
        if (status & FELIX_STATUS_FW_TRUNC) {
            r |= ROBStatus::Specific::Transmission::FirmwareTrunc;
        }
        if (status & FELIX_STATUS_SW_TRUNC) {
            r |= ROBStatus::Specific::Transmission::SoftwareTrunc;
        }
    }
    if ( status & (FELIX_STATUS_FW_CRC | FELIX_STATUS_FW_MALF | FELIX_STATUS_SW_MALF)) {
        r |= ROBStatus::Specific::TxError;
        r |= ROBStatus::Generic::CorruptData;
        if (status & FELIX_STATUS_FW_CRC) {
            r |= ROBStatus::Specific::Transmission::CRCError;
        }
        if (status & FELIX_STATUS_SW_MALF) {
            r |= ROBStatus::Specific::Transmission::SoftwareMalf;
        }
        if (status & FELIX_STATUS_FW_MALF) {
            r |= ROBStatus::Specific::Transmission::FirmwareMalf;
        }
    }

    ERS_DEBUG(4, "Status 0x" << std::hex << status << " has been translated to 0x" << r);

    return r;

}

void NetioNextInput::run(const bool& ) {
    netio_run(&m_context.evloop);
}
