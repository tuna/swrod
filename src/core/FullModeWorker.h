/*
 * FullModeWorker.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_FULLMODEWORKER_H_
#define SWROD_FULLMODEWORKER_H_

#include <memory>

#include <eformat/HeaderMarker.h>

#include <swrod/InputLinkId.h>
#include <swrod/DataInput.h>
#include <swrod/ROBFragmentWorkerBase.h>
#include <swrod/ROBStatus.h>
#include <swrod/detail/MemoryPool.h>

namespace swrod {
    constexpr uint32_t RODHeaderMarker_LE = eformat::ROD;
    constexpr uint32_t RODHeaderMarker_BE = __builtin_bswap32(eformat::ROD);

    template <bool ReceiveTTC> class FullModeBuilder;

    template <bool ReceiveTTC>
    class FullModeWorker: public ROBFragmentWorkerBase {
    public:
        FullModeWorker(const InputLinkVector & links,
                const boost::property_tree::ptree & robConfig,
                const Core & core, const std::shared_ptr<DataInput> & input,
                FullModeBuilder<ReceiveTTC>& builder) :
                    ROBFragmentWorkerBase(links, robConfig, core, input,
                            detail::InputCallback::make<
                                FullModeWorker<ReceiveTTC>, Link,
                                &FullModeWorker<ReceiveTTC>::dataReceived>(*this)),
                    m_builder(builder),
                    m_memory_pool(std::make_shared<detail::MemoryPool>(m_max_message_size))
        { }

    private:
        void dataReceived(Link & link, const uint8_t* data, uint32_t size,
                uint8_t status, uint32_t rob_status);

    private:
        FullModeBuilder<ReceiveTTC> & m_builder;
        std::shared_ptr<detail::MemoryPool> m_memory_pool;
    };

    template <bool ReceiveTTC>
    void FullModeWorker<ReceiveTTC>::dataReceived(Link &link,
            const uint8_t *data, uint32_t packet_size,
            uint8_t felix_status, uint32_t rob_status) {

        ERS_DEBUG(5, "Data packet for the link 0x" << std::hex << link.m_fid
                << " is received, size = " << std::dec << packet_size
                << " packet counter = " << link.m_packets_counter);

        if (not m_running) {
            link.m_packets_dropped++;
            ers::error(DroppedPacketException(ERS_HERE, link.m_fid,
                    link.m_packets_counter, link.m_packets_dropped,
                    SpuriousPacketException(ERS_HERE, link.m_fid)));
            return;
        }

        link.m_packets_counter++;
        try {
            auto [l1id, l1id_mask, bcid] = m_trigger_info_extractor(data, packet_size);

            ERS_DEBUG(5, link.m_packets_counter << " packets received for link 0x"
                    << std::hex << link.m_fid << " L1ID = 0x" << l1id
                    << " BCID = 0x" << bcid << " L1ID bit mask = 0x" << l1id_mask);

            ROBFragment::DataBlock block = m_memory_pool->alloc();
            if (block.append(data, packet_size) != packet_size) {
                rob_status |= ROBStatus::Specific::Truncated;
                rob_status |= ROBStatus::Generic::BufferOverflow;
            }

            if (m_builder.isRODHeaderPresent()) {
                if (block.dataSize()) {
                    uint32_t tag = *block.dataBegin();
                    if (tag != RODHeaderMarker_LE and tag != RODHeaderMarker_BE) {
                        rob_status |= ROBStatus::Specific::Format::HeaderMarkerError;
                        rob_status |= ROBStatus::Specific::TxError;
                        rob_status |= ROBStatus::Generic::CorruptData;
                    }
                }

                if (block.dataSize() < swrod::RODHeaderTrailerSize) {
                    rob_status |= ROBStatus::Specific::ShortFragment;
                    rob_status |= ROBStatus::Generic::CorruptData;
                } else if (not (rob_status & ROBStatus::Specific::Truncated)) {
                    uint32_t size =   *(block.dataEnd() - 2) // the number of data words from the trailer
                                    + *(block.dataEnd() - 3) // the number of status words from the trailer
                                    + swrod::RODHeaderTrailerSize;
                    if (block.dataSize() != size) {
                        rob_status |= ROBStatus::Specific::TxError;
                        rob_status |= ROBStatus::Specific::Format::FragmentSizeError;
                        rob_status |= ROBStatus::Generic::CorruptData;
                    }
                }
            }

            m_builder.dataReceived(link, l1id, bcid, rob_status, std::move(block));
        }
        catch (DuplicatePacketException &ex) {
            link.m_packets_dropped++;
            ers::error(DroppedPacketException(
                    ERS_HERE, link.m_fid, link.m_packets_counter,
                    link.m_packets_dropped, ex));
        }
        catch (Exception &ex) {
            link.m_packets_corrupted++;
            ers::error(DataCorruptionException(
                    ERS_HERE, link.m_fid, link.m_packets_counter,
                    link.m_packets_corrupted, ex));
        }
    }
}

#endif /* SWROD_FULLMODEWORKER_H_ */
