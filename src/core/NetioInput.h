/*
 * NetioInput.h
 *
 *  Created on: Jun 14, 2019
 *      Author: kolos
 */

#ifndef SRC_CORE_NETIOINPUT_H_
#define SRC_CORE_NETIOINPUT_H_

#include <map>

#include <boost/property_tree/ptree.hpp>

#include <felixbus/bus.hpp>
#include <felixbus/elinktable.hpp>
#include <felixbus/felixtable.hpp>

#include <netio/netio.hpp>

#include <swrod/DataInput.h>
#include <swrod/detail/WorkerThread.h>

namespace swrod {
    class Core;

    class NetioInput : public DataInput {
    public:
        NetioInput(const boost::property_tree::ptree & config);

        ~NetioInput();

        void actionsPending() override;

        void subscribeToFelix(const InputLinkId & link) override;

        void unsubscribeFromFelix(const InputLinkId & link) override;

    private:
        static void netioMessageReceived(uint8_t* data, size_t size, void* usr_data);

        void signalHandler(void *);

        void run(const bool & active);

        void read(const bool & active);

    private:
        const bool m_use_callback;
        const uint32_t m_timeout_ms;
        felix::bus::Bus m_felix_bus;
        felix::bus::FelixTable m_felix_table;
        felix::bus::ElinkTable m_elink_table;
        netio::context m_context;
        netio::signal m_signal;
        netio::endpoint m_endpoint;
        netio::subscribe_socket m_socket;
        std::map<InputLinkId,netio::endpoint> m_subscriptions;
        detail::WorkerThread m_thread;
        detail::WorkerThread m_read_thread;
    };
}

#endif /* SRC_CORE_NETIOINPUT_H_ */
