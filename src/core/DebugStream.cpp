/*
 * DebugStream.cpp
 *
 *  Created on: Jun 18, 2019
 *      Author: kolos
 */

#include "DebugStream.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    Factory<ROBFragmentConsumer>::Registrator __reg__("DebugStream",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<DebugStream>(config, core);
            });

    std::string makeSamplerName(const ptree & config, const Core & core) {
        std::string base_name;
        if (config.count("RobConfig")) {
            uint32_t ROB_id(PTREE_GET_VALUE(config.get_child("RobConfig"), uint32_t, "Id"));
            base_name = std::to_string(ROB_id);
        } else {
            base_name = PTREE_GET_VALUE(core.getConfiguration(), std::string, "Application.Name");
        }
        return base_name + "-Debug";
    }
}

DebugStream::DebugStream(const ptree & config,
        const Core & core)
try : SamplerFacade<Event>(
        IPCPartition(
                PTREE_GET_VALUE(core.getConfiguration(), std::string, "Partition.Name")),
        emon::SamplingAddress("SWROD", makeSamplerName(config, core)), 1),
      m_run_number(-1),
      m_running(false),
      m_threads(PTREE_GET_VALUE(config, uint16_t, "WorkersNumber"),
                  std::bind(&DebugStream::run, this, std::placeholders::_1),
                  "DebugStream",
                  PTREE_GET_VALUE(config, std::string, "CPU"))
{
    m_fragment_queue.set_capacity(PTREE_GET_VALUE(config, uint32_t, "EventBufferSize"));
}
catch (ptree_error & ex) {
    throw BadConfigurationException(ERS_HERE, ex.what(), ex);
}

void DebugStream::sampleEvent(Event & event, emon::EventChannel & channel) {

    if (event.m_serialized.empty()) {;
        event.m_serialized.reserve(16);
        event.m_serialized.push_back(
                iovec({&event.m_header, event.m_header.m_header_size * sizeof(uint32_t)}));

        uint32_t size = 0;
        std::vector<std::pair<uint8_t*, uint32_t>> data =
               event.m_fragment->serialize(m_run_number);
        for (auto & slice : data) {
            event.m_serialized.push_back(iovec({slice.first, slice.second}));
            size += slice.second;
        }

        size /= sizeof(uint32_t);
        event.m_header.m_event_size = size + event.m_header.m_header_size;
        event.m_header.m_payload_size = size;
        event.m_header.m_l1id = event.m_fragment->m_l1id;
        event.m_header.m_bcid = event.m_fragment->m_bcid;
        event.m_header.m_l1_trigger_type = event.m_fragment->m_trigger_type;
        event.m_header.m_run_number = m_run_number;
    }

    channel.pushEvent(&event.m_serialized[0], event.m_serialized.size());

    ERS_DEBUG(3, "Event with l1id = " << event.m_fragment->m_l1id
            << " was sent to the debug stream");
}

void DebugStream::insertROBFragment(const std::shared_ptr<ROBFragment>& fragment) {
    forwardROBFragment(fragment);

    if (not fragment->statusWord()) {
        return;
    }

    if (not m_running) {
        return;
    }

    if (not m_fragment_queue.try_push(fragment)) {
        std::shared_ptr<ROBFragment> trash;
        if (m_fragment_queue.try_pop(trash)) {
            m_fragment_queue.try_push(fragment);
        }
    }
}

void DebugStream::run(const bool & active) {
    while (active) {
        Event ev;
        if (not m_fragment_queue.try_pop(ev.m_fragment)) {
            usleep(10);
            continue;
        }
        pushEvent(ev);
    }
}

void DebugStream::runStarted(const RunParams & run_params) {
    ERS_DEBUG(1, "starting sampling thread");

    m_run_number = run_params.run_number;
    m_running = true;
    m_threads.start();

    ERS_DEBUG(1, "done");
}

void DebugStream::runStopped() {
    ERS_DEBUG(1, "stopping sampling thread");

    m_running = false;
    m_fragment_queue.clear();
    m_threads.stop();

    ERS_DEBUG(1, "done");
}
