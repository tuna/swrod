#include <mutex>

#include "HLTDataServer.h"
#include "HLTRequestHandler.h"

#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/DataRequestMsg.h"
#include "ROSasyncmsg/ClearMsg.h"

#include "asyncmsg/Message.h"            // for InputMessage, OutputMessage

#include "SwrodOutputMsg.h"

#include "swrod/exceptions.h"
#include "ers/ers.h"

using namespace swrod;
using ROS::DataRequestMsg;
using ROS::DataServerSession;
//using ROS::ClearMsg;

// ==============================================================================


HLTDataServer::HLTDataServer(std::vector<boost::asio::io_service>& ioServices,
                             const std::string& iomName,
                             daq::asyncmsg::NameService* nameService,
                             HLTRequestHandler* hltRequestHandler) :
DataServer(ioServices,iomName,nameService),m_hltRequestHandler(hltRequestHandler) {
}

HLTDataServer::~HLTDataServer() {
}

void HLTDataServer::onAccept(std::shared_ptr<daq::asyncmsg::Session> session) noexcept {
   std::lock_guard<std::mutex> lock(m_sessionsMutex);
   m_sessions.emplace_back(std::dynamic_pointer_cast<DataServerSession>(session));
      
   int server=getIoService();
   asyncAccept(std::make_shared<HLTDataServerSession>
               (m_ioServerService[server],m_hltRequestHandler));
}



// ==============================================================================


HLTDataServerSession::HLTDataServerSession(boost::asio::io_service& ioService,
                                           HLTRequestHandler* hltRequestHandler) :
   DataServerSession(ioService), m_hltRequestHandler(hltRequestHandler) {
}
HLTDataServerSession::~HLTDataServerSession() {
}


void HLTDataServerSession::onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) {

   if (message->typeId()==DataRequestMsg::TYPE_ID) {
      std::unique_ptr<DataRequestMsg> requestMsg(dynamic_cast<DataRequestMsg*>(message.release()));
      if (requestMsg==0) {
         ers::warning(MsgCastIssue(ERS_HERE, "DataRequestMsg"));
         return;
      }

//        std::cout << std::hex
//                  << "Received request message " << requestMsg->transactionId()
//                  << " for event " << requestMsg->level1Id()
//                  << " fingering " << requestMsg->nRequestedRols() << " ROLs:";
//        std::cout << std::dec << std::endl;
      m_hltRequestHandler->processDataRequest(requestMsg->level1Id(),
                                              requestMsg->requestedRols(),
                                              shared_from_this(),
                                              requestMsg->transactionId()
         );
   }
   else if (message->typeId()==ClearMsg::TYPE_ID) {
      std::unique_ptr<ClearMsg> clearMsg(dynamic_cast<ClearMsg*>(message.release()));
      if (clearMsg!=0) {
         //std::cout << "Received clear message for " << clearMsg->nEvents() << " events\n";
         m_hltRequestHandler->processClearRequest(clearMsg->m_l1Ids,clearMsg->transactionId());
      }
      else {
         ers::warning(MsgCastIssue(ERS_HERE, "ClearMsg"));
      }
   }
   else {
      ers::warning(MsgTypeIssue(ERS_HERE, message->typeId()));
   }

   asyncReceive();
}


void HLTDataServerSession::onSend(
   std::unique_ptr<const daq::asyncmsg::OutputMessage>  message) noexcept {
   //std::cout << "Sent message " << message->transactionId() << std::endl;

   std::unique_ptr<const SwrodOutputMsg>
      dataMessage(dynamic_cast<const SwrodOutputMsg*>(message.release()));
   if (dataMessage) {
      m_hltRequestHandler->finalise(dataMessage->m_descriptor);
   }
}

// ==============================================================================

void ClearSession::onReceive(
   std::unique_ptr<daq::asyncmsg::InputMessage> message) {
   if (message->typeId()==ClearMsg::TYPE_ID) {
      std::unique_ptr<ClearMsg> clearMsg(dynamic_cast<ClearMsg*>(message.release()));
      if (clearMsg!=0) {
         //std::cout << "Received clear message for " << clearMsg->nEvents() << " events\n";
         m_hltRequestHandler->processClearRequest(clearMsg->m_l1Ids,clearMsg->transactionId());
      }
      else {
         ers::warning(MsgCastIssue(ERS_HERE, "ClearMsg"));
      }
   }
   asyncReceive();
}
