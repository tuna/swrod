#include "HLTRequestManager.h"
#include "HLTRequestDescriptor.h"

#include "ROSDescriptorNP/RequestDescriptor.h"  // for RequestDescriptor
#include "src/core/HLTRequestHandler.h"         // for HLTRequestHandler


using namespace swrod;

HLTRequestManager::HLTRequestManager(HLTRequestHandler* handler,
                                     unsigned int conditionTimeout,
                                     unsigned int requestTimeout,
                                     unsigned int clearTimeout,
                                     unsigned int maxPendingClears) :
   ROS::RequestManager(conditionTimeout,requestTimeout,
                       clearTimeout,maxPendingClears),
   m_requestHandler(handler) {
}

HLTRequestManager::~HLTRequestManager() {
}

std::vector<unsigned int> HLTRequestManager::pollLevel1(){
   return m_requestHandler->latestL1();
}

void HLTRequestManager::doRequest(ROS::RequestDescriptor* desc){
   m_requestHandler->processDataRequest(
      dynamic_cast<HLTRequestDescriptor*>(desc));
}

void HLTRequestManager::doClears(std::vector<unsigned int>& level1IdList){
   m_requestHandler->clear(level1IdList);
}
   
