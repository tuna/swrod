/*
 * FelixInput.h
 *
 *  Created on: Jun 14, 2019
 *      Author: kolos
 */

#ifndef SRC_CORE_FELIXINPUT_H_
#define SRC_CORE_FELIXINPUT_H_

#include <boost/property_tree/ptree.hpp>

#include <felix_proxy/ClientThread.h>

#include <swrod/DataInput.h>

namespace swrod {
    class Core;

    class FelixInput : public DataInput {
    public:
        FelixInput(const boost::property_tree::ptree & config);

        ~FelixInput();

        void actionsPending() override;

        void synchronise(uint8_t ECR_value) override;

        void subscribeToFelix(const InputLinkId & link) override;

        void unsubscribeFromFelix(const InputLinkId & link) override;

        uint32_t translateStatus(uint8_t s) const noexcept override;

    private:
        felix_proxy::ClientThread m_felix;
    };
}

#endif /* SRC_CORE_FELIXINPUT_H_ */
