// -*- c++ -*-
#ifndef HLTREQUESTHANDLER_H
#define HLTREQUESTHANDLER_H

#include <string>
#include <chrono>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <vector>

#include <boost/property_tree/ptree.hpp>
#include <boost/asio/io_service.hpp>          // for io_service

#include "monsvc/ptr.h"
#include "rc/RunParams.h"

class TH1D;

namespace monsvc {
   class MonitoringService;
}
namespace daq{
   namespace asyncmsg{
      class NameService;
      class Session;
   }
}
namespace swrod{
   class ClearSession;
   class HLTDataServer;
   class HLTRequestManager;
   class HLTRequestDescriptor;
   class HLTDataBuffer;

   class HLTRequestHandler {
   private:
      HLTRequestHandler(const boost::property_tree::ptree & config,
                        const boost::property_tree::ptree & coreConfig);
   public:
      ~HLTRequestHandler();

      static std::shared_ptr<HLTRequestHandler> instance(const boost::property_tree::ptree& config,
                                                         const boost::property_tree::ptree& coreConfig);
      std::shared_ptr<HLTDataBuffer> addBuffer(uint32_t robId,
                                               const boost::property_tree::ptree& config);
      void processDataRequest(unsigned int level1Id,
                              std::vector<unsigned int>* robs,
                              std::shared_ptr<daq::asyncmsg::Session> session,
                              unsigned int transactionId);
      void processDataRequest(HLTRequestDescriptor* desc);
      void processClearRequest(std::vector<unsigned int>& l1Ids,
                               unsigned int transactionId);
      void clear(std::vector<unsigned int>& l1Ids);
      std::vector<unsigned int> latestL1()const;
      void finalise(HLTRequestDescriptor* desc);
      void resetStats();

      void ROBDisabled(unsigned int robId);
      void ROBEnabled(unsigned int robId);
      void runStarted(const RunParams& runPars);
      void runStopped();

      unsigned int dataRequestsReceived() const {return m_dataRequestsReceived;}
      unsigned int clearRequestsReceived() const {return m_clearRequestsReceived;}
      unsigned int clearRequestsMissed() const {return m_clearRequestsMissed;}
      unsigned int clearRequestsDuplicated() const {return m_clearRequestsDuplicated;}
      unsigned int badRobRequestsReceived() const {return m_badRobRequests;}
      unsigned long totalRobsRequested() const {return m_totalRobsRequested;}
   private:
      static std::weak_ptr<HLTRequestHandler> s_instance;
      static std::mutex s_mutex;

      bool m_active;
      std::vector<std::weak_ptr<HLTDataBuffer> > m_dataBuffer;
      std::map<unsigned int, unsigned int> m_channelIndex;
      unsigned int m_nChannels;
      std::set<unsigned int> m_robs;
      std::vector<bool> m_disabled;
      std::shared_ptr<HLTRequestManager> m_requestManager;
      daq::asyncmsg::NameService* m_nameService;
      unsigned int m_runNumber;
      unsigned int m_requestTimeOut;
      std::unique_ptr<std::vector<boost::asio::io_service> > m_dataIoServices;
      std::unique_ptr<std::vector<boost::asio::io_service> > m_clearIoService;

      std::shared_ptr<HLTDataServer> m_dataServer;
      std::shared_ptr<HLTDataServer> m_clearServer;
      std::shared_ptr<ClearSession> m_clearSession;

      unsigned int m_lastClearXId;
      monsvc::MonitoringService& m_monitoringService;
      monsvc::ptr<TH1D> m_timeHistogram;
      monsvc::ptr<TH1D> m_sizeHistogram;
      monsvc::ptr<TH1D> m_channelsHistogram;

      unsigned int m_maxIndex;
      bool m_ignoreClearXId;

      unsigned int m_dataRequestsReceived;
      unsigned int m_clearRequestsReceived;
      unsigned int m_clearRequestsMissed;
      unsigned int m_clearRequestsDuplicated;
      unsigned int m_badRobRequests;
      unsigned long m_totalRobsRequested;
   };

}
#endif
