/*
 * Collator.cpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kolos
 */

#include <swrod/detail/Collator.h>

using namespace swrod;

Collator::Collator(uint32_t robs_number, const Callback& callback,
        int32_t queue_size, const std::string & cpu_set) :
        ROBFragmentConsumerBase(
                std::bind(&Collator::fragmentReceived, this, std::placeholders::_1),
                ROBFragmentConsumerBase::QueuingPolicy::Wait,
                ROBFragmentConsumerBase::ForwardingPolicy::None,
                1, queue_size, "Collator", cpu_set),
        m_robs_number(robs_number),
        m_callback(callback)
{
}

void Collator::fragmentReceived(const std::shared_ptr<ROBFragment>& fragment) {
    if (not m_control_queue.empty()) {
        std::function<void()> action;
        while (m_control_queue.try_pop(action)) {
            action();
        }
    }

    if (not m_disabled_robs.empty() && m_disabled_robs.count(fragment->m_source_id)) {
        return ;
    }

    if (m_robs_number == 1) {
        m_callback(Event(1, fragment));
        return;
    }

    EventMap::iterator it = m_events.find(fragment->m_l1id);
    if (it == m_events.end()) {
        it = m_events.emplace(fragment->m_l1id, Event()).first;
        it->second.reserve(m_robs_number);
        it->second.push_back(fragment);
        return;
    }

    it->second.push_back(fragment);
    if (it->second.size() >= m_robs_number) {
        m_callback(std::move(it->second));
        m_events.erase(it);
    }
}

void Collator::disable(uint32_t ROB_id) {
    if (not m_disabled_robs.insert(ROB_id).second) {
        ERS_LOG(ROB_id << " ROB was already marked as disabled");
        return;
    }

    --m_robs_number;

    // We have to acknowledge as complete all events which contain
    // the new number of ROBs and do not contain the ROB which has
    // just been disabled as otherwise they get stuck in the map forever
    EventMap::iterator it = m_events.begin();
    while (it != m_events.end()) {
        if (it->second.size() == m_robs_number) {
            bool contains_disabled =
                    it->second.end() != std::find_if(
                                    it->second.begin(), it->second.end(),
                                    [ROB_id](const auto & fragment) {
                                            return fragment->m_source_id == ROB_id;
                                    });
            if (not contains_disabled) {
                m_callback(std::move(it->second));
                it = m_events.erase(it);
                continue;
            }
        }
        ++it;
    }
}

void Collator::enable(uint32_t ROB_id) {
    if (m_disabled_robs.erase(ROB_id)) {
        ++m_robs_number;
    } else {
        ERS_LOG(ROB_id << " ROB was not marked as disabled");
    }
}
