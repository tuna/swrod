/*
 * CustomProcessingFramework.cpp
 *
 *  Created on: May 29, 2019
 *      Author: kolos
 */

#include <dlfcn.h>

#include <string>

#include <swrod/exceptions.h>
#include <swrod/CustomProcessingFramework.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;

CustomProcessingFramework::DataIntegrityChecker
    CustomProcessingFramework::s_default_data_integrity_checker(
            [](const uint8_t *, uint32_t) -> std::optional<bool> { return {}; }
    );

CustomProcessingFramework::SharedLibrary::SharedLibrary(
        const std::string & library_name,
        const std::string & trigger_info_extractor_name,
        const std::string & processor_factory_name,
        const std::string & data_integrity_checker_name) :
        m_name(library_name),
        m_handle(nullptr),
        m_tie_function(nullptr),
        m_pf_function(nullptr),
        m_dic_function(s_default_data_integrity_checker)
{
    m_handle = dlopen(m_name.c_str(), RTLD_LAZY | RTLD_GLOBAL);
    ERS_DEBUG(1, m_name << " shared library handler is " << m_handle);

    if (!m_handle) {
        throw PluginException(ERS_HERE, m_name, dlerror());
    }

    void * fptr = dlsym(m_handle, trigger_info_extractor_name.c_str());
    if (!fptr) {
        throw PluginException(ERS_HERE, m_name, dlerror());
    }
    m_tie_function = reinterpret_cast<decltype(m_tie_function)>(fptr);
    ERS_DEBUG(1, "Trigger info extractor function '"
            << trigger_info_extractor_name << "' has been successfully resolved");

    if (!processor_factory_name.empty()) {
        fptr = dlsym(m_handle, processor_factory_name.c_str());
        if (!fptr) {
            throw PluginException(ERS_HERE, m_name, dlerror());
        }
        m_pf_function = reinterpret_cast<decltype(m_pf_function)>(fptr);
        ERS_DEBUG(1, "Custom processor factory function '"
                << processor_factory_name << "' has been successfully resolved");
    }

    if (!data_integrity_checker_name.empty()) {
        fptr = dlsym(m_handle, data_integrity_checker_name.c_str());
        if (!fptr) {
            throw PluginException(ERS_HERE, m_name, dlerror());
        }
        m_dic_function = reinterpret_cast<decltype(m_dic_function)>(fptr);
        ERS_DEBUG(1, "Data integrity check function '"
                << data_integrity_checker_name << "' has been successfully resolved");
    }
}

CustomProcessingFramework::SharedLibrary::~SharedLibrary()
{
    ERS_DEBUG(1, "Closing " << m_handle << " library handler");
    dlclose(m_handle);
}

CustomProcessingFramework::CustomProcessingFramework(
        const boost::property_tree::ptree & config)
{
    for (const ptree::value_type & m : PTREE_GET_CHILD(config, "Modules")) {
        const ptree & module_config = m.second;
        for (const ptree::value_type & r : PTREE_GET_CHILD(module_config, "ROBs")) {
            const ptree & rob_config = r.second;
            uint32_t ROB_id = PTREE_GET_VALUE(rob_config, uint32_t, "Id");
            ptree lib_config = PTREE_GET_CHILD(rob_config, "Contains.CustomLib");
            std::shared_ptr<SharedLibrary> shlib = loadSharedLibrary(
                    PTREE_GET_VALUE(lib_config, std::string, "LibraryName"),
                    PTREE_GET_VALUE(lib_config, std::string, "TrigInfoExtractor"),
                    PTREE_GET_VALUE(lib_config, std::string, "ProcessorFactory"),
                    PTREE_GET_VALUE(lib_config, std::string, "DataIntegrityChecker"));
            m_trigger_info_extractors[ROB_id] = shlib->getTriggerInfoExtractor();
            if (shlib->getProcessingFactory()) {
                m_processing_factories[ROB_id] = shlib->getProcessingFactory();
            }
            m_data_intergrity_checkers[ROB_id] = shlib->getDataIntegrityChecker();
        }
    }
}

CustomProcessingFramework::TriggerInfoExtractor
CustomProcessingFramework::getTriggerInfoExtractor(uint32_t ROB_id) const
{
    ExtractorsMap::const_iterator it = m_trigger_info_extractors.find(ROB_id);
    if (it != m_trigger_info_extractors.end()) {
        return it->second;
    }
    throw BadConfigurationException(ERS_HERE,
            "Custom trigger info extraction function is not defined for '"
            + std::to_string(ROB_id) + "' ROB");
}

std::unique_ptr<CustomProcessor>
CustomProcessingFramework::createCustomProcessor(uint32_t ROB_id,
        const boost::property_tree::ptree & config) const
{
    FactoriesMap::const_iterator factory = m_processing_factories.find(ROB_id);
    if (factory != m_processing_factories.end()) {
        if (factory->second) {
            std::unique_ptr<CustomProcessor> processor(
                    factory->second(PTREE_GET_CHILD(config, "RobConfig.Contains")));
            return processor;
        }
    }
    throw BadConfigurationException(ERS_HERE,
            "Custom processor factory is not defined for '"
            + std::to_string(ROB_id) + "' ROB");
}

CustomProcessingFramework::DataIntegrityChecker
CustomProcessingFramework::getDataIntegrityChecker(uint32_t ROB_id) const noexcept {
    CheckersMap::const_iterator it = m_data_intergrity_checkers.find(ROB_id);
    if (it != m_data_intergrity_checkers.end()) {
        return it->second;
    }
    return s_default_data_integrity_checker;

}

std::shared_ptr<CustomProcessingFramework::SharedLibrary>
CustomProcessingFramework::loadSharedLibrary(const std::string & library_name,
        const std::string & trigger_info_extractor_name,
        const std::string & processor_factory_name,
        const std::string & data_integrity_checker_name)
{
    std::shared_ptr<SharedLibrary> lib;
    LibrariesMap::iterator it = m_libraries.find(library_name);
    if (it == m_libraries.end()) {
        lib = std::make_shared<SharedLibrary>(
                        library_name, trigger_info_extractor_name,
                        processor_factory_name, data_integrity_checker_name);
        m_libraries[library_name] = lib;
    } else {
        lib = it->second;
    }
    return lib;
}
