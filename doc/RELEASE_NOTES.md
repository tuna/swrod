# swrod

### Data Alignment

Fragment building algorithms have been modified to produce 4-byte aligned data. Both GBT and FULL mode algorithms expect that every incoming data chunk has a size that is multiple of 4 bytes. If this is not the case the algorithms add padding zeros at the end of the chunk to make it 4-byte aligned.

### Public API changes

The type of the **swrod::ROBFragment::m_data** attribute has been changed. This affects custom processing code that deals with ROB fragments payload. See the updated [User's Guide](https://gitlab.cern.ch/atlas-tdaq-software/swrod#rob-fragment-memory-management) for the detailed explanation of the new API.

### Configuration schema changes

* A new class **SwRodMonitoringApplication** has been added. It can be used in place of the normal **SwRodApplication** class if data fragments produced by the corresponding SW ROD shall not be used for event building and therefore shall be hiden from HLT.

* **SwRodModule** class has a number of new parameters:
    * **InputMethod** relationship has been moved to this class from the **SwRodRob** one. This is done to be able to share the same input object by multiple ROBs if they belong to the same module.
    * **CPU** attribute has been moved to this class from the **SwRodRob** one. It can be used to set CPU affinity for the data receiving threads. 
    * **WorkersNumber** attribute has been moved from the **SwRodFragmentBuilder** class. This attribute defines the number of data receiving threads that will be used by the input object.
    * **L1AHandler** is a new optional relationship. This relationship allows to define a specific TTC-to-Host e-link for the ROBs that are referenced by a given module. If this relationship is left empty then TTC-to-Host e-link from the default **L1AHandler** object referenced by the **SwRodConfiguration** will be used instead. If the latter is also empty then fragment building will be data-driven.

* Two attributes of the **SwRodFragmentBuilder** class have been renamed, which affects as well **SwRodFullModeBuilder** and **SwRodGBTModeBuilder** classes, which inherit from it:
    * **SendersNumber** attribute has been renamed to **BuildersNumber**
    * **FlashBufferAtStop** attribute has been renamed to **FlushBufferAtStop**

* A new attribute **FlushBufferAtStop** has been added to the **SwRodFragmentConsumer** class. This attribute is inherited by all specific Consumer classes.

* **BufferPages** and **BufferPageSize** attributes have been removed from the **SwRodNetioNextInput** as now these parameters are taken directly from the FelixBus where they are published by the service providers.

* The type of **CPU** attribute of the SW ROD configuration classes have been changed from integer to string. A string value may contain comma separated list of CPU core numbers and optionally may contain ranges, for example: 0,5,7,9-11. By default the attribute value is set to an empty string, which means that no affinity is set for the corresponding worker threads.


## tdaq-09-02-01

This release introduces an OKS configuration schema change to facilitate splitting of the TDAQ and detector specific portions of SW ROD configuration. The new schema has a new class called **SwRodDataChannel** that should be used by detector experts to configure how data for a given data channel has to be collected and processed. This class emerges from the legacy **SwRodRob** one and steals two relationships from it:

* **Contains** relationship should contain a set of e-links to be used to receive data for the data channel **SwRodDataChannel**
 
* **CustomLib** relationship shall point to the custom plugin that will be used for data assembling and post-processing of the fragments produced for the given data channel

Finally an instance of the **SwRodDataChannel** class has to be linked to an instance of the **SwRodRob** class via the **Contains** relationship of the latter. Note that despite the fact that this relationship allows multiple objects to be referenced an instance of the **SwRodRob** must point to exactly one instance of the **SwRodDataChannel** class. If that is not the case SW ROD configuration will fail and an error message will be produced.

More details about the new way of configuring SW ROD can be found in the updated Reference Manual as well as in the README file of the package.

## tdaq-09-01-00

* Two attributes of the SwRodInputLink class have been renamed:
    * **DetectorID** => **DetectorResourceId**
    * **DetectorName** => **DetectorResourceName**

* HLT Request Handler implementation has been updated to improve performance. As a result an object of the **SwRodHLTRequestHandler** class has to be linked with each **SwRodRob** object via its _Consumers_ relationship. A link between **SwRodConfiguration** and **SwRodHLTRequestHandler** objects must be removed.
 
* By default ROBs and E-Links IDs in a SW ROD OKS configuration are shown in hexadecimal format.
 
* Common SW ROD configuration objects, which don't normally require customization have been placed to the **daq/sw/swrod-common.data.xml** OKS configuration class that is installed to the **installed/share/data** area of the TDAQ release. Any SW ROD OKS configuration is advised to use these objects instead of creating custom ones unless any parameters modification is required.
 
* **GBTModeBuilder** and **FullModeBuilder** algorithms now automatically detect if the respective SW ROD application uses TTC data handler for getting L1 Accept packets from FELIX. If that is the case the algorithms will run in TTC-aware mode, otherwise they will be data-driven. Contrary to the previous release there is no need to change the value of the Type parameter of the **ROBFragmentBuilder** class in the OKS configuration to change the fragment building mode. In the new implementation one should either link an instance of the **SwRodL1AInputHandler** with the **SwRodConfiguration** object via the _L1AHandler_ relationship to use TTC-aware variant of the chosen algorithm or otherwise to leave this relationship empty to use the fragment building algorithm in data-driven mode.
 
* Each detector custom plugin may now provide a function for data integrity validation following the example given below. If a plugin provides such a function the function name shall be set to the _DataIntegrityChecker_ attribute of the **SwRodCustomProcessingLib** OKS configuration object that describes this plugin.
    
```cpp
extern "C"
std::optional<bool> dataIntegrityChecker(const uint8_t * data) {
	if (contains_checksum(data)) {
		uint8_t checksum = get_checksum(data);
		return checksum == calculate_checksum(data) ? true : false;
	}
	return std::nullopt;
}
```

* The package provides so called Felix Emulator that can be used to send input data to an arbitrary SW ROD Application via Netio protocol. OKS file **data/FelixEmulatorSegment.data.xml** shows how to configure Felix Emulator. This example defines a FelixEmulator that can provide input to the SW ROD Application defined in the **data/SwRodSegment.data.xml** file. The FelixEmulator uses internal data generators to create L1A and data packets and sends these packets to the SW ROD Application. The Emulator is implemented by the SW ROD framework using a special plugin that can publish generated data via Netio protocol. Generated data packets can be customized either by modifying the _DataInput_ interface implementation provided by the **test/core/InternalDataGenerator.h(cpp)** files or by creating a new plugin that declares and implements a new generator class that inherits the **swrod::test::InternalDataGenerator** and overrides its _generatePacket()_ virtual function.