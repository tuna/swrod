/*
 * stopless_recovery.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#include <iostream>
#include <iterator>
#include <vector>

#include <boost/bind/bind.hpp>
#include <boost/test/included/unit_test.hpp>
#include <boost/test/parameterized_test.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <ipc/core.h>

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/Factory.h>

#include "core/ROBFragmentValidator.h"
#include "test/core/InternalL1AGenerator.h"

using namespace boost::placeholders;
using namespace boost::unit_test;
using namespace boost::property_tree;
using namespace swrod;
using namespace swrod::detail;
using namespace swrod::test;

const uint64_t L1ALinkID = 12345;

ptree createConfiguration(bool full_mode, uint32_t ROBs_number, uint32_t packet_size,
        uint32_t events_number, uint32_t links_number, uint32_t workers_number,
        uint32_t buffer_size, uint32_t ECR_interval)
{
    ptree configuration;
    configuration.add("Partition.Name", "initial");
    configuration.add("Application.Name", "unit_test_data_assembling");
    configuration.add("ISServerName", "DF");

    ptree plugins;

    ptree plugin1;
    plugin1.add("LibraryName", "libswrod_core_unit_test.so");
    plugins.add_child("Plugin", plugin1);
    ptree plugin2;
    plugin2.add("LibraryName", "libswrod_core_test.so");
    plugins.add_child("Plugin", plugin2);
    ptree plugin3;
    plugin3.add("LibraryName", "libswrod_core_impl.so");
    plugins.add_child("Plugin", plugin3);
    configuration.add_child("Plugins", plugins);

    ptree input;
    input.add("Type", "InternalData");
    input.add("PacketSize", packet_size);
    if (full_mode) {
        input.add("IsFullMode", true);
        input.add("TotalFullModeLinks", links_number);
    }
    configuration.add_child("InputMethod", input);

    for (uint32_t i = 0; i < ROBs_number; ++i) {
        ptree rob;
        rob.add("Id", i);
        rob.add("CPU", "");

        ptree event_builder;
        event_builder.add("Type", full_mode ? "FullModeBuilder" : "GBTModeBuilder");
        event_builder.add("BuildersNumber", 0); // must be zero as otherwise events counting may be broken
        event_builder.add("MaxMessageSize", packet_size + 4);
        event_builder.add("BufferSize", buffer_size);
        event_builder.add("MinimumBufferSize", 1024);
        event_builder.add("L1AWaitTimeout", 1000);
        event_builder.add("ResynchTimeout", 1000);
        event_builder.add("ReadyQueueSize", 10000);
        event_builder.add("RecoveryDepth", 10);
        event_builder.add("FlushBufferAtStop", false);
        event_builder.add("RODHeaderPresent", false);
        event_builder.add("DropCorruptedPackets", false);
        rob.add_child("FragmentBuilder", event_builder);

        ptree custom_proc;
        custom_proc.add("LibraryName", "libswrod_test_plugin.so");
        custom_proc.add("TrigInfoExtractor", "testTriggerInfoExtractor");
        custom_proc.add("DataIntegrityChecker", "testDataIntegrityChecker");
        custom_proc.add("ProcessorFactory", "createTestCustomProcessor");

        ptree data_channel;
        for (size_t e = 0; e < links_number; ++e) {
            ptree link;
            link.add("FelixId", i * links_number + e);
            link.add("DetectorResourceId", i * links_number + e);
            data_channel.add_child("Contains.InputLink", link);
        }
        data_channel.add_child("CustomLib", custom_proc);

        rob.add_child("Contains", data_channel);

        ptree consumers;
        ptree validator;
        validator.add("Type", "ROBFragmentValidator");
        consumers.add_child("Consumer", validator);
        rob.add_child("Consumers", consumers);

        ptree module;
        module.add("WorkersNumber", workers_number);
        module.add("CPU", "");
        module.add_child("ROBs.ROB", rob);
        configuration.add_child("Modules.Module", module);
    }

    ptree l1a_input;
    l1a_input.add("Type", "InternalL1A");
    l1a_input.add("EcrInterval", ECR_interval);
    l1a_input.add("EventsNumber", events_number);
    l1a_input.add("IsSingleton", true);

    ptree l1a_config;
    l1a_config.add("Type", "L1AInputHandler");
    l1a_config.add("Link", L1ALinkID);
    l1a_config.add("CPU", "");
    l1a_config.add_child("InputMethod", l1a_input);
    configuration.add_child("L1AHandler", l1a_config);

    return configuration;
}

namespace boost {
    namespace unit_test {
        template<class T>
        std::ostream& operator<<(std::ostream& out, const std::vector<T> & v) {
            out << '{' << std::hex;
            for (auto e : v) {
                out << "0x" << e << ", ";
            }
            if (!v.empty()) {
                out << "\b\b";
            }
            out << '}' << std::dec;
            return out;
        }
    }
}

const uint32_t workers_number = 2;
const uint32_t events_number = 10000;
const uint32_t buffer_size = 100000;
const uint32_t ECR_interval = 2000;
const uint32_t links_number = 20;
const uint32_t ROBs_number = 3;

void test_link_recovery(bool full_mode, const std::vector<uint64_t> & disabled_links)
{
    ptree config = createConfiguration(full_mode, ROBs_number, 100,
            events_number, links_number, workers_number, buffer_size, ECR_interval);

    BOOST_TEST_MESSAGE((full_mode ? "Full mode" : "GBT mode")
            << " link recovery test with links = " << disabled_links);

    uint32_t totalFragments = events_number * ROBs_number;
    uint32_t totalPackets = full_mode ? totalFragments : events_number * ROBs_number * links_number;
    uint32_t badLinksNumber = 0;
    for (auto l : disabled_links) {
        if (l >= ROBs_number * links_number) {
            ++badLinksNumber;
        }
    }
    uint32_t goodLinksNumber = (disabled_links.size() - badLinksNumber);

    try {
        Core core(config);
        std::vector<std::shared_ptr<ROBFragmentValidator>> validators;
        for (auto c : core.getConsumers()) {
            validators.push_back(
                    std::static_pointer_cast<ROBFragmentValidator>(c));
        }

        std::shared_ptr<InternalL1AGenerator> l1aGen = InternalL1AGenerator::instance();
        l1aGen->reset();

        core.connectToFelix();

        uint32_t last_L1ID = l1aGen->pause();
        BOOST_TEST_MESSAGE("Test recovery with zero triggers sent, last L1ID = 0x"
                << std::hex << last_L1ID);
        BOOST_CHECK(last_L1ID == uint32_t(-1));

        core.runStarted(RunParams());

        std::vector<uint64_t> links(disabled_links);
        core.disableInputLinks(links);
        BOOST_CHECK(links.size() == goodLinksNumber);

        links = disabled_links;
        core.enableInputLinks(links, last_L1ID);
        BOOST_CHECK(links.size() == goodLinksNumber);

        l1aGen->resume();
        l1aGen->waitForTriggersNumber(100);

        links = disabled_links;
        core.disableInputLinks(links);
        BOOST_CHECK(links.size() == goodLinksNumber);

        l1aGen->waitForTriggersNumber(100);

        last_L1ID = l1aGen->pause();
        BOOST_TEST_MESSAGE("Test recovery with active trigger, last L1ID = 0x"
                << std::hex << last_L1ID);
        BOOST_CHECK(last_L1ID > 0);

        links = disabled_links;
        core.enableInputLinks(links, last_L1ID);
        BOOST_CHECK(links.size() == goodLinksNumber);

        l1aGen->resume();

        core.runStopped();
        core.disconnectFromFelix();

        uint32_t v_fragments = 0, v_packets = 0, v_missed_packets = 0, v_corrupt_fragments = 0;
        for (auto v : validators) {
            v_fragments += v->getFragmentsNumber();
            v_packets += v->getPacketsNumber();
            v_missed_packets += v->getMissedPacketsNumber();
            v_corrupt_fragments += v->getCorruptFragmentsNumber();
       }

        BOOST_TEST_MESSAGE("Fragments built = " << v_fragments);
        BOOST_TEST_MESSAGE("Corrupt fragments = " << v_corrupt_fragments);
        BOOST_TEST_MESSAGE("Total packets = " << v_packets);

        BOOST_CHECK(v_fragments == totalFragments);
        BOOST_CHECK(v_missed_packets == 0);
        BOOST_CHECK(v_corrupt_fragments == 0);
        if (disabled_links.empty() || full_mode) {
            BOOST_CHECK(v_packets == totalPackets);
        }
        else {
            BOOST_CHECK(v_packets < totalPackets);
        }
    } catch (ers::Issue & ex) {
        ers::fatal(ex);
    }
}

void test_ROB_recovery(bool full_mode, const std::vector<uint32_t> & disabled_ROBs)
{
    ptree config = createConfiguration(full_mode, ROBs_number, 100,
            events_number, links_number, workers_number, buffer_size, ECR_interval);

    BOOST_TEST_MESSAGE((full_mode ? "Full mode" : "GBT mode")
            << " ROB recovery test with ROBs = " << disabled_ROBs);

    uint32_t totalFragments = events_number * ROBs_number;
    uint32_t badROBsNumber = 0;
    for (auto l : disabled_ROBs) {
        if (l >= ROBs_number) {
            ++badROBsNumber;
        }
    }
    uint32_t goodROBsNumber = disabled_ROBs.size() - badROBsNumber;

    try {
        Core core(config);
        std::vector<std::shared_ptr<ROBFragmentValidator>> validators;
        for (auto c : core.getConsumers()) {
            validators.push_back(
                    std::static_pointer_cast<ROBFragmentValidator>(c));
        }

        std::shared_ptr<InternalL1AGenerator> l1aGen = InternalL1AGenerator::instance();
        l1aGen->reset();

        core.connectToFelix();

        uint32_t last_L1ID = l1aGen->pause();
        BOOST_TEST_MESSAGE("Test recovery with zero triggers sent, last L1ID = 0x"
                << std::hex << last_L1ID);
        BOOST_CHECK(last_L1ID == uint32_t(-1));

        core.runStarted(RunParams());

        std::vector<uint32_t> ROBs(disabled_ROBs);
        core.disableROBs(ROBs);
        BOOST_CHECK(ROBs.size() == goodROBsNumber);

        ROBs = disabled_ROBs;
        core.enableROBs(ROBs, last_L1ID);
        BOOST_CHECK(ROBs.size() == goodROBsNumber);

        l1aGen->resume();
        l1aGen->waitForTriggersNumber(100);

        ROBs = disabled_ROBs;
        core.disableROBs(ROBs);
        BOOST_CHECK(ROBs.size() == goodROBsNumber);

        l1aGen->waitForTriggersNumber(100);

        last_L1ID = l1aGen->pause(true);
        BOOST_TEST_MESSAGE("Test recovery with active trigger, last L1ID = 0x"
                << std::hex << last_L1ID);
        BOOST_CHECK(last_L1ID > 0);

        ROBs = disabled_ROBs;
        core.enableROBs(ROBs, last_L1ID);
        BOOST_CHECK(ROBs.size() == goodROBsNumber);

        l1aGen->resume();

        core.runStopped();
        core.disconnectFromFelix();

        uint32_t v_fragments = 0, v_corrupt_fragments = 0;
        for (auto v : validators) {
            v_fragments += v->getFragmentsNumber();
            v_corrupt_fragments += v->getCorruptFragmentsNumber();
        }

        BOOST_TEST_MESSAGE("Fragments built = " << v_fragments);
        BOOST_TEST_MESSAGE("Corrupt fragments = " << v_corrupt_fragments);

        BOOST_CHECK(v_corrupt_fragments == 0);
        if (disabled_ROBs.empty()) {
            BOOST_CHECK(v_fragments == totalFragments);
        }
        else {
            BOOST_CHECK(v_fragments && v_fragments < totalFragments);
        }
    } catch (ers::Issue & ex) {
        ers::fatal(ex);
    }
}

void test_restart(bool full_mode)
{
    ptree config = createConfiguration(full_mode, ROBs_number, 100,
            events_number, links_number, workers_number, buffer_size, ECR_interval);

    BOOST_TEST_MESSAGE((full_mode ? "Full mode" : "GBT mode")
            << " restart test with " << events_number << " events");

    try {
        std::unique_ptr<Core> core(new Core(config));
        std::vector<std::shared_ptr<ROBFragmentValidator>> validators;
        for (auto c : core->getConsumers()) {
            validators.push_back(
                    std::static_pointer_cast<ROBFragmentValidator>(c));
        }

        std::shared_ptr<InternalL1AGenerator> l1aGen = InternalL1AGenerator::instance();
        l1aGen->reset();

        core->connectToFelix();
        core->runStarted(RunParams());

        l1aGen->waitForTriggersNumber(events_number/7);
        uint32_t last_L1ID = l1aGen->pause(true);

        // Destroy SW ROD
        core.reset(0);
        l1aGen->unsubscribe(L1ALinkID);

        // Start a new SW ROD
        core.reset(new Core(config));
        for (auto c : core->getConsumers()) {
            validators.push_back(
                    std::static_pointer_cast<ROBFragmentValidator>(c));
        }

        // Go to the Running state
        core->connectToFelix();
        core->runStarted(RunParams());

        // Resynchronize
        core->resynchAfterRestart(last_L1ID);

        // Resume the trigger
        l1aGen->resume();

        // Stop the run
        core->runStopped();
        core->disconnectFromFelix();

        uint32_t v_fragments = 0, v_packets = 0, v_missed_packets = 0;
        for (auto v : validators) {
            v_fragments += v->getFragmentsNumber();
            v_packets += v->getPacketsNumber();
            v_missed_packets += v->getMissedPacketsNumber();
        }

        uint32_t totalFragments = events_number * ROBs_number;
        uint32_t totalPackets = full_mode ? totalFragments
                : events_number * ROBs_number * links_number;
        BOOST_TEST_MESSAGE("Fragments built = " << v_fragments);
        BOOST_TEST_MESSAGE("Total packets = " << v_packets);
        BOOST_TEST_MESSAGE("Missed packets = " << v_missed_packets);

        BOOST_CHECK(v_fragments == totalFragments);
        BOOST_CHECK(v_packets == totalPackets);
        BOOST_CHECK(v_missed_packets == 0);
    } catch (ers::Issue & ex) {
        ers::fatal(ex);
    }
}

test_suite* init_unit_test_suite(int argc, char*argv[])
{
    if (argc != 2 || std::string("verbose") != argv[1]) {
        setenv("TDAQ_ERS_LOG", "null", 1);
        setenv("TDAQ_ERS_ERROR", "null", 1);
    }

    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::Exception & ex) {
        ers::fatal(ex);
        return 0;
    }

    std::vector<std::vector<uint64_t>> disabled_links = {{}, {2}, {2, 4}, {10, 30, 50}, {5, 15, 77}};
    std::vector<std::vector<uint32_t>> disabled_ROBs  = {{}, {0}, {1, 2}, {0, 1, 2}, {2, 3, 4}};

    // GBT mode tests
    boost::function<void (const std::vector<uint64_t>&)> test_method_1 =
            boost::bind(&test_link_recovery, false, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_1, disabled_links.begin(), disabled_links.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_2 =
            boost::bind(&test_ROB_recovery, false, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_2, disabled_ROBs.begin(), disabled_ROBs.end()));

    framework::master_test_suite()
        .add(BOOST_TEST_CASE(boost::bind(&test_restart, false)));

    // Full mode tests
    boost::function<void (const std::vector<uint64_t>&)> test_method_3 =
            boost::bind(&test_link_recovery, true, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_3, disabled_links.begin(), disabled_links.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_4 =
            boost::bind(&test_ROB_recovery, true, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_4, disabled_ROBs.begin(), disabled_ROBs.end()));

    framework::master_test_suite()
        .add(BOOST_TEST_CASE(boost::bind(&test_restart, true)));

   return 0;
}
