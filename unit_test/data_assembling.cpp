/*
 * data_assembling.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#include <iostream>
#include <iterator>
#include <vector>

#include <boost/bind/bind.hpp>
#include <boost/test/included/unit_test.hpp>
#include <boost/test/parameterized_test.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <ipc/core.h>

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/Factory.h>

#include "test/core/InternalL1AGenerator.h"
#include "core/ROBFragmentValidator.h"

using namespace boost::placeholders;
using namespace boost::unit_test;
using namespace boost::property_tree;
using namespace swrod;
using namespace swrod::detail;
using namespace swrod::test;

ptree createConfiguration(bool full_mode, bool use_TTC, bool drop_corrupt,
        uint32_t ROB_id, uint32_t packet_size,
        uint32_t events_number, uint32_t links_number, uint32_t workers_number,
        uint32_t buffer_size, uint32_t ecr_interval,
        uint32_t corrupt_data = 0, uint32_t corrupt_l1id = 0, uint32_t corrupt_bcid = 0,
        const std::vector<uint32_t> & l1ids_dropped = {},
        const std::vector<uint32_t> & links_dropped = {},
        const std::vector<uint32_t> & l1id_params = {16, 8, 2})
{
    ptree configuration;
    configuration.add("Partition.Name", "initial");
    configuration.add("Application.Name", "unit_test_data_assembling");
    configuration.add("ISServerName", "DF");

    ptree plugins;

    ptree plugin1;
    plugin1.add("LibraryName", "libswrod_core_unit_test.so");
    plugins.add_child("Plugin", plugin1);
    ptree plugin2;
    plugin2.add("LibraryName", "libswrod_core_test.so");
    plugins.add_child("Plugin", plugin2);
    ptree plugin3;
    plugin3.add("LibraryName", "libswrod_core_impl.so");
    plugins.add_child("Plugin", plugin3);
    configuration.add_child("Plugins", plugins);

    ptree input;
    input.add("Type", "TestData");
    input.add("PacketSize", packet_size);
    input.add("corrupt_data", corrupt_data);
    input.add("corrupt_l1id", corrupt_l1id);
    input.add("corrupt_bcid", corrupt_bcid);
    input.add("l1id_bit_mask1", l1id_params[0]);
    input.add("l1id_bit_mask2", l1id_params[1]);
    input.add("l1id_bit_mask_variation_step", l1id_params[2]);
    if (full_mode) {
        input.add("IsFullMode", true);
        input.add("TotalFullModeLinks", links_number);
    }
    if (!full_mode || use_TTC) {
        input.add("send_duplicates", true);
    }
    for (auto l : l1ids_dropped) {
        input.add("l1ids_dropped.l1id", l);
    }
    for (auto l : links_dropped) {
        input.add("links_dropped.link", l);
    }
    configuration.add_child("InputMethod", input);

    ptree rob;
    rob.add("Id", ROB_id);
    rob.add("CPU", "");

    ptree event_builder;
    event_builder.add("Type", full_mode ? "FullModeBuilder" : "GBTModeBuilder");
    event_builder.add("BuildersNumber", 0); // must be zero as otherwise events counting may be broken
    event_builder.add("MaxMessageSize", packet_size);
    event_builder.add("L1AWaitTimeout", 1000);
    event_builder.add("BufferSize", buffer_size);
    event_builder.add("MinimumBufferSize", 1024);
    event_builder.add("L1AWaitTimeout", 1000);
    event_builder.add("ResynchTimeout", 1000);
    event_builder.add("ReadyQueueSize", 10000);
    event_builder.add("FlushBufferAtStop", false);
    event_builder.add("RODHeaderPresent", false);
    event_builder.add("RecoveryDepth", 10);
    event_builder.add("DropCorruptedPackets", drop_corrupt);
    event_builder.add("CPU", "");
    rob.add_child("FragmentBuilder", event_builder);

    ptree custom_proc;
    custom_proc.add("LibraryName", "libswrod_test_plugin.so");
    custom_proc.add("TrigInfoExtractor", "testTriggerInfoExtractor");
    custom_proc.add("DataIntegrityChecker", "testDataIntegrityChecker");
    custom_proc.add("ProcessorFactory", "createTestCustomProcessor");

    ptree data_channel;
    for (size_t e = 0; e < links_number; ++e) {
        ptree link;
        link.add("FelixId", e);
        link.add("DetectorResourceId", e);
        data_channel.add_child("Contains.InputLink", link);
    }
    data_channel.add_child("CustomLib", custom_proc);
    rob.add_child("Contains", data_channel);

    ptree consumers;
    ptree custom_processing;
    custom_processing.add("Type", "ROBFragmentProcessor");
    custom_processing.add("WorkersNumber", 1);
    custom_processing.add("QueueSize", 100000);
    custom_processing.add("FlushBufferAtStop", false);
    custom_processing.add("DeferProcessing", false);
    custom_processing.add("CPU", "");
    consumers.add_child("Consumer", custom_processing);

    ptree validator;
    validator.add("Type", "ROBFragmentValidator");
    consumers.add_child("Consumer", validator);
    rob.add_child("Consumers", consumers);

    ptree module;
    module.add("WorkersNumber", workers_number);
    module.add("CPU", "");
    module.add_child("ROBs.ROB", rob);
    configuration.add_child("Modules.Module", module);

    if (use_TTC) {
        ptree l1a_input;
        l1a_input.add("Type", "InternalL1A");
        l1a_input.add("EcrInterval", ecr_interval);
        l1a_input.add("EventsNumber", events_number);
        l1a_input.add("TTCQueueSize", ecr_interval * 5);
        if (!full_mode) {
            l1a_input.add("DropMask", 997);
        }

        ptree l1a_config;
        l1a_config.add("Type", "L1AInputHandler");
        l1a_config.add("Link", 12345);
        l1a_config.add("CPU", "");
        l1a_config.add_child("InputMethod", l1a_input);
        configuration.add_child("L1AHandler", l1a_config);
    }

    return configuration;
}

const uint32_t links_number = 20;
const uint32_t workers_number = 1;
const uint32_t events_number = 20000;
const uint32_t buffer_size = 0x0fff;  // must be a (2^n)-1 as only in this case it will match the real buffer size

void run(Core & core, bool use_TTC, uint32_t iterations) {
    std::shared_ptr<InternalL1AGenerator> l1aGen = InternalL1AGenerator::instance();

    for (uint32_t i = 0; i < iterations; ++i) {
        core.connectToFelix();
        core.runStarted(RunParams());
        if (!use_TTC) {
            l1aGen->runStarted(RunParams());
            l1aGen->runStopped();
        }
        core.runStopped();
        core.disconnectFromFelix();
    }
}

namespace boost {
    namespace unit_test {
        template<class T>
        std::ostream& operator<<(std::ostream& out, const std::vector<T> & v) {
            out << '{' << std::hex;
            for (auto e : v) {
                out << "0x" << e << ", ";
            }
            if (!v.empty()) {
                out << "\b\b";
            }
            out << '}' << std::dec;
            return out;
        }
    }
}

void test_GBT_mode_assembling(uint32_t corrupt_packets,
        const std::vector<uint32_t> & l1ids_dropped,
        const std::vector<uint32_t> & links_dropped,
        const std::vector<uint32_t> & l1id_pattern)
{
    const uint32_t ROB_id = 11;
    const uint32_t ECR_interval = 50;
    InternalL1AGenerator::instance()->setECRInterval(ECR_interval);

    for (int use_TTC = 1; use_TTC >= 0; --use_TTC) {
        for (int drop_corrupt_packets = 1; drop_corrupt_packets >= 0; --drop_corrupt_packets) {
            ptree config = createConfiguration(
                    false /* GBT mode */, use_TTC, drop_corrupt_packets, ROB_id, 40,
                    events_number, links_number, workers_number, buffer_size, ECR_interval,
                    corrupt_packets/3, corrupt_packets/3, corrupt_packets/3,
                    l1ids_dropped, links_dropped, l1id_pattern);

            BOOST_TEST_MESSAGE("GBT Mode (TTC=" << use_TTC << "): L1ID pattern = " << l1id_pattern
                        << " dropped L1IDs = " << l1ids_dropped << " dropped links = " << links_dropped
                        << " corrupt # = " << corrupt_packets << " drop corrupt = " << drop_corrupt_packets);

            try {
                Core core(config);
                const Core::FragmentConsumers & cc = core.getConsumers();
                BOOST_REQUIRE(!cc.empty());
                std::shared_ptr<ROBFragmentValidator> validator =
                        std::dynamic_pointer_cast<ROBFragmentValidator>(*cc.rbegin());
                BOOST_REQUIRE(validator);

                const uint32_t iterations = 2;
                run(core, use_TTC, iterations);

                uint32_t corrupt_packets_total = corrupt_packets * workers_number * iterations;

                uint32_t totalFragments = events_number * iterations;
                uint32_t emptyFragments = l1ids_dropped.size() * totalFragments / ECR_interval;
                uint32_t l1idErrors = corrupt_packets_total / 3 * (!drop_corrupt_packets);
                uint32_t bcidErrors = corrupt_packets_total / 3 * (!drop_corrupt_packets);
                uint32_t crcErrors = (l1idErrors + bcidErrors) / 2;
                uint32_t droppedPackets = corrupt_packets_total * (1 + 2 * drop_corrupt_packets) / 3;
                uint32_t corruptPackets = l1idErrors + bcidErrors;
                uint32_t missedPackets = emptyFragments * (links_number - links_dropped.size())
                        + links_dropped.size() * totalFragments
                        + droppedPackets;

                BOOST_TEST_MESSAGE("Fragments built = " << validator->getFragmentsNumber()
                        << " L1ID corrupt = " << validator->getL1IDCorruptNumber()
                        << " BCID corrupt = " << validator->getBCIDCorruptNumber()
                        << " L1ID errors = " << validator->getL1IDErrorsNumber()
                        << " BCID errors = " << validator->getBCIDErrorsNumber()
                        << " CRC errors = " << validator->getCRCErrorsNumber()
                        << " corrupt errors = " << validator->getCorruptErrorsNumber()
                        << " L1ID mismatches = " << validator->getL1IDMismatchesNumber()
                        << " BCID mismatches = " << validator->getBCIDMismatchesNumber()
                        << " empty events = " << validator->getEmptyFragmentsNumber()
                        << " missed packets = " << validator->getMissedPacketsNumber()
                        << " corrupt packets = " << validator->getCorruptPacketsNumber()
                        << " broken packets = " << validator->getBrokenPacketsNumber());

                BOOST_CHECK(validator->getL1IDCorruptNumber() == emptyFragments * !use_TTC);
                BOOST_CHECK(validator->getBCIDCorruptNumber() == emptyFragments * !use_TTC);
                BOOST_CHECK(validator->getL1IDErrorsNumber() == l1idErrors);
                BOOST_CHECK(validator->getBCIDErrorsNumber() == bcidErrors);
                BOOST_CHECK(validator->getCRCErrorsNumber() == crcErrors);
                BOOST_CHECK(validator->getCorruptErrorsNumber() == 0);
                BOOST_CHECK(validator->getL1IDMismatchesNumber() == l1idErrors);
                BOOST_CHECK(validator->getBCIDMismatchesNumber() == bcidErrors);
                BOOST_CHECK(validator->getEmptyFragmentsNumber() == emptyFragments);
                BOOST_CHECK(validator->getFragmentsNumber() == totalFragments);
                BOOST_CHECK(validator->getMissedPacketsNumber() == missedPackets);
                BOOST_CHECK(validator->getCorruptPacketsNumber() == corruptPackets);
                BOOST_CHECK(validator->getBrokenPacketsNumber() == 0);
            } catch (ers::Issue & ex) {
                ers::fatal(ex);
            }
        }
    }
}

void test_full_mode_assembling(uint32_t corrupt_packets,
        const std::vector<uint32_t> & l1ids_dropped,
        const std::vector<uint32_t> & links_dropped,
        const std::vector<uint32_t> & l1id_pattern)
{
    const uint32_t ROB_id = 11;
    const uint32_t ECR_interval = 1000;

    for (int use_TTC = 1; use_TTC >= 0; --use_TTC) {
        ptree config = createConfiguration(
                true /* Full mode */, use_TTC, true /* corrupt packets always dropped */, ROB_id, 4000,
                events_number, links_number, workers_number, buffer_size, ECR_interval,
                corrupt_packets/3, corrupt_packets/3, corrupt_packets/3,
                l1ids_dropped, links_dropped, l1id_pattern);

        BOOST_TEST_MESSAGE("Full Mode (TTC=" << use_TTC << "): L1ID pattern = " << l1id_pattern
                    << " dropped L1IDs = " << l1ids_dropped << " dropped links = " << links_dropped
                    << " corrupt # = " << corrupt_packets);

        try {
            Core core(config);
            const Core::FragmentConsumers & cc = core.getConsumers();
            BOOST_REQUIRE(!cc.empty());
            std::shared_ptr<ROBFragmentValidator> validator =
                    std::dynamic_pointer_cast<ROBFragmentValidator>(*cc.rbegin());
            BOOST_REQUIRE(validator);

            const uint32_t iterations = 2;
            run(core, use_TTC, iterations);

            uint32_t missedFragments = (l1ids_dropped.size() * events_number / ECR_interval
                            + events_number * links_dropped.size() / links_number
                            + corrupt_packets / 3 * (use_TTC + 1) * workers_number) * iterations;
            uint32_t totalFragments = events_number * iterations - missedFragments;
            uint32_t bcidMismatches = corrupt_packets / 3 * workers_number * iterations * use_TTC;

            BOOST_TEST_MESSAGE("Fragments built = " << validator->getFragmentsNumber()
                    << " L1ID corrupt = " << validator->getL1IDCorruptNumber()
                    << " BCID corrupt = " << validator->getBCIDCorruptNumber()
                    << " L1ID mismatches = " << validator->getL1IDMismatchesNumber()
                    << " BCID mismatches = " << validator->getBCIDMismatchesNumber()
                    << " empty events = " << validator->getEmptyFragmentsNumber()
                    << " missed packets = " << validator->getMissedPacketsNumber()
                    << " corrupt packets = " << validator->getCorruptPacketsNumber()
                    << " broken packets = " << validator->getBrokenPacketsNumber());

            BOOST_CHECK(validator->getL1IDCorruptNumber() == 0);
            BOOST_CHECK(validator->getBCIDCorruptNumber() == 0);
            BOOST_CHECK(validator->getFragmentsNumber() == totalFragments);
            BOOST_CHECK(validator->getL1IDMismatchesNumber() == 0);
            BOOST_CHECK(validator->getBCIDMismatchesNumber() == bcidMismatches);
            BOOST_CHECK(validator->getEmptyFragmentsNumber() == 0);
            BOOST_CHECK(validator->getMissedPacketsNumber() == 0);
            BOOST_CHECK(validator->getBrokenPacketsNumber() == 0);
            BOOST_CHECK(validator->getCorruptPacketsNumber() == 0);
        } catch (ers::Issue & ex) {
            ers::fatal(ex);
        }
    }
}

test_suite* init_unit_test_suite(int argc, char*argv[])
{
    if (argc != 2 || std::string("verbose") != argv[1]) {
        setenv("TDAQ_ERS_ERROR", "null", 1);
        setenv("TDAQ_ERS_LOG", "null", 1);
    }

    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::Exception & ex) {
        ers::fatal(ex);
        return 0;
    }

    std::vector<std::vector<uint32_t>> l1id_patterns =
        {{0xff, 0xff, 2}, {0xff, 0xffff, 2}, {0xffff, 0xffff, 2}, {0xffffffff, 0xffffffff, 2}};
    std::vector<uint32_t> l1ids_dropped = {0x1, 0x2, 0x0f, 0x10, 0x12, 48};
    std::vector<uint32_t> links_dropped = {2, 4, 15, 17};
    uint32_t corrupt_packets = 600;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Full mode tests
    // Skip the first 3 L1ID patterns as current algorithm will loose data for them
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    boost::function<void (const std::vector<uint32_t>&)> test_method_full_1 = boost::bind(
            &test_full_mode_assembling, 0, std::vector<uint32_t>(), std::vector<uint32_t>(), _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_full_1, l1id_patterns.begin() + 3, l1id_patterns.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_full_1_1 = boost::bind(
            &test_full_mode_assembling, corrupt_packets, std::vector<uint32_t>(), std::vector<uint32_t>(), _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_full_1_1, l1id_patterns.begin() + 3, l1id_patterns.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_full_2 = boost::bind(
            &test_full_mode_assembling, 0, std::vector<uint32_t>(), links_dropped, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_full_2, l1id_patterns.begin() + 3, l1id_patterns.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_full_3 = boost::bind(
            &test_full_mode_assembling, 0, l1ids_dropped, std::vector<uint32_t>(), _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_full_3, l1id_patterns.begin() + 3, l1id_patterns.end()));

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // GBT mode tests
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    boost::function<void (const std::vector<uint32_t>&)> test_method_GBT_1 = boost::bind(
            &test_GBT_mode_assembling, 0, std::vector<uint32_t>(), std::vector<uint32_t>(), _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_GBT_1, l1id_patterns.begin(), l1id_patterns.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_GBT_1_1 = boost::bind(
            &test_GBT_mode_assembling, corrupt_packets, std::vector<uint32_t>(), std::vector<uint32_t>(), _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_GBT_1_1, l1id_patterns.begin(), l1id_patterns.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_GBT_2 = boost::bind(
            &test_GBT_mode_assembling, 0, l1ids_dropped, std::vector<uint32_t>(), _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_GBT_2, l1id_patterns.begin(), l1id_patterns.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_GBT_3 = boost::bind(
            &test_GBT_mode_assembling, 0, std::vector<uint32_t>(), links_dropped, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_GBT_3, l1id_patterns.begin(), l1id_patterns.end()));

    boost::function<void (const std::vector<uint32_t>&)> test_method_GBT_4 = boost::bind(
            &test_GBT_mode_assembling, 0, l1ids_dropped, links_dropped, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_GBT_4, l1id_patterns.begin(), l1id_patterns.end()));

    return 0;
}
