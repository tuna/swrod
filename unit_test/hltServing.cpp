#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE swrod::HLTRequestHandler
#include <boost/test/unit_test.hpp>
#include <memory>
#include <iostream>
#include <chrono>
#include <thread>                                       // for thread
#include <utility>                                      // for pair, move
#include <vector>                                       // for vector

#include "../src/core/HLTRequestHandler.h"
#include "../src/core/HLTDataBuffer.h"
#include "swrod/ROBFragment.h"
#include "swrod/exceptions.h"

#include "rc/RunParams.h"

#include "UnicastClear.h"
#include "DataRequester.h"

using namespace std::literals::chrono_literals;
using namespace swrod;
using boost::property_tree::ptree;
using ROS::UnicastClear;

void insertEvents(std::vector<std::shared_ptr<HLTDataBuffer> > buffer,
                  unsigned int nRobs, unsigned int nEvents) {
   const unsigned int NBYTES=16;
   for (unsigned int rob=0;rob<nRobs;rob++) {
      unsigned int sourceId=0x007c0000+rob;
      for (unsigned int l1Id=0; l1Id<nEvents; l1Id++) {
         unsigned int* data=new unsigned int[NBYTES];
         for (unsigned int byte=0;byte<NBYTES;byte++) {
            data[byte]=byte;
         }

         std::vector<ROBFragment::DataBlock> v;
         v.push_back(ROBFragment::DataBlock(data, NBYTES, NBYTES));
         std::shared_ptr<ROBFragment> frag=std::make_shared<ROBFragment>(
                 sourceId, l1Id, 0, 0, 0, 0, 0, std::move(v));
         buffer[rob]->insertROBFragment(frag);
      }
   }
}

BOOST_AUTO_TEST_CASE(hltServing){
   const unsigned int nRobs=5;
   const unsigned int nEvents=150;

   ptree coreConfig;
   coreConfig.put("DataFlowParameters.MulticastAddress","tcp:");
   coreConfig.add("DataFlowParameters.networks.network","127.0.0.1/255.255.255.255");
   coreConfig.put("Application.Name","unit_test");

   ptree config;
   config.put("IgnoreClearXId",false);
   config.put("DataRequestTimeout",1000);
   config.put("ClearTimeout",1000);
   config.put("IOServices",1);
   config.put("DataServerThreads",2);
   config.put("MaxIndex",1000);
   config.put("environment.standalone",true);
   config.put("CPU","");
   ptree modConf;
   for (unsigned int rob=0;rob<nRobs;rob++) {
      ptree robConf;
      robConf.put("Id",0x007c0000+rob);
      modConf.add_child("ROBs.rob",robConf);
   }
   coreConfig.add_child("Modules.Module",modConf);

   std::vector<std::shared_ptr<HLTDataBuffer> > buffer;

   std::shared_ptr<HLTRequestHandler> handler(HLTRequestHandler::instance(config,coreConfig));
   for (const ptree::value_type & v : coreConfig.get_child("Modules.Module.ROBs")) {
      const ptree & rob_config = v.second;
      uint32_t robId = rob_config.get<uint32_t>("Id");
      buffer.emplace_back(handler->addBuffer(robId,config));
   }

   for (auto buf : buffer) {
      BOOST_CHECK(buf->size()==0);
   }
   insertEvents(buffer,nRobs,nEvents);
   // No run started, events should have been ignored
   for (auto buf : buffer) {
      BOOST_CHECK(buf->size()==0);
   }

   RunParams runPars;
   runPars.run_number=12345;
   runPars.max_events=10;
   for (auto buf : buffer) {
      buf->runStarted(runPars);
   }

   insertEvents(buffer,nRobs,nEvents);

   BOOST_CHECK(buffer[0]->size()==nEvents);

   std::vector<unsigned int> robs{0x007c0001,0x007c0002,0x007c0004};
   handler->processDataRequest(1,&robs,0,0);
   auto stats=dynamic_cast<const HLTRequestStatistics*>(buffer[0]->getStatistics());
   BOOST_CHECK(stats->badROBs==0);

   robs.push_back(0x007c0003);
   handler->processDataRequest(1,&robs,0,0);
   stats=dynamic_cast<const HLTRequestStatistics*>(buffer[0]->getStatistics());
   BOOST_CHECK(stats->badROBs==0);

   robs.push_back(0x007c0006);
   handler->processDataRequest(1,&robs,0,0);
   stats=dynamic_cast<const HLTRequestStatistics*>(buffer[0]->getStatistics());
   BOOST_CHECK(stats->badROBs==1);

   robs.pop_back();

   std::vector<unsigned int> clears;
   for (unsigned int l1Id=0;l1Id<3; l1Id++) {
      clears.push_back(l1Id);
   }
   // This request should work and not complain about transactionId
   // doesn't start at 0 as the 1st transactionId could be anything
   handler->processClearRequest(clears,100);
   std::this_thread::sleep_for(50ms);
   stats=dynamic_cast<const HLTRequestStatistics*>(buffer[0]->getStatistics());
   BOOST_CHECK(stats->clearRequestsReceived==1);
   std::cout << "HLTRequestStatistics:" << *stats << std::endl;

   BOOST_CHECK(stats->fragmentsCleared==3);
   BOOST_CHECK(stats->clearRequestsDuplicated==0);
   BOOST_CHECK(stats->clearRequestsMissed==0);

   // This request should fail as it has a duplicate transactionId
   handler->processClearRequest(clears,100);
   std::this_thread::sleep_for(50ms);
   stats=dynamic_cast<const HLTRequestStatistics*>(buffer[0]->getStatistics());
   BOOST_CHECK(stats->clearRequestsReceived==1);
   BOOST_CHECK(stats->fragmentsCleared==3);
   BOOST_CHECK(stats->clearRequestsDuplicated==1);
   BOOST_CHECK(stats->clearRequestsMissed==0);

   // This request should record 99 missing clears as transactionId
   // is wrong and it should generate 3 clear failures as
   // previous clear request should have alredy cleared these events
   handler->processClearRequest(clears,200);
   std::this_thread::sleep_for(50ms);
   stats=dynamic_cast<const HLTRequestStatistics*>(buffer[0]->getStatistics());
   BOOST_CHECK(stats->clearRequestsReceived==2);
   BOOST_CHECK(stats->fragmentsCleared==3);
   BOOST_CHECK(stats->failedClears==3);
   BOOST_CHECK(stats->clearRequestsDuplicated==1);
   BOOST_CHECK(stats->clearRequestsMissed==99);

   handler->processDataRequest(1,&robs,0,0);

   clears.clear();
   for (unsigned int l1Id=3;l1Id<nEvents; l1Id++) {
      clears.push_back(l1Id);
   }
   handler->processClearRequest(clears,201);
   //std::cout << "clears issued, buffer[0]->size()=" << buffer[0]->size() << std::endl;std::cout.flush();
   std::this_thread::sleep_for(50ms);
   //std::cout << "waited 500us, buffer[0]->size()=" << buffer[0]->size() << std::endl;std::cout.flush();
   BOOST_CHECK(buffer[0]->size()==0);

   stats=dynamic_cast<const HLTRequestStatistics*>(buffer[0]->getStatistics());
   BOOST_CHECK(stats->dataRequestsReceived==4);
   BOOST_CHECK(stats->clearRequestsReceived==3);
   BOOST_CHECK(stats->fragmentsCleared==nEvents);
   BOOST_CHECK(stats->clearRequestsDuplicated==1);
   BOOST_CHECK(stats->clearRequestsMissed==99);

   //handler->dumpIndex();

   insertEvents(buffer,nRobs,nEvents);
   std::this_thread::sleep_for(50ms);
   std::cout << "nEvents=" << nEvents << ", buffer[0]->size()=" << buffer[0]->size() << std::endl;std::cout.flush();
   BOOST_CHECK(buffer[0]->size()==nEvents);
 
   //std::cout << "Check that runStopped() empties index\n";
   for (auto buf : buffer) {
      buf->runStopped();
   }
   std::this_thread::sleep_for(50ms);
   for (auto buf : buffer) {
      BOOST_CHECK(buf->size()==0);
   }

   runPars.run_number++;
   for (auto buf : buffer) {
      buf->runStarted(runPars);
   }
   std::this_thread::sleep_for(10ms);
   
   insertEvents(buffer,nRobs,nEvents);

   BOOST_CHECK(buffer[0]->size()==nEvents);


   //std::cout << "Instantiating Requester\n";std::cout.flush();
   boost::asio::io_service dataIOService;
   auto dataWork=std::make_shared<boost::asio::io_service::work>(dataIOService);
   auto dataThread=new std::thread
      (boost::bind(&boost::asio::io_service::run,
                   &dataIOService));
   auto requester=std::make_shared<swrod::testing::DataRequester> (dataIOService);

   auto endPoint=boost::asio::ip::tcp::endpoint(
      boost::asio::ip::address::from_string("127.0.0.1"),9000);

   //std::cout << "Opening connection\n";std::cout.flush();
   requester->asyncOpen("test",endPoint);
   while(requester->state() != daq::asyncmsg::Session::State::OPEN) {
      std::this_thread::sleep_for(100ms);
   }

   // Request an event that should exist
   std::unique_ptr<ROS::DataRequestMsg> requestMsg(
      new ROS::DataRequestMsg(robs,5,0));
   requester->asyncSend(std::move(requestMsg));

   // Request an event that should NOT exist
   requestMsg.reset(new ROS::DataRequestMsg(robs,0x05050505,0));
   requester->asyncSend(std::move(requestMsg));

   auto clearIOService=new boost::asio::io_service;
   auto clearWork=std::make_shared<boost::asio::io_service::work>(*clearIOService);
   auto clearThread=new std::thread
      (boost::bind(&boost::asio::io_service::run,
                   clearIOService));
 
   auto clearSession=std::make_shared<UnicastClear>(50,
                                                    *clearIOService,
                                                    &endPoint);
   clearSession->connect();

   for (unsigned int l1Id=0; l1Id<nEvents+50; l1Id++) {
      clearSession->add_event(l1Id);
   }


   // Wait for the async stuff to do its thing
   std::this_thread::sleep_for(1s);

   BOOST_CHECK(requester->packetsReceived()==2);
   BOOST_CHECK(requester->eformatErrors()==0);
   BOOST_CHECK(requester->missingEvents()==1);

   BOOST_CHECK(buffer[0]->size()==0);


   for (auto buf : buffer) {
      buf->runStopped();
   }
   std::this_thread::sleep_for(1s);

   std::cout << "Resetting HLTRequestHandler shared pointer\n";std::cout.flush();
   handler.reset();

   std::cout << "Resetting HLTDataBuffer shared pointers\n";std::cout.flush();
   for (auto buf : buffer) {
      buf.reset();
   }
   std::cout << "Clearing up Clear session\n";std::cout.flush();
   clearWork.reset();
   clearIOService->stop();
   clearSession.reset();
   if (clearThread->joinable()) {
      clearThread->join();
   }

   std::this_thread::sleep_for(1s);

   //std::cout << "Clearing up data session\n";std::cout.flush();
   dataWork.reset();
   requester->asyncClose();
   dataIOService.stop();
   requester.reset();
   if (dataThread->joinable()) {
      dataThread->join();
   }

}
