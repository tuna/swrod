#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE swrod::FileWriter
#include <boost/test/unit_test.hpp>
#include <unistd.h>
#include <memory>
#include <iostream>
#include <chrono>
#include <cstdint>                                      // for uint32_t, uin...
#include <sstream>                                      // for basic_stringb...
#include <string>                                       // for operator<<
#include <utility>                                      // for pair
#include <vector>                                       // for vector
#include <boost/property_tree/ptree.hpp>

#include "../src/core/FileWriter.h"
#include "swrod/FragmentCollator.h"
#include "swrod/ROBFragment.h"
#include "swrod/detail/HeaderHelper.h"

#include "rc/RunParams.h"
#include "eformat/FullEventFragment.h"
#include "EventStorage/pickDataReader.h"

using namespace swrod;
using boost::property_tree::ptree;

BOOST_AUTO_TEST_CASE(fileWriting){
   unsigned int nRobs=3;

   ptree coreConfig;
   for (unsigned int rob=0;rob<nRobs;rob++) {
      coreConfig.add("Modules.Module.ROBs.rob",rob);
   }
   coreConfig.put("Application.Name","unit_test");
   ptree config;
   config.put("QueueSize",1024);
   config.put("IgnoreRecordingEnable",false);
   config.put("EventStorage.DirectoryToWrite","/tmp");
   config.put("EventStorage.MaxEventsPerFile", 0);
   config.put("EventStorage.MaxMegaBytesPerFile", 256);

   FileWriter fw(config,coreConfig);
   ///FileWriter fw("fileWritingTest","/tmp",nRobs);

   RunParams runPars;
   runPars.run_number=12345;
   runPars.max_events=10;

   runPars.recording_enabled=false;
   fw.runStarted(runPars);
   BOOST_CHECK(!fw.active());

   runPars.recording_enabled=true;
   fw.runStarted(runPars);
   BOOST_CHECK(fw.active());

   std::string fileName=fw.fileName();
   std::cout << "Writing " << fileName << std::endl;

   const unsigned int NBYTES=16;
   unsigned int nEvents=12;
   unsigned int loopCount=0;
   unsigned int disablePoint=18;
   for (unsigned int rob=0;rob<nRobs;rob++) {
      unsigned int sourceId=0x007c0001+rob;
      for (unsigned int l1Id=0; l1Id<nEvents; l1Id++) {
         unsigned int* data=new unsigned int[NBYTES];
         for (unsigned int byte=0;byte<NBYTES;byte++) {
            data[byte]=byte;
         }
         std::vector<ROBFragment::DataBlock> v;
         v.push_back(ROBFragment::DataBlock(data, NBYTES, NBYTES));
         std::shared_ptr<ROBFragment> frag=std::make_shared<ROBFragment>(
                 sourceId, l1Id, 0, 0, 0, 0, 0, std::move(v));
         fw.insertROBFragment(frag);
         loopCount++;
         if (loopCount==disablePoint) {
            fw.ROBDisabled(sourceId);
         }
      }
   }
   sleep(2);
   fw.runStopped();
   BOOST_CHECK(!fw.active());

   unsigned int eventsRead=0;
   EventStorage::DataReader* reader=pickDataReader(fileName);
   while (reader->good()) {
      unsigned int eventSize;
      char* buffer;
      DRError errorCode = reader->getData(eventSize,&buffer);
      if (errorCode==DROK) {
         //std::cout << "read event size " << eventSize << std::endl;
         unsigned int* header=reinterpret_cast<uint32_t*>(buffer);
         BOOST_CHECK(header[0]==0xaa1234aa);
         if (header[0]==0xaa1234aa) {
            const eformat::FullEventFragment<const uint32_t*> fragment(header);
            BOOST_CHECK(fragment.check_tree_noex());
            eventsRead++;
         }
      }
   }
   BOOST_CHECK(eventsRead==nEvents);
}

using namespace swrod::helper;

BOOST_AUTO_TEST_CASE(fragmentCollating) {

   FragmentCollator collator;
   collator.expected(4);
   HeaderHelper headerHelper;
   headerHelper.runNumber(0x1234);
   bool doDisable=true;
   for (unsigned int l1Id=0;l1Id<3;l1Id++) { 
      // Spurious fragment for source 0 should cause warning message
      std::shared_ptr<ROBFragment> badFrag=std::make_shared<ROBFragment>(0, l1Id, 0);
      collator.insert(badFrag);
      for (unsigned int part=0; part<4; part++) {
         std::shared_ptr<ROBFragment> frag=std::make_shared<ROBFragment>(part, l1Id, 0);
         //std::cout << "inserting part " << part <<std::endl;
         bool complete=collator.insert(frag);
         if (part!=3) {
            BOOST_CHECK(!complete);
         }
         else {
            BOOST_CHECK(complete);
         }

         if (complete) {
            auto collation=collator.get(l1Id);
            BOOST_CHECK(collation.size()==collator.expected());

            // std::shared_ptr<unsigned int[]> ffHead;
            // ffHead.reset(headerHelper.makeHeader(collation));
            // std::cout << std::hex << ffHead[0] << " " << ffHead[2]
            //           << " " << ffHead[13] << " " << ffHead[15]
            //           << std::dec << std::endl;
         }
      }
      if (doDisable) {
         collator.disable(1);
      }
      else {
         collator.enable(1);
      }
      doDisable=!doDisable;
      collator.clear(l1Id);
   }
   BOOST_CHECK(collator.size()==0);
   //std::cout << collator.size() << " events in collator" << std::endl;
   collator.enable(1);

   const unsigned int nParts=8;
   std::vector<std::shared_ptr<ROBFragment>> fragVec[nParts];
   collator.expected(nParts);
   // Speed test
   unsigned int nEvents=100000;
   for (unsigned int l1Id=0;l1Id<nEvents;l1Id++) { 
      for (unsigned int part=0; part<nParts; part++) {
         std::shared_ptr<ROBFragment> frag=std::make_shared<ROBFragment>(part, l1Id, 0);
         fragVec[part].push_back(frag);
      }
   }
   std::cout << "Start inserting\n";
   auto start=std::chrono::system_clock::now();
   for (unsigned int part=0; part<nParts; part++) {
      for (unsigned int loop=0;loop<nEvents;loop++) { 
         //std::cout << "inserting part " << part <<std::endl;
         auto frag=fragVec[part].back();
         fragVec[part].pop_back();
         bool complete=collator.insert(frag);
         if (complete) {
            auto collation=collator.get(frag->m_l1id);
//            BOOST_CHECK(collation.size()==collator.expected());
            collator.clear(frag->m_l1id);
         }
      }
   }
   auto now=std::chrono::system_clock::now();
   auto elapsed=std::chrono::duration_cast<std::chrono::milliseconds> (
      now-start).count();
   std::cout << "Time for " << nEvents << " events=" << (float)elapsed/1000.0 << " s\n";

   BOOST_CHECK(collator.size()==0);

   for (unsigned int l1Id=0;l1Id<nEvents;l1Id++) { 
      for (unsigned int part=0; part<nParts; part++) {
         std::shared_ptr<ROBFragment> frag=std::make_shared<ROBFragment>(part, l1Id, 0);
         fragVec[part].push_back(frag);
      }
   }
   collator.disable(3);
   collator.disable(5);
   start=std::chrono::system_clock::now();
   for (unsigned int loop=0;loop<nEvents;loop++) { 
      for (unsigned int part=0; part<nParts; part++) {
         //std::cout << "inserting part " << part <<std::endl;
         auto frag=fragVec[part].back();
         fragVec[part].pop_back();
         bool complete=collator.insert(frag);
         if (complete) {
            auto collation=collator.get(frag->m_l1id);
//            BOOST_CHECK(collation.size()==collator.expected());
            collator.clear(frag->m_l1id);
         }
      }
   }
   now=std::chrono::system_clock::now();
   elapsed=std::chrono::duration_cast<std::chrono::milliseconds> (
      now-start).count();
   std::cout << "Time for " << nEvents << " events=" << (float)elapsed/1000.0 << " s\n";

}
