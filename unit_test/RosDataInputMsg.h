// -*- c++ -*-
//
#ifndef ROS_DATA_INPUT_MSG_H
#define ROS_DATA_INPUT_MSG_H

#include "ROSasyncmsg/RosDataMsg.h"

namespace ROS {
   class RosDataInputMsg: public RosDataMsg,
                          public daq::asyncmsg::InputMessage {
   public:
      explicit RosDataInputMsg(std::uint32_t messageSize, std::uint32_t transactionId) :
         RosDataMsg(transactionId), m_size(messageSize) {
         m_fragments=new std::uint32_t[messageSize/sizeof(std::uint32_t)];
      };

      RosDataInputMsg(const RosDataInputMsg&)=delete;

      RosDataInputMsg& operator =(const RosDataInputMsg&)=delete;

      virtual ~RosDataInputMsg() {
         delete[] m_fragments;
      };

      virtual std::uint32_t size() const {
         return m_size;
      };

      std::uint32_t* m_fragments;
      std::uint32_t m_size;

      void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) final {
         buffers.emplace_back(m_fragments, m_size);
      }
   };
}
#endif
