// -*- c++ -*-
#ifndef SWROD_TEST_DATAREQUESTER_H
#define SWROD_TEST_DATAREQUESTER_H

#include "asyncmsg/Session.h"
#include "RosDataInputMsg.h"
#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/DataRequestMsg.h"
#include "ROSasyncmsg/RosDataMsg.h"

# include "eformat/eformat.h"

namespace swrod{
   namespace testing {

      class DataRequester: public ROS::DataServerSession {//daq::asyncmsg::Session {
      public:
         explicit DataRequester(boost::asio::io_service& ioService);
         virtual ~DataRequester();
         unsigned int packetsReceived()const {return m_packetsReceived;};
         unsigned int eformatErrors()const {return m_eformatErrors;};
         unsigned int missingEvents()const {return m_missingEvents;};
      private:
         virtual std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(
            std::uint32_t typeId, std::uint32_t transactionId,
            std::uint32_t size) noexcept final;
         virtual void onSend(
            std::unique_ptr<const daq::asyncmsg::OutputMessage> message)
            noexcept final;
         virtual void onReceive(
            std::unique_ptr<daq::asyncmsg::InputMessage> message) final;
         unsigned int m_packetsReceived;
         unsigned int m_eformatErrors;
         unsigned int m_missingEvents;
      };

      DataRequester::DataRequester(boost::asio::io_service& ioService)
         : ROS::DataServerSession(ioService),
           m_packetsReceived(0),
           m_eformatErrors(0),
           m_missingEvents(0) {
      }
      DataRequester::~DataRequester(){
      }
      void DataRequester::onSend(
         std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept{
         asyncReceive(); // Expect reply
      }
      std::unique_ptr<daq::asyncmsg::InputMessage> DataRequester::createMessage(
         std::uint32_t typeId,
         std::uint32_t transactionId,
         std::uint32_t size) noexcept{
         if (typeId == RosDataMsg::TYPE_ID) {
            std::unique_ptr<daq::asyncmsg::InputMessage> message;
            message.reset(new ROS::RosDataInputMsg(size,transactionId));
            return message;
         }
         else {
            ERS_LOG("Unexpected message type " << typeId << " from " << remoteEndpoint() << ". Ignoring.");
            return std::unique_ptr<daq::asyncmsg::InputMessage>();
         }
      }

      void DataRequester::onReceive(
         std::unique_ptr<daq::asyncmsg::InputMessage> message){
         std::cout << "DataRequester::onReceive() Received data message\n";
         std::unique_ptr<ROS::RosDataInputMsg> dataMsg(
            dynamic_cast<ROS::RosDataInputMsg*>(message.release()));
         if (!dataMsg) {
            std::cerr << "Could not cast input message to RosDataInputMsg\n";
            return;
         }
         auto fragment=new
            eformat::ROBFragment<const unsigned int*>(dataMsg->m_fragments);
         unsigned int offset=0;
         bool status=false;
         do {
            if (!fragment->check_rob_noex()) {
               std::vector<eformat::FragmentProblem> probs;
               fragment->rob_problems(probs);
               for (auto p : probs) {
                  std::cout << " ROB problem=" << p << std::endl;
                  m_eformatErrors++;
               }
               fragment->rod_problems(probs);
               for (auto p : probs) {
                  std::cout << " ROD problem=" << p << std::endl;
                  m_eformatErrors++;
               }
            }
            status=status || ((*fragment->status() & 0xffff0000) != 0);

            if (offset<dataMsg->size()) {
               int fragSize=fragment->fragment_size_word()*4;
               if (fragSize) {
                  offset+=fragSize;
                  auto newfragment=
                     new eformat::ROBFragment<const unsigned int*>(fragment->end());
                  delete fragment;
                  fragment=newfragment;
               }
               else {
                  std::cout << "Bad fragment, size 0\n";
                  offset=dataMsg->size();
                  m_eformatErrors++;
               }
            }
         } while (offset<dataMsg->size());
         delete fragment;

         m_packetsReceived++;
         if (status) {
            m_missingEvents++;
         }
      }
   }
}
#endif
