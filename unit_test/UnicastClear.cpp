#include <chrono>
#include <thread>
#include <utility>

#include "UnicastClear.h"

#include "asyncmsg/NameService.h"
#include "asyncmsg/Session.h"
#include "asyncmsg/Message.h"                    // for OutputMessage, Input...
#include "ers/ers.h"

using namespace ROS;

namespace ROS {
    class ROSSession : public daq::asyncmsg::Session   {
    public:
        explicit ROSSession(boost::asio::io_service& service);
        ~ROSSession();

        // Session interface
        virtual void onOpen() noexcept override;
        virtual void onOpenError(const boost::system::error_code& error) noexcept override;

        virtual void onClose() noexcept override;
        virtual void onCloseError(const boost::system::error_code& error) noexcept override;

        virtual std::unique_ptr<daq::asyncmsg::InputMessage> 
        createMessage(std::uint32_t typeId, std::uint32_t transactionId, std::uint32_t size) noexcept override;

        virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override;
        virtual void onReceiveError(const boost::system::error_code& error, std::unique_ptr<daq::asyncmsg::InputMessage> message) noexcept override;

        virtual void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept override;
        virtual void onSendError(const boost::system::error_code& error, std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept override;
    };
    class ClearMessage : public daq::asyncmsg::OutputMessage {
    public:

        static const uint32_t ID = 0x00DCDF10;

        ClearMessage(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events);
        ~ClearMessage();
        virtual uint32_t typeId()        const override;
        virtual uint32_t transactionId() const override;
        virtual void     toBuffers(std::vector<boost::asio::const_buffer>&) const override;                
    private:
        uint32_t                               m_sequence;
        std::shared_ptr<std::vector<uint32_t>> m_events;
    };
}
    ClearMessage::ClearMessage(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events)
        : m_sequence(sequence),
          m_events(events)
    {
    }

    ClearMessage::~ClearMessage()
    {
    }

    uint32_t ClearMessage::typeId()        const
    {
        return ID;
    }

    uint32_t ClearMessage::transactionId() const 
    {
        return m_sequence;
    }

    void  ClearMessage::toBuffers(std::vector<boost::asio::const_buffer>& bufs) const
    {
        bufs.push_back(boost::asio::buffer(*m_events));
    }


    UnicastClear::UnicastClear(size_t threshold, 
                                     boost::asio::io_service& service, 
                                     daq::asyncmsg::NameService* name_service)
        : ROSClear(threshold),
          m_service(service),
          m_name_service(name_service),
          m_endPoint(0)
    {
    }


    UnicastClear::UnicastClear(size_t threshold, 
                                     boost::asio::io_service& service, 
                                     boost::asio::ip::tcp::endpoint* endPoint)
        : ROSClear(threshold),
          m_service(service),
          m_name_service(0),
          m_endPoint(endPoint)
    {
    }


    UnicastClear::~UnicastClear()
    {
        // must do this here, while object still exists.
        flush();
    }

void UnicastClear::connect()
{
   if (m_name_service) {
        auto endpoints = m_name_service->lookup("CLEAR");

        ERS_LOG("Found " << endpoints.size() << " ROSes");

        // ERS_ASSERT(m_endpoints.size() != 0)  ? or just warning ?

        for(size_t i = 0; i < endpoints.size(); i++) {
            m_sessions.push_back(std::make_shared<ROSSession>(m_service));
        }

        for(size_t i = 0; i < endpoints.size(); i++) {
            ERS_LOG("Connection ROSSession to " << endpoints[i]);
            m_sessions[i]->asyncOpen("HLTSV", endpoints[i]);
        }
   }
   else {
       m_sessions.push_back(std::make_shared<ROSSession>(m_service));
       m_sessions[0]->asyncOpen("HLTSV", *m_endPoint);
   }
   for(auto& session : m_sessions) {
      while(session->state() != daq::asyncmsg::Session::State::OPEN) {
         std::this_thread::sleep_for(std::chrono::milliseconds(10));
      }
   }

}

    void UnicastClear::do_flush(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events)
    {
        using namespace daq::asyncmsg;

        //std::cout << "Flushing " << events->size() << " L1 IDs" << std::endl;

        for(auto session : m_sessions) {
            std::unique_ptr<const OutputMessage> msg(new ClearMessage(sequence,events));
            session->asyncSend(std::move(msg));
        }
    }



    ROSClear::ROSClear(size_t threshold)
        : m_threshold(threshold),
          m_sequence()
    {
        m_event_ids.reserve(m_threshold);
    }

    ROSClear::~ROSClear()
    {
        // ERS_ASSERT(m_event_ids.empty());

        // We cannot call flush() here since the
        // derived object no longer exists at this point.
    }
        
    // Add event to group of processed events. If threshold
    // reached, send ROS clear message to ROS.
    void ROSClear::add_event(uint32_t l1_event_id)
    {
        std::scoped_lock lock(m_mutex);
        m_event_ids.push_back(l1_event_id);

        if(m_event_ids.size() >= m_threshold) {

            // create empty vector inside shared_ptr
            auto data = std::make_shared<std::vector<uint32_t>>();

            // swap event vector with local data
            m_event_ids.swap(*data);

            // reserve  in advance
            m_event_ids.reserve(m_threshold);

            uint32_t seq = m_sequence++;

            do_flush(seq, data);
        }
    }

    void ROSClear::flush()
    {
        auto data = std::make_shared<std::vector<uint32_t>>();

        std::scoped_lock lock(m_mutex);
        m_event_ids.swap(*data);
        uint32_t seq = m_sequence++;

        do_flush(seq, data);
    }

    void ROSClear::connect()
    {
    }

    void ROSClear::prepareForRun()
    {
        std::scoped_lock lock(m_mutex);
        m_sequence = 0;
    }



    ROSSession::ROSSession(boost::asio::io_service& service)
        : daq::asyncmsg::Session(service)
    {
    }

    ROSSession::~ROSSession()
    {
    }

    // Session interface
    void ROSSession::onOpen() noexcept  
    {
        ERS_LOG("opened connection to ROS: " << remoteName() << " addr: " << remoteEndpoint());
    }

    void ROSSession::onOpenError(const boost::system::error_code& error) noexcept  
    {
        ERS_LOG("openError: " << error );
        // report error
    }

    void ROSSession::onClose() noexcept
    {
        ERS_LOG("closed connection to ROS: " << remoteName() << " addr: " << remoteEndpoint());
    }

    void ROSSession::onCloseError(const boost::system::error_code& error) noexcept
    {
        ERS_LOG("closeError: " << error );
        // report error
    }

    std::unique_ptr<daq::asyncmsg::InputMessage> 
    ROSSession::createMessage(std::uint32_t , std::uint32_t , std::uint32_t ) noexcept  
    {
        ERS_ASSERT_MSG(false, "Should never happen");
        return std::unique_ptr<daq::asyncmsg::InputMessage>();
    }

    void ROSSession::onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> )  
    {
        ERS_ASSERT_MSG(false, "Should never happen");
    }
     
    void ROSSession::onReceiveError(const boost::system::error_code& error, std::unique_ptr<daq::asyncmsg::InputMessage> ) noexcept  
    {
        ERS_ASSERT_MSG(false, "Should never happen: " << error);
    }

    void ROSSession::onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> ) noexcept  
    {
        // let message get deleted -> will decrease shared_ptr count
    }

    void ROSSession::onSendError(const boost::system::error_code& error, std::unique_ptr<const daq::asyncmsg::OutputMessage> ) noexcept  
    {
        ERS_LOG("sendError: " << error << " from " << remoteName());
        // report error
        // ?? close session ? how to handle this ?
    }
