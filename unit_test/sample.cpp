/*
 * sample.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE swrod_sample
#define BOOST_TEST_MAIN

#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using namespace boost::property_tree;

BOOST_AUTO_TEST_CASE(test_sample)
{
    BOOST_TEST_MESSAGE("Executing sample test");

    BOOST_CHECK(1 == 1);
}
