/*
 * TestDataGenerator.cpp
 *
 *  Created on: May 14, 2019
 *      Author: kolos
 */

#include <ers/ers.h>

#include "test/core/InternalL1AGenerator.h"
#include "TestDataGenerator.h"

#include <swrod/Factory.h>

using namespace swrod;
using namespace swrod::test;
using namespace boost::property_tree;

namespace {
    Factory<DataInput>::Registrator __reg__(
            "TestData",
            [](const boost::property_tree::ptree& config, const Core & ) {
                return std::make_shared<TestDataGenerator>(config);
            });
}

TestDataGenerator::TestDataGenerator(const boost::property_tree::ptree& config) :
    InternalDataGenerator(config),
    m_corrupt_data_cnt(0),
    m_corrupt_l1id_cnt(0),
    m_corrupt_bcid_cnt(0)
{
    m_l1id_bit_mask1 = config.get<uint32_t>("l1id_bit_mask1", -1);
    m_l1id_bit_mask2 = config.get<uint32_t>("l1id_bit_mask2", -1);
    m_l1id_bit_mask_variation_step = config.get<uint32_t>("l1id_bit_mask_variation_step", 2);
    m_corrupt_data = config.get<uint32_t>("corrupt_data", 0);
    m_corrupt_l1id = config.get<uint32_t>("corrupt_l1id", 0);
    m_corrupt_bcid = config.get<uint32_t>("corrupt_bcid", 0);
    m_send_duplicates = config.count("send_duplicates");

    if (config.count("l1ids_dropped")) {
        for (const ptree::value_type & e : config.get_child("l1ids_dropped")) {
            m_l1ids_dropped.insert(e.second.get_value<uint32_t>());
        }
    }

    if (config.count("links_dropped")) {
        for (const ptree::value_type & e : config.get_child("links_dropped")) {
            m_links_dropped.insert(e.second.get_value<InputLinkId>());
        }
    }
}

void TestDataGenerator::generatePacket(InputLinkId link, uint32_t l1id, uint16_t bcid) {
    uint32_t bit_mask = m_packets_counter % m_l1id_bit_mask_variation_step
                    ? m_l1id_bit_mask1 : m_l1id_bit_mask2;
    uint32_t test_bcid = m_full_mode || (m_packets_counter % m_l1id_bit_mask_variation_step) == 0
                    ? bcid : 0xffff;

    uint32_t restricted_l1id = l1id & bit_mask;

    if (   !m_l1ids_dropped.count(l1id & 0x00ffffff)
        && !m_links_dropped.count(link)) {

        *(uint32_t*)(m_packet + 0) = restricted_l1id;
        *(uint16_t*)(m_packet + 4) = 1; // CRC check
        *(uint16_t*)(m_packet + 6) = test_bcid;
        *(uint32_t*)(m_packet + 8) = bit_mask;

        bool corrupt = false;
        if ((m_packets_counter % 10) && (m_triggers_counter % 2)) {
            if ((m_packets_counter % 17) == 0) {
                if (m_corrupt_l1id_cnt) {
                    uint32_t corrupt_l1id = 0xffffffff - m_corrupt_l1id_cnt;
                    ERS_DEBUG(1, "Injecting packet with corrupt L1ID = 0x" << std::hex << corrupt_l1id
                            << " instead of 0x" << restricted_l1id);
                    if (m_corrupt_l1id_cnt%2) {
                        *((uint16_t*)(m_packet + 4)) = 2; // instruct test plugin CRC check to return false
                    }
                    *(uint32_t*)(m_packet + 0) = corrupt_l1id;
                    *(uint16_t*)(m_packet + 6) = bcid;
                    --m_corrupt_l1id_cnt;
                    corrupt = true;
                }
            } else if ((m_packets_counter % 11) == 0) {
                if (m_corrupt_bcid_cnt) {
                    uint16_t corrupt_bcid = bcid + 2;
                    ERS_DEBUG(1, "Injecting packet with corrupt BCID = 0x" << std::hex << corrupt_bcid
                            << " instead of 0x" << test_bcid);
                    if (m_corrupt_bcid_cnt%2) {
                        *(uint16_t*)(m_packet + 4) = 2; // instruct test plugin CRC check to return false
                    }
                    *((uint16_t*)(m_packet + 6)) = corrupt_bcid;
                    --m_corrupt_bcid_cnt;
                    corrupt = true;
                }
            } else if ((m_packets_counter % 7) == 0) {
                if (m_corrupt_data_cnt) {
                    ERS_DEBUG(1, "Injecting corrupt packet with L1ID = 0x" << std::hex << l1id);
                    *(uint32_t*)(m_packet + 8) = 0;    // instructs the test TTC info extractor to throw exception
                    --m_corrupt_data_cnt;
                    corrupt = true;
                }
            }
        }

        dataReceived(link, m_packet, m_packet_size, 0);
        if (!corrupt && m_send_duplicates && m_packets_counter && (m_packets_counter % 18099) == 0) {
            ERS_DEBUG(1, "Sending duplicate packet with L1ID = 0x" << std::hex << l1id);
            dataReceived(link, m_packet, m_packet_size, 0);
        }
    } else {
        ERS_DEBUG(1, "Dropping data packet with L1ID = 0x" << std::hex << l1id);
    }
}

void TestDataGenerator::beforeStart() {
    m_corrupt_data_cnt = m_corrupt_data;
    m_corrupt_l1id_cnt = m_corrupt_l1id;
    m_corrupt_bcid_cnt = m_corrupt_bcid;
}
