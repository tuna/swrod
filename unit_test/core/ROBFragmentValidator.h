/*
 * ROBFragmentValidator.h
 *
 *  Created on: May 8, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTVALIDATOR_H_
#define SWROD_ROBFRAGMENTVALIDATOR_H_

#include <atomic>

#include <swrod/CustomProcessingFramework.h>
#include <swrod/GBTChunk.h>
#include <swrod/ROBFragmentConsumer.h>

namespace swrod {
    class Core;

    class ROBFragmentValidator: public ROBFragmentConsumer {
    public:
        ROBFragmentValidator(const boost::property_tree::ptree &config, const Core &core);

        const std::string & getName() const override {
            static std::string s("FragmentValidator");
            return s;
        }

        void insertROBFragment(const std::shared_ptr<ROBFragment> &fragment) override;

        uint64_t getFragmentsNumber() const {
            return m_fragments;
        }

        uint64_t getCorruptFragmentsNumber() const {
            return m_corrupt_fragments;
        }

        uint64_t getL1IDErrorsNumber() const {
            return m_l1id_errors;
        }

        uint64_t getBCIDErrorsNumber() const {
            return m_bcid_errors;
        }

        uint64_t getCorruptErrorsNumber() const {
            return m_corrupt_errors;
        }

        uint64_t getCRCErrorsNumber() const {
            return m_crc_errors;
        }

        uint64_t getL1IDMismatchesNumber() const {
            return m_l1id_mismatches;
        }

        uint64_t getBCIDMismatchesNumber() const {
            return m_bcid_mismatches;
        }

        uint64_t getL1IDCorruptNumber() const {
            return m_l1id_corrupt;
        }

        uint64_t getBCIDCorruptNumber() const {
            return m_bcid_corrupt;
        }

        uint64_t getEmptyFragmentsNumber() const {
            return m_empty_fragments;
        }

        uint64_t getMissedPacketsNumber() const {
            return m_missed_packets;
        }

        uint64_t getCorruptPacketsNumber() const {
            return m_corrupt_packets;
        }

        uint64_t getBrokenPacketsNumber() const {
            return m_broken_packets;
        }

        uint64_t getPacketsNumber() const {
            return m_packets;
        }

    private:
        const uint32_t m_ROB_id;
        const bool m_FULL_mode;

        std::atomic<uint64_t> m_fragments = 0;
        std::atomic<uint64_t> m_corrupt_fragments = 0;
        std::atomic<uint64_t> m_l1id_errors = 0;
        std::atomic<uint64_t> m_bcid_errors = 0;
        std::atomic<uint64_t> m_corrupt_errors = 0;
        std::atomic<uint64_t> m_crc_errors = 0;
        std::atomic<uint64_t> m_l1id_corrupt = 0;
        std::atomic<uint64_t> m_bcid_corrupt = 0;

        std::atomic<uint64_t> m_l1id_mismatches = 0;
        std::atomic<uint64_t> m_bcid_mismatches = 0;
        std::atomic<uint64_t> m_empty_fragments = 0;
        std::atomic<uint64_t> m_missed_packets = 0;
        std::atomic<uint64_t> m_corrupt_packets = 0;
        std::atomic<uint64_t> m_broken_packets = 0;
        std::atomic<uint64_t> m_packets = 0;
        CustomProcessingFramework::TriggerInfoExtractor m_trigger_info_extractor;
    };
}

#endif /* SWROD_ROBFRAGMENTVALIDATOR_H_ */
