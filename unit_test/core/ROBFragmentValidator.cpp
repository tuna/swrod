/*
 * ROBFragmentValidator.cpp
 *
 *  Created on: Dec 2, 2020
 *      Author: kolos
 */


#include "ROBFragmentValidator.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    Factory<ROBFragmentConsumer>::Registrator __reg__(
            "ROBFragmentValidator",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<ROBFragmentValidator>(config, core);
            });
}

ROBFragmentValidator::ROBFragmentValidator(
        const boost::property_tree::ptree & config, const Core & core) :
        m_ROB_id(config.get<uint32_t>("RobConfig.Id")),
        m_FULL_mode(
                config.get<std::string>("RobConfig.FragmentBuilder.Type") == "FullModeBuilder"),
        m_trigger_info_extractor(
                core.getProcessingFramework().getTriggerInfoExtractor(m_ROB_id))
{ }

void ROBFragmentValidator::insertROBFragment(
        const std::shared_ptr<ROBFragment> &fragment) {
    ++m_fragments;
    m_corrupt_packets += fragment->m_corrupted_packets;
    m_missed_packets += fragment->m_missed_packets;
    if (fragment->m_l1id == uint32_t(-1)) {
        ++m_l1id_corrupt;
    }
    if (fragment->m_bcid == uint16_t(-1)) {
        ++m_bcid_corrupt;
    }
    if (fragment->statusWord() != 0) {
        ++m_corrupt_fragments;
    }
    uint32_t packets_counter = 0;
    for (auto &block : fragment->m_data) {
        const uint32_t *data = block.dataBegin();
        if (m_FULL_mode == 1) {
            try {
                auto [l1id, l1id_mask, bcid] = m_trigger_info_extractor(
                        (uint8_t*)data, block.dataSize()<<2);

                if (fragment->m_l1id != l1id) {
                    ++m_l1id_mismatches;
                }
                if (bcid != 0xffff && fragment->m_bcid != bcid) {
                    ++m_bcid_mismatches;
                }
            } catch (...) {
                ++m_broken_packets;
            }
            ++packets_counter;
            continue;
        }

        const uint32_t * end = block.dataEnd();
        while (data < end) {
            const GBTChunk::Header *h = (const GBTChunk::Header*) data;
            m_l1id_errors += (bool) (h->m_swrod_status & GBTChunk::Status::L1IdMismatch);
            m_bcid_errors += (bool) (h->m_swrod_status & GBTChunk::Status::BCIdMismatch);
            m_crc_errors += (bool) (h->m_swrod_status & GBTChunk::Status::CRCError);
            m_corrupt_errors += (bool) (h->m_swrod_status & GBTChunk::Status::Corrupt);

            size_t size = h->m_size;
            try {
                auto [l1id, l1id_mask, bcid] = m_trigger_info_extractor(
                        (uint8_t*)data + sizeof(GBTChunk::Header),
                        (size<<2) - sizeof(GBTChunk::Header));
                if ((fragment->m_l1id & l1id_mask) != l1id) {
                    ++m_l1id_mismatches;
                }
                if (bcid != 0xffff && fragment->m_bcid != bcid) {
                    ++m_bcid_mismatches;
                }
            } catch (...) {
                ++m_broken_packets;
            }
            data += size;
            ++packets_counter;
        }
    }
    if (!packets_counter) {
        ++m_empty_fragments;
    }
    m_packets += packets_counter;
}
