// this is -*- C++ -*-
#ifndef UNICASTCLEAR_H_
#define UNICASTCLEAR_H_


#include <boost/asio/io_service.hpp>  // for io_service
#include <boost/asio/ip/tcp.hpp>      // for tcp, tcp::endpoint
#include <cstddef>                   // for size_t
#include <cstdint>                   // for uint32_t
#include <memory>                     // for shared_ptr
#include <mutex>                      // for mutex
#include <vector>                     // for vector

namespace daq { namespace asyncmsg { class NameService; } }

namespace ROS {
    class ROSSession;
    /**
     * The ROSClear interface.
     * This is all the rest of the HLTSV sees.
     *
     * The connect() method should be called at the
     * appropriate time to setup the connections.
     */
    class ROSClear {
    public:
        
        /**
         * The threshold is the number of events after which
         * the internal list of event IDs is sent to the ROS.
         */
        explicit ROSClear(size_t threshold = 100);
        virtual ~ROSClear();

        ROSClear(const ROSClear& ) = delete;
        ROSClear& operator=(const ROSClear& ) = delete;

        /** 
         * Add event to group of processed events. If threshold
         * reached, send ROS clear message to ROS.
         */
        void add_event(uint32_t l1_event_id);

        /// Flush all existing events to the ROS.
        void flush();

        /// Do any connect operation that is necessary
        virtual void connect();

        /// Do any prepareForRun action (now: reset sequence number)
        void prepareForRun();

    protected:
        
        /** 
         * Send any existing events to the ROS.
         * This is implemented in a derived class, using either
         * multicast or multiple sends.
         */
        virtual void do_flush(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events) = 0;

    private:
        size_t                m_threshold;
        uint32_t              m_sequence;
        std::mutex            m_mutex;
        std::vector<uint32_t> m_event_ids;
    };


    /**
     * \brief Unicast implementation of ROSClear, using TCP.
     */
   class UnicastClear : public ROSClear {
    public:
        UnicastClear(size_t threshold, boost::asio::io_service& service, daq::asyncmsg::NameService* name_service);
       UnicastClear(size_t threshold, boost::asio::io_service& service, boost::asio::ip::tcp::endpoint*);
        ~UnicastClear();

        virtual void connect() override;
    private:
        /// The implementation of the flush operation.

        virtual void do_flush(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events) override;
    private:
        boost::asio::io_service&                     m_service;
        daq::asyncmsg::NameService*                   m_name_service;
      std::vector<std::shared_ptr<ROSSession>>     m_sessions;
      boost::asio::ip::tcp::endpoint* m_endPoint;
    };

    
}

#endif // HLTSV_UNICASTROSCLEAR_H_
